from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    db_host: str = "localhost"
    db_port: str = "5432"
    db_username: str = "admin"
    db_password: str = "secret"
    db: str = "annot"
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    disable_db_migrations: bool = False
    debug: bool = False
    api_v1_integration: str = ""
    api_v3_integration: str = ""
    root_path: str = ""
    disable_openapi: bool = False
    v1_item_limit: int = 10000
    v1_post_limit: int = 10000
    v3_item_limit: int = 10000
    v3_post_limit: int = 10000
    v3_pixelmaps_post_limit: int = 16
    v3_pixelmaps_get_limit: int = 16
    enable_profiler: bool = False
    enable_file_profiler: bool = False
    enable_timing: bool = False
    allow_external_ids: bool = False
    pixelmapd_host: str = "pixelmapd"
    pixelmapd_port: int = 5556
    pixelmaps_data_path: str = "/app/data/pixelmaps/"
    cds_url: str = ""

    model_config = SettingsConfigDict(env_prefix="annot_", env_file=".env", extra="ignore")
