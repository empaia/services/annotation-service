import json
import uuid
from asyncio import Queue
from typing import Dict

import zmq
from zmq.asyncio import Context, Poller, Socket

POLL_TIMEOUT = 10000
CONNECTION_POOL_SIZE = 32


class ConnectionTimeout(Exception):
    pass


class _Connection:
    def __init__(self, context: Context, host, port):
        self._context = context
        self._host = host
        self._port = port

        self._is_closed = False

        self._client_id = f"client_{uuid.uuid4()}"
        self._socket: Socket = self._context.socket(zmq.DEALER)
        self._socket.identity = self._client_id.encode("ascii")
        self._socket.connect(self.address)
        self._poller: Poller = Poller()
        self._poller.register(self._socket, zmq.POLLIN)

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    @property
    def address(self):
        return f"tcp://{self.host}:{self.port}"

    @property
    def is_closed(self):
        return self._is_closed

    @property
    def client_id(self):
        return self._client_id

    @property
    def socket(self):
        return self._socket

    @property
    def poller(self):
        return self._poller

    def close(self):
        if self._is_closed:
            return
        self._is_closed = True
        self._poller.unregister(self._socket)
        self._socket.setsockopt(zmq.LINGER, 0)
        self._socket.close()


class PixelmapdConnection:
    def __init__(self, host, port, logger):
        self._host = host
        self._port = port
        self._logger = logger
        self._context = Context()
        self._queue = Queue()

    @property
    def host(self):
        return self._host

    @property
    def port(self):
        return self._port

    def initialize(self):
        if CONNECTION_POOL_SIZE is None:
            return
        for _ in range(CONNECTION_POOL_SIZE):
            connection = _Connection(context=self._context, host=self.host, port=self.port)
            self._queue.put_nowait(connection)

    async def is_alive(self) -> bool:
        connection = _Connection(
            context=self._context,
            host=self.host,
            port=self.port,
        )
        socket, poller = connection.socket, connection.poller

        try:
            req_msg = {"req": "ALIVE"}
            await socket.send_multipart([json.dumps(req_msg).encode("ascii")])
            if not await poller.poll(POLL_TIMEOUT):
                raise ConnectionTimeout("Connection Timeout")
            raw_data = await socket.recv_multipart()
            data = json.loads(raw_data[0])
            return data["is_alive"]
        except Exception as e:
            self._logger.error("Exception in is_alive %s", str(e))
            return False  # resource can not be reached
        finally:
            connection.close()

    def close(self):
        self._context.term()

    async def _get_connection(self) -> _Connection:
        if CONNECTION_POOL_SIZE is None:
            return _Connection(context=self._context, host=self.host, port=self.port)
        return await self._queue.get()

    def _put_connection(self, connection: _Connection):
        if CONNECTION_POOL_SIZE is None:
            connection.close()
            return
        self._queue.put_nowait(connection)

    async def send_and_recv_multi(self, req_msg: Dict, req_bytes: bytes = None):
        connection = await self._get_connection()
        socket, poller = connection.socket, connection.poller

        try:
            if req_bytes is None:
                await socket.send_multipart([json.dumps(req_msg).encode("ascii")])
            else:
                await socket.send_multipart([json.dumps(req_msg).encode("ascii"), req_bytes])

            if not await poller.poll(POLL_TIMEOUT):
                raise ConnectionTimeout("Connection Timeout")
            raw_data = await socket.recv_multipart()
            rep_msg, rep_bytes = json.loads(raw_data[0]), None
            if rep_msg["rep"] == "error":
                return rep_msg, rep_bytes
            if len(raw_data) == 2:
                rep_bytes = raw_data[1]
            return rep_msg, rep_bytes
        except Exception as e:
            self._logger.error("Exception in send_and_recv_multi %s", str(e))
            rep_msg = {
                "rep": "error",
                "status_code": 500,
                "detail": "Could not connect to pixelmapd.",
            }
            return rep_msg, None
        finally:
            self._put_connection(connection=connection)
