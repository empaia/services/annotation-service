from typing import Optional

from fastapi import HTTPException

from .pixelmapd_connection import PixelmapdConnection


class PixelmapdManager:
    def __init__(self, settings, logger):
        self._settings = settings
        self._logger = logger
        self._connection = None

    async def initialize_connection(self):
        connection = PixelmapdConnection(
            host=self._settings.pixelmapd_host,
            port=self._settings.pixelmapd_port,
            logger=self._logger,
        )
        if not await connection.is_alive():
            connection.close()
            self._logger.warning("Pixelmapd does not seem to be alive.")
            return

        connection.initialize()
        self._connection = connection
        self._logger.info("Pixelmapd is alive.")

    async def create_tile(self, case_id: Optional[str], pixelmap, level, x, y, content_encoding, req_bytes):
        req_msg = {
            "req": "CREATE_TILE",
            "case_id": case_id,
            "pixelmap": pixelmap,
            "level": level,
            "x": x,
            "y": y,
            "content_encoding": content_encoding,
        }

        try:
            rep_msg, _ = await self._connection.send_and_recv_multi(req_msg=req_msg, req_bytes=req_bytes)
        except Exception as e:
            raise HTTPException(500, detail=f"Error while saving tile: {e}") from e

        return rep_msg

    async def create_tiles(
        self,
        case_id: Optional[str],
        pixelmap,
        level,
        start_x,
        start_y,
        end_x,
        end_y,
        content_encoding,
        req_bytes,
    ):
        req_msg = {
            "req": "CREATE_TILES",
            "case_id": case_id,
            "pixelmap": pixelmap,
            "level": level,
            "start_x": start_x,
            "start_y": start_y,
            "end_x": end_x,
            "end_y": end_y,
            "content_encoding": content_encoding,
        }

        try:
            rep_msg, _ = await self._connection.send_and_recv_multi(req_msg=req_msg, req_bytes=req_bytes)
        except Exception as e:
            raise HTTPException(500, detail=f"Error while saving tile: {e}") from e

        return rep_msg

    async def get_tile(self, case_id: Optional[str], pixelmap, level, x, y, accept_encoding):
        req_msg = {
            "req": "GET_TILE",
            "case_id": case_id,
            "pixelmap": pixelmap,
            "level": level,
            "x": x,
            "y": y,
            "accept_encoding": accept_encoding,
        }

        try:
            rep_msg, rep_bytes = await self._connection.send_and_recv_multi(req_msg=req_msg)
        except Exception as e:
            raise HTTPException(500, detail=f"Error while reading tile: {e}") from e

        return rep_msg, rep_bytes

    async def get_tiles(self, case_id: Optional[str], pixelmap, level, start_x, start_y, end_x, end_y, accept_encoding):
        req_msg = {
            "req": "GET_TILES",
            "case_id": case_id,
            "pixelmap": pixelmap,
            "level": level,
            "start_x": start_x,
            "start_y": start_y,
            "end_x": end_x,
            "end_y": end_y,
            "accept_encoding": accept_encoding,
        }

        try:
            rep_msg, rep_bytes = await self._connection.send_and_recv_multi(req_msg=req_msg)
        except Exception as e:
            raise HTTPException(500, detail=f"Error while saving tile: {e}") from e

        return rep_msg, rep_bytes

    async def delete_tile(self, case_id: Optional[str], pixelmap, level, x, y):
        req_msg = {
            "req": "DELETE_TILE",
            "case_id": case_id,
            "pixelmap": pixelmap,
            "level": level,
            "x": x,
            "y": y,
        }

        try:
            rep_msg, _ = await self._connection.send_and_recv_multi(req_msg=req_msg)
        except Exception as e:
            raise HTTPException(500, detail=f"Error while reading tile: {e}") from e

        return rep_msg

    async def delete(self, case_id: Optional[str], pixelmap):
        req_msg = {
            "req": "DELETE",
            "case_id": case_id,
            "pixelmap": pixelmap,
        }

        try:
            rep_msg, _ = await self._connection.send_and_recv_multi(req_msg=req_msg)
        except Exception as e:
            raise HTTPException(500, detail=f"Error while deleting pixelmap: {e}") from e

        return rep_msg
