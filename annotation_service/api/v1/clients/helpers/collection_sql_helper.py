from typing import List

from pydantic import UUID4

from annotation_service.models.v1.annotation.collections import CollectionQuery
from annotation_service.singletons import settings

from ...clients.helpers.query_builder import concat_sql_statemment


async def get_insert_tmptable_sql(external_ids: bool):
    base_sql = """
    INSERT INTO tmptable_col(
        id,
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        item_type,
        is_locked
    )
    VALUES {value_template}
    """

    if settings.allow_external_ids and external_ids:
        value_template = "($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)"
    else:
        value_template = "(gen_random_uuid(), $1, $2, $3, $4, $5, $6, $7, $8, $9)"

    return base_sql.format(value_template=value_template)


async def get_insert_sql():
    return """
    INSERT INTO c_collections SELECT * FROM tmptable_col
    RETURNING
    jsonb_build_object(
        'id', c_collections.id,
        'type', c_collections.type,
        'name', c_collections.name,
        'description', c_collections.description,
        'creator_id', c_collections.creator_id,
        'creator_type', c_collections.creator_type,
        'reference_id', c_collections.reference_id,
        'reference_type', c_collections.reference_type,
        'item_type', c_collections.item_type,
        'is_locked', c_collections.is_locked
    ) AS json;
    """


async def get_insert_items_tmptable_sql():
    return """
    INSERT INTO tmptable_items(
        collection_id,
        item_id
    )
    VALUES ($1, $2);
    """


async def get_insert_items_sql():
    return """
    WITH rows AS (
        INSERT INTO c_collection_items
        SELECT * FROM tmptable_items
        RETURNING 1
    )
    SELECT count(*) FROM rows
    """


async def get_where_sql(collection_query: CollectionQuery, ids: List[UUID4]):
    where_terms = []
    args = []
    arg_counter = 1
    if collection_query.creators is not None:
        where_terms.append(f"c.creator_id = ANY(${arg_counter})")
        args.append(collection_query.creators)
        arg_counter += 1
    if collection_query.references is not None:
        where_terms.append(f"c.reference_id = ANY(${arg_counter})")
        args.append(collection_query.references)
        arg_counter += 1
    if collection_query.item_types is not None:
        where_terms.append(f"c.item_type = ANY(${arg_counter})")
        args.append(collection_query.item_types)
        arg_counter += 1
    if ids is not None and len(ids) > 0:
        where_terms.append(f"c.id = ANY(${arg_counter})")
        args.append(ids)
        arg_counter += 1

    return await concat_sql_statemment(terms=where_terms, prefix=" WHERE ", separator=" AND "), args


async def get_count_sql(collection_query: CollectionQuery, ids: List[UUID4] = None):
    base_sql = """
    SELECT COUNT(c.id)
    FROM c_collections as c
    {where_sql_str};
    """

    where_sql_str, args = await get_where_sql(collection_query, ids)
    return base_sql.format(where_sql_str=where_sql_str), args


async def get_select_sql(collection_query: CollectionQuery, ids: List[UUID4] = None):
    where_sql_str, args = await get_where_sql(collection_query, ids)

    base_sql = f"""
    SELECT row_to_json(row) AS json
    FROM (
        SELECT c.*,
               COUNT(ci.item_id) AS item_count,
               COALESCE(JSON_AGG(json_build_object('id', ci.item_id))
               FILTER (WHERE ci.item_id IS NOT NULL), '[]') AS items
        FROM c_collections as c
        LEFT JOIN c_collection_items AS ci ON ci.collection_id=c.id
        {where_sql_str}
        GROUP BY c.id
        LIMIT ${len(args) + 1} OFFSET ${len(args) + 2}
    ) AS row;
    """

    return base_sql, args


async def get_references_sql(collection_query: CollectionQuery, ids: List[UUID4] = None):
    base_sql = """
    WITH
    {ids_jobs_sql_str}
    ids_collections AS (
        SELECT id
        FROM c_collections AS c
        {where_sql_str}
    ),
    ids_a_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS a_references
        FROM c_collections AS c
        WHERE c.id in (SELECT * FROM ids_collections)
        and c.reference_type = 'annotation'
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS w_references
        FROM c_collections AS c
        WHERE c.id in (SELECT * FROM ids_collections)
        and c.reference_type = 'wsi'
    ),
    ids_n_refs AS (
        SELECT count(*) as n_count
        FROM c_collections AS c
        WHERE c.id in (SELECT * FROM ids_collections)
        and c.reference_type IS NULL
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.a_references IS NULL THEN '[]'::jsonb
            ELSE ar.a_references
        END,
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END,
        'contains_items_without_reference',
        CASE
            WHEN ar.n_count > 0 THEN true
            ELSE false
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_a_refs, ids_w_refs, ids_n_refs
    ) AS ar
    """

    where_sql_str, args = await get_where_sql(collection_query, ids)
    arg_counter = len(args) + 1

    ids_jobs_sql_str = ""
    if collection_query.jobs is not None:
        ids_jobs_sql_str = f"""
        ids_jobs AS (
            SELECT DISTINCT collection_id
            FROM j_job_collections
            WHERE job_id = ANY(${arg_counter})
        ),
        """
        args.append(collection_query.jobs)
        arg_counter += 1
        if where_sql_str != "":
            where_sql_str += " AND c.id IN (SELECT * FROM ids_jobs)"
        else:
            where_sql_str = "WHERE c.id IN (SELECT * FROM ids_jobs)"

    return base_sql.format(ids_jobs_sql_str=ids_jobs_sql_str, where_sql_str=where_sql_str), args


async def get_single_select_sql():
    return """
    SELECT row_to_json(row) AS json
    FROM (
        SELECT c.*, COUNT(ci.collection_id) AS item_count
        FROM c_collections AS c
        LEFT JOIN c_collection_items AS ci
            ON ci.collection_id=c.id
        WHERE c.id = $1
        GROUP BY c.id
    ) AS row;
    """


async def get_select_item_ids_sql():
    return """
    SELECT item_id
    FROM c_collection_items
    WHERE collection_id = $1
    """


async def get_update_sql():
    return """
    UPDATE c_collections
        SET name = $2,
            description = $3,
            creator_id = $4,
            creator_type = $5,
            reference_id = $6,
            reference_type = $7,
            item_type = $8
    WHERE id = $1;
    """
