from annotation_service.models.v1.annotation.jobs import NodeType

from .query_builder import concat_sql_statemment


async def _get_tree_node_sub_select(node_type: NodeType):
    coll_sub_select = """
    (
        SELECT ci.item_id::uuid AS id
        FROM c_collections AS c
        LEFT JOIN j_job_collections AS jc
        ON jc.collection_id = c.id
        LEFT JOIN c_collection_items AS ci
        ON ci.collection_id = c.id
        WHERE c.reference_id = $1
        AND c.item_type IN ('point', 'line', 'arrow', 'circle', 'rectangle', 'polygon')
        AND jc.job_id = $2
    )
    """

    annot_sub_select = """
    (
        SELECT a.id AS id
        FROM a_annotations AS a
        JOIN j_job_annotations AS ja
        ON a.id=ja.annotation_id
        WHERE
        ja.job_id = $2
        AND a.reference_id = $1
        AND a.id NOT IN (
            SELECT *
            FROM (
                SELECT ci.item_id::uuid
                FROM c_collections AS c
                LEFT JOIN j_job_collections AS jc
                ON jc.collection_id = c.id
                LEFT JOIN c_collection_items AS ci
                ON ci.collection_id = c.id
                WHERE c.reference_id <> $1
                AND c.item_type IN ('point', 'line', 'arrow', 'circle', 'rectangle', 'polygon')
                AND jc.job_id = $2
            ) AS tmp
            WHERE item_id IS NOT NULL
        )
    )
    """

    sub_selects = [coll_sub_select]
    if node_type == NodeType.WSI:
        sub_selects.append(annot_sub_select)

    return await concat_sql_statemment(terms=sub_selects, prefix="", separator=" UNION ")


async def get_tree_node_count_sql(node_type: NodeType):
    base_sql = """
    SELECT COUNT(id)
    FROM a_annotations AS a
    WHERE a.id IN (
        SELECT *
        FROM (
            {sub_selects}
        ) AS ids
    )
    """

    return base_sql.format(sub_selects=await _get_tree_node_sub_select(node_type))


async def get_tree_node_sql(node_type: NodeType):
    base_sql = """
    SELECT '{{"description":null, "npp_viewing":null}}'::jsonb || json_strip_nulls(row_to_json(row))::jsonb AS json
    FROM
    (
        SELECT
        a.id,a.type,a.name,a.description,a.creator_id,a.creator_type,
        a.reference_id,a.reference_type,a.npp_created,
        NULLIF(to_jsonb(array_remove(ARRAY[a.npp_viewing_min, a.npp_viewing_max], null)), '[]') AS npp_viewing,
        a.is_locked,
        EXTRACT(epoch FROM a.created_at)::int AS created_at,
        EXTRACT(epoch FROM a.updated_at)::int AS updated_at,
        a.coordinates,a.head,a.tail,a.center,a.radius,
        a.upper_left,a.width,a.height,
        a.centroid,
        COALESCE(json_agg(cl.*) FILTER (WHERE cl.id IS NOT NULL), '[]') AS classes
        FROM a_annotations AS a
        LEFT JOIN cl_classes AS cl
            ON cl.reference_id = a.id
            AND cl.id in (
                SELECT class_id::uuid
                FROM j_job_classes
                WHERE job_id= $2 AND class_id <> ''
            )
        WHERE a.id IN (
            SELECT *
            FROM (
                {sub_selects}
            ) AS ids
            ORDER BY ids.id ASC
        )
        GROUP BY a.id
        ORDER BY a.updated_at DESC, a.id ASC
        LIMIT $3
        OFFSET $4
    ) AS row;
    """

    return base_sql.format(sub_selects=await _get_tree_node_sub_select(node_type))


async def get_tree_node_position_sql(node_type: NodeType):
    base_sql = """
    SELECT id, position
    FROM
    (
        SELECT a.id,
        row_number() over(
            ORDER BY a.updated_at DESC, a.id ASC
        ) AS position
        FROM a_annotations AS a
        WHERE a.id IN (
            SELECT *
            FROM (
                {sub_selects}
            ) AS ids
            ORDER BY ids.id ASC
        )
        ORDER BY a.updated_at DESC, a.id ASC
    ) AS id_position
    WHERE id = $3;
    """

    return base_sql.format(sub_selects=await _get_tree_node_sub_select(node_type))
