from typing import List

from pydantic import UUID4

from annotation_service.models.v1.annotation.classes import ClassQuery
from annotation_service.singletons import settings

from .query_builder import concat_sql_statemment


async def get_insert_tmptable_sql(external_ids: bool):
    base_sql = """
    INSERT INTO tmptable_cl(
        id,
        type,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        value,
        is_locked
    )
    VALUES {value_template};
    """

    if settings.allow_external_ids and external_ids:
        value_template = "($1, $2, $3, $4, $5, $6, $7, $8)"
    else:
        value_template = "(gen_random_uuid(), $1, $2, $3, $4, $5, $6, $7)"

    return base_sql.format(value_template=value_template)


async def get_insert_sql():
    return """
    INSERT INTO cl_classes SELECT * FROM tmptable_cl
    RETURNING jsonb_build_object(
        'id', cl_classes.id,
        'type', cl_classes.type,
        'creator_id', cl_classes.creator_id,
        'creator_type', cl_classes.creator_type,
        'reference_id', cl_classes.reference_id,
        'reference_type', cl_classes.reference_type,
        'value', cl_classes.value,
        'is_locked', cl_classes.is_locked
    );
    """


async def get_where_sql(class_query: ClassQuery, ids: List[UUID4]):
    where_terms = []
    args = []
    arg_counter = 1
    if class_query.creators is not None:
        where_terms.append(f"cl.creator_id = ANY(${arg_counter})")
        args.append(class_query.creators)
        arg_counter += 1
    if class_query.references is not None:
        where_terms.append(f"cl.reference_id = ANY(${arg_counter})")
        args.append(class_query.references)
        arg_counter += 1
    if ids is not None and len(ids) > 0:
        where_terms.append(f"cl.id = ANY(${arg_counter})")
        args.append(ids)
        arg_counter += 1

    return await concat_sql_statemment(terms=where_terms, prefix=" WHERE ", separator=" AND "), args


async def get_count_sql(class_query: ClassQuery, ids: List[UUID4] = None):
    base_sql = """
    SELECT COUNT(cl.id)
    FROM cl_classes AS cl
    {where_sql_str};
    """

    where_sql_str, args = await get_where_sql(class_query, ids)
    return base_sql.format(where_sql_str=where_sql_str), args


async def get_select_sql(class_query: ClassQuery, ids: List[UUID4] = None):
    where_sql_str, args = await get_where_sql(class_query, ids)

    base_sql = f"""
    SELECT row_to_json(row) AS json
    FROM (
        SELECT *
        FROM cl_classes AS cl
        {where_sql_str}
        LIMIT ${len(args) + 1} OFFSET ${len(args) + 2}
    ) AS row;
    """

    return base_sql, args


async def get_values_sql(class_query: ClassQuery, ids: List[UUID4] = None):
    base_sql = """
    SELECT json_build_object('values', json_agg(v.value)) AS json
    FROM (
        SELECT DISTINCT cl.value
        FROM cl_classes AS cl
        {where_sql_str}
    ) AS v;
    """

    where_sql_str, args = await get_where_sql(class_query, ids)
    return base_sql.format(where_sql_str=where_sql_str), args


async def get_references_sql(class_query: ClassQuery, ids: List[UUID4] = None):
    base_sql = """
    WITH
    {ids_jobs_sql_str}
    ids_classes AS (
        SELECT id
        FROM cl_classes AS cl
        {where_sql_str}
    )
    SELECT jsonb_build_object('annotation',
        CASE
            WHEN refs.references IS NULL THEN '[]'::jsonb
            ELSE refs.references
        END
    ) AS u_refs
    FROM
    (
        SELECT JSONB_AGG(DISTINCT(cl.reference_id)) AS references
        FROM cl_classes AS cl
        WHERE cl.id in (SELECT * FROM ids_classes)
    ) AS refs
    """

    where_sql_str, args = await get_where_sql(class_query, ids)
    arg_counter = len(args) + 1

    ids_jobs_sql_str = ""
    if class_query.jobs is not None:
        ids_jobs_sql_str = f"""
        ids_jobs AS (
            SELECT DISTINCT class_id::uuid
            FROM j_job_classes
            WHERE job_id = ANY(${arg_counter})
        ),
        """
        args.append(class_query.jobs)
        arg_counter += 1
        if where_sql_str != "":
            where_sql_str += " AND cl.id IN (SELECT * FROM ids_jobs)"
        else:
            where_sql_str = "WHERE cl.id IN (SELECT * FROM ids_jobs)"

    return base_sql.format(ids_jobs_sql_str=ids_jobs_sql_str, where_sql_str=where_sql_str), args


async def get_update_sql():
    return """
    UPDATE cl_classes
        SET creator_id = $2,
            creator_type = $3,
            reference_id = $4,
            reference_type = $5,
            value = $6
    WHERE id=$1;
    """
