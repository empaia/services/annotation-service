from typing import List

from pydantic import UUID4

from annotation_service.models.v1.annotation.primitives import PrimitiveQuery, PrimitiveType
from annotation_service.singletons import settings

from .query_builder import concat_sql_statemment

TYPE_COLUMN_NAMES = {
    PrimitiveType.INTEGER: "ivalue",
    PrimitiveType.FLOAT: "fvalue",
    PrimitiveType.BOOL: "bvalue",
    PrimitiveType.STRING: "svalue",
}


async def get_insert_tmptable_sql(primitive_type: PrimitiveType, external_ids: bool):
    base_sql = """
    INSERT INTO tmptable_pr(
        id,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        type,
        is_locked,
        {type_column_name}
    )
    VALUES {value_template};
    """

    if settings.allow_external_ids and external_ids:
        value_template = "($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)"
    else:
        value_template = "(gen_random_uuid(), $1, $2, $3, $4, $5, $6, $7, $8, $9)"

    return base_sql.format(
        type_column_name=TYPE_COLUMN_NAMES[primitive_type],
        value_template=value_template,
    )


async def get_insert_sql(primitive_type: PrimitiveType):
    base_sql = """
    INSERT INTO p_primitives SELECT * FROM tmptable_pr
    RETURNING
    '{{"description":null, "reference_id":null, "reference_type":null}}' || jsonb_strip_nulls(
        jsonb_build_object(
            'id', p_primitives.id,
            'name', p_primitives.name,
            'description', p_primitives.description,
            'creator_id', p_primitives.creator_id,
            'creator_type', p_primitives.creator_type,
            'reference_id', p_primitives.reference_id,
            'reference_type', p_primitives.reference_type,
            'type', p_primitives.type,
            'is_locked', p_primitives.is_locked,
            {type_column_name_response}
        )
    );
    """

    type_column_names_response = {
        PrimitiveType.INTEGER: "'value', p_primitives.ivalue",
        PrimitiveType.FLOAT: "'value', p_primitives.fvalue",
        PrimitiveType.BOOL: "'value', p_primitives.bvalue",
        PrimitiveType.STRING: "'value', p_primitives.svalue",
    }

    return base_sql.format(
        type_column_name_response=type_column_names_response[primitive_type],
    )


async def get_where_sql(primitive_query: PrimitiveQuery, ids: List[UUID4], reference_type: bool = False):
    where_terms = []
    args = []
    arg_counter = 1
    references_terms = []
    if primitive_query.references is not None:
        include_null_references = None in primitive_query.references
        if include_null_references:
            references_terms.append("reference_id IS NULL")
            if len(primitive_query.references) > 1:
                references_terms.append(f"reference_id = ANY(${arg_counter})")
                args.append(primitive_query.references)
                arg_counter += 1
        else:
            references_terms.append(f"reference_id = ANY(${arg_counter})")
            args.append(primitive_query.references)
            arg_counter += 1
    if reference_type:
        references_terms.append("reference_type='collection'")

    if len(references_terms) > 0:
        references_sql = f"({await concat_sql_statemment(terms=references_terms, prefix='', separator=' OR ')})"
        where_terms.append(references_sql)
    if primitive_query.creators is not None:
        where_terms.append(f"creator_id = ANY(${arg_counter})")
        args.append(primitive_query.creators)
        arg_counter += 1
    if primitive_query.types is not None:
        where_terms.append(f"type = ANY(${arg_counter})")
        args.append(primitive_query.types)
        arg_counter += 1
    if ids is not None and len(ids) > 0:
        where_terms.append(f"id = ANY(${arg_counter})")
        args.append(ids)
        arg_counter += 1

    return await concat_sql_statemment(terms=where_terms, prefix="WHERE ", separator=" AND "), args


async def get_count_sql(primitive_query: PrimitiveQuery, ids: List[UUID4] = None, reference_type: bool = False):
    base_sql = """
    SELECT COUNT(id)
    FROM p_primitives
    {where_sql_str};
    """

    where_sql_str, args = await get_where_sql(primitive_query, ids, reference_type)

    return base_sql.format(where_sql_str=where_sql_str), args


async def get_select_sql(primitive_query: PrimitiveQuery, ids: List[UUID4] = None, reference_type: bool = False):
    where_sql_str, args = await get_where_sql(primitive_query, ids, reference_type)

    null_json = '\'{"description":null, "reference_id":null, "reference_type":null}\'::jsonb || '

    base_sql = f"""
    SELECT {null_json} json_strip_nulls(row_to_json(row))::jsonb AS json
    FROM (
        SELECT
        id,
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        is_locked,
        to_json(ivalue) AS value,
        to_json(fvalue) AS value,
        to_json(bvalue) AS value,
        to_json(svalue) AS value
        FROM p_primitives
        {where_sql_str}
        LIMIT ${len(args) + 1} OFFSET ${len(args) + 2}
    ) AS row;
    """

    return base_sql, args


async def get_references_sql(primitive_query: PrimitiveQuery, ids: List[UUID4] = None):
    base_sql = """
    WITH
    {ids_jobs_sql_str}
    ids_primitives AS (
        SELECT id
        FROM p_primitives AS p
        {where_sql_str}
    ),
    ids_a_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS a_references
        FROM p_primitives AS p
        WHERE p.id in (SELECT * FROM ids_primitives)
        and p.reference_type = 'annotation'
    ),
    ids_c_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS c_references
        FROM p_primitives AS p
        WHERE p.id in (SELECT * FROM ids_primitives)
        and p.reference_type = 'collection'
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS w_references
        FROM p_primitives AS p
        WHERE p.id in (SELECT * FROM ids_primitives)
        and p.reference_type = 'wsi'
    ),
    ids_n_refs AS (
        SELECT count(*) as n_count
        FROM p_primitives AS p
        WHERE p.id in (SELECT * FROM ids_primitives)
        and p.reference_type IS NULL
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.a_references IS NULL THEN '[]'::jsonb
            ELSE ar.a_references
        END,
        'collection',
        CASE
            WHEN ar.c_references IS NULL THEN '[]'::jsonb
            ELSE ar.c_references
        END,
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END,
        'contains_items_without_reference',
        CASE
            WHEN ar.n_count > 0 THEN true
            ELSE false
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_a_refs, ids_c_refs, ids_w_refs, ids_n_refs
    ) AS ar
    """

    where_sql_str, args = await get_where_sql(primitive_query, ids)
    arg_counter = len(args) + 1

    ids_jobs_sql_str = ""
    if primitive_query.jobs is not None:
        ids_jobs_sql_str = f"""
        ids_jobs AS (
            SELECT DISTINCT primitive_id
            FROM j_job_primitives
            WHERE job_id = ANY(${arg_counter})
        ),
        """
        args.append(primitive_query.jobs)
        arg_counter += 1
        if where_sql_str != "":
            where_sql_str += " AND p.id IN (SELECT * FROM ids_jobs)"
        else:
            where_sql_str = "WHERE p.id IN (SELECT * FROM ids_jobs)"

    return base_sql.format(ids_jobs_sql_str=ids_jobs_sql_str, where_sql_str=where_sql_str), args


async def get_update_sql(primitive_type: PrimitiveType):
    base_sql = """
    UPDATE p_primitives
        SET name = $2,
            description = $3,
            creator_id = $4,
            creator_type = $5,
            reference_id = $6,
            reference_type = $7,
            type = $8,
            {type_column_name} = $9
    WHERE id = $1;
    """

    return base_sql.format(type_column_name=TYPE_COLUMN_NAMES[primitive_type])
