from annotation_service.singletons import settings

CLASS_SELECT_SQL_STR = """
        ,
        COALESCE(
            json_agg(cl.*) FILTER (
                WHERE
                    cl.id IS NOT NULL
            ),
            '[]'
        ) AS classes
        """


async def get_single_sql(with_classes: bool):
    base_sql = """
    SELECT
        json_strip_nulls(row_to_json(ROW)) AS annotation
    FROM
        (
            SELECT
                a.id,
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                NULLIF(
                    to_jsonb(
                        array_remove(ARRAY [ a.npp_viewing_min, a.npp_viewing_max ], NULL)
                    ),
                    '[]'
                ) AS npp_viewing,
                a.is_locked,
                EXTRACT(epoch FROM a.created_at)::int as created_at,
                EXTRACT(epoch FROM a.updated_at)::int as updated_at,
                a.coordinates,
                a.head,
                a.tail,
                a.center,
                a.radius,
                a.upper_left,
                a.width,
                a.height,
                a.centroid
                {class_select_sql_str}
            FROM
                a_annotations AS a
                {class_join_sql_str}
            WHERE
                a.id = $1
            GROUP BY
                a.id
        ) AS ROW;
    """

    class_select_sql_str = ""
    class_join_sql_str = ""

    if with_classes:
        class_select_sql_str = CLASS_SELECT_SQL_STR
        class_join_sql_str = "LEFT JOIN cl_classes AS cl ON a.id = cl.reference_id"

    return base_sql.format(class_select_sql_str=class_select_sql_str, class_join_sql_str=class_join_sql_str)


async def get_all_sql(with_classes: bool):
    base_sql = """
    WITH a_count AS (
        SELECT
            count(*)
        FROM
            a_annotations
    )
    SELECT
        json_build_object('item_count', item_count, 'items', items) :: TEXT AS json
    FROM
        (
            SELECT
                a_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                    THEN raise_exception(
                        NULL :: json,
                        format({error_message}),
                        'EM001'
                    )
                    ELSE (
                        SELECT
                            COALESCE(
                                json_agg(json_strip_nulls(row_to_json(ROW))),
                                '[]' :: json
                            )
                        FROM
                            (
                                SELECT
                                    annots.*
                                    {class_select_sql_str}
                                FROM
                                    (
                                        SELECT
                                            a.id,
                                            a.type,
                                            a.name,
                                            a.description,
                                            a.creator_id,
                                            a.creator_type,
                                            a.reference_id,
                                            a.reference_type,
                                            a.npp_created,
                                            NULLIF(
                                                to_jsonb(
                                                    array_remove(ARRAY [ a.npp_viewing_min, a.npp_viewing_max ], NULL)
                                                ),
                                                '[]'
                                            ) AS npp_viewing,
                                            a.is_locked,
                                            EXTRACT(epoch FROM a.created_at)::int as created_at,
                                            EXTRACT(epoch FROM a.updated_at)::int as updated_at,
                                            a.coordinates,
                                            a.head,
                                            a.tail,
                                            a.center,
                                            a.radius,
                                            a.upper_left,
                                            a.width,
                                            a.height,
                                            a.centroid
                                        FROM
                                            a_annotations AS a
                                        ORDER BY
                                            a.updated_at DESC,
                                            a.id ASC
                                        LIMIT
                                            $1
                                        OFFSET
                                            $2
                                    ) AS annots
                                    {class_join_sql_str}
                                GROUP BY
                                    annots.id,
                                    annots.type,
                                    annots.name,
                                    annots.description,
                                    annots.creator_id,
                                    annots.creator_type,
                                    annots.reference_id,
                                    annots.reference_type,
                                    annots.npp_created,
                                    annots.npp_viewing,
                                    annots.is_locked,
                                    annots.created_at,
                                    annots.updated_at,
                                    annots.coordinates,
                                    annots.head,
                                    annots.tail,
                                    annots.center,
                                    annots.radius,
                                    annots.upper_left,
                                    annots.width,
                                    annots.height,
                                    annots.centroid
                                ORDER BY
                                    annots.updated_at DESC,
                                    annots.id ASC
                            ) ROW
                    )
                END items
            FROM
                a_count
        ) sel;
    """

    limit_comparison_sql_str = (
        f"a_count.count > {settings.v1_item_limit} AND ($1::int IS NULL OR $1::int > {settings.v1_item_limit})"
    )

    error_message = f"'Count of requested items (%s) exceeds server limit of {settings.v1_item_limit}.', a_count.count"

    class_select_sql_str = ""
    class_join_sql_str = ""

    if with_classes:
        class_select_sql_str = CLASS_SELECT_SQL_STR
        class_join_sql_str = "LEFT JOIN cl_classes AS cl ON annots.id = cl.reference_id"

    return base_sql.format(
        limit_comparison_sql_str=limit_comparison_sql_str,
        error_message=error_message,
        class_select_sql_str=class_select_sql_str,
        class_join_sql_str=class_join_sql_str,
    )
