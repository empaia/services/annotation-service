from annotation_service.models.v1.annotation.annotations import AnnotationType
from annotation_service.singletons import settings

TYPE_COLUMN_NAMES = {
    **dict.fromkeys([AnnotationType.POINT, AnnotationType.LINE, AnnotationType.POLYGON], "coordinates"),
    AnnotationType.ARROW: "head, tail",
    AnnotationType.CIRCLE: "center, radius",
    AnnotationType.RECTANGLE: "upper_left, width, height",
}

INSERT_TEMPLATE_MAPPING_EXT_IDS = {
    **dict.fromkeys(
        [AnnotationType.POINT, AnnotationType.LINE, AnnotationType.POLYGON],
        """
        ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,
        current_timestamp, current_timestamp, $15, $16, $17)
        """,
    ),
    **dict.fromkeys(
        [AnnotationType.ARROW, AnnotationType.CIRCLE],
        """
        ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,
        current_timestamp, current_timestamp, $15, $16, $17, $18)
        """,
    ),
    AnnotationType.RECTANGLE: """
        ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14,
        current_timestamp, current_timestamp, $15, $16, $17, $18, $19)
        """,
}

INSERT_TEMPLATE_MAPPING = {
    **dict.fromkeys(
        [AnnotationType.POINT, AnnotationType.LINE, AnnotationType.POLYGON],
        """
        (gen_random_uuid(), $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13,
        current_timestamp, current_timestamp, $14, $15, $16)
        """,
    ),
    **dict.fromkeys(
        [AnnotationType.ARROW, AnnotationType.CIRCLE],
        """
        (gen_random_uuid(), $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13,
        current_timestamp, current_timestamp, $14, $15, $16, $17)
        """,
    ),
    AnnotationType.RECTANGLE: """
        (gen_random_uuid(), $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13,
        current_timestamp, current_timestamp, $14, $15, $16, $17, $18)
        """,
}

TYPE_COLUMN_NAMES_RESPONSE = {
    **dict.fromkeys(
        [AnnotationType.POINT, AnnotationType.LINE, AnnotationType.POLYGON], "'coordinates', a_annotations.coordinates"
    ),
    AnnotationType.ARROW: "'head', a_annotations.head, 'tail', a_annotations.tail",
    AnnotationType.CIRCLE: "'center', a_annotations.center, 'radius', a_annotations.radius",
    AnnotationType.RECTANGLE: """
    'upper_left', a_annotations.upper_left,
    'width', a_annotations.width,
    'height', a_annotations.height
    """,
}


async def get_insert_tmptable_sql(annotation_type: AnnotationType, external_ids: bool):
    base_sql = """
    INSERT INTO
        tmptable_annot (
            id,
            TYPE,
            NAME,
            description,
            creator_id,
            creator_type,
            reference_id,
            reference_type,
            npp_created,
            npp_viewing_min,
            npp_viewing_max,
            npp_viewing_min_query,
            npp_viewing_max_query,
            is_locked,
            created_at,
            updated_at,
            {type_column_names},
            centroid,
            bounding_box
        )
    VALUES
        {value_template};
    """

    if settings.allow_external_ids and external_ids:
        value_template = INSERT_TEMPLATE_MAPPING_EXT_IDS[annotation_type]
    else:
        value_template = INSERT_TEMPLATE_MAPPING[annotation_type]

    return base_sql.format(
        type_column_names=TYPE_COLUMN_NAMES[annotation_type],
        value_template=value_template,
    )


async def get_insert_sql(annotation_type: AnnotationType):
    base_sql = """
    INSERT INTO
        a_annotations
    SELECT
        *
    FROM
        tmptable_annot RETURNING '{{"description":null, "npp_viewing":null}}' :: jsonb || jsonb_strip_nulls(
            jsonb_build_object(
                'id', a_annotations.id,
                'type', a_annotations.type,
                'name', a_annotations.name,
                'description', a_annotations.description,
                'creator_id', a_annotations.creator_id,
                'creator_type', a_annotations.creator_type,
                'reference_id', a_annotations.reference_id,
                'reference_type', a_annotations.reference_type,
                'npp_created', a_annotations.npp_created,
                'npp_viewing',
                NULLIF(
                    to_jsonb(
                        array_remove(
                            ARRAY[a_annotations.npp_viewing_min, a_annotations.npp_viewing_max], NULL
                        )
                    ),
                    '[]'
                ),
                'is_locked', a_annotations.is_locked,
                'created_at', EXTRACT(epoch FROM a_annotations.created_at) :: INT,
                'updated_at', EXTRACT(epoch FROM a_annotations.updated_at) :: INT,
                {type_column_names_response},
                'centroid', a_annotations.centroid
            )
        );
    """

    return base_sql.format(type_column_names_response=TYPE_COLUMN_NAMES_RESPONSE[annotation_type])
