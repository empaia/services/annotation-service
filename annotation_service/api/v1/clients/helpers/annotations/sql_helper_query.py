from annotation_service.models.v1.annotation.annotations import AnnotationQuery, AnnotationViewerQuery
from annotation_service.singletons import settings

from ..query_builder import concat_sql_statemment
from .sql_helper_commons import (
    get_annot_ids_class_job,
    get_annot_ids_sql_str,
    get_build_sql_str,
    get_centroids_sql_str,
    get_class_left_join_sql,
    get_where_sql,
)


async def get_query_sql(annotation_query: AnnotationQuery, with_classes: bool, with_centroids: bool, raw: bool = False):
    base_sql = """
    WITH
    {class_sql_str}
    {annot_ids_sql_str},
    {centroids_cte_str}
    a_count AS (
        SELECT
            count(*)
        FROM
            (
                SELECT
                    *
                FROM
                    {ids_cte}
            ) AS ids
    ),
    annotations AS (
        SELECT
            annots.*
            {classes_select_term}
        FROM
            (
                SELECT
                    a.id,
                    a.type,
                    a.name,
                    a.description,
                    a.creator_id,
                    a.creator_type,
                    a.reference_id,
                    a.reference_type,
                    a.npp_created,
                    NULLIF(
                        to_jsonb(
                            array_remove(ARRAY [ a.npp_viewing_min, a.npp_viewing_max ], NULL)
                        ),
                        '[]'
                    ) AS npp_viewing,
                    a.is_locked,
                    a.created_at,
                    a.updated_at,
                    a.coordinates,
                    a.head,
                    a.tail,
                    a.center,
                    a.radius,
                    a.upper_left,
                    a.width,
                    a.height,
                    a.centroid
                FROM
                    a_annotations AS a
                WHERE
                    a.id IN (
                        SELECT
                            *
                        FROM
                            {ids_cte}
                    )
                ORDER BY
                    a.updated_at DESC,
                    a.id ASC
                {limit_offset_str}
            ) AS annots
            {classes_left_join_str}
        GROUP BY
            annots.id,
            annots.type,
            annots.name,
            annots.description,
            annots.creator_id,
            annots.creator_type,
            annots.reference_id,
            annots.reference_type,
            annots.npp_created,
            annots.npp_viewing,
            annots.is_locked,
            annots.created_at,
            annots.updated_at,
            annots.coordinates,
            annots.head,
            annots.tail,
            annots.center,
            annots.radius,
            annots.upper_left,
            annots.width,
            annots.height,
            annots.centroid
        ORDER BY
            annots.updated_at DESC,
            annots.id ASC
    )
    {build_sql_str}
    FROM
        (
            SELECT
                a_count.count AS item_count,
                CASE
                    WHEN {count_condition_str}
                    THEN raise_exception(
                        NULL :: json,
                        format({error_message}, a_count.count),
                        'EM001'
                    )
                    ELSE (
                        SELECT
                            COALESCE(
                                json_agg(json_strip_nulls(row_to_json(ROW))),
                                '[]' :: json
                            )
                        FROM
                            (
                                SELECT
                                    {final_select_terms_str}
                                FROM
                                    annotations
                            ) ROW
                    )
                END items
                {centroids_agg_str}
            FROM
                a_count
                {centroids_select_str}
        ) AS sel;
    """

    where_sql_str, arg_counter, args = await get_where_sql(annotation_query)

    classes_select_term = ""
    if with_classes:
        classes_select_term = """
        ,COALESCE(
            json_agg(cl.*) FILTER (
                WHERE
                    cl.id IS NOT NULL
            ),
            '[]'
        ) AS classes
        """

    classes_left_join_str, arg_counter, args = await get_class_left_join_sql(
        annotation_query, with_classes, arg_counter, args
    )

    class_sql_str, where_sql_str, args, arg_counter = await get_annot_ids_class_job(
        annotation_query, where_sql_str, arg_counter, args
    )

    annot_ids_sql_str, args, arg_counter = await get_annot_ids_sql_str(
        annotation_query, where_sql_str, args, arg_counter
    )

    centroids_cte_str, centroids_select_str, centroids_agg_str, args, arg_counter = await get_centroids_sql_str(
        annotation_query, where_sql_str, with_centroids, args, arg_counter
    )

    limit_offset_str = f"LIMIT ${arg_counter} OFFSET ${arg_counter + 1}"
    count_condition_str = (
        f"a_count.count > {settings.v1_item_limit} AND (${arg_counter} "
        f"IS NULL OR ${arg_counter} > {settings.v1_item_limit})"
    )
    error_message = f"'Count of requested items (%s) exceeds server limit of {settings.v1_item_limit}.'"
    arg_counter += 2

    build_sql_str = await get_build_sql_str(raw, with_centroids)

    final_select_terms_str = """
    annotations.id,
    annotations.type,
    annotations.name,
    annotations.description,
    annotations.creator_id,
    annotations.creator_type,
    annotations.reference_id,
    annotations.reference_type,
    annotations.npp_created,
    annotations.npp_viewing,
    annotations.is_locked,
    EXTRACT(epoch FROM annotations.created_at)::int as created_at,
    EXTRACT(epoch FROM annotations.updated_at)::int as updated_at,
    annotations.coordinates,
    annotations.head,
    annotations.tail,
    annotations.center,
    annotations.radius,
    annotations.upper_left,
    annotations.width,
    annotations.height,
    annotations.centroid
    """

    if with_classes:
        final_select_terms_str += ", annotations.classes"

    return (
        base_sql.format(
            class_sql_str=class_sql_str,
            annot_ids_sql_str=annot_ids_sql_str,
            centroids_cte_str=centroids_cte_str,
            centroids_agg_str=centroids_agg_str,
            centroids_select_str=centroids_select_str,
            classes_select_term=classes_select_term,
            classes_left_join_str=classes_left_join_str,
            limit_offset_str=limit_offset_str,
            build_sql_str=build_sql_str,
            count_condition_str=count_condition_str,
            error_message=error_message,
            final_select_terms_str=final_select_terms_str,
            ids_cte="ids_annot_npp" if annotation_query.npp_viewing is not None else "ids_annot",
        ),
        args,
    )


async def get_references_sql(annotation_query: AnnotationQuery):
    base_sql = """
    WITH
    {class_sql_str}
    {annot_ids_sql_str}
    SELECT
        jsonb_build_object(
            'wsi',
            CASE
                WHEN refs.references IS NULL THEN '[]' :: jsonb
                ELSE refs.references
            END
        ) AS u_refs
    FROM
        (
            SELECT
                JSONB_AGG(DISTINCT(a.reference_id)) AS references
            FROM
                a_annotations AS a
            WHERE
                a.id IN (
                    (
                        SELECT
                            *
                        FROM
                            {ids_cte}
                    )
                )
        ) AS refs;
    """

    where_sql_str, arg_counter, args = await get_where_sql(annotation_query)

    class_sql_str, where_sql_str, args, arg_counter = await get_annot_ids_class_job(
        annotation_query, where_sql_str, arg_counter, args
    )

    annot_ids_sql_str, args, arg_counter = await get_annot_ids_sql_str(
        annotation_query, where_sql_str, args, arg_counter
    )

    return (
        base_sql.format(
            class_sql_str=class_sql_str,
            annot_ids_sql_str=annot_ids_sql_str,
            ids_cte="ids_annot_npp" if annotation_query.npp_viewing is not None else "ids_annot",
        ),
        args,
    )


async def get_viewer_sql(annotation_viewer_query: AnnotationViewerQuery):
    base_sql = """
    WITH
    {class_sql_str}
    ids_annot AS (
        SELECT
            id
        FROM
            a_annotations AS a
            {where_sql_str}
    ),
    ids_annot_npp AS (
        SELECT
            id
        FROM
            a_annotations AS a
        WHERE
            a.id IN (
                SELECT
                    *
                FROM
                    ids_annot
            )
            AND {npp_viewing_sql_str}
    ),
    c_npp AS (
        SELECT
            id
        FROM
            a_annotations AS a
            {where_sql_str} AND
            {npp_viewing_centroids_sql_str}
            AND a.id NOT IN (
                SELECT
                    *
                FROM
                    ids_annot_npp
            )
    )
    SELECT
        jsonb_agg(ids.id) FILTER (
            WHERE
                ids.id IN (
                    SELECT
                        *
                    FROM
                        ids_annot_npp
                )
        ) AS annotations,
        jsonb_agg(ids.centroid) FILTER (
            WHERE
                ids.id IN (
                    SELECT
                        *
                    FROM
                        c_npp
                )
        ) AS centroids
    FROM
        (
            SELECT
                id,
                centroid
            FROM
                a_annotations AS a
            WHERE
                a.id IN (
                    SELECT
                        *
                    FROM
                        ids_annot
                )
        ) AS ids;
    """

    where_sql_str, arg_counter, args = await get_where_sql(annotation_viewer_query)
    npp_viewing_sql_str = f"""
    GREATEST(${arg_counter}, a.npp_viewing_min_query) <= LEAST(${arg_counter + 1}, a.npp_viewing_max_query)
    """
    min_npp = min(annotation_viewer_query.npp_viewing[0], annotation_viewer_query.npp_viewing[1])
    max_npp = max(annotation_viewer_query.npp_viewing[0], annotation_viewer_query.npp_viewing[1])
    args.append(min_npp)
    args.append(max_npp)
    arg_counter += 2

    npp_viewing_centroids_sql_str = f"""
    GREATEST(${arg_counter}, a.npp_viewing_min_query) <= LEAST(${arg_counter + 1}, a.npp_viewing_max_query)
    """
    min_npp_c = 0
    max_npp_c = min_npp
    args.append(min_npp_c)
    args.append(max_npp_c)
    arg_counter += 2

    class_sql_str, where_sql_str, args, arg_counter = await get_annot_ids_class_job(
        annotation_viewer_query, where_sql_str, arg_counter, args
    )

    return (
        base_sql.format(
            class_sql_str=class_sql_str,
            where_sql_str=where_sql_str,
            npp_viewing_sql_str=npp_viewing_sql_str,
            npp_viewing_centroids_sql_str=npp_viewing_centroids_sql_str,
        ),
        args,
    )


async def get_position_sql(annotation_query: AnnotationQuery):
    where_sql_str, arg_counter, args = await get_where_sql(annotation_query, True)

    base_sql = f"""
    SELECT
        id,
        position
    FROM
        (
            SELECT
                a.id,
                row_number() OVER(
                    ORDER BY
                        a.updated_at DESC,
                        a.id ASC
                ) AS position
            FROM
                a_annotations AS a
                {where_sql_str}
            ORDER BY
                a.updated_at DESC,
                a.id ASC
        ) AS id_position
    WHERE
        id = ${arg_counter};
    """

    return base_sql, args


async def get_count_sql(annotation_query: AnnotationQuery):
    base_sql = """
    WITH
    {class_sql_str}
    {annot_ids_sql_str},
    a_count AS (
        SELECT
            count(*)
        FROM
            (
                SELECT
                    *
                FROM
                    ids_annot
                WHERE
                    id IN (
                        SELECT
                            *
                        FROM
                            {ids_cte}
                    )
            ) AS ids
    )
    SELECT
        jsonb_build_object('item_count', item_count) AS count
    FROM
        (
            SELECT
                a_count.count AS item_count
            FROM
                a_count
        ) AS sel;
    """

    where_sql_str, arg_counter, args = await get_where_sql(annotation_query)

    class_sql_str, where_sql_str, args, arg_counter = await get_annot_ids_class_job(
        annotation_query, where_sql_str, arg_counter, args
    )

    annot_ids_sql_str, args, arg_counter = await get_annot_ids_sql_str(
        annotation_query, where_sql_str, args, arg_counter
    )

    return (
        base_sql.format(
            class_sql_str=class_sql_str,
            annot_ids_sql_str=annot_ids_sql_str,
            ids_cte="ids_annot_npp" if annotation_query.npp_viewing is not None else "ids_annot",
        ),
        args,
    )


async def get_values_sql(annotation_query: AnnotationQuery):
    base_sql = """
    SELECT
        json_build_object('values', json_agg(v.value)) AS json
    FROM
        (
            SELECT
                DISTINCT cl.value
            FROM
                (
                    SELECT
                        a.id,
                        a.creator_id
                    FROM
                        a_annotations AS a
                        {where_sql_str}
                ) AS annots
                {classes_left_join_str}
                {classes_where_str}
        ) AS v;
    """

    where_sql_str, arg_counter, args = await get_where_sql(annotation_query, for_values=True)

    classes_left_join_str = ""
    classes_join_conditions = ["LEFT JOIN cl_classes AS cl ON cl.reference_id = annots.id "]
    if annotation_query.creators is not None:
        classes_join_conditions.append("cl.creator_id = annots.creator_id")

    classes_left_join_str = await concat_sql_statemment(terms=classes_join_conditions, prefix="", separator=" AND ")

    classes_where_str = ""
    if annotation_query.jobs is not None and None not in annotation_query.jobs:
        classes_where_str = f"""
        WHERE
            cl.id IN (
                SELECT
                    class_id :: uuid
                FROM
                    j_job_classes
                WHERE
                    job_id = ANY(${arg_counter})
                    AND class_id <> ''
            )
            OR annots.id IN (
                SELECT
                    annot_id
                FROM
                    j_job_classes
                WHERE
                    job_id = ANY(${arg_counter})
                    AND VALUE = ''
            )
        """
        args.append(annotation_query.jobs)
        arg_counter += 1

    return (
        base_sql.format(
            where_sql_str=where_sql_str,
            classes_left_join_str=classes_left_join_str,
            classes_where_str=classes_where_str,
        ),
        args,
    )
