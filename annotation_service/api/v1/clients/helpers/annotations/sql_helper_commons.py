from typing import List

from asyncpg import Box

from annotation_service.models.v1.annotation.annotations import AnnotationQuery

from ..query_builder import concat_sql_statemment


async def get_where_sql(annotation_query: AnnotationQuery, for_values: bool = False):
    where_terms = []
    args = []
    arg_counter = 1
    if annotation_query.creators is not None:
        where_terms.append(f"a.creator_id = ANY(${arg_counter})")
        args.append(annotation_query.creators)
        arg_counter += 1
    if annotation_query.references is not None:
        where_terms.append(f"a.reference_id = ANY(${arg_counter})")
        args.append(annotation_query.references)
        arg_counter += 1
    if annotation_query.types is not None:
        where_terms.append(f"a.type = ANY(${arg_counter})")
        args.append(annotation_query.types)
        arg_counter += 1
    if annotation_query.viewport is not None:
        where_terms.append(f"bounding_box ?# ${arg_counter}")
        viewport = Box(
            [annotation_query.viewport.x, annotation_query.viewport.y],
            [
                annotation_query.viewport.x + annotation_query.viewport.width,
                annotation_query.viewport.y + annotation_query.viewport.height,
            ],
        )
        args.append(viewport)
        arg_counter += 1
    if for_values:
        if annotation_query.npp_viewing is not None:
            where_terms.append(
                f"""
                GREATEST(${arg_counter}, npp_viewing_min_query) <= LEAST(${arg_counter + 1}, npp_viewing_max_query)
                """
            )
            min_npp = min(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])
            max_npp = max(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])
            args.append(min_npp)
            args.append(max_npp)
            arg_counter += 2
        class_job_where_sql, arg_counter, args = await get_class_job_where_sql(annotation_query, arg_counter, args)
        if class_job_where_sql is not None:
            where_terms.append(class_job_where_sql)

    return await concat_sql_statemment(terms=where_terms, prefix="WHERE ", separator=" AND "), arg_counter, args


async def get_class_job_where_sql(annotation_query: AnnotationQuery, arg_counter: int, args: List[str]):
    class_job_sql_base = """
    a.id IN
    (
        {id_sql}
    )
    """

    id_sql = ""
    where_terms = []
    if annotation_query.jobs is not None:
        none_in_jobs = None in annotation_query.jobs

        if hasattr(annotation_query, "class_values") and annotation_query.class_values is not None:
            if not none_in_jobs:
                id_sql = f"""
                SELECT
                    DISTINCT(annot_id)
                FROM
                    j_job_classes
                WHERE
                    job_id = ANY(${arg_counter})
                    AND VALUE = ANY(${arg_counter + 1})
                """
                args.append(annotation_query.jobs)
                args.append(annotation_query.class_values)
                arg_counter += 2
            else:
                id_sql, arg_counter, args = await get_class_where_sql(annotation_query, arg_counter, args)
        else:
            if not none_in_jobs:
                id_sql = f"""
                SELECT
                    DISTINCT(annotation_id)
                FROM
                    j_job_annotations
                WHERE
                    job_id = ANY(${arg_counter})
                """
                args.append(annotation_query.jobs)
                arg_counter += 1

        if id_sql != "":
            class_job_sql = class_job_sql_base.format(id_sql=id_sql)
            where_terms.append(class_job_sql)

        if none_in_jobs:
            where_terms.append("a.is_locked = 'false'")

        return f"({await concat_sql_statemment(terms=where_terms, prefix='', separator=' AND ')})", arg_counter, args

    elif hasattr(annotation_query, "class_values") and annotation_query.class_values is not None:
        id_sql, arg_counter, args = await get_class_where_sql(annotation_query, arg_counter, args)

        if id_sql != "":
            class_job_sql = class_job_sql_base.format(id_sql=id_sql)
            where_terms.append(class_job_sql)

        return f"({await concat_sql_statemment(terms=where_terms, prefix='', separator=' OR ')})", arg_counter, args
    else:
        return None, arg_counter, args


async def get_class_where_sql(annotation_query: AnnotationQuery, arg_counter: int, args: List[str]):
    class_sql_base = """
    SELECT
        DISTINCT(a.id)
    FROM
        a_annotations AS a
        LEFT JOIN cl_classes AS cl ON a.id = cl.reference_id
        {where_sql}
    """

    class_none_where_sql = "WHERE value IS NULL"
    class_value_where_sql = f"WHERE value = ANY(${arg_counter})"
    class_none_and_value_where_sql = f"WHERE value = ANY(${arg_counter}) OR value IS NULL"

    if annotation_query.class_values is not None:
        value_count = len(annotation_query.class_values)
        none_in_values = "" in annotation_query.class_values
        str_in_values = (none_in_values and value_count > 1) or (not none_in_values and value_count > 0)

        if none_in_values and str_in_values:
            args.append(annotation_query.class_values)
            arg_counter += 1
            return class_sql_base.format(where_sql=class_none_and_value_where_sql), arg_counter, args
        elif none_in_values:
            return class_sql_base.format(where_sql=class_none_where_sql), arg_counter, args
        elif str_in_values:
            args.append(annotation_query.class_values)
            arg_counter += 1
            return class_sql_base.format(where_sql=class_value_where_sql), arg_counter, args
    else:
        return class_sql_base.format(where_sql=""), arg_counter, args


async def get_class_left_join_sql(
    annotation_query: AnnotationQuery, with_classes: bool, arg_counter: int, args: List[str]
):
    if with_classes:
        classes_join_conditions = ["LEFT JOIN cl_classes AS cl ON cl.reference_id = annots.id"]
        if annotation_query.creators is not None:
            classes_join_conditions.append("cl.creator_id = annots.creator_id")
        if annotation_query.jobs is not None and None not in annotation_query.jobs:
            classes_join_conditions.append(
                f"""
                cl.id IN (
                    SELECT
                        class_id :: uuid
                    FROM
                        j_job_classes
                    WHERE
                        job_id = ANY(${arg_counter})
                        AND class_id <> ''
                )
                """
            )
            args.append(annotation_query.jobs)
            arg_counter += 1
        return (
            await concat_sql_statemment(terms=classes_join_conditions, prefix="", separator=" AND "),
            arg_counter,
            args,
        )
    else:
        return "", arg_counter, args


async def get_centroids_sql_str(
    annotation_query: AnnotationQuery, where_sql_str: str, with_centroids: bool, args: List[str], arg_counter: int
):
    centroids_cte_str = ""
    centroids_select_str = ""
    centroids_agg_str = ""
    if with_centroids:
        centroids_cte_str = """
        c_npp AS (
            SELECT
                id
            FROM
                a_annotations AS a
                {where_sql_str}
                AND {npp_viewing_centroids_sql_str}
                AND id NOT IN (
                    SELECT
                        *
                    FROM
                        ids_annot_npp
                )
        ),
        """

        npp_viewing_centroids_sql_str = f"""
        GREATEST(${arg_counter}, a.npp_viewing_min_query) <= LEAST(${arg_counter + 1}, a.npp_viewing_max_query)
        """

        centroids_cte_str = centroids_cte_str.format(
            where_sql_str=where_sql_str, npp_viewing_centroids_sql_str=npp_viewing_centroids_sql_str
        )

        min_npp_c = 0
        max_npp_c = min(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])
        args.append(min_npp_c)
        args.append(max_npp_c)
        arg_counter += 2

        centroids_agg_str = """
        ,COALESCE(
            jsonb_agg(ids.centroid) FILTER (
                WHERE
                    ids.id IN (
                        SELECT
                            *
                        FROM
                            c_npp
                    )
            ),
            '[]' :: jsonb
        ) AS centroids
        """

        centroids_select_str = """
        ,(
            SELECT
                id,
                centroid
            FROM
                a_annotations AS a
            WHERE
                a.id IN (
                    SELECT
                        *
                    FROM
                        ids_annot
                )
        ) AS ids
        GROUP BY
            a_count.count
        """

    return centroids_cte_str, centroids_select_str, centroids_agg_str, args, arg_counter


async def get_annot_ids_class_job(
    annotation_query: AnnotationQuery, where_sql_str: str, arg_counter: int, args: List[str]
):
    class_sql_str = ""
    if annotation_query.jobs is None and annotation_query.class_values is None:
        return (class_sql_str, where_sql_str, args, arg_counter)

    # append unlocked to generel filters
    if annotation_query.class_values is None and annotation_query.jobs is not None and None in annotation_query.jobs:
        if where_sql_str != "":
            where_sql_str += " AND a.is_locked = 'false'"
        else:
            where_sql_str = "WHERE a.is_locked = 'false'"
        return (class_sql_str, where_sql_str, args, arg_counter)

    # ids from j_jobs_classes -> job ids in query
    if annotation_query.jobs is not None and None not in annotation_query.jobs:
        class_sql_str, arg_counter, args = await get_class_job_where_sql_str(annotation_query, arg_counter, args)

    # ids from a_annotations with class join
    if annotation_query.jobs is None or None in annotation_query.jobs:
        class_sql_str, arg_counter, args = await get_class_where_sql_str(annotation_query, arg_counter, args)

    if where_sql_str != "":
        where_sql_str += " AND id IN (SELECT * FROM ids_classes)"
    else:
        where_sql_str = "WHERE id IN (SELECT * FROM ids_classes)"

    return (class_sql_str, where_sql_str, args, arg_counter)


async def get_class_job_where_sql_str(annotation_query: AnnotationQuery, arg_counter: int, args: List[str]):
    class_job_sql_base = """
    ids_classes AS (
        SELECT
            annot_id AS id
        FROM
            j_job_classes
            {where_str}
    ),
    """

    where_terms = []
    where_terms.append(f"job_id = ANY(${arg_counter})")
    args.append(annotation_query.jobs)
    arg_counter += 1
    if annotation_query.class_values is not None:
        where_terms.append(f"value = ANY(${arg_counter})")
        args.append(annotation_query.class_values)
        arg_counter += 1

    return (
        class_job_sql_base.format(
            where_str=await concat_sql_statemment(terms=where_terms, prefix="WHERE ", separator=" AND ")
        ),
        arg_counter,
        args,
    )


async def get_class_where_sql_str(annotation_query: AnnotationQuery, arg_counter: int, args: List[str]):
    class_sql_base = """
    ids_classes AS (
        SELECT
            a.id
        FROM
            a_annotations AS a
            {left_join_sql_str}
            {where_sql}
    ),
    """

    class_none_where_sql = "value IS NULL"
    class_value_where_sql = f"value = ANY(${arg_counter})"
    class_none_and_value_where_sql = f"({class_value_where_sql} OR {class_none_where_sql})"

    left_join_sql_str = ""
    where_terms = []
    if annotation_query.class_values is not None:
        value_count = len(annotation_query.class_values)
        none_in_values = "" in annotation_query.class_values
        str_in_values = (none_in_values and value_count > 1) or (not none_in_values and value_count > 0)
        left_join_sql_str = "LEFT JOIN cl_classes AS cl ON a.id = cl.reference_id"

        if none_in_values and str_in_values:
            where_terms.append(class_none_and_value_where_sql)
            args.append(annotation_query.class_values)
            arg_counter += 1
        elif none_in_values:
            where_terms.append(class_none_where_sql)
        elif str_in_values:
            where_terms.append(class_value_where_sql)
            args.append(annotation_query.class_values)
            arg_counter += 1

    if annotation_query.jobs is not None and None in annotation_query.jobs:
        where_terms.append("a.is_locked = 'false'")

    return (
        class_sql_base.format(
            where_sql=await concat_sql_statemment(terms=where_terms, prefix="WHERE ", separator=" AND "),
            left_join_sql_str=left_join_sql_str,
        ),
        arg_counter,
        args,
    )


async def get_annot_ids_sql_str(
    annotation_query: AnnotationQuery, where_sql_str: str, args: List[str], arg_counter: int
):
    annot_ids_sql_str = ""
    if hasattr(annotation_query, "annotations") and annotation_query.annotations is not None:
        select_ids_sql_str = f"SELECT * FROM unnest(${arg_counter}::uuid[])"
        args.append(annotation_query.annotations)
        arg_counter += 1

        annot_ids_sql_str = f"""
        ids_annot AS (
            SELECT
                *
            FROM
                (
                    (
                        SELECT
                            id
                        FROM
                            a_annotations AS a
                            {where_sql_str}
                    )
                    INTERSECT
                    (
                        {select_ids_sql_str}
                    )
                ) AS ids
        )
        """
    else:
        annot_ids_sql_str = f"""
        ids_annot AS (
            SELECT
                id
            FROM
                a_annotations AS a
                {where_sql_str}
        )
        """

    ids_npp_cte_str = ""
    if annotation_query.npp_viewing is not None:
        ids_npp_cte_str = f"""
        ,ids_annot_npp AS (
            SELECT
                id
            FROM
                a_annotations AS a
            WHERE
                a.id IN (
                    SELECT
                        *
                    FROM
                        ids_annot
                )
                AND
                GREATEST(${arg_counter}, a.npp_viewing_min_query) <= LEAST(${arg_counter + 1}, a.npp_viewing_max_query)
        )
        """
        min_npp = min(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])
        max_npp = max(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])
        args.append(min_npp)
        args.append(max_npp)
        arg_counter += 2

    return annot_ids_sql_str + ids_npp_cte_str, args, arg_counter


async def get_build_sql_str(raw: bool, with_centroids: bool):
    if raw:
        build_sql_str = """
        SELECT
            item_count,
            items
        """
    else:
        build_sql_str = """
        SELECT
            json_build_object(
                'item_count', item_count,
                'items', items
                {centroid_build_sql}
            ) :: TEXT AS json
        """
        centroid_build_sql = ""
        if with_centroids:
            centroid_build_sql = ",'low_npp_centroids', centroids"
        build_sql_str = build_sql_str.format(centroid_build_sql=centroid_build_sql)

    return build_sql_str
