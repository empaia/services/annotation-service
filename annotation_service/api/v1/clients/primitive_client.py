from typing import List

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from pydantic import UUID4

from annotation_service.models.v1.annotation.primitives import (
    PostPrimitive,
    PostPrimitives,
    Primitive,
    PrimitiveQuery,
    PrimitiveReferenceType,
)
from annotation_service.singletons import logger, settings

from .helpers import primitive_sql_helper as sqlh
from .utils import annotation_utils as autils
from .utils import collection_utils as cutils
from .utils import primitive_utils as utils


class PrimitiveClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_primitives(self, primitives: PostPrimitives, external_ids: bool = False, raw: bool = False):
        if isinstance(primitives, PostPrimitive.__args__):
            return await self.create_primitives([primitives], external_ids, raw)
        elif len(primitives.items) > 0:
            if len(primitives.items) > settings.v1_post_limit:
                raise HTTPException(
                    413,
                    (
                        f"Number of posted items ({len(primitives.items)})"
                        f" exceeds server limit of {settings.v1_post_limit}."
                    ),
                )
            return await self.create_primitives(primitives.items, external_ids, raw)

        raise HTTPException(405, "No valid post data")

    async def create_primitives(self, primitives: List[Primitive], external_ids: bool = False, raw: bool = False):
        insert_tmptable_sql = await sqlh.get_insert_tmptable_sql(primitives[0].type, external_ids)
        insert_sql = await sqlh.get_insert_sql(primitives[0].type)
        logger.debug(insert_tmptable_sql)
        logger.debug(insert_sql)

        annot_ref_ids = []
        col_ref_ids = []
        new_primitives = []
        for p in primitives:
            primitive = [
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                p.type,
                False,
                p.value,
            ]

            if settings.allow_external_ids and external_ids:
                primitive.insert(0, p.id)
            new_primitives.append(primitive)

            if p.reference_type == PrimitiveReferenceType.ANNOTATION:
                annot_ref_ids.append(p.reference_id)
            elif p.reference_type == PrimitiveReferenceType.COLLECTION:
                col_ref_ids.append(p.reference_id)

        # check consistency of references
        if len(annot_ref_ids) > 0:
            annot_ref_ids = list(set(annot_ref_ids))
            if not await autils.check_if_annotations_exist(self.conn, annot_ref_ids):
                raise HTTPException(
                    404, "Can not create primitives. At least one primitive references a nonexistent annotation."
                )

        if len(col_ref_ids) > 0:
            col_ref_ids = list(set(col_ref_ids))
            if not await cutils.check_if_collections_exist(self.conn, col_ref_ids):
                raise HTTPException(
                    404, "Can not create primitives. At least one primitive references a nonexistent collection."
                )

        try:
            async with self.conn.transaction():
                await self.conn.execute("CREATE TEMP TABLE IF NOT EXISTS tmptable_pr (LIKE p_primitives)")
                await self.conn.execute("TRUNCATE tmptable_pr")
                await self.conn.executemany(insert_tmptable_sql, new_primitives)
                result = await self.conn.fetch(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Primitive with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive could not be created") from e

        if result is None:
            raise HTTPException(500, "Primitives could not be created (result None)")

        items = [p[0] for p in result]

        if raw:
            return len(items), items
        if len(items) == 1:
            return JSONResponse(items[0], status_code=201)
        return JSONResponse({"item_count": len(items), "items": items}, status_code=201)

    async def get_all(self, skip: int, limit: int):
        return await self.query(primitive_query=PrimitiveQuery(), skip=skip, limit=limit)

    async def get(self, primitive_id: UUID4):
        result = await self.query(
            primitive_query=PrimitiveQuery(primitives=[str(primitive_id)]),
            skip=None,
            limit=None,
            single=True,
        )
        if not result:
            raise HTTPException(404, f"No primitive with id '{primitive_id}' found")
        return result

    async def query(
        self,
        primitive_query: PrimitiveQuery,
        skip: int,
        limit: int,
        single: bool = False,
        raw: bool = False,
        reference_type: bool = False,
    ):
        if primitive_query.references is not None:
            primitive_query.references = list(set(primitive_query.references))

        ids = []

        if primitive_query.jobs is not None:
            job_sql = """
            SELECT DISTINCT primitive_id
            FROM j_job_primitives
            WHERE job_id = ANY($1);
            """

            try:
                id_results = await self.conn.fetch(job_sql, primitive_query.jobs)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Jobs could not be queried (in primitive query)") from e

            if len(id_results) == 0:
                if raw:
                    return 0, []
                else:
                    return JSONResponse({"item_count": 0, "items": []})

            ids_temp = [str(id_result["primitive_id"]) for id_result in id_results]

            if single and primitive_query.primitives is not None and len(primitive_query.primitives) == 1:
                if primitive_query.primitives[0] not in ids_temp:
                    if raw:
                        return 0, []
                    else:
                        return JSONResponse({"item_count": 0, "items": []})
            else:
                ids = ids_temp

        if primitive_query.primitives:
            if len(ids) > 0:
                ids = list(set(ids) & set(primitive_query.primitives))
            else:
                ids = primitive_query.primitives

        count_sql, args = await sqlh.get_count_sql(primitive_query, ids, reference_type)
        logger.debug(count_sql)

        try:
            if len(args) == 0:
                count = await self.conn.fetchval(count_sql)
            else:
                count = await self.conn.fetchval(count_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'primitives' must be of type UUID4.") from e
            raise HTTPException(500, "Primitive could not be queried (count)") from e

        if count == 0:
            if single:
                return None
            if raw:
                return 0, []
            return JSONResponse({"item_count": 0, "items": []})
        elif count > settings.v1_item_limit and (limit is None or limit > settings.v1_item_limit):
            raise HTTPException(
                413, f"Count of requested items ({count}) exceeds server limit of {settings.v1_item_limit}."
            )
        else:
            select_sql, args = await sqlh.get_select_sql(primitive_query, ids, reference_type)
            logger.debug(select_sql)

            args.extend([limit, skip])

            try:
                if len(args) == 0:
                    result = await self.conn.fetch(select_sql)
                else:
                    result = await self.conn.fetch(select_sql, *args)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Primitive could not be queried (select)") from e

        if result is not None:
            items = [r["json"] for r in result]
            if raw:
                return count, items
            if single:
                return JSONResponse(items[0])
            return JSONResponse({"item_count": count, "items": items})
        else:
            raise HTTPException(500, "Primitive could not be queried (result None).")

    async def unique_references_query(self, primitive_query: PrimitiveQuery, ids: List[UUID4] = None):
        if primitive_query.references is not None:
            primitive_query.references = list(set(primitive_query.references))

        references_sql, args = await sqlh.get_references_sql(primitive_query, ids)
        logger.debug(references_sql)

        try:
            if len(args) == 0:
                references_result = await self.conn.fetchrow(references_sql)
            else:
                references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitives could not be queried (primitive references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Primitives cound not be queried (unique references None).")

    async def update(self, primitive_id: UUID4, primitive: PostPrimitive):
        await utils.check_primitive(self.conn, primitive_id, "update", primitive)

        update_sql = await sqlh.get_update_sql(primitive.type)
        logger.debug(update_sql)

        try:
            await self.conn.execute(
                update_sql,
                primitive_id,
                primitive.name,
                primitive.description,
                primitive.creator_id,
                primitive.creator_type,
                primitive.reference_id,
                primitive.reference_type,
                primitive.type,
                primitive.value,
            )
            return await self.get(primitive_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive could not be updated") from e

    async def delete(self, primitive_id: UUID4):
        await utils.check_primitive(self.conn, primitive_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM p_primitives
            WHERE id = $1
            RETURNING *
        ) SELECT COUNT(*) FROM deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, primitive_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(primitive_id)})
        else:
            raise HTTPException(500, "Primitive could not be deleted (delete count 0)")
