from typing import List

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from pydantic import UUID4

from annotation_service.models.v1.annotation.classes import ClassQuery, PostClass, PostClasses
from annotation_service.singletons import logger, settings

from .helpers import class_sql_helper as sqlh
from .utils import annotation_utils as a_utils
from .utils import class_utils as utils


class ClassClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_classes(self, classes: PostClasses, external_ids: bool = False, raw: bool = False):
        if isinstance(classes, PostClass):
            return await self.create_new_classes([classes], external_ids, raw)
        elif len(classes.items) > 0:
            if len(classes.items) > settings.v1_post_limit:
                raise HTTPException(
                    413,
                    f"Number of posted items ({len(classes.items)}) exceeds server limit of {settings.v1_post_limit}.",
                )
            return await self.create_new_classes(classes.items, external_ids, raw)

        raise HTTPException(405, "No valid post data")

    async def create_new_classes(self, classes: List[PostClass], external_ids: bool = False, raw: bool = False):
        insert_tmptable_sql = await sqlh.get_insert_tmptable_sql(external_ids)
        insert_sql = await sqlh.get_insert_sql()
        logger.debug(insert_tmptable_sql)
        logger.debug(insert_sql)

        new_classes = []
        for c in classes:
            await a_utils.check_annotation(self.conn, c.reference_id)
            cl = [
                c.type,
                c.creator_id,
                c.creator_type,
                c.reference_id,
                c.reference_type,
                c.value,
                False,
            ]

            if settings.allow_external_ids and external_ids:
                cl.insert(0, c.id)

            new_classes.append(cl)

        try:
            async with self.conn.transaction():
                await self.conn.execute("CREATE TEMP TABLE IF NOT EXISTS tmptable_cl (LIKE cl_classes)")
                await self.conn.execute("TRUNCATE tmptable_cl")
                await self.conn.executemany(insert_tmptable_sql, new_classes)
                result = await self.conn.fetch(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Class with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Classes could not be created") from e

        if result is None:
            raise HTTPException(500, "Classes could not be created (result None)")

        items = [cl[0] for cl in result]

        if raw:
            return len(items), items
        if len(items) == 1:
            return JSONResponse(items[0], status_code=201)
        return JSONResponse({"item_count": len(items), "items": items}, status_code=201)

    async def get_all(self, with_unique_class_values: bool, skip: int, limit: int):
        return await self.query(ClassQuery(), with_unique_class_values, skip, limit)

    async def get(self, class_id: UUID4):
        result = await self.query(
            class_query=ClassQuery(classes=[str(class_id)]),
            with_unique_class_values=False,
            skip=None,
            limit=None,
            single=True,
        )
        if not result:
            raise HTTPException(404, f"No class with id '{class_id}' found")
        return result

    async def query(
        self,
        class_query: ClassQuery,
        with_unique_class_values: bool,
        skip: int,
        limit: int,
        single: bool = False,
        raw: bool = False,
    ):
        ids = []

        if class_query.jobs is not None:
            job_sql = """
            SELECT DISTINCT class_id
            FROM j_job_classes
            WHERE
            job_id = ANY($1) AND
            class_id != '';
            """

            try:
                id_results = await self.conn.fetch(job_sql, class_query.jobs)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Jobs could not be queried (in class query)") from e

            if len(id_results) == 0:
                if raw:
                    return 0, []
                if with_unique_class_values:
                    return JSONResponse({"item_count": 0, "items": [], "unique_class_values": []})
                else:
                    return JSONResponse({"item_count": 0, "items": []})

            ids = [str(id_result["class_id"]) for id_result in id_results]

        if class_query.classes:
            if len(ids) > 0:
                ids = list(set(ids) & set(class_query.classes))
            else:
                ids = class_query.classes

        count_sql, args = await sqlh.get_count_sql(class_query, ids)
        logger.debug(count_sql)

        try:
            if len(args) == 0:
                count = await self.conn.fetchval(count_sql)
            else:
                count = await self.conn.fetchval(count_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'classes' must be of type UUID4.") from e
            raise HTTPException(500, "Classes could not be queried (count)") from e

        if count == 0:
            if raw:
                return 0, []
            if single:
                return None
            if with_unique_class_values:
                return JSONResponse({"item_count": 0, "items": [], "unique_class_values": []})
            else:
                return JSONResponse({"item_count": 0, "items": []})
        elif count > settings.v1_item_limit and (limit is None or limit > settings.v1_item_limit):
            raise HTTPException(
                413, f"Count of requested items ({count}) exceeds server limit of {settings.v1_item_limit}."
            )
        else:
            select_sql, args = await sqlh.get_select_sql(class_query, ids)
            logger.debug(select_sql)

            args.extend([limit, skip])

            try:
                if len(args) == 0:
                    result = await self.conn.fetch(select_sql)
                else:
                    result = await self.conn.fetch(select_sql, *args)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Classes could not be queried (select)") from e

            if with_unique_class_values:
                values_sql, args = await sqlh.get_values_sql(class_query, ids)
                logger.debug(values_sql)

                try:
                    if len(args) == 0:
                        values_result = await self.conn.fetchrow(values_sql)
                    else:
                        values_result = await self.conn.fetchrow(values_sql, *args)
                except PostgresError as e:
                    logger.debug(e)
                    raise HTTPException(500, "Classes could not be queried (class values)") from e

                if values_result is not None:
                    unique_values = (
                        values_result["json"]["values"] if values_result["json"]["values"] is not None else []
                    )
                else:
                    raise HTTPException(500, "Classes cound not be queried (class values None).")

        if result is not None:
            items = [r["json"] for r in result]

            if raw:
                return count, items
            if single:
                return JSONResponse(items[0])
            if with_unique_class_values:
                return JSONResponse({"item_count": count, "items": items, "unique_class_values": unique_values})
            else:
                return JSONResponse({"item_count": count, "items": items})
        else:
            raise HTTPException(500, "Classes cound not be queried (result None).")

    async def unique_references_query(self, class_query: ClassQuery, ids: List[UUID4] = None):
        references_sql, args = await sqlh.get_references_sql(class_query, ids)
        logger.debug(references_sql)

        try:
            if len(args) == 0:
                references_result = await self.conn.fetchrow(references_sql)
            else:
                references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Classes could not be queried (class references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Classes cound not be queried (unique references None).")

    async def update(self, class_id: UUID4, put_class: PostClass):
        await utils.check_class(self.conn, class_id, "update", put_class)

        update_sql = await sqlh.get_update_sql()
        logger.debug(update_sql)

        try:
            await self.conn.execute(
                update_sql,
                class_id,
                put_class.creator_id,
                put_class.creator_type,
                put_class.reference_id,
                put_class.reference_type,
                put_class.value,
            )
            return await self.get(class_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class could not be updated") from e

    async def delete(self, class_id: UUID4):
        await utils.check_class(self.conn, class_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM cl_classes
            WHERE id = $1
            RETURNING *
        ) SELECT COUNT(*) FROM deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, class_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(class_id)})
        else:
            raise HTTPException(500, "Class could not be deleted (delete count 0)")
