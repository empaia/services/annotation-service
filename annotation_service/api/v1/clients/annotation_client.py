from typing import List

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse, Response
from pydantic import UUID4

from annotation_service.models.v1.annotation.annotations import (
    AnnotationQuery,
    AnnotationViewerQuery,
    PostAnnotation,
    PostAnnotations,
)
from annotation_service.singletons import logger, settings

from .helpers.annotations import sql_helper_get, sql_helper_insert, sql_helper_query
from .utils import annotation_utils as utils


class AnnotationClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_annotations(self, annotations: PostAnnotations, external_ids: bool = False, raw: bool = False):
        if isinstance(annotations, PostAnnotation.__args__):
            return await self.create_annotations([annotations], external_ids, raw)
        elif len(annotations.items) > 0:
            if len(annotations.items) > settings.v1_post_limit:
                raise HTTPException(
                    413,
                    (
                        f"Number of posted items ({len(annotations.items)})"
                        f" exceeds server limit of {settings.v1_post_limit}."
                    ),
                )
            return await self.create_annotations(annotations.items, external_ids, raw)

        raise HTTPException(405, "No valid post data")

    async def create_annotations(
        self, annotations: List[PostAnnotation], external_ids: bool = False, raw: bool = False
    ):
        insert_tmptable_sql = await sql_helper_insert.get_insert_tmptable_sql(annotations[0].type, external_ids)
        insert_sql = await sql_helper_insert.get_insert_sql(annotations[0].type)
        logger.debug(insert_tmptable_sql)
        logger.debug(insert_sql)

        new_annotations = await utils.get_formated_insert_data(annotations, external_ids)

        try:
            async with self.conn.transaction():
                await self.conn.execute("CREATE TEMP TABLE IF NOT EXISTS tmptable_annot (LIKE a_annotations)")
                await self.conn.execute("TRUNCATE tmptable_annot")
                await self.conn.executemany(insert_tmptable_sql, new_annotations)
                result = await self.conn.fetch(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Annotation with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be created") from e

        if result is None:
            raise HTTPException(500, "Annotations could not be created (result None)")

        items = [a[0] for a in result]

        if raw:
            return len(items), items
        if len(items) == 1:
            return JSONResponse(items[0], status_code=201)
        return JSONResponse({"item_count": len(items), "items": items}, status_code=201)

    async def get_all(self, with_classes: bool, skip, limit):
        sql = await sql_helper_get.get_all_sql(with_classes)
        logger.debug(sql)

        args = [limit, skip]

        try:
            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'annotations' must be of type UUID4.") from e
            raise HTTPException(500, "Annotations could not be queried (get all)") from e

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def get(self, annot_id: UUID4, with_classes: bool, raw: bool = False):
        sql = await sql_helper_get.get_single_sql(with_classes)
        logger.debug(sql)

        try:
            result = await self.conn.fetchrow(sql, annot_id)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'annotations' must be of type UUID4.") from e
            raise HTTPException(500, "Annotations could not be queried (get)") from e

        if not result:
            raise HTTPException(404, f"No annotation with id '{annot_id}' found")
        return result["annotation"]

    async def query(
        self,
        annotation_query: AnnotationQuery,
        with_classes: bool,
        with_low_npp_centroids: bool,
        skip: int,
        limit: int,
        raw: bool = False,
    ):
        if limit is not None and limit > settings.v1_item_limit:
            raise HTTPException(413, f"Provided limit ({limit}) exceeds server limit of {settings.v1_item_limit}.")

        if with_low_npp_centroids and annotation_query.npp_viewing is None:
            raise HTTPException(
                400, "Key npp_viewing must be specified when query parameter with_low_npp_centroids set to true!"
            )

        if annotation_query.class_values is not None:
            annotation_query.class_values = list(set(annotation_query.class_values))
            annotation_query.class_values = [
                class_value if class_value is not None else "" for class_value in annotation_query.class_values
            ]

        if annotation_query.jobs is not None:
            annotation_query.jobs = list(set(annotation_query.jobs))

        sql, args = await sql_helper_query.get_query_sql(annotation_query, with_classes, with_low_npp_centroids, raw)
        logger.debug(sql)

        args.extend([limit, skip])

        try:
            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'annotations' must be of type UUID4.") from e
            raise HTTPException(500, "Annotations could not be queried (query)") from e

        if raw:
            if result is None:
                return 0, []
            return result["item_count"], result["items"]

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def query_position(self, annot_id: UUID4, annotation_query: AnnotationQuery):
        await utils.check_annotation(self.conn, annot_id)

        if annotation_query.class_values is not None:
            annotation_query.class_values = list(set(annotation_query.class_values))
            if None in annotation_query.class_values and annotation_query.jobs is not None:
                annotation_query.class_values = [
                    class_value if class_value is not None else "" for class_value in annotation_query.class_values
                ]

        position_sql, args = await sql_helper_query.get_position_sql(annotation_query)
        logger.debug(position_sql)

        args.append(annot_id)

        try:
            if len(args) == 1:
                result = await self.conn.fetchrow(position_sql, annot_id)
            else:
                result = await self.conn.fetchrow(position_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation position could not be queried") from e

        if result is None:
            raise HTTPException(500, "Annotation position could not be queried (result None)")

        return JSONResponse({"id": str(result["id"]), "position": result["position"] - 1})

    async def count_query(self, annotation_query: AnnotationQuery):
        if annotation_query.class_values is not None:
            annotation_query.class_values = list(set(annotation_query.class_values))
            annotation_query.class_values = [
                class_value if class_value is not None else "" for class_value in annotation_query.class_values
            ]

        if annotation_query.jobs is not None:
            annotation_query.jobs = list(set(annotation_query.jobs))

        count_sql, args = await sql_helper_query.get_count_sql(annotation_query)
        logger.debug(count_sql)

        try:
            if len(args) == 0:
                count_result = await self.conn.fetchrow(count_sql)
            else:
                count_result = await self.conn.fetchrow(count_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (count)") from e

        if count_result is not None:
            return JSONResponse(count_result["count"])
        else:
            raise HTTPException(500, "Annotations cound not be queried (count None).")

    async def unique_classes_query(self, annotation_query: AnnotationQuery):
        if annotation_query.jobs is not None:
            annotation_query.jobs = list(set(annotation_query.jobs))

        values_sql, args = await sql_helper_query.get_values_sql(annotation_query)
        logger.debug(values_sql)

        try:
            if len(args) == 0:
                values_result = await self.conn.fetchrow(values_sql)
            else:
                values_result = await self.conn.fetchrow(values_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (class values)") from e

        if values_result is not None:
            unique_class_values = values_result["json"]["values"] if values_result["json"]["values"] is not None else []
            result_dict = dict()
            result_dict["unique_class_values"] = unique_class_values
            return JSONResponse(result_dict)
        else:
            raise HTTPException(500, "Annotations cound not be queried (class values None).")

    async def unique_references_query(self, annotation_query: AnnotationQuery):
        if annotation_query.class_values is not None:
            annotation_query.class_values = list(set(annotation_query.class_values))
            annotation_query.class_values = [
                class_value if class_value is not None else "" for class_value in annotation_query.class_values
            ]

        if annotation_query.jobs is not None:
            annotation_query.jobs = list(set(annotation_query.jobs))

        references_sql, args = await sql_helper_query.get_references_sql(annotation_query)
        logger.debug(references_sql)

        try:
            if len(args) == 0:
                references_result = await self.conn.fetchrow(references_sql)
            else:
                references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (annotation references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Annotations cound not be queried (unique references None).")

    async def viewer_query(self, annotation_viewer_query: AnnotationViewerQuery):
        if annotation_viewer_query.npp_viewing is None:
            raise HTTPException(400, "Key npp_viewing must be specified!")

        if annotation_viewer_query.class_values is not None:
            annotation_viewer_query.class_values = list(set(annotation_viewer_query.class_values))
            annotation_viewer_query.class_values = [
                class_value if class_value is not None else "" for class_value in annotation_viewer_query.class_values
            ]

        if annotation_viewer_query.jobs is not None:
            annotation_viewer_query.jobs = list(set(annotation_viewer_query.jobs))

        viewer_sql, args = await sql_helper_query.get_viewer_sql(annotation_viewer_query)
        logger.debug(viewer_sql)

        try:
            viewer_result = await self.conn.fetch(viewer_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (viewer query)") from e

        if viewer_result is not None:
            result_dict = dict()
            result_dict["annotations"] = (
                viewer_result[0]["annotations"] if viewer_result[0]["annotations"] is not None else []
            )
            result_dict["low_npp_centroids"] = (
                viewer_result[0]["centroids"] if viewer_result[0]["centroids"] is not None else []
            )
            return JSONResponse(result_dict)
        else:
            raise HTTPException(500, "Annotations cound not be queried (class values None).")

    async def delete(self, annotation_id: UUID4):
        await utils.check_annotation(self.conn, annotation_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM
                a_annotations
            WHERE
                id = $1 RETURNING *
        )
        SELECT
            COUNT(*)
        FROM
            deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, annotation_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(annotation_id)})
        else:
            raise HTTPException(500, "Annotation could not be deleted (delete count 0)")
