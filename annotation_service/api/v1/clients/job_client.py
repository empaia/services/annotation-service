from typing import List

from asyncpg.exceptions import PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from pydantic import UUID4

from annotation_service.models.v1.annotation.collections import ItemQuery, SlideList
from annotation_service.models.v1.annotation.jobs import NodeType, PrimitiveDetails, ReferenceData
from annotation_service.models.v1.annotation.primitives import PrimitiveQuery
from annotation_service.models.v1.commons import Id
from annotation_service.singletons import logger, settings

from .helpers import job_sql_helper as sqlh
from .utils import annotation_utils as a_utils
from .utils import class_utils as cl_utils
from .utils import collection_utils as col_utils
from .utils import job_utils as utils
from .utils import primitive_utils as p_utils


class JobClient:
    def __init__(self, conn):
        self.conn = conn

    async def lock_annotation(self, job_id: Id, annotation_id: UUID4):
        await a_utils.check_annotation(self.conn, annotation_id)

        set_locked_sql = """
        UPDATE a_annotations
            SET is_locked = True
        WHERE id = $1;
        """

        job_lock_sql = """
        INSERT INTO j_job_annotations(
            job_id,
            annotation_id
        )
        VALUES ($1, $2);
        """

        class_count_sql = """
        SELECT count(annot_id)
        FROM j_job_classes
        WHERE job_id = $1
        AND annot_id = $2
        AND class_id <> '';
        """

        class_job_lock_sql = """
        INSERT INTO j_job_classes(
            job_id,
            class_id,
            annot_id,
            value
        )
        VALUES ($1, '', $2, '');
        """

        logger.debug(set_locked_sql)
        logger.debug(job_lock_sql)
        logger.debug(class_count_sql)
        logger.debug(class_job_lock_sql)

        try:
            async with self.conn.transaction():
                await self.conn.execute(set_locked_sql, annotation_id)
                await self.conn.execute(job_lock_sql, job_id, annotation_id)
                class_count = await self.conn.fetchval(class_count_sql, job_id, annotation_id)
                if class_count == 0:
                    await self.conn.execute(class_job_lock_sql, job_id, annotation_id)
                return JSONResponse({"item_id": str(annotation_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(annotation_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation can not be locked") from e

    async def lock_class(self, job_id: Id, class_id: UUID4):
        await cl_utils.check_class(self.conn, class_id)

        set_locked_sql = """
        UPDATE cl_classes
            SET is_locked = True
        WHERE id = $1;
        """

        delete_old_lock_sql = """
        DELETE FROM j_job_classes
        WHERE
        job_id = $1 AND
        class_id = '' AND
        annot_id =
        (
            SELECT reference_id
            FROM cl_classes
            WHERE id = $2
        ) AND
        value = '';
        """

        job_lock_sql = """
        INSERT INTO j_job_classes(job_id, class_id, annot_id, value)
        (
            SELECT $1, id, reference_id, value
            FROM cl_classes
            WHERE id = $2
        );
        """

        try:
            async with self.conn.transaction():
                await self.conn.execute(set_locked_sql, class_id)
                await self.conn.execute(delete_old_lock_sql, job_id, class_id)
                await self.conn.execute(job_lock_sql, job_id, class_id)
                return JSONResponse({"item_id": str(class_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(class_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class can not be locked") from e

    async def lock_primitive(self, job_id: Id, primitive_id: UUID4):
        await p_utils.check_primitive(self.conn, primitive_id)

        set_locked_sql = """
        UPDATE p_primitives
            SET is_locked = True
        WHERE id = $1;
        """

        job_lock_sql = """
        INSERT INTO j_job_primitives(
            job_id,
            primitive_id
        )
        VALUES ($1, $2);
        """

        logger.debug(set_locked_sql)
        logger.debug(job_lock_sql)

        try:
            async with self.conn.transaction():
                await self.conn.execute(set_locked_sql, primitive_id)
                await self.conn.execute(job_lock_sql, job_id, primitive_id)
                return JSONResponse({"item_id": str(primitive_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(primitive_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive can not be locked") from e

    async def lock_slide(self, job_id: Id, slide_id: Id):
        job_lock_sql = """
        INSERT INTO j_job_slides(
            job_id,
            slide_id
        )
        VALUES ($1, $2);
        """

        logger.debug(job_lock_sql)

        try:
            await self.conn.execute(job_lock_sql, job_id, slide_id)
            return JSONResponse({"item_id": str(slide_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(slide_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Slide can not be locked") from e

    async def lock_annotations(self, job_id: Id, annotation_ids: List[UUID4]):
        if not await a_utils.check_if_annotations_exist(self.conn, annotation_ids):
            raise HTTPException(404, "Annotation not found")

        lock_sql = """
        UPDATE a_annotations
            SET is_locked = True
        WHERE id = ANY($1);
        """

        check_sql = """
        SELECT annot_id
        FROM j_job_classes
        WHERE job_id = $1;
        """

        job_sql = """
        INSERT INTO j_job_annotations(
            job_id,
            annotation_id
        )
        VALUES ($1, $2);
        """

        job_classes_sql = """
        INSERT INTO j_job_classes(
            job_id,
            class_id,
            annot_id,
            value
        )
        VALUES ($1, $2, $3, $4);
        """

        job_data = [(job_id, annot_id) for annot_id in annotation_ids]

        try:
            async with self.conn.transaction():
                await self.conn.execute(lock_sql, annotation_ids)
                annots = await self.conn.fetch(check_sql, job_id)
                excluded_annot_ids = [str(a["annot_id"]) for a in annots]
                final_annot_ids = list(set(annotation_ids).difference(excluded_annot_ids))
                job_classes_data = [(job_id, "", annot_id, "") for annot_id in final_annot_ids]
                await self.conn.executemany(job_sql, job_data)
                await self.conn.executemany(job_classes_sql, job_classes_data)
        except UniqueViolationError as e:
            raise HTTPException(400, f"At least one annotation is already locked for job '{job_id}'") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations can not be locked (collection lock)") from e

    async def lock_classes(self, job_id: Id, class_ids: List[UUID4]):
        if not await cl_utils.check_if_classes_exist(self.conn, class_ids):
            raise HTTPException(404, "Class not found")

        lock_sql = """
        UPDATE cl_classes
            SET is_locked = True
        WHERE id = ANY($1);
        """

        delete_sql = """
        DELETE FROM j_job_classes
        WHERE
        job_id = $1 AND
        class_id = '' AND
        annot_id IN
        (
            SELECT reference_id
            FROM cl_classes
            WHERE id = ANY($2)
        )
        AND value = '';
        """

        job_sql = """
        INSERT INTO j_job_classes(job_id, class_id, annot_id, value)
        (
            SELECT '{job_id}', id, reference_id, value
            FROM cl_classes
            WHERE id = ANY($1)
        );
        """.format(
            job_id=job_id
        )

        try:
            async with self.conn.transaction():
                await self.conn.execute(lock_sql, class_ids)
                await self.conn.execute(delete_sql, job_id, class_ids)
                await self.conn.execute(job_sql, class_ids)
        except UniqueViolationError as e:
            raise HTTPException(400, f"At least one class is already locked for job '{job_id}'") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Classes can not be locked (collection lock)") from e

    async def lock_collections(self, job_id: Id, collection_ids: List[UUID4]):
        if not await col_utils.check_if_collections_exist(self.conn, collection_ids):
            raise HTTPException(404, "Collection not found")

        set_locked_sql = """
        UPDATE c_collections
            SET is_locked = True
        WHERE id = ANY($1);
        """

        job_lock_sql = """
        INSERT INTO j_job_collections(
            job_id,
            collection_id
        )
        VALUES ($1, $2);
        """

        job_data = [(job_id, collection_id) for collection_id in collection_ids]

        try:
            async with self.conn.transaction():
                await self.conn.execute(set_locked_sql, collection_ids)
                await self.conn.executemany(job_lock_sql, job_data)
        except UniqueViolationError as e:
            raise HTTPException(400, f"At least one collection is already locked for job '{job_id}'") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections can not be locked (collection lock)") from e

    async def lock_primitives(self, job_id: Id, primitive_ids: List[UUID4]):
        if not await p_utils.check_if_primitives_exist(self.conn, primitive_ids):
            raise HTTPException(404, "Primitive not found")

        set_locked_sql = """
        UPDATE p_primitives
            SET is_locked = True
        WHERE id = ANY($1);
        """

        job_lock_sql = """
        INSERT INTO j_job_primitives(
            job_id,
            primitive_id
        )
        VALUES ($1, $2);
        """

        job_data = [(job_id, primitive_id) for primitive_id in primitive_ids]

        try:
            async with self.conn.transaction():
                await self.conn.execute(set_locked_sql, primitive_ids)
                await self.conn.executemany(job_lock_sql, job_data)
        except UniqueViolationError as e:
            raise HTTPException(400, f"At least one primitive is already locked for job '{job_id}'") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitives can not be locked (collection lock)") from e

    async def lock_slides(self, job_id: Id, slide_ids: List[Id]):
        job_lock_sql = """
        INSERT INTO j_job_slides(
            job_id,
            slide_id
        )
        VALUES ($1, $2);
        """

        job_data = [(job_id, slide_id) for slide_id in slide_ids]

        try:
            await self.conn.executemany(job_lock_sql, job_data)
        except UniqueViolationError as e:
            raise HTTPException(400, f"At least one WSI is already locked for job '{job_id}'") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "WSIs can not be locked  (collection lock)") from e

    async def lock_collection(self, job_id: Id, collection_id: UUID4):
        async with self.conn.transaction():
            await col_utils.check_collection(self.conn, collection_id)

            item_ids = await utils.get_all_item_ids(self.conn, collection_id)

            try:
                if len(item_ids["annotation_ids"]) > 0:
                    await self.lock_annotations(job_id, item_ids["annotation_ids"])
                if len(item_ids["class_ids"]) > 0:
                    await self.lock_classes(job_id, item_ids["class_ids"])
                if len(item_ids["collection_ids"]) > 0:
                    await self.lock_collections(job_id, item_ids["collection_ids"])
                if len(item_ids["primitive_ids"]) > 0:
                    await self.lock_primitives(job_id, item_ids["primitive_ids"])
                if len(item_ids["slide_ids"]) > 0:
                    await self.lock_slides(job_id, item_ids["slide_ids"])
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Collection can not be locked") from e

            return JSONResponse({"item_id": str(collection_id), "job_id": str(job_id)})

    async def get_tree_primitives(self, clients, job_id: Id, skip: int = None, limit: int = None):
        return await clients.primitive.query(
            primitive_query=PrimitiveQuery(jobs=[job_id], references=[None]),
            skip=skip,
            limit=limit,
            reference_type=True,
        )

    async def get_tree_primitive_details(self, clients, job_id: Id, primitive_id: UUID4):
        count, primitive = await clients.primitive.query(
            primitive_query=PrimitiveQuery(jobs=[job_id], primitives=[str(primitive_id)]),
            skip=None,
            limit=None,
            single=True,
            raw=True,
        )

        if count == 0:
            raise HTTPException(404, "Primitive not found")

        if primitive[0]["reference_type"] is None:
            return PrimitiveDetails(reference_type=None, reference_data=None)
        elif primitive[0]["reference_type"] == "collection":
            item_count, items = await clients.collection.query_items(
                primitive[0]["reference_id"], item_query=ItemQuery(), clients=clients, skip=None, limit=None, raw=True
            )
            return PrimitiveDetails(
                reference_type="collection", reference_data=ReferenceData(item_count=item_count, items=items)
            )

    async def get_tree_items(self, clients, job_id: Id, skip: int = None, limit: int = None):
        count_sql = """
        SELECT COUNT(*)
        FROM j_job_slides
        WHERE job_id = $1;
        """

        try:
            count = await self.conn.fetchval(count_sql, job_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Tree items could not be queried (count)") from e

        if count == 0:
            return SlideList(item_count=0, items=[])

        if count > settings.v1_item_limit and (limit is None or limit > settings.v1_item_limit):
            raise HTTPException(
                413, f"Count of requested items ({count['count']}) exceeds server limit of {settings.v1_item_limit}."
            )

        if count == 0:
            return SlideList(item_count=0, items=[])

        select_sql = """
        SELECT slide_id AS id
        FROM j_job_slides
        WHERE job_id = $1
        ORDER BY slide_id ASC
        LIMIT $2 OFFSET $3;
        """

        logger.debug(select_sql)

        try:
            result = await self.conn.fetch(select_sql, job_id, limit, skip)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Tree items could not be queried (select)") from e

        items = [{"id": r["id"], "type": "wsi"} for r in result]
        return SlideList(item_count=count, items=items)

    async def get_tree_node_primitives(self, clients, job_id: Id, node_id: Id, skip: int = None, limit: int = None):
        return await clients.primitive.query(
            primitive_query=PrimitiveQuery(jobs=[job_id], references=[node_id]), skip=skip, limit=limit
        )

    async def get_tree_node_items(
        self,
        clients,
        job_id: Id,
        node_type: NodeType,
        node_id: Id,
        skip: int = None,
        limit: int = None,
    ):
        count = 0
        items = []
        count_sql = await sqlh.get_tree_node_count_sql(node_type)
        logger.debug(count_sql)

        try:
            count = await self.conn.fetchval(count_sql, node_id, job_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Tree node items could not be queried (count)") from e

        if count > settings.v1_item_limit and (limit is None or limit > settings.v1_item_limit):
            raise HTTPException(
                413, f"Count of requested items ({count}) exceeds server limit of {settings.v1_item_limit}."
            )

        if count > 0:
            tree_node_sql = await sqlh.get_tree_node_sql(node_type)
            logger.debug(tree_node_sql)

            try:
                result = await self.conn.fetch(tree_node_sql, node_id, job_id, limit, skip)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Tree node items could not be queried (select)") from e

            if result is None:
                raise HTTPException(500, "Tree node items cound not be queried (result None).")

            if len(result) > 0:
                items = [r["json"] for r in result]

        return JSONResponse({"item_count": count, "items": items})

    async def get_tree_item_sequence(self, clients, job_id: Id, item_id: UUID4):
        await a_utils.check_annotation(self.conn, item_id)
        if not await a_utils.check_if_locked_for_job(self.conn, item_id, job_id):
            raise HTTPException(404, f"Item '{item_id}' not locked for job '{job_id}'")

        sequence_items = []
        annot = await clients.annotation.get(annot_id=item_id, with_classes=False, raw=True)
        a_reference_id = annot["reference_id"]
        a_reference_type = annot["reference_type"]

        collection_id = await a_utils.get_collection_id(self.conn, item_id)
        if collection_id:
            if not await col_utils.check_if_locked_for_job(self.conn, collection_id, job_id):
                raise HTTPException(404, f"Collection '{collection_id}' not locked for job '{job_id}'")
            collection = await clients.collection.get(
                collection_id=collection_id, shallow=True, with_leaf_ids=False, clients=clients, raw=True
            )
            c_reference_id = collection["reference_id"]
            c_reference_type = collection["reference_type"]
            if c_reference_id:
                if c_reference_id != a_reference_id and c_reference_type == NodeType.ANNOTATION:
                    item_details = await self._get_position(
                        c_reference_type, c_reference_id, job_id, item_id, "annotation"
                    )
                    parent = await clients.annotation.get(annot_id=c_reference_id, with_classes=False, raw=True)
                    p_reference_id = parent["reference_id"]
                    p_reference_type = parent["reference_type"]
                    parent_details = await self._get_position(
                        p_reference_type, p_reference_id, job_id, c_reference_id, "annotation"
                    )
                    slide_details = await self._get_slide_position(job_id, p_reference_id)

                    sequence_items.append(slide_details)
                    sequence_items.append(parent_details)
                    sequence_items.append(item_details)
                    return JSONResponse({"node_sequence": sequence_items})

        item_details = await self._get_position(a_reference_type, a_reference_id, job_id, item_id, "annotation")
        slide_details = await self._get_slide_position(job_id, a_reference_id)

        sequence_items.append(slide_details)
        sequence_items.append(item_details)
        return JSONResponse({"node_sequence": sequence_items})

    async def _get_position(self, node_type: NodeType, node_id, job_id, item_id, item_type):
        get_pos_sql = await sqlh.get_tree_node_position_sql(node_type)
        logger.debug(get_pos_sql)

        try:
            result = await self.conn.fetchrow(get_pos_sql, node_id, job_id, item_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Item position could not be queried (select)") from e

        if result is None:
            raise HTTPException(500, f"Position for item '{item_id}' could not be queried")
        return {"node_id": str(item_id), "node_type": item_type, "position": result["position"] - 1}

    async def _get_slide_position(self, job_id, slide_id):
        sql = """
        SELECT id, position
        FROM
        (
            SELECT slide_id AS id,
            row_number() over(
                ORDER BY slide_id ASC
            ) AS position
            FROM j_job_slides
            WHERE job_id = $1
            ORDER BY slide_id ASC
        ) AS id_position
        WHERE id = $2;
        """

        try:
            result = await self.conn.fetchrow(sql, job_id, slide_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Item position could not be queried (select)") from e

        if result is None:
            raise HTTPException(500, f"Position for slide '{slide_id}' could not be queried")
        return {"node_id": str(slide_id), "node_type": NodeType.WSI, "position": result["position"] - 1}
