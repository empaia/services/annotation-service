from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v1.annotation.collections import CollectionItemType
from annotation_service.singletons import logger

from . import collection_utils as col_utils


async def get_all_item_ids(conn, collection_id: UUID4):
    annotation_ids = []
    class_ids = []
    collection_ids = [str(collection_id)]
    primitive_ids = []
    slide_ids = []

    item_ids_sql = """
    SELECT item_id
    FROM c_collection_items
    WHERE collection_id = $1;
    """

    try:
        items = await conn.fetch(item_ids_sql, collection_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Items cound not be retrieved (collection lock)") from e

    item_ids = [item["item_id"] for item in items]

    if len(item_ids) > 0:
        item_type = await col_utils.get_item_type(conn, collection_id)
        if (
            item_type == CollectionItemType.POINT
            or item_type == CollectionItemType.LINE
            or item_type == CollectionItemType.ARROW
            or item_type == CollectionItemType.CIRCLE
            or item_type == CollectionItemType.RECTANGLE
            or item_type == CollectionItemType.POLYGON
        ):
            annotation_ids.extend(item_ids)
        elif (
            item_type == CollectionItemType.INTEGER
            or item_type == CollectionItemType.FLOAT
            or item_type == CollectionItemType.BOOL
            or item_type == CollectionItemType.STRING
        ):
            primitive_ids.extend(item_ids)
        elif item_type == CollectionItemType.CLASS:
            class_ids.extend(item_ids)
        elif item_type == CollectionItemType.WSI:
            slide_ids.extend(item_ids)
        elif item_type == CollectionItemType.COLLECTION:
            for item_id in item_ids:
                child_item_ids = await get_all_item_ids(conn, item_id)
                annotation_ids.extend(child_item_ids["annotation_ids"])
                class_ids.extend(child_item_ids["class_ids"])
                collection_ids.extend(child_item_ids["collection_ids"])
                primitive_ids.extend(child_item_ids["primitive_ids"])
                slide_ids.extend(child_item_ids["slide_ids"])

    return {
        "annotation_ids": annotation_ids,
        "class_ids": class_ids,
        "collection_ids": collection_ids,
        "primitive_ids": primitive_ids,
        "slide_ids": slide_ids,
    }
