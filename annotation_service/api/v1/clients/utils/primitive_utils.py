from typing import List

from asyncpg.exceptions import DataError, PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v1.annotation.primitives import PostPrimitive
from annotation_service.singletons import logger


async def check_if_primitives_exist(conn, primitive_ids: List[UUID4]):
    if len(primitive_ids) == 0:
        return False

    sql = """
    SELECT COUNT(*)
    FROM p_primitives
    WHERE id = ANY($1);
    """

    try:
        return await conn.fetchval(sql, primitive_ids) == len(primitive_ids)
    except DataError as e:
        logger.debug(e)
        raise HTTPException(422, e.args) from e
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking primitives exist") from e


async def check_primitive(conn, primitive_id: UUID4, mode: str = None, primitive: PostPrimitive = None):
    if not await _check_if_primitive_exists(conn, primitive_id):
        raise HTTPException(404, "Primitive not found")
    if mode == "lock" or mode == "update":
        if await _check_if_locked(conn, primitive_id):
            raise HTTPException(423, "Primitive is locked")
    if mode == "update":
        sql = """
        SELECT *
        FROM p_primitives
        WHERE id = $1;
        """

        try:
            p = await conn.fetchrow(sql, primitive_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Error while checking primitive") from e

        if p["type"] != primitive.type:
            raise HTTPException(405, "Primitive type not matching")
        if p["reference_id"] != primitive.reference_id:
            raise HTTPException(405, "Reference id not matching")
        if p["reference_type"] != primitive.reference_type:
            raise HTTPException(405, "Reference type not matching")
        if p["creator_id"] != primitive.creator_id:
            raise HTTPException(405, "Creator id not matching")
        if p["creator_type"] != primitive.creator_type:
            raise HTTPException(405, "Creator type not matching")


async def _check_if_primitive_exists(conn, primitive_id: UUID4):
    sql = """
    SELECT COUNT(1)
    FROM p_primitives
    WHERE id = $1;
    """

    try:
        return await conn.fetchval(sql, primitive_id) == 1
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking primitive exists") from e


async def _check_if_locked(conn, primitive_id: UUID4):
    sql = """
    SELECT is_locked
    FROM p_primitives
    WHERE id = $1;
    """

    try:
        return await conn.fetchval(sql, primitive_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking primitive locked") from e
