from typing import List

from asyncpg.exceptions import DataError, PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v1.annotation.annotations import AnnotationType, PostAnnotation
from annotation_service.models.v1.commons import Id
from annotation_service.singletons import logger, settings

from ..helpers import geometry_helper as geoh


async def check_if_annotations_exist(conn, annot_ids: List[UUID4]):
    if len(annot_ids) == 0:
        return False

    sql = """
    SELECT COUNT(*)
    FROM a_annotations
    WHERE id = ANY($1);
    """

    try:
        return await conn.fetchval(sql, annot_ids) == len(annot_ids)
    except DataError as e:
        logger.debug(e)
        raise HTTPException(422, e.args) from e
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotations exist") from e


async def check_annotation(conn, annotation_id: UUID4, mode: str = None, annotation: PostAnnotation = None):
    if not await _check_if_annotation_exists(conn, annotation_id):
        raise HTTPException(404, "Annotation not found")
    if mode == "lock" or mode == "update":
        if await _check_if_locked(conn, annotation_id):
            raise HTTPException(423, "Annotation is locked")
    if mode == "update":
        sql = """
        SELECT *
        FROM a_annotations
        WHERE id = $1;
        """

        try:
            a = await conn.fetchrow(sql, annotation_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Error while checking annotation") from e

        if a["type"] != annotation.type:
            raise HTTPException(405, "Annotation type not matching")
        if a["reference_id"] != annotation.reference_id:
            raise HTTPException(405, "Reference id not matching")
        if a["reference_type"] != annotation.reference_type:
            raise HTTPException(405, "Reference type not matching")
        if a["creator_id"] != annotation.creator_id:
            raise HTTPException(405, "Creator id not matching")
        if a["creator_type"] != annotation.creator_type:
            raise HTTPException(405, "Creator type not matching")


async def _check_if_annotation_exists(conn, annotation_id: UUID4):
    sql = """
    SELECT COUNT(1)
    FROM a_annotations
    WHERE id = $1;
    """

    try:
        return await conn.fetchval(sql, annotation_id) == 1
    except DataError as e:
        logger.debug(e)
        raise HTTPException(422, e.args) from e
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotation exists") from e


async def _check_if_locked(conn, annotation_id: UUID4):
    sql = """
    SELECT is_locked
    FROM a_annotations
    WHERE id = $1;
    """

    try:
        return await conn.fetchval(sql, annotation_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotation locked") from e


async def check_if_locked_for_job(conn, annotation_id: UUID4, job_id: Id):
    sql = """
    SELECT COUNT(*)
    FROM j_job_annotations
    WHERE annotation_id = $1 AND job_id = $2;
    """

    try:
        return await conn.fetchval(sql, annotation_id, job_id) == 1
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotation locked for job") from e


async def get_collection_id(conn, annotation_id: UUID4):
    sql = """
    SELECT collection_id
    FROM c_collection_items
    WHERE item_id = $1;
    """

    try:
        return await conn.fetchval(sql, str(annotation_id))
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotation in collection") from e


async def get_formated_insert_data(annotations: List[PostAnnotation], external_ids: bool):
    annot_type = annotations[0].type
    new_annotations = []
    for a in annotations:
        if a.centroid is None:
            a.centroid = await geoh.get_centroid(a)

        new_annotation = [
            a.type,
            a.name,
            a.description,
            a.creator_id,
            a.creator_type,
            a.reference_id,
            a.reference_type,
            a.npp_created,
            a.npp_viewing[0] if a.npp_viewing is not None else None,
            a.npp_viewing[1] if a.npp_viewing is not None else None,
            a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
            a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
            False,
        ]

        if (
            annot_type == AnnotationType.POINT
            or annot_type == AnnotationType.LINE
            or annot_type == AnnotationType.POLYGON
        ):
            new_annotation.append(a.coordinates)
        elif annot_type == AnnotationType.ARROW:
            new_annotation.extend([a.head, a.tail])
        elif annot_type == AnnotationType.CIRCLE:
            new_annotation.extend([a.center, a.radius])
        elif annot_type == AnnotationType.RECTANGLE:
            new_annotation.extend([a.upper_left, a.width, a.height])

        new_annotation.extend([a.centroid, await geoh.get_bounding_box(a)])

        if settings.allow_external_ids and external_ids:
            new_annotation.insert(0, a.id)
        new_annotations.append(new_annotation)

    return new_annotations
