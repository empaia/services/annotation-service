from typing import Any, List

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from pydantic import UUID4

from annotation_service.models.v1.annotation.annotations import AnnotationQuery, PostAnnotationList
from annotation_service.models.v1.annotation.classes import ClassQuery, PostClassList
from annotation_service.models.v1.annotation.collections import (
    CollectionItemType,
    CollectionQuery,
    CollectionReferenceType,
    ItemQuery,
    PostCollection,
    PostItem,
    PostItemList,
    PostItems,
)
from annotation_service.models.v1.annotation.primitives import PostPrimitiveList, PrimitiveQuery
from annotation_service.models.v1.commons import Id, IdObject
from annotation_service.singletons import logger, settings

from .helpers import collection_sql_helper as sqlh
from .utils import annotation_utils as autils
from .utils import collection_utils as utils


class CollectionClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_collection(self, collection: PostCollection, clients, external_ids: bool = False, raw: bool = False):
        async with self.conn.transaction():
            if isinstance(collection, PostCollection):
                return await self.create_filled_collection(collection, clients, external_ids, raw=raw)
            else:
                items = []
                for c in collection.items:
                    items.append(await self.create_filled_collection(c, clients, external_ids, raw=raw))
                return len(items), items

    async def create_filled_collection(
        self, collection: PostCollection, clients, external_ids: bool = False, raw: bool = False
    ):
        item_count = len(collection.items) if collection.items is not None else 0
        if item_count == 0:
            return await self._create_collection(collection, external_ids, raw=raw)

        item_type = collection.item_type
        first_item = collection.items[0]
        if not isinstance(first_item, IdObject):
            if not await utils.check_item_type_post_put(first_item, item_type):
                raise HTTPException(405, "Item type and type of items not matching")

            leaf_item_count = await utils.get_leaf_item_count(collection)

            if leaf_item_count > settings.v1_post_limit:
                raise HTTPException(
                    413,
                    f"Number of posted items ({leaf_item_count}) exceeds server limit of {settings.v1_post_limit}.",
                )

            new_collection = await self._create_collection(collection, external_ids, raw=True)
            collection_id = new_collection["id"]
            if (
                item_type is CollectionItemType.INTEGER
                or item_type is CollectionItemType.FLOAT
                or item_type is CollectionItemType.BOOL
                or item_type is CollectionItemType.STRING
            ):
                post_items = PostPrimitiveList(items=collection.items)
                i_count, items = await clients.primitive.add_primitives(post_items, external_ids=external_ids, raw=True)
                count = await self.set_items(collection_id, items)
            elif (
                item_type is CollectionItemType.POINT
                or item_type is CollectionItemType.LINE
                or item_type is CollectionItemType.ARROW
                or item_type is CollectionItemType.CIRCLE
                or item_type is CollectionItemType.RECTANGLE
                or item_type is CollectionItemType.POLYGON
            ):
                post_items = PostAnnotationList(items=collection.items)
                i_count, items = await clients.annotation.add_annotations(
                    post_items, external_ids=external_ids, raw=True
                )
                count = await self.set_items(collection_id, items)
            elif item_type is CollectionItemType.CLASS:
                post_items = PostClassList(items=collection.items)
                i_count, items = await clients.aclass.add_classes(post_items, external_ids=external_ids, raw=True)
                count = await self.set_items(collection_id, items)
            elif item_type is CollectionItemType.WSI:
                ids = [item.id for item in collection.items]
                if len(ids) != len(set(ids)):
                    raise HTTPException(405, "Duplicate items found.")
                items = [{"id": item_id, "type": "wsi"} for item_id in ids]
                i_count = len(items)
                count = await self.set_items(collection_id, items)
            elif item_type is CollectionItemType.COLLECTION:
                items = []
                for item in collection.items:
                    items.append(
                        await self.create_filled_collection(item, clients=clients, external_ids=external_ids, raw=True)
                    )
                i_count = len(items)
                count = await self.set_items(collection_id, items)
            else:
                raise HTTPException(405, "Invalid type of items")
            if i_count != count:
                raise HTTPException(500, "Items could not be created (count not matching)")
        else:
            new_collection = await self._create_collection(collection, external_ids, raw=True)
            try:
                items = [
                    IdObject.model_validate(dict(t))
                    for t in {tuple(d.items()) for d in [item.model_dump() for item in collection.items]}
                ]
                if await self.add_items_by_id(new_collection["id"], items, item_type):
                    items = [item.model_dump() for item in items]
                else:
                    raise HTTPException(500, "The resources could not be created (count not matching)")
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "The resources could not be created (items by id)") from e

        new_collection["item_count"] = len(items)
        new_collection["items"] = items

        if raw:
            return new_collection
        return JSONResponse(new_collection, status_code=201)

    async def _create_collection(self, collection: PostCollection, external_ids: bool = False, raw: bool = False):
        insert_tmptable_sql = await sqlh.get_insert_tmptable_sql(external_ids)
        insert_sql = await sqlh.get_insert_sql()
        logger.debug(insert_tmptable_sql)
        logger.debug(insert_sql)

        annot_ref_id = None
        new_collection = [
            collection.type,
            collection.name,
            collection.description,
            collection.creator_id,
            collection.creator_type,
            collection.reference_id,
            collection.reference_type,
            collection.item_type,
            False,
        ]

        if settings.allow_external_ids and external_ids:
            new_collection.insert(0, collection.id)

        if collection.reference_type == CollectionReferenceType.ANNOTATION:
            annot_ref_id = collection.reference_id

        if annot_ref_id:
            try:
                await autils.check_annotation(self.conn, annot_ref_id)
            except HTTPException as e:
                if e.status_code == 404:
                    raise HTTPException(
                        404, f"Can not create collection. Referenced annotation '{annot_ref_id}' does not exist."
                    ) from e

        try:
            async with self.conn.transaction():
                await self.conn.execute("CREATE TEMP TABLE IF NOT EXISTS tmptable_col (LIKE c_collections)")
                await self.conn.execute("TRUNCATE tmptable_col")
                await self.conn.execute(insert_tmptable_sql, *new_collection)
                result = await self.conn.fetchrow(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Collection with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collection could not be created") from e

        if result is None:
            raise HTTPException(500, "Collection could not be created (result None)")

        collection = result["json"]
        collection["item_count"] = 0
        collection["items"] = []

        if raw:
            return collection
        return JSONResponse(collection, status_code=201)

    async def set_items(self, collection_id: UUID4, items: List[Any]):
        insert_items_tmptable_sql = await sqlh.get_insert_items_tmptable_sql()
        insert_items_sql = await sqlh.get_insert_items_sql()
        logger.debug(insert_items_tmptable_sql)
        logger.debug(insert_items_sql)

        new_items = [(collection_id, item["id"]) for item in items]

        try:
            async with self.conn.transaction():
                await self.conn.execute("CREATE TEMP TABLE IF NOT EXISTS tmptable_items (LIKE c_collection_items)")
                await self.conn.execute("TRUNCATE tmptable_items")
                await self.conn.executemany(insert_items_tmptable_sql, new_items)
                return await self.conn.fetchval(insert_items_sql)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Items could not be added to collection (insert)") from e

    async def add_items_by_id(self, collection_id: UUID4, items: List[IdObject], item_type: CollectionItemType):
        ids = []
        for item in items:
            ids.append(item.id)
        ids = list(set(ids))

        if item_type == CollectionItemType.WSI:
            count = await self.add_item_ids(collection_id, ids)
        else:
            if await utils.check_type_ids(self.conn, ids, item_type):
                count = await self.add_item_ids(collection_id, ids)
            else:
                raise HTTPException(404, "Not all items found")

        if count == len(ids):
            return True
        else:
            return False

    async def add_item_ids(self, collection_id: UUID4, ids: List[Any]):
        insert_items_tmptable_sql = await sqlh.get_insert_items_tmptable_sql()
        insert_items_sql = await sqlh.get_insert_items_sql()
        logger.debug(insert_items_tmptable_sql)
        logger.debug(insert_items_sql)

        new_items = []
        for item_id in ids:
            new_item = (collection_id, item_id)
            new_items.append(new_item)

        try:
            async with self.conn.transaction():
                await self.conn.execute("CREATE TEMP TABLE IF NOT EXISTS tmptable_items (LIKE c_collection_items)")
                await self.conn.execute("TRUNCATE tmptable_items")
                await self.conn.executemany(insert_items_tmptable_sql, new_items)
                return await self.conn.fetchval(insert_items_sql)
        except UniqueViolationError as e:
            raise HTTPException(405, "Item already in collection") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Items could not be added (items by id)") from e

    async def get_all(self, skip: int, limit: int):
        return await self.query(CollectionQuery(), skip, limit)

    async def get(self, collection_id: UUID4, shallow: bool, with_leaf_ids: bool, clients, raw: bool = False):
        select_sql = await sqlh.get_single_select_sql()
        logger.debug(select_sql)

        async with self.conn.transaction():
            try:
                collection = await self.conn.fetchrow(select_sql, collection_id)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Collection could not be queried") from e

            if collection is None:
                raise HTTPException(404, f"No collection with id '{collection_id}' found")

            result = collection["json"]
            item_count = result["item_count"]
            item_type = result["item_type"]

            items = []
            item_ids = None
            _items_ids = None

            if item_count == 0:
                item_ids = [] if with_leaf_ids and item_type != "collection" else None
            else:
                if item_type == "collection" or not shallow or with_leaf_ids:
                    select_item_ids_sql = await sqlh.get_select_item_ids_sql()
                    logger.debug(select_item_ids_sql)

                    try:
                        item_ids_result = await self.conn.fetch(select_item_ids_sql, collection_id)
                    except PostgresError as e:
                        logger.debug(e)
                        raise HTTPException(500, "Item ids could not be queried (get collection)") from e

                    _items_ids = [item_id["item_id"] for item_id in item_ids_result]

                if with_leaf_ids and item_type != "collection":
                    item_ids = _items_ids

                if not shallow or item_type == "collection":
                    _items = None
                    if (
                        item_type == CollectionItemType.POINT
                        or item_type == CollectionItemType.LINE
                        or item_type == CollectionItemType.ARROW
                        or item_type == CollectionItemType.CIRCLE
                        or item_type == CollectionItemType.RECTANGLE
                        or item_type == CollectionItemType.POLYGON
                    ):
                        _, _items = await clients.annotation.query(
                            annotation_query=AnnotationQuery(annotations=_items_ids),
                            with_classes=False,
                            with_low_npp_centroids=False,
                            skip=None,
                            limit=None,
                            raw=True,
                        )
                    elif (
                        item_type == CollectionItemType.INTEGER
                        or item_type == CollectionItemType.FLOAT
                        or item_type == CollectionItemType.BOOL
                        or item_type == CollectionItemType.STRING
                    ):
                        _, _items = await clients.primitive.query(
                            primitive_query=PrimitiveQuery(primitives=_items_ids), skip=None, limit=None, raw=True
                        )
                    elif item_type == CollectionItemType.CLASS:
                        _, _items = await clients.aclass.query(
                            class_query=ClassQuery(classes=_items_ids),
                            with_unique_class_values=False,
                            skip=None,
                            limit=None,
                            raw=True,
                        )
                    elif item_type == CollectionItemType.WSI:
                        _items = [{"id": item_id, "type": "wsi"} for item_id in _items_ids]
                    elif item_type == CollectionItemType.COLLECTION:
                        collection_items = []
                        for item_id in _items_ids:
                            collection_items.append(
                                await self.get(
                                    collection_id=item_id,
                                    shallow=shallow,
                                    with_leaf_ids=with_leaf_ids,
                                    clients=clients,
                                    raw=True,
                                )
                            )
                        _items = collection_items

                    items = _items

            result["items"] = items
            if item_ids or item_ids == []:
                result["item_ids"] = item_ids

            if raw:
                return result
            return JSONResponse(result)

    async def query(
        self, collection_query: CollectionQuery, skip: int, limit: int, ids: List[UUID4] = None, raw: bool = False
    ):
        if collection_query.jobs is not None:
            job_sql = """
            SELECT DISTINCT collection_id
            FROM j_job_collections
            WHERE job_id = ANY($1);
            """

            try:
                id_results = await self.conn.fetch(job_sql, collection_query.jobs)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Jobs could not be queried (in collection query)") from e

            if len(id_results) == 0:
                if raw:
                    return 0, []
                else:
                    return JSONResponse({"item_count": 0, "items": []})

            ids = [id_result["collection_id"] for id_result in id_results]

        count_sql, args = await sqlh.get_count_sql(collection_query, ids)
        logger.debug(count_sql)

        try:
            if len(args) == 0:
                count = await self.conn.fetchval(count_sql)
            else:
                count = await self.conn.fetchval(count_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be queried (count)") from e

        if count == 0:
            if raw:
                return 0, []
            return JSONResponse({"item_count": 0, "items": []})
        elif count > settings.v1_item_limit and (limit is None or limit > settings.v1_item_limit):
            raise HTTPException(
                413, f"Count of requested items ({count}) exceeds server limit of {settings.v1_item_limit}."
            )
        else:
            select_sql, args = await sqlh.get_select_sql(collection_query, ids)
            logger.debug(select_sql)

            args.extend([limit, skip])

            try:
                if len(args) == 0:
                    result = await self.conn.fetch(select_sql)
                else:
                    result = await self.conn.fetch(select_sql, *args)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Collections could not be queried (select)") from e

        if result is not None:
            items = [r["json"] for r in result]

            if raw:
                return count, items
            return JSONResponse({"item_count": count, "items": items})
        else:
            raise HTTPException(500, "Collections could not be queried (result None)")

    async def unique_references_query(self, collection_query: CollectionQuery, ids: List[UUID4] = None):
        references_sql, args = await sqlh.get_references_sql(collection_query, ids)
        logger.debug(references_sql)

        try:
            if len(args) == 0:
                references_result = await self.conn.fetchrow(references_sql)
            else:
                references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be queried (collection references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Collections cound not be queried (unique references None).")

    async def update(self, collection_id: UUID4, collection: PostCollection):
        await utils.check_collection(self.conn, collection_id, "update", collection)

        update_sql = await sqlh.get_update_sql()
        logger.debug(update_sql)

        try:
            await self.conn.execute(
                update_sql,
                collection_id,
                collection.name,
                collection.description,
                collection.creator_id,
                collection.creator_type,
                collection.reference_id,
                collection.reference_type,
                collection.item_type,
            )
            return await self.get(collection_id=collection_id, shallow=True, with_leaf_ids=False, clients=None)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be updated") from e

    async def delete(self, collection_id: UUID4):
        await utils.check_collection(self.conn, collection_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM c_collections
            WHERE id = $1
            RETURNING *
        ) SELECT COUNT(*) FROM deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(collection_id)})
        else:
            raise HTTPException(500, "Collections could not be deleted (delete count 0)")

    async def add_items(self, collection_id: UUID4, items: PostItems, clients, external_ids: bool = False):
        async with self.conn.transaction():
            await utils.check_collection(self.conn, collection_id, "lock")

            if isinstance(items, PostItem.__args__):
                return await self.add_single_item(collection_id, items, clients, external_ids)
            else:
                return await self.add_item_list(collection_id, items, clients, external_ids)

    async def add_single_item(self, collection_id: UUID4, item: PostItem, clients, external_ids: bool = False):
        item_type = await utils.get_item_type(self.conn, collection_id)
        if isinstance(item, IdObject):
            if await self.add_item_by_id(collection_id, item.id, item_type):
                return JSONResponse(status_code=201, content={"success": "Item added"})
            else:
                raise HTTPException(500, "Item could not be added (add item by id)")
        else:
            if await utils.check_item_type_post_put(item, item_type):
                if (
                    item_type == CollectionItemType.INTEGER
                    or item_type == CollectionItemType.FLOAT
                    or item_type == CollectionItemType.BOOL
                    or item_type == CollectionItemType.STRING
                ):
                    _, new_items = await clients.primitive.add_primitives(item, external_ids, True)
                    await self.add_item(collection_id, new_items[0])
                    return new_items[0]
                elif (
                    item_type == CollectionItemType.POINT
                    or item_type == CollectionItemType.LINE
                    or item_type == CollectionItemType.ARROW
                    or item_type == CollectionItemType.CIRCLE
                    or item_type == CollectionItemType.RECTANGLE
                    or item_type == CollectionItemType.POLYGON
                ):
                    _, new_items = await clients.annotation.add_annotations(item, external_ids, True)
                    await self.add_item(collection_id, new_items[0])
                    return new_items[0]
                elif item_type == CollectionItemType.CLASS:
                    _, new_items = await clients.aclass.add_classes(item, external_ids, True)
                    await self.add_item(collection_id, new_items[0])
                    return new_items[0]
                elif item_type == CollectionItemType.WSI:
                    new_item = {"id": item.id}
                    await self.add_item(collection_id, new_item)
                    return item
                elif item_type == CollectionItemType.COLLECTION:
                    new_item = await self.add_collection(item, clients, external_ids, True)
                    await self.add_item(collection_id, new_item)
                    return new_item
            else:
                raise HTTPException(405, "Item type and type of item not matching")

    async def add_item_by_id(self, collection_id: UUID4, item_id: UUID4, item_type: CollectionItemType):
        if item_type == CollectionItemType.WSI:
            count = await self.add_item_ids(collection_id, [item_id])
        else:
            if await utils.check_type_ids(self.conn, [item_id], item_type):
                count = await self.add_item_ids(collection_id, [item_id])
            else:
                raise HTTPException(404, f"{item_type} not found")
        if count == 1:
            return True
        else:
            return False

    async def add_item(self, collection_id: UUID4, item: Any):
        insert_sql = """
        INSERT INTO c_collection_items(
            collection_id,
            item_id
        )
        VALUES($1, $2);
        """

        try:
            await self.conn.execute(insert_sql, collection_id, item["id"])
        except UniqueViolationError as e:
            raise HTTPException(405, "Item already in collection") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Item could not be added (insert)") from e

    async def add_item_list(self, collection_id: UUID4, items: PostItemList, clients, external_ids: bool = False):
        item_type = await utils.get_item_type(self.conn, collection_id)
        if len(items.items) == 0:
            raise HTTPException(404, "No items found")
        elif len(items.items) > settings.v1_post_limit:
            raise HTTPException(
                413,
                f"Number of posted items ({len(items.items)}) exceeds server limit of {settings.v1_post_limit}.",
            )

        first_item = items.items[0]
        if isinstance(first_item, IdObject):
            if await self.add_items_by_id(collection_id=collection_id, items=items.items, item_type=item_type):
                return JSONResponse(status_code=201, content={"success": "Items added"})
            else:
                raise HTTPException(500, "The resources could not be added (add items by id)")
        else:
            if await utils.check_item_type_post_put(first_item, item_type):
                count = 0
                if (
                    item_type == CollectionItemType.INTEGER
                    or item_type == CollectionItemType.FLOAT
                    or item_type == CollectionItemType.BOOL
                    or item_type == CollectionItemType.STRING
                ):
                    count, new_items = await clients.primitive.add_primitives(
                        items, external_ids=external_ids, raw=True
                    )
                elif (
                    item_type == CollectionItemType.POINT
                    or item_type == CollectionItemType.LINE
                    or item_type == CollectionItemType.ARROW
                    or item_type == CollectionItemType.CIRCLE
                    or item_type == CollectionItemType.RECTANGLE
                    or item_type == CollectionItemType.POLYGON
                ):
                    count, new_items = await clients.annotation.add_annotations(
                        items, external_ids=external_ids, raw=True
                    )
                elif item_type == CollectionItemType.CLASS:
                    count, new_items = await clients.aclass.add_classes(items, external_ids=external_ids, raw=True)
                elif item_type == CollectionItemType.WSI:
                    ids = [item.id for item in items.items]
                    if len(ids) != len(set(ids)):
                        raise HTTPException(405, "Duplicate items found.")
                    new_items = [{"id": item_id, "type": "wsi"} for item_id in ids]
                    count = len(new_items)
                elif item_type == CollectionItemType.COLLECTION:
                    count, new_items = await self.add_collection(items, clients, external_ids=external_ids, raw=True)

                ids = [item["id"] for item in new_items]
                await self.add_item_ids(collection_id, ids)

                return JSONResponse({"item_count": count, "items": new_items}, status_code=201)
            else:
                raise HTTPException(405, "Item type and type of item not matching")

    async def query_items(
        self, collection_id: UUID4, item_query: ItemQuery, clients, skip: int, limit: int, raw: bool = False
    ):
        await utils.check_collection(self.conn, collection_id)

        select_item_id_sql = """
        SELECT item_id
        FROM c_collection_items
        WHERE collection_id = $1
        LIMIT $2 OFFSET $3;
        """

        try:
            result = await self.conn.fetch(select_item_id_sql, collection_id, limit, skip)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Item ids could not be queried (query items)") from e

        item_ids = [r["item_id"] for r in result]

        if len(item_ids) == 0:
            if raw:
                return 0, []
            return JSONResponse({"item_count": 0, "items": []})
        elif len(item_ids) > settings.v1_item_limit:
            raise HTTPException(
                413,
                f"Count of requested items ({len(item_ids)}) exceeds server limit of {settings.v1_item_limit}.",
            )

        item_type = await utils.get_item_type(self.conn, collection_id)
        if (
            item_type == CollectionItemType.INTEGER
            or item_type == CollectionItemType.FLOAT
            or item_type == CollectionItemType.BOOL
            or item_type == CollectionItemType.STRING
        ):
            if item_query.references is not None:
                query = PrimitiveQuery(references=item_query.references, primitives=item_ids)
            else:
                query = PrimitiveQuery(primitives=item_ids)
            count, items = await clients.primitive.query(primitive_query=query, skip=None, limit=None, raw=True)
        elif (
            item_type == CollectionItemType.POINT
            or item_type == CollectionItemType.LINE
            or item_type == CollectionItemType.ARROW
            or item_type == CollectionItemType.CIRCLE
            or item_type == CollectionItemType.RECTANGLE
            or item_type == CollectionItemType.POLYGON
        ):
            if item_query.references is not None:
                query = AnnotationQuery(
                    annotations=item_ids,
                    references=item_query.references,
                    viewport=item_query.viewport,
                    npp_viewing=item_query.npp_viewing,
                )
            else:
                query = AnnotationQuery(
                    annotations=item_ids,
                    viewport=item_query.viewport,
                    npp_viewing=item_query.npp_viewing,
                )
            count, items = await clients.annotation.query(
                annotation_query=query,
                with_classes=False,
                with_low_npp_centroids=False,
                skip=None,
                limit=None,
                raw=True,
            )
        elif item_type == CollectionItemType.CLASS:
            if item_query.references is not None:
                query = ClassQuery(references=item_query.references, classes=item_ids)
            else:
                query = ClassQuery(classes=item_ids)
            count, items = await clients.aclass.query(
                class_query=query,
                with_unique_class_values=False,
                skip=None,
                limit=None,
                raw=True,
            )
        elif item_type == CollectionItemType.COLLECTION:
            if item_query.references is not None:
                query = CollectionQuery(references=item_query.references)
            else:
                query = CollectionQuery()
            count, items = await self.query(collection_query=query, skip=None, limit=None, ids=item_ids, raw=True)
        else:
            count = 0
            items = []

        if raw:
            return count, items

        return JSONResponse({"item_count": count, "items": items})

    async def unique_references_query_items(self, collection_id: UUID4, item_query: ItemQuery, clients):
        await utils.check_collection(self.conn, collection_id)

        select_item_id_sql = """
        SELECT item_id
        FROM c_collection_items
        WHERE collection_id = $1;
        """

        try:
            result = await self.conn.fetch(select_item_id_sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Item ids could not be queried (query items)") from e

        item_ids = [r["item_id"] for r in result]

        if len(item_ids) == 0:
            return JSONResponse(
                {"annotation": [], "collection": [], "wsi": [], "contains_items_without_reference": False}
            )

        item_type = await utils.get_item_type(self.conn, collection_id)
        if (
            item_type == CollectionItemType.INTEGER
            or item_type == CollectionItemType.FLOAT
            or item_type == CollectionItemType.BOOL
            or item_type == CollectionItemType.STRING
        ):
            query = PrimitiveQuery(references=item_query.references, primitives=item_ids)
            return await clients.primitive.unique_references_query(query)
        elif (
            item_type == CollectionItemType.POINT
            or item_type == CollectionItemType.LINE
            or item_type == CollectionItemType.ARROW
            or item_type == CollectionItemType.CIRCLE
            or item_type == CollectionItemType.RECTANGLE
            or item_type == CollectionItemType.POLYGON
        ):
            query = AnnotationQuery(
                annotations=item_ids,
                references=item_query.references,
                viewport=item_query.viewport,
                npp_viewing=item_query.npp_viewing,
            )
            return await clients.annotation.unique_references_query(
                annotation_query=query,
            )
        elif item_type == CollectionItemType.CLASS:
            query = ClassQuery(references=item_query.references, classes=item_ids)
            return await clients.aclass.unique_references_query(
                class_query=query,
                ids=item_ids,
            )
        elif item_type == CollectionItemType.COLLECTION:
            query = CollectionQuery(references=item_query.references)
            return await self.unique_references_query(collection_query=query, ids=item_ids)
        else:
            return JSONResponse(
                {"annotation": [], "collection": [], "wsi": [], "contains_items_without_reference": False}
            )

    async def delete_item(self, collection_id: UUID4, item_id: Id):
        await utils.check_collection(self.conn, collection_id, "lock")
        if not await utils.check_if_item_exists(self.conn, collection_id, item_id):
            raise HTTPException(404, "Collection contains no item with given id")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM c_collection_items
            WHERE collection_id = $1 AND item_id = $2
            RETURNING *
        ) SELECT COUNT(*) FROM deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, collection_id, item_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Item could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(item_id)})
        else:
            raise HTTPException(500, "Item could not be deleted (delete count 0)")
