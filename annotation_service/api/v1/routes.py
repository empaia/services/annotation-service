from .annotation_routes import add_routes_annotations
from .class_routes import add_routes_classes
from .collection_routes import add_routes_collections
from .job_routes import add_routes_jobs
from .primitive_routes import add_routes_primitives
from .server_routes import add_routes_misc


def add_routes_v1(app, late_init, api_v1_integration):
    add_routes_annotations(app, late_init, api_v1_integration)
    add_routes_classes(app, late_init, api_v1_integration)
    add_routes_collections(app, late_init, api_v1_integration)
    add_routes_primitives(app, late_init, api_v1_integration)
    add_routes_jobs(app, late_init, api_v1_integration)
    add_routes_misc(app, late_init, api_v1_integration)
