from pydantic import UUID4

from annotation_service.models.v1.annotation.annotations import AnnotationList
from annotation_service.models.v1.annotation.collections import SlideList
from annotation_service.models.v1.annotation.jobs import JobLock, NodeType, PrimitiveDetails, TreeNodeSequence
from annotation_service.models.v1.annotation.primitives import PrimitiveList
from annotation_service.models.v1.commons import Id, Message

from .db import db_clients


def add_routes_jobs(app, late_init, api_v1_integration):
    @app.put(
        "/jobs/{job_id}/lock/annotations/{annotation_id}",
        tags=["Jobs"],
        summary="Locks annotation for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, annotation_id: UUID4, payload=api_v1_integration.global_depends()):
        """
        Locks the specified annotation for given job ID.

        The annotation and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_annotation(job_id=job_id, annotation_id=annotation_id)

    @app.put(
        "/jobs/{job_id}/lock/classes/{class_id}",
        tags=["Jobs"],
        summary="Locks class for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, class_id: UUID4, payload=api_v1_integration.global_depends()):
        """
        Locks the specified class for given job ID.

        The class and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_class(job_id=job_id, class_id=class_id)

    @app.put(
        "/jobs/{job_id}/lock/collections/{collection_id}",
        tags=["Jobs"],
        summary="Locks collection for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, collection_id: UUID4, payload=api_v1_integration.global_depends()):
        """
        Locks the specified collection for given job ID.

        **Items in the collection are recursivly locked for the job. Works also for nested collections.**

        The collection and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_collection(job_id=job_id, collection_id=collection_id)

    @app.put(
        "/jobs/{job_id}/lock/primitives/{primitive_id}",
        tags=["Jobs"],
        summary="Locks primitive for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, primitive_id: UUID4, payload=api_v1_integration.global_depends()):
        """
        Locks the specified primitive for given job ID.

        The primitive and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_primitive(job_id=job_id, primitive_id=primitive_id)

    @app.put(
        "/jobs/{job_id}/lock/slides/{slide_id}",
        tags=["Jobs"],
        summary="Locks slide for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, slide_id: Id, payload=api_v1_integration.global_depends()):
        """
        Locks the specified slide for given job ID.

        The slide and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_slide(job_id=job_id, slide_id=slide_id)

    @app.get(
        "/jobs/{job_id}/tree/items",
        tags=["Deprecated"],
        summary="Returns result tree root items",
        responses={
            200: {"model": SlideList},
        },
    )
    async def _(job_id: Id, skip: int = None, limit: int = None, payload=api_v1_integration.global_depends()):
        """
        Returns the requested result tree root items.

        The count is not affected by `limit`.

        Items of the result tree root are always slides locked for specified job ID.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.get_tree_items(clients=clients, job_id=job_id, skip=skip, limit=limit)

    @app.get(
        "/jobs/{job_id}/tree/primitives",
        tags=["Deprecated"],
        summary="Returns result tree root primitives",
        responses={
            200: {"model": PrimitiveList},
        },
    )
    async def _(job_id: Id, skip: int = None, limit: int = None, payload=api_v1_integration.global_depends()):
        """
        Returns the requested result tree root primitives.

        The count is not affected by `limit`.

        Primitives of the result tree root are locked for specified job and either:
        - primitives without reference
        - primitives referencing a collection locked for specified job ID
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.get_tree_primitives(clients=clients, job_id=job_id, skip=skip, limit=limit)

    @app.get(
        "/jobs/{job_id}/tree/primitives/{primitive_id}/details",
        tags=["Deprecated"],
        summary="Returns result tree primitive details",
        responses={
            200: {"model": PrimitiveDetails},
            404: {
                "model": Message,
                "description": "The primitive was not found",
            },
        },
    )
    async def _(job_id: Id, primitive_id: UUID4, payload=api_v1_integration.global_depends()):
        """
        Returns the requested tree primitive details.

        The primitive details contain:
        - `reference_type` (can be `null` or `collection`)
        - `reference_data`
          - is `null` when `reference_type` is `null`
          - list of collection items when `reference_type` is `collection`
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.get_tree_primitive_details(
                clients=clients, job_id=job_id, primitive_id=primitive_id
            )

    @app.get(
        "/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/items",
        tags=["Deprecated"],
        summary="Returns result tree node items",
        responses={
            200: {"model": AnnotationList},
        },
    )
    async def _(
        job_id: Id,
        node_type: NodeType,
        node_id: Id,
        skip: int = None,
        limit: int = None,
        payload=api_v1_integration.global_depends(),
    ):
        """
        Returns the requested result tree node items.

        The count is not affected by `limit`.

        Items of a result tree node are always annotations locked for specified job and either:
        - reference the node directly and are
          - not in a collection with a different reference
          - in a collection without a reference
        - in a collection referencing the node
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.get_tree_node_items(
                clients=clients, job_id=job_id, node_type=node_type, node_id=node_id, skip=skip, limit=limit
            )

    @app.get(
        "/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/primitives",
        tags=["Deprecated"],
        summary="Returns result tree node primitives",
        responses={
            200: {"model": PrimitiveList},
        },
    )
    async def _(
        job_id: Id,
        node_type: NodeType,
        node_id: Id,
        skip: int = None,
        limit: int = None,
        payload=api_v1_integration.global_depends(),
    ):
        """
        Returns the requested result tree node primitives.

        The count is not affected by `limit`.

        Primitives of a result tree node are locked for specified job.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.get_tree_node_primitives(
                clients=clients, job_id=job_id, node_id=node_id, skip=skip, limit=limit
            )

    @app.get(
        "/jobs/{job_id}/tree/items/{item_id}/sequence",
        tags=["Deprecated"],
        summary="Returns the tree node sequence for the specified item",
        responses={
            200: {"model": TreeNodeSequence},
        },
    )
    async def _(
        job_id: Id,
        item_id: UUID4,
        payload=api_v1_integration.global_depends(),
    ):
        """
        Returns the tree node sequence for the specified item.

        **Currently only works for items of type annotation.**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.get_tree_item_sequence(clients=clients, job_id=job_id, item_id=item_id)
