from annotation_service.models.v1.annotation.server_settings import AnnotationServiceSettings
from annotation_service.singletons import settings


def add_routes_misc(app, late_init, api_v1_integration):
    @app.get(
        "/settings",
        tags=["Service"],
        summary="Returns service settings",
        responses={
            200: {"model": AnnotationServiceSettings},
        },
    )
    async def _(payload=api_v1_integration.global_depends()):
        """
        Returns the server settings:
        - Maximal number of items returned in a request.
        - Maximal number of items accepted in a post.
        """
        return AnnotationServiceSettings(item_limit=settings.v1_item_limit, post_limit=settings.v1_post_limit)
