from asyncpg import Connection

from ...db import set_type_codecs
from .clients.annotation_client import AnnotationClient
from .clients.class_client import ClassClient
from .clients.collection_client import CollectionClient
from .clients.job_client import JobClient
from .clients.primitive_client import PrimitiveClient


class Clients:
    def __init__(self, conn):
        self.annotation = AnnotationClient(conn=conn)
        self.aclass = ClassClient(conn=conn)
        self.collection = CollectionClient(conn=conn)
        self.primitive = PrimitiveClient(conn=conn)
        self.job = JobClient(conn=conn)


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return Clients(conn=conn)
