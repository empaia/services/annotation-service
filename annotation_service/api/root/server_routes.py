from fastapi import Response

from annotation_service import __version__ as version
from annotation_service.models.commons import ServiceStatus, ServiceStatusEnum

from ...singletons import logger
from .db import db_clients


def add_routes_misc(app, late_init):
    @app.get(
        "/alive",
        tags=["Service"],
        summary="Returns service health status",
        responses={
            200: {"model": ServiceStatus},
            500: {"model": ServiceStatus},
        },
    )
    async def _(response: Response):
        """
        Returns the health status of the service.

        Checks if database is running and responding.
        """
        try:
            async with late_init.pool.acquire() as conn:
                clients = await db_clients(conn=conn)
                service_status = await clients.server.check_server_health()
                response.status_code = 200 if service_status.status == ServiceStatusEnum.OK else 500
                return service_status
        except ConnectionRefusedError as e:
            logger.debug(e)
            response.status_code = 500
            return ServiceStatus(
                status=ServiceStatusEnum.FAILURE.value,
                version=version,
                message="ERROR: Database Server is not reachable!",
            )
