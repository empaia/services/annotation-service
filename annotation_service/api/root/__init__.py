from .server_routes import add_routes_misc


def add_routes_root(app, late_init):
    add_routes_misc(app, late_init)
