from asyncpg.exceptions import PostgresError

from annotation_service import __version__ as version
from annotation_service.models.commons import ServiceStatus, ServiceStatusEnum

from ....singletons import logger


class ServerClient:
    def __init__(self, conn):
        self.conn = conn

    async def check_server_health(self) -> ServiceStatus:
        # currently only database connection is checked
        try:
            await self.conn.fetch("SELECT datname FROM pg_database;")
            return ServiceStatus(status=ServiceStatusEnum.OK.value, version=version)
        except PostgresError as e:
            logger.debug(e)
            return ServiceStatus(
                status=ServiceStatusEnum.FAILURE.value,
                version=version,
                message="ERROR: Database Server reachable but not working properly!",
            )
