from annotation_service.models.v3.annotation.server_settings import AnnotationServiceSettings
from annotation_service.singletons import settings


def add_routes_misc(app, late_init, api_v3_integration):
    @app.get(
        "/settings",
        tags=["Service"],
        summary="Returns service settings",
        responses={
            200: {"model": AnnotationServiceSettings},
        },
    )
    async def _(payload=api_v3_integration.global_depends()):
        """
        Returns the server settings:
        - Maximal number of items returned in a request.
        - Maximal number of items accepted in a post.
        """
        return AnnotationServiceSettings(item_limit=settings.v3_item_limit, post_limit=settings.v3_post_limit)
