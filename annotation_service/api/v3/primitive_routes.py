from pydantic import UUID4

from annotation_service.models.v3.annotation.primitives import (
    PostPrimitive,
    PostPrimitives,
    Primitive,
    PrimitiveList,
    PrimitiveQuery,
    Primitives,
)
from annotation_service.models.v3.commons import IdObject, Message, UniqueReferences

from .db import db_clients


def add_routes_primitives(app, late_init, api_v3_integration):
    @app.get(
        "/primitives",
        tags=["Primitives"],
        summary="Returns all primitives",
        responses={
            200: {"model": PrimitiveList},
        },
    )
    async def _(skip: int = None, limit: int = None, payload=api_v3_integration.global_depends()):
        """
        Returns all primitives.

        The count is not affected by `limit`.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.primitive.get_all(skip=skip, limit=limit)

    @app.post(
        "/primitives",
        tags=["Primitives"],
        summary="Creates new primitive / primitives",
        status_code=201,
        responses={201: {"model": Primitives}},
    )
    async def _(primitives: PostPrimitives, external_ids: bool = False, payload=api_v3_integration.global_depends()):
        """
        Creates new primitive / primitives.

        If `external_ids` is `True`, all items must contain a valid id of type `UUID4`
        (`ANNOT_ALLOW_EXTERNAL_IDS` must also be set to `True` in the `.env` file).
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.primitive.add_primitives(primitives=primitives, external_ids=external_ids)

    @app.put(
        "/primitives/query",
        tags=["Primitives"],
        summary="Returns a list of filtered primitives",
        status_code=200,
        responses={200: {"model": PrimitiveList}},
    )
    async def _(
        primitive_query: PrimitiveQuery,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a count and a list of primitives matching given filter criteria.

        The count is not affected by `limit`.

        Primitives can be filtered by:
        - primitive ids
        - creators
        - references
          - list can contain `null` &rarr; primitives with no reference are included
        - jobs the primitives are locked for
        - types
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.primitive.query(primitive_query=primitive_query, skip=skip, limit=limit)

    @app.put(
        "/primitives/query/unique-references",
        tags=["Primitives"],
        summary="Returns a list of unique references for primitives matching filter criteria",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(primitive_query: PrimitiveQuery, payload=api_v3_integration.global_depends()):
        """
        Returns a list of unique references for primitives matching filter criteria.

        Primitives can be filtered by:
        - creators
        - references
          - list can contain `null` &rarr; primitives with no reference are included
        - jobs the primitives are locked for
        - types
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.primitive.unique_references_query(primitive_query=primitive_query)

    @app.get(
        "/primitives/{primitive_id}",
        tags=["Primitives"],
        summary="Returns a primitive",
        responses={
            200: {"model": Primitive},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(primitive_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Returns the specified primitive.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.primitive.get(primitive_id=primitive_id)

    @app.put(
        "/primitives/{primitive_id}",
        tags=["Primitives"],
        summary="Updates a primitive",
        responses={
            200: {"model": Primitive},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Primitive is locked"},
        },
    )
    async def _(primitive_id: UUID4, primitive: PostPrimitive, payload=api_v3_integration.global_depends()):
        """
        Updates the specified primitive.

        The updated primitive is returned.

        **A locked primitive can not be updated!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.primitive.update(primitive_id=primitive_id, primitive=primitive)

    @app.delete(
        "/primitives/{primitive_id}",
        tags=["Primitives"],
        summary="Deletes a primitive",
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Primitive is locked"},
        },
    )
    async def _(primitive_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Deletes the specified primitive.

        The primitive ID is returned.

        **A locked primitive can not be deleted!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.primitive.delete(primitive_id=primitive_id)
