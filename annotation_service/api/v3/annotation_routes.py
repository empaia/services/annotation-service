from pydantic import UUID4

from annotation_service.models.v3.annotation.annotations import (
    Annotation,
    AnnotationCountResponse,
    AnnotationList,
    AnnotationQuery,
    AnnotationQueryPosition,
    Annotations,
    AnnotationUniqueClassesQuery,
    AnnotationViewerList,
    AnnotationViewerQuery,
    PostAnnotations,
    UniqueClassValues,
)
from annotation_service.models.v3.commons import IdObject, Message, UniqueReferences

from .db import db_clients


def add_routes_annotations(app, late_init, api_v3_integration):
    @app.get(
        "/annotations",
        tags=["Annotations"],
        summary="Returns all annotations",
        status_code=200,
        responses={
            200: {"model": AnnotationList},
            400: {"model": Message, "description": "Viewport not valid"},
        },
    )
    async def _(
        with_classes: bool = False,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns all annotations.

        The count is not affected by `limit`.

        If `with_classes` is `True`, annotations are returned with assigned classes.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.get_all(with_classes=with_classes, skip=skip, limit=limit)

    @app.post(
        "/annotations",
        tags=["Annotations"],
        summary="Creates new annotation / annotations",
        status_code=201,
        responses={201: {"model": Annotations}},
    )
    async def _(annotations: PostAnnotations, external_ids: bool = False, payload=api_v3_integration.global_depends()):
        """
        Creates new annotation / annotations.

        If `external_ids` is `True`, all items must contain a valid id of type `UUID4`
        (`ANNOT_ALLOW_EXTERNAL_IDS` must also be set to `True` in the `.env` file).
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.add_annotations(annotations=annotations, external_ids=external_ids)

    @app.put(
        "/annotations/query",
        tags=["Annotations"],
        summary="Returns a list of filtered annotations",
        status_code=200,
        responses={
            200: {"model": AnnotationList},
            400: {
                "model": Message,
                "description": "npp_viewing must be specified when query parameter with_low_npp_centroids is true",
            },
        },
    )
    async def _(
        annotation_query: AnnotationQuery,
        with_classes: bool = False,
        with_low_npp_centroids: bool = False,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a count and a list of annotations matching given filter criteria.

        The count is not affected by `limit`.

        Annotations can be filtered by:
        - annotation ids
        - creators
        - references
        - jobs the annotations are locked for
        - types
        - viewport &rarr; a rectangle the annotations must be inside
        - npp_viewing &rarr; resolution range in npp (nanometer per pixel)
        - class values &rarr; list of class values of assigned classes
          - can include `null` if annotations without assigned classes should be included

        If `with_classes` is `True`, annotations are returned with assigned classes.

        If `with_unique_class_values` is `True`, a list of unique class values of all classes assigned
        to returned annotations is part of response (not affected by `limit`).

        If `with_low_npp_centroids` is `True`, a list of centroids is part of response. Only centroids
        inside the specified viewport and with higher resolution are returned (not affected by `limit`).
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.query(
                annotation_query=annotation_query,
                with_classes=with_classes,
                with_low_npp_centroids=with_low_npp_centroids,
                skip=skip,
                limit=limit,
            )

    @app.put(
        "/annotations/query/count",
        tags=["Annotations"],
        summary="Returns a count for filtered annotations",
        status_code=200,
        responses={
            200: {"model": AnnotationCountResponse},
            400: {
                "model": Message,
                "description": "npp_viewing must be specified when query parameter with_low_npp_centroids is true",
            },
        },
    )
    async def _(annotation_query: AnnotationQuery, pyload=api_v3_integration.global_depends()):
        """
        Returns a count of annotations matching given filter criteria.

        Annotations can be filtered by:
        - annotation ids
        - creators
        - references
        - jobs the annotations are locked for
        - types
        - viewport &rarr; a rectangle the annotations must be inside
        - npp_viewing &rarr; resolution range in npp (nanometer per pixel)
        - class values &rarr; list of class values of assigned classes
          - can include `null` if annotations without assigned classes should be included
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.count_query(annotation_query=annotation_query)

    @app.put(
        "/annotations/query/unique-class-values",
        tags=["Annotations"],
        summary="Returns a list of unique class values for annotations matching filter criteria",
        status_code=200,
        responses={
            200: {"model": UniqueClassValues},
        },
    )
    async def _(
        annotation_query: AnnotationUniqueClassesQuery,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a list of unique class values for annotations matching filter criteria.

        Annotations can be filtered by:
        - annotation ids
        - creators
        - references
        - jobs the annotations are locked for
        - types
        - viewport &rarr; a rectangle the annotations must be inside
        - npp_viewing &rarr; resolution range in npp (nanometer per pixel)
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.unique_classes_query(
                annotation_query=annotation_query,
            )

    @app.put(
        "/annotations/query/unique-references",
        tags=["Annotations"],
        summary="Returns a list of unique references for annotations matching filter criteria",
        status_code=200,
        responses={
            200: {"model": UniqueReferences},
        },
    )
    async def _(
        annotation_query: AnnotationQuery,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a list of unique references for annotations matching filter criteria.

        Annotations can be filtered by:
        Annotations can be filtered by:
        - annotation ids
        - creators
        - references
        - jobs the annotations are locked for
        - types
        - viewport &rarr; a rectangle the annotations must be inside
        - npp_viewing &rarr; resolution range in npp (nanometer per pixel)
        - class values &rarr; list of class values of assigned classes
          - can include `null` if annotations without assigned classes should be included
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.unique_references_query(
                annotation_query=annotation_query,
            )

    @app.put(
        "/annotations/query/viewer",
        tags=["Annotations"],
        summary="Returns a list of annotation ids for filtered annotations and low npp centroids",
        status_code=200,
        responses={
            200: {"model": AnnotationViewerList},
            400: {
                "model": Message,
                "description": "npp_viewing must be specified when query parameter with_low_npp_centroids is true",
            },
        },
    )
    async def _(
        annotation_viewer_query: AnnotationViewerQuery,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a list of annotation ids for filtered annotations and low npp centroids.

        Annotations can be filtered by:
        - creators
        - references
        - jobs the annotations are locked for
        - types
        - viewport &rarr; a rectangle the annotations must be inside
        - npp_viewing &rarr; resolution range in npp (nanometer per pixel)
        - class values &rarr; list of class values of assigned classes
          - can include `null` if annotations without assigned classes should be included
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.viewer_query(
                annotation_viewer_query=annotation_viewer_query,
            )

    @app.get(
        "/annotations/{annotation_id}",
        tags=["Annotations"],
        summary="Returns an annotation",
        responses={
            200: {"model": Annotation},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        annotation_id: UUID4,
        with_classes: bool = False,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Return the specified annotation.

        If `with_classes` is `True`, annotation is returned with assigned classes.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.get(annot_id=annotation_id, with_classes=with_classes)

    @app.put(
        "/annotations/{annotation_id}/query",
        tags=["Annotations"],
        summary="Returns annotion position in query.",
        responses={
            200: {"model": AnnotationQueryPosition},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        annotation_id: UUID4,
        annotation_query: AnnotationQuery,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns the position of specified annotion in given query.

        Query should be the same as in route `PUT /annotations/query`
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.query_position(annot_id=annotation_id, annotation_query=annotation_query)

    @app.delete(
        "/annotations/{annotation_id}",
        tags=["Annotations"],
        summary="Deletes an annotation",
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Annotation is locked"},
        },
    )
    async def _(annotation_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Deletes the specified annotation.

        The annotation ID is returned.

        **A locked annotation can not be deleted!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.annotation.delete(annotation_id=annotation_id)
