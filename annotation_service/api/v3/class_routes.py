from pydantic import UUID4

from annotation_service.models.v3.annotation.classes import (
    Class,
    Classes,
    ClassList,
    ClassQuery,
    PostClass,
    PostClasses,
)
from annotation_service.models.v3.commons import IdObject, Message, UniqueReferences

from .db import db_clients


def add_routes_classes(app, late_init, api_v3_integration):
    @app.get(
        "/classes",
        tags=["Classes"],
        summary="Returns all classes",
        responses={
            200: {"model": ClassList},
        },
    )
    async def _(
        with_unique_class_values: bool = False,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns all classes.

        The count is not affected by `limit`.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.aclass.get_all(
                with_unique_class_values=with_unique_class_values, skip=skip, limit=limit
            )

    @app.post(
        "/classes",
        tags=["Classes"],
        summary="Creates new class / classes",
        status_code=201,
        responses={201: {"model": Classes}},
    )
    async def _(classes: PostClasses, external_ids: bool = False, payload=api_v3_integration.global_depends()):
        """
        Creates new class / classes.

        If `external_ids` is `True`, all items must contain a valid id of type `UUID4`
        (`ANNOT_ALLOW_EXTERNAL_IDS` must also be set to `True` in the `.env` file).
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.aclass.add_classes(classes=classes, external_ids=external_ids)

    @app.put(
        "/classes/query",
        tags=["Classes"],
        summary="Returns a list of filtered classes",
        status_code=200,
        responses={200: {"model": ClassList}},
    )
    async def _(
        class_query: ClassQuery,
        with_unique_class_values: bool = False,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a count and a list of classes matching given filter criteria.

        The count is not affected by `limit`.

        Classes can be filtered by:
        - class ids
        - creators
        - references (annotation IDs)
        - jobs the classes are locked for

        If `with_unique_class_values` is `True`, a list of unique values of all returned classes is part of response.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.aclass.query(
                class_query=class_query, with_unique_class_values=with_unique_class_values, skip=skip, limit=limit
            )

    @app.put(
        "/classes/query/unique-references",
        tags=["Classes"],
        summary="Returns a list of unique references for classes matching filter criteria",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        class_query: ClassQuery,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a list of unique references for classes matching filter criteria

        Classes can be filtered by:
        - creators
        - references (annotation IDs)
        - jobs the classes are locked for
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.aclass.unique_references_query(class_query=class_query)

    @app.get(
        "/classes/{class_id}",
        tags=["Classes"],
        summary="Returns a class",
        responses={
            200: {"model": Class},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(class_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Returns the specified class.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.aclass.get(class_id=class_id)

    @app.put(
        "/classes/{class_id}",
        tags=["Classes"],
        summary="Updates a class",
        responses={
            200: {"model": Class},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Class is locked"},
        },
    )
    async def _(class_id: UUID4, put_class: PostClass, payload=api_v3_integration.global_depends()):
        """
        Updates the specified class.

        The updated class is returned.

        **A locked class can not be updated!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.aclass.update(class_id=class_id, put_class=put_class)

    @app.delete(
        "/classes/{class_id}",
        tags=["Classes"],
        summary="Deletes a class",
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Class is locked"},
        },
    )
    async def _(class_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Deletes the specified class.

        The class ID is returned.

        **A locked class can not be deleted!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.aclass.delete(class_id=class_id)
