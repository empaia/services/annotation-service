from .annotation_routes import add_routes_annotations
from .class_routes import add_routes_classes
from .collection_routes import add_routes_collections
from .job_routes import add_routes_jobs
from .pixelmap_routes import add_routes_pixelmaps
from .primitive_routes import add_routes_primitives
from .private_routes import add_routes_private
from .server_routes import add_routes_misc


def add_routes_v3(app, late_init, api_v3_integration):
    add_routes_annotations(app, late_init, api_v3_integration)
    add_routes_classes(app, late_init, api_v3_integration)
    add_routes_collections(app, late_init, api_v3_integration)
    add_routes_primitives(app, late_init, api_v3_integration)
    add_routes_pixelmaps(app, late_init, api_v3_integration)
    add_routes_jobs(app, late_init, api_v3_integration)
    add_routes_misc(app, late_init, api_v3_integration)


def add_private_routes_v3(app, late_init, api_v3_integration):
    add_routes_private(app, late_init, api_v3_integration)
