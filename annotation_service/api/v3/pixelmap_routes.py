from typing import Annotated

from fastapi import Header, Request
from fastapi.responses import StreamingResponse
from pydantic import UUID4

from annotation_service.models.v3.annotation.pixelmaps import (
    Pixelmap,
    PixelmapList,
    PixelmapQuery,
    Pixelmaps,
    PixelmapSlideInfo,
    PostPixelmaps,
    UpdatePixelmap,
)
from annotation_service.models.v3.commons import IdObject, Message, UniqueReferences

from .db import db_clients

PixelmapTileResponse = {
    200: {
        "content": {
            "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
        },
        "description": "Pixelmap tile response.",
    },
    404: {"model": Message, "description": "Pixelmap not found"},
}


def add_routes_pixelmaps(app, late_init, api_v3_integration):
    @app.post(
        "/pixelmaps",
        tags=["Pixelmaps"],
        summary="Creates a new pixelmap",
        status_code=201,
        responses={201: {"model": Pixelmaps}},
    )
    async def _(pixelmaps: PostPixelmaps, external_ids: bool = False, payload=api_v3_integration.global_depends()):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.add_pixelmaps(pixelmaps, external_ids)

    @app.put(
        "/pixelmaps/query",
        tags=["Pixelmaps"],
        summary="Returns a list of filtered pixelmaps",
        status_code=200,
        responses={200: {"model": PixelmapList}},
    )
    async def _(
        pixelmap_query: PixelmapQuery,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a count and a list of pixelmaps matching given filter criteria.

        The count is not affected by `limit`.

        Pixelmaps can be filtered by:
        - pixelmap ids
        - creators
        - references
        - jobs the pixelmaps are locked for
        - types
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.query(pixelmap_query=pixelmap_query, skip=skip, limit=limit)

    @app.put(
        "/pixelmaps/query/unique-references",
        tags=["Pixelmaps"],
        summary="Returns a list of unique references for pixelmaps matching filter criteria",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(pixelmap_query: PixelmapQuery, payload=api_v3_integration.global_depends()):
        """
        Returns a list of unique references for pixelmaps matching filter criteria.

        Primitives can be filtered by:
        - pixelmap ids
        - creators
        - references
        - jobs the pixelmaps are locked for
        - types
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.unique_references_query(pixelmap_query=pixelmap_query)

    @app.get(
        "/pixelmaps/{pixelmap_id}",
        tags=["Pixelmaps"],
        summary="Returns a Pixelmap",
        status_code=200,
        responses={200: {"model": Pixelmap}, 404: {"model": Message, "description": "Pixelmap not found"}},
    )
    async def _(pixelmap_id: UUID4, payload=api_v3_integration.global_depends()):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.get(pixelmap_id)

    @app.put(
        "/pixelmaps/{pixelmap_id}",
        tags=["Pixelmaps"],
        summary="Update a Pixelmap",
        status_code=200,
        responses={
            200: {"model": Pixelmap},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: UUID4,
        update_pixelmap: UpdatePixelmap,
        payload=api_v3_integration.global_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.update(pixelmap_id, update_pixelmap)

    @app.delete(
        "/pixelmaps/{pixelmap_id}",
        tags=["Pixelmaps"],
        summary="Deletes the pixelmap",
        status_code=200,
        responses={
            200: {"model": IdObject},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(pixelmap_id: UUID4, payload=api_v3_integration.global_depends()):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.delete(pixelmap_id)

    @app.get(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["Pixelmaps"],
        summary="Get the data for a tile of a pixelmap",
        status_code=200,
        responses=PixelmapTileResponse,
        response_class=StreamingResponse,
    )
    async def _(
        pixelmap_id: UUID4,
        level: int,
        tile_x: int,
        tile_y: int,
        accept_encoding: Annotated[str | None, Header()] = None,
        payload=api_v3_integration.global_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.get_tile(str(pixelmap_id), level, tile_x, tile_y, accept_encoding)

    @app.put(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["Pixelmaps"],
        summary="Uploads data for a tile of a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: UUID4,
        level: int,
        tile_x: int,
        tile_y: int,
        request: Request,
        content_encoding: Annotated[str | None, Header()] = None,
        payload=api_v3_integration.global_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            data = await request.body()
            return await clients.pixelmap.add_tile(pixelmap_id, level, tile_x, tile_y, content_encoding, data)

    @app.delete(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["Pixelmaps"],
        summary="Delete the data for a tile of a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(pixelmap_id: UUID4, level: int, tile_x: int, tile_y: int, payload=api_v3_integration.global_depends()):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.delete_tile(pixelmap_id, level, tile_x, tile_y)

    @app.get(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        tags=["Pixelmaps"],
        summary="Bulk tile retrieval for a pixelmap",
        status_code=200,
        responses={
            200: {
                "content": {
                    "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
                },
                "description": "Pixelmap tile response.",
            },
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Requested data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
        },
    )
    async def _(
        pixelmap_id: UUID4,
        level: int,
        start_x: int,
        start_y: int,
        end_x: int,
        end_y: int,
        accept_encoding: Annotated[str | None, Header()] = None,
        payload=api_v3_integration.global_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.get_tiles(
                pixelmap_id,
                level,
                start_x,
                start_y,
                end_x,
                end_y,
                accept_encoding,
            )

    @app.put(
        "/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        tags=["Pixelmaps"],
        summary="Bulk tile upload for a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Posted data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: UUID4,
        level: int,
        start_x: int,
        start_y: int,
        end_x: int,
        end_y: int,
        request: Request,
        content_encoding: Annotated[str | None, Header()] = None,
        payload=api_v3_integration.global_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            data = await request.body()
            return await clients.pixelmap.add_tiles(
                pixelmap_id, level, start_x, start_y, end_x, end_y, content_encoding, data
            )

    @app.get(
        "/pixelmaps/{pixelmap_id}/info",
        tags=["Pixelmaps"],
        summary="Get level and resolution of the referenced slide",
        status_code=200,
        responses={
            200: {"model": PixelmapSlideInfo},
            404: {"model": Message, "description": "Pixelmap not found or info not available"},
        },
    )
    async def _(
        pixelmap_id: UUID4,
        payload=api_v3_integration.global_depends(),
    ):
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.pixelmap.get_info(pixelmap_id)
