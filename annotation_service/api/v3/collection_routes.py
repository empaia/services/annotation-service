from fastapi import HTTPException
from pydantic import UUID4

from annotation_service.models.v3.annotation.collections import (
    Collection,
    CollectionQuery,
    ItemPostResponse,
    ItemQuery,
    ItemQueryList,
    PostCollection,
    PostItems,
)
from annotation_service.models.v3.commons import Id, IdObject, Message, UniqueReferences

from .db import db_clients


def add_routes_collections(app, late_init, api_v3_integration):
    @app.get(
        "/collections",
        tags=["Deprecated"],
        summary="Returns all collections",
        responses={
            501: {
                "model": Message,
                "description": "No longer supported",
            },
        },
    )
    async def _(skip: int = None, limit: int = None, payload=api_v3_integration.global_depends()):
        """
        **ENDPOINT IS DEPRECATED**

        <s>Returns all collections.

        The count is not affected by `limit`.</s>
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.get_all(skip=skip, limit=limit)

    @app.post(
        "/collections",
        tags=["Collections"],
        summary="Creates new collection / collections",
        status_code=201,
        responses={201: {"model": Collection}},
    )
    async def _(collection: PostCollection, external_ids: bool = False, payload=api_v3_integration.global_depends()):
        """
        Creates new collection / collections.

        If `external_ids` is `True`, all items must contain a valid id of type `UUID4`
        (`ANNOT_ALLOW_EXTERNAL_IDS` must also be set to `True` in the `.env` file).
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.add_collection(
                collection=collection, external_ids=external_ids, clients=clients
            )

    @app.put(
        "/collections/query",
        tags=["Deprecated"],
        summary="Returns a list of filtered collections",
        responses={
            501: {
                "model": Message,
                "description": "No longer supported",
            },
        },
    )
    async def _(
        collection_query: CollectionQuery,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        **ENDPOINT IS DEPRECATED**

        <s>Returns a count and a list of collections matching given filter criteria.

        The count is not affected by `limit`.

        Collections can be filtered by:
        - creators
        - references
        - jobs the collections are locked for
        - item types</s>
        """
        raise HTTPException(501, "Requesting all collections is no longer supported!")
        # async with late_init.pool.acquire() as conn:
        #    clients = await db_clients(conn=conn)
        #    return await clients.collection.query(collection_query=collection_query, skip=skip, limit=limit)

    @app.put(
        "/collections/query/unique-references",
        tags=["Collections"],
        summary="Returns a list of unique references for collections matching filter criteria",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(collection_query: CollectionQuery, payload=api_v3_integration.global_depends()):
        """
        Returns a list of unique references for collections matching filter criteria.

        Collections can be filtered by:
        - creators
        - references
        - jobs the collections are locked for
        - item types
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.unique_references_query(collection_query=collection_query)

    @app.get(
        "/collections/{collection_id}",
        tags=["Collections"],
        summary="Returns a collection",
        responses={
            200: {"model": Collection},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        collection_id: UUID4,
        shallow: bool = False,
        with_leaf_ids: bool = False,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns the specified collection.

        If `shallow` is `True`, the collection is returned without leafs.

        If `with_leaf_ids` is `True` collections (except when item type is `collection`)
        contain a list with item ids in the property `item_ids`.

        Leafs are items of all item types except collection. Nested collections are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.get(
                collection_id=collection_id, shallow=shallow, with_leaf_ids=with_leaf_ids, clients=clients
            )

    @app.put(
        "/collections/{collection_id}",
        tags=["Collections"],
        summary="Updates a collection",
        responses={
            200: {"model": Collection},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Collection is locked"},
        },
    )
    async def _(
        collection_id: UUID4,
        collection: Collection,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Updates the specified collection.

        The updated collection is returned.

        **A locked collection can not be updated!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.update(collection_id=collection_id, collection=collection)

    @app.delete(
        "/collections/{collection_id}",
        tags=["Collections"],
        summary="Deletes a collection",
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            423: {"model": Message, "description": "Collection is locked"},
        },
    )
    async def _(collection_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Deletes the specified collection.

        The collection ID is returned.

        **A locked collection can not be deleted!**

        **The items of the collection are not deleted!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.delete(collection_id=collection_id)

    @app.post(
        "/collections/{collection_id}/items",
        tags=["Collections"],
        summary="Adds items to a collection",
        status_code=201,
        responses={
            201: {"model": ItemPostResponse},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            405: {"model": Message, "description": "Resource is locked"},
        },
    )
    async def _(
        collection_id: UUID4,
        items: PostItems,
        external_ids: bool = False,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Adds new item / items to specified collection.

        **The items must match the item type of the collection!**

        Possibilities:
        - single item
            - a new item &rarr; will be created
            - an existing item by ID
        - list of items
            - list of new items &rarr; will be created
            - list of existing items by IDs

        **Items can not be added to a locked collection!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.add_items(
                collection_id=collection_id, items=items, clients=clients, external_ids=external_ids
            )

    @app.put(
        "/collections/{collection_id}/items/query",
        tags=["Collections"],
        summary="Returns a list of filtered collection items",
        status_code=200,
        responses={200: {"model": ItemQueryList}},
    )
    async def _(
        collection_id: UUID4,
        item_query: ItemQuery,
        skip: int = None,
        limit: int = None,
        payload=api_v3_integration.global_depends(),
    ):
        """
        Returns a count and a list of collection items matching given filter criteria.

        The count is not affected by `limit`.

        Collection items can be filtered by:
        - references

        If the item type is annotation, additional filters can be used:
        - viewport &rarr; a rectangle the annotations must be inside
        - npp_viewing &rarr; resolution range in npp (nanometer per pixel)
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.query_items(
                collection_id=collection_id, item_query=item_query, clients=clients, skip=skip, limit=limit
            )

    @app.put(
        "/collections/{collection_id}/items/query/unique-references",
        tags=["Collections"],
        summary="Returns a list of unique references for collection items matching filter criteria",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(collection_id: UUID4, item_query: ItemQuery, payload=api_v3_integration.global_depends()):
        """
        Returns a list of unique references for collection items matching filter criteria.

        Collection items can be filtered by:
        - references

        If the item type is annotation, additional filters can be used:
        - viewport &rarr; a rectangle the annotations must be inside
        - npp_viewing &rarr; resolution range in npp (nanometer per pixel)
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.unique_references_query_items(
                collection_id=collection_id, item_query=item_query, clients=clients
            )

    @app.delete(
        "/collections/{collection_id}/items/{item_id}",
        tags=["Collections"],
        summary="Removes an item from collection",
        responses={
            200: {"model": IdObject},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
            405: {"model": Message, "description": "Resource is locked"},
        },
    )
    async def _(collection_id: UUID4, item_id: Id, payload=api_v3_integration.global_depends()):
        """
        Removes the specified item from the collection.

        The item ID is returned.

        **The item is not deleted!"

        **Items can not be removed from a locked collection!**
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.collection.delete_item(collection_id=collection_id, item_id=item_id, clients=clients)
