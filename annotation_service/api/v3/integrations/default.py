from fastapi import Depends, HTTPException, status


def _unauthorized():
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Integration not configured",
    )


class Default:
    def __init__(self, settings, logger):
        self.settings = settings
        self.logger = logger

    def global_depends(self):
        return Depends(_unauthorized)
