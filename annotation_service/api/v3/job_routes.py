from pydantic import UUID4

from annotation_service.models.v3.annotation.jobs import JobLock
from annotation_service.models.v3.commons import Id, Message

from .db import db_clients


def add_routes_jobs(app, late_init, api_v3_integration):
    @app.put(
        "/jobs/{job_id}/lock/annotations/{annotation_id}",
        tags=["Jobs"],
        summary="Locks annotation for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, annotation_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Locks the specified annotation for given job ID.

        The annotation and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_annotation(job_id=job_id, annotation_id=annotation_id)

    @app.put(
        "/jobs/{job_id}/lock/classes/{class_id}",
        tags=["Jobs"],
        summary="Locks class for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, class_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Locks the specified class for given job ID.

        The class and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_class(job_id=job_id, class_id=class_id)

    @app.put(
        "/jobs/{job_id}/lock/collections/{collection_id}",
        tags=["Jobs"],
        summary="Locks collection for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, collection_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Locks the specified collection for given job ID.

        **Items in the collection are recursivly locked for the job. Works also for nested collections.**

        The collection and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_collection(job_id=job_id, collection_id=collection_id)

    @app.put(
        "/jobs/{job_id}/lock/primitives/{primitive_id}",
        tags=["Jobs"],
        summary="Locks primitive for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, primitive_id: UUID4, payload=api_v3_integration.global_depends()):
        """
        Locks the specified primitive for given job ID.

        The primitive and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_primitive(job_id=job_id, primitive_id=primitive_id)

    @app.put(
        "/jobs/{job_id}/lock/slides/{slide_id}",
        tags=["Jobs"],
        summary="Locks slide for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, slide_id: Id, payload=api_v3_integration.global_depends()):
        """
        Locks the specified slide for given job ID.

        The slide and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_slide(job_id=job_id, slide_id=slide_id)

    @app.put(
        "/jobs/{job_id}/lock/pixelmaps/{pixelmap_id}",
        tags=["Jobs"],
        summary="Locks pixelmap for job",
        responses={
            200: {"model": JobLock},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(job_id: Id, pixelmap_id: Id, payload=api_v3_integration.global_depends()):
        """
        Locks the specified pixelmap for given job ID.

        The pixelmap and job ID are returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.lock_pixelmap(job_id=job_id, pixelmap_id=pixelmap_id)
