from asyncpg.exceptions import PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
from pydantic import UUID4

from annotation_service.api.v3.clients.utils import job_utils as utils
from annotation_service.models.v3.commons import Id
from annotation_service.singletons import logger


class JobClient:
    def __init__(self, conn):
        self.conn = conn

    async def lock_annotation(self, job_id: Id, annotation_id: UUID4):
        job_lock_sql = """
        INSERT INTO
            v3_j_job_classes(
                job_id,
                class_id,
                annot_id,
                value
            )
            VALUES ($1, '', $2, '');
        """

        logger.debug(job_lock_sql)

        try:
            await self.conn.execute(job_lock_sql, job_id, annotation_id)
            await self.conn.execute(f"SELECT v3_clean_up_class_locking_table('{job_id}')")
            return JSONResponse({"item_id": str(annotation_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(annotation_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation can not be locked") from e

    async def lock_class(self, job_id: Id, class_id: UUID4):
        job_lock_sql = """
        INSERT INTO
            v3_j_job_classes(job_id, class_id, annot_id, value) (
                SELECT
                    $1,
                    id,
                    reference_id,
                    value
                FROM
                    v3_cl_classes
                WHERE
                    id = $2
            );
        """

        logger.debug(job_lock_sql)

        try:
            await self.conn.execute(job_lock_sql, job_id, class_id)
            await self.conn.execute(f"SELECT v3_clean_up_class_locking_table('{job_id}')")
            return JSONResponse({"item_id": str(class_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(class_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class can not be locked") from e

    async def lock_primitive(self, job_id: Id, primitive_id: UUID4):
        job_lock_sql = """
        INSERT INTO
            v3_j_job_primitives(
                job_id,
                primitive_id
            )
            VALUES ($1, $2);
        """

        logger.debug(job_lock_sql)

        try:
            await self.conn.execute(job_lock_sql, job_id, primitive_id)
            return JSONResponse({"item_id": str(primitive_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(primitive_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive can not be locked") from e

    async def lock_slide(self, job_id: Id, slide_id: Id):
        job_lock_sql = """
        INSERT INTO
            v3_j_job_slides(
                job_id,
                slide_id
            )
            VALUES ($1, $2);
        """

        logger.debug(job_lock_sql)

        try:
            await self.conn.execute(job_lock_sql, job_id, slide_id)
            return JSONResponse({"item_id": str(slide_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(slide_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Slide can not be locked") from e

    async def lock_pixelmap(self, job_id: Id, pixelmap_id: UUID4):
        job_lock_sql = """
        INSERT INTO
            v3_j_job_pixelmaps(
                job_id,
                pixelmap_id
            )
            VALUES ($1, $2);
        """

        logger.debug(job_lock_sql)

        try:
            await self.conn.execute(job_lock_sql, job_id, pixelmap_id)
            return JSONResponse({"item_id": str(pixelmap_id), "job_id": str(job_id)})
        except UniqueViolationError:
            return JSONResponse({"item_id": str(pixelmap_id), "job_id": str(job_id)})
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmap can not be locked") from e

    async def lock_collection(self, job_id: Id, collection_id: UUID4):
        collections_for_locking = await utils.get_all_collections_for_locking(self.conn, collection_id)

        job_data = [(job_id, collection[0], collection[1]) for collection in collections_for_locking]

        try:
            await self.conn.copy_records_to_table("v3_j_job_collections", records=job_data)
        except UniqueViolationError as e:
            raise HTTPException(400, f"At least one collection is already locked for job '{job_id}'") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections can not be locked (collection lock)") from e

        return JSONResponse({"item_id": str(collection_id), "job_id": str(job_id)})

    async def aggregate_job(self, job_id: Id, clients):
        # aggregate annotations
        await clients.annotation.create_materialized_view(job_id)

        # aggregate classes
        await clients.aclass.create_materialized_view(job_id)

        # aggregate primitives
        await clients.primitive.create_materialized_view(job_id)

        # aggregate pixelmaps
        await clients.pixelmap.create_materialized_view(job_id)

        # aggregate collections
        await clients.collection.create_materialized_view(job_id)

        return {"id": job_id}
