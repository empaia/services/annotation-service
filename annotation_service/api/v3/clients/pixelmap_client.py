from typing import List
from uuid import uuid4

import aiohttp
from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse, Response
from pydantic import UUID4

from annotation_service.models.v3.annotation.pixelmaps import (
    PixelmapQuery,
    PostPixelmap,
    PostPixelmaps,
    UpdateContinuousPixelmap,
    UpdateDiscretePixelmap,
    UpdateNominalPixelmap,
    UpdatePixelmap,
)

from ....singletons import logger, pixelmap_cache, pixelmapd, settings
from .helpers.pixelmaps import sql_helper_get, sql_helper_insert, sql_helper_query
from .helpers.sql_helper import get_existing_views_sql
from .utils import pixelmap_utils as utils

PIXELMAP_MAT_VIEW_BASE_NAME = "mat_view_pixelmaps"

AIOHTTP_TIMEOUT = 300
AIOHTTP_CONNECTION_LIMIT_PER_HOST = 100


class PixelmapClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_pixelmaps(
        self, pixelmaps: PostPixelmaps, external_ids: bool = False, raw: bool = False, collection_id: UUID4 = None
    ):
        if isinstance(pixelmaps, PostPixelmap.__args__):
            return await self.create_pixelmaps([pixelmaps], external_ids, raw, collection_id)
        elif len(pixelmaps.items) > 0:
            if len(pixelmaps.items) > settings.v3_post_limit:
                raise HTTPException(
                    413,
                    (
                        f"Number of posted items ({len(pixelmaps.items)})"
                        f" exceeds server limit of {settings.v3_post_limit}."
                    ),
                )
            return await self.create_pixelmaps(pixelmaps.items, external_ids, raw, collection_id)

        raise HTTPException(405, "No valid post data")

    async def create_pixelmaps(
        self,
        pixelmaps: List[PostPixelmap],
        external_ids: bool = False,
        raw: bool = False,
        collection_id: UUID4 = None,
    ):
        # create random name for temporary table
        temp_table_name = f"tmp_{uuid4().hex}"

        allow_external_ids = settings.allow_external_ids and external_ids

        columns, insert_sql = await sql_helper_insert.get_insert_sql(allow_external_ids, temp_table_name)
        logger.debug(insert_sql)

        new_pixelmaps = await utils.format_post_data(pixelmaps, allow_external_ids, collection_id)

        alter_table_sql = f"""
        ALTER TABLE {temp_table_name} DROP COLUMN created_at;
        ALTER TABLE {temp_table_name} DROP COLUMN updated_at;
        """

        if not allow_external_ids:
            alter_table_sql += f"\nALTER TABLE {temp_table_name} DROP COLUMN id;"

        try:
            async with self.conn.transaction():
                await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")
                await self.conn.execute(
                    f"CREATE UNLOGGED TABLE IF NOT EXISTS {temp_table_name} (LIKE v3_pm_pixelmaps);"
                )
                await self.conn.execute(f"TRUNCATE {temp_table_name};")
                await self.conn.execute(alter_table_sql)
                await self.conn.copy_records_to_table(temp_table_name, records=new_pixelmaps, columns=columns)
                result = await self.conn.fetch(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Pixelmap with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmaps could not be created") from e
        finally:
            await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")

        if result is None:
            raise HTTPException(500, "Pixelmaps could not be created (result None)")

        items = [s[0] for s in result]

        for item in items:
            pixelmap_cache.add(item["id"], item)

        if raw:
            return len(items), items
        if len(items) == 1:
            return JSONResponse(items[0], status_code=201)
        return JSONResponse({"item_count": len(items), "items": items}, status_code=201)

    async def get(self, pixelmap_id: UUID4):
        sql = await sql_helper_get.get_single_sql()
        logger.debug(sql)

        try:
            result = await self.conn.fetchrow(sql, pixelmap_id)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'pixelmaps' must be of type UUID4.") from e
            raise HTTPException(500, "Pixelmaps could not be queried (get)") from e

        if not result:
            raise HTTPException(404, f"No pixelmap with id '{pixelmap_id}' found")
        return result["pixelmap"]

    async def get_info(self, pixelmap_id: UUID4):
        if not settings.cds_url:
            raise HTTPException(404, "Pixelmap not found or info not available")

        sql = """
        SELECT info
        FROM public.v3_pm_info
        WHERE id=$1;
        """
        logger.debug(sql)
        row = await self.conn.fetchrow(sql, str(pixelmap_id))
        if row is not None:
            info = row["info"]
            return JSONResponse(info, status_code=200)

        cds_url = settings.cds_url.rstrip("/")
        pixelmap = await self._get_pixelmap_from_cache(pixelmap_id=pixelmap_id)
        slide_id = pixelmap["reference_id"]
        slide_info_url = f"{cds_url}/v3/slides/{slide_id}/info"

        timeout = aiohttp.ClientTimeout(total=AIOHTTP_TIMEOUT)
        connector = aiohttp.TCPConnector(limit_per_host=AIOHTTP_CONNECTION_LIMIT_PER_HOST, force_close=True)
        session = aiohttp.ClientSession(timeout=timeout, connector=connector)

        async with session as client:
            async with client.get(slide_info_url) as response:
                if response.status != 200:
                    raise HTTPException(404, "Pixelmap not found or info not available")
                slide_info = await response.json()

        info = {
            "extent": slide_info["extent"],
            "num_levels": slide_info["num_levels"],
            "pixel_size_nm": slide_info["pixel_size_nm"],
            "levels": slide_info["levels"],
        }

        sql = """
        INSERT INTO public.v3_pm_info (id, info)
        VALUES ($1, $2);
        """
        logger.debug(sql)
        try:
            await self.conn.execute(sql, str(pixelmap_id), info)
        except UniqueViolationError:
            logger.info("UniqueViolationError for pixelmap info %s", str(pixelmap_id))

        return JSONResponse(info, status_code=200)

    async def query(
        self, pixelmap_query: PixelmapQuery, skip: int, limit: int, raw: bool = False, collection_id: UUID4 = None
    ):
        try:
            if not pixelmap_query.jobs:
                # if jobs not in query
                sql = await sql_helper_query.get_query_sql(raw)
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{PIXELMAP_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in pixelmap_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

                sql = await sql_helper_query.get_query_sql(raw, existing_views)

            logger.debug(sql)

            args = [
                pixelmap_query.references,
                pixelmap_query.creators,
                pixelmap_query.types,
                pixelmap_query.pixelmaps,
                [collection_id] if collection_id else None,
                limit,
                skip,
            ]

            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'pixelmaps' must be of type UUID4.") from e
            raise HTTPException(500, "Pixelmaps could not be queried (query)") from e

        if raw:
            if result is None:
                return 0, []
            return result["item_count"], result["items"]

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def unique_references_query(self, pixelmap_query: PixelmapQuery, collection_id: UUID4 = None):
        try:
            if not pixelmap_query.jobs:
                # if jobs not in query
                references_sql = await sql_helper_query.get_references_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{PIXELMAP_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in pixelmap_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(content={"wsi": []}, media_type="application/json")

                references_sql = await sql_helper_query.get_references_sql(existing_views)

            logger.debug(references_sql)

            args = [
                pixelmap_query.references,
                pixelmap_query.creators,
                pixelmap_query.types,
                pixelmap_query.pixelmaps,
                [collection_id] if collection_id else None,
            ]

            references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmaps could not be queried (pixelmap references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Pixelmaps cound not be queried (unique references None).")

    async def update(self, pixelmap_id: UUID4, pixelmap: UpdatePixelmap):
        await utils.check_pixelmap(self.conn, pixelmap_id, "update")

        update_sql = await sql_helper_insert.get_update_sql()
        logger.debug(update_sql)

        type_mapping = {
            UpdateContinuousPixelmap: "continuous_pixelmap",
            UpdateDiscretePixelmap: "discrete_pixelmap",
            UpdateNominalPixelmap: "nominal_pixelmap",
        }

        pixelmap_type = type_mapping[type(pixelmap)]

        min_value_int = (
            pixelmap.min_value if (pixelmap_type != "nominal_pixelmap" and type(pixelmap.min_value) is int) else None
        )
        min_value_float = (
            pixelmap.min_value if (pixelmap_type != "nominal_pixelmap" and type(pixelmap.min_value) is float) else None
        )
        neutral_value_int = pixelmap.neutral_value if type(pixelmap.neutral_value) is int else None
        neutral_value_float = pixelmap.neutral_value if type(pixelmap.neutral_value) is float else None
        max_value_int = (
            pixelmap.max_value if (pixelmap_type != "nominal_pixelmap" and type(pixelmap.max_value) is int) else None
        )
        max_value_float = (
            pixelmap.max_value if (pixelmap_type != "nominal_pixelmap" and type(pixelmap.max_value) is float) else None
        )
        element_class_mapping = (
            [cm.model_dump() for cm in pixelmap.channel_class_mapping]
            if pixelmap_type != "continuous_pixelmap"
            else None
        )

        try:
            await self.conn.execute(
                update_sql,
                pixelmap_id,
                pixelmap.name,
                pixelmap.description,
                [cm.model_dump() for cm in pixelmap.channel_class_mapping],
                min_value_int,
                min_value_float,
                neutral_value_int,
                neutral_value_float,
                max_value_int,
                max_value_float,
                element_class_mapping,
            )
            updated_pixelmap = await self.get(pixelmap_id)
            pixelmap_cache.update(str(pixelmap_id), updated_pixelmap)
            return updated_pixelmap
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmap could not be updated") from e

    async def delete(self, pixelmap_id: UUID4):
        await utils.check_pixelmap(self.conn, pixelmap_id, "lock")

        pixelmap = await self._get_pixelmap_from_cache(pixelmap_id)

        delete_sql = """
        WITH deleted AS (
            DELETE FROM
                v3_pm_pixelmaps
            WHERE
                id = $1
            RETURNING *
        )
        SELECT
            COUNT(*)
        FROM
            deleted;
        """

        try:
            async with self.conn.transaction():
                delete_count = await self.conn.fetchval(delete_sql, pixelmap_id)
                if delete_count > 0:
                    rep_msg = await pixelmapd.delete(
                        case_id=None,
                        pixelmap=pixelmap,
                    )
                    pixelmap_cache.delete(str(pixelmap_id))

                    if rep_msg["rep"] == "error":
                        raise HTTPException(rep_msg["status_code"], rep_msg["detail"])

                    return JSONResponse({"id": str(pixelmap_id)})
                else:
                    raise HTTPException(500, "Pixelmap could not be deleted (delete count 0)")
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmap could not be deleted") from e

    # TILES

    async def add_tile(self, pixelmap_id: UUID4, level: int, x: int, y: int, content_encoding: str, data: bytes):
        # check if locked
        await utils.check_pixelmap(self.conn, pixelmap_id, "lock")

        pixelmap = await self._get_pixelmap_from_cache(pixelmap_id)

        # check level and indexes
        pixelmap_level = await self._get_pixelmap_level(pixelmap, level)
        await self._check_level_and_indexes(level=pixelmap_level, start_x=x, start_y=y)

        # save tile
        rep_msg = await pixelmapd.create_tile(
            case_id=None,
            pixelmap=pixelmap,
            level=level,
            x=x,
            y=y,
            content_encoding=content_encoding,
            req_bytes=data,
        )

        if rep_msg["rep"] == "error":
            raise HTTPException(rep_msg["status_code"], rep_msg["detail"])

        return Response(status_code=204)

    async def add_tiles(
        self,
        pixelmap_id: UUID4,
        level: int,
        start_x: int,
        start_y: int,
        end_x: int,
        end_y: int,
        content_encoding: str,
        data: bytes,
    ):
        # check if locked
        await utils.check_pixelmap(self.conn, pixelmap_id, "lock")

        pixelmap = await self._get_pixelmap_from_cache(pixelmap_id)

        # check level and indexes
        pixelmap_level = await self._get_pixelmap_level(pixelmap, level)
        tile_count = (end_x - start_x + 1) * (end_y - start_y + 1)
        await self._check_level_and_indexes(
            level=pixelmap_level, start_x=start_x, start_y=start_y, end_x=end_x, end_y=end_y, tile_count=tile_count
        )

        rep_msg = await pixelmapd.create_tiles(
            case_id=None,
            pixelmap=pixelmap,
            level=level,
            start_x=start_x,
            start_y=start_y,
            end_x=end_x,
            end_y=end_y,
            content_encoding=content_encoding,
            req_bytes=data,
        )

        if rep_msg["rep"] == "error":
            raise HTTPException(rep_msg["status_code"], rep_msg["detail"])

        return Response(status_code=204)

    async def get_tile(self, pixelmap_id: UUID4, level: int, x: int, y: int, accept_encoding: str):
        pixelmap = await self._get_pixelmap_from_cache(pixelmap_id)

        rep_msg, rep_bytes = await pixelmapd.get_tile(
            case_id=None,
            pixelmap=pixelmap,
            level=level,
            x=x,
            y=y,
            accept_encoding=accept_encoding,
        )

        if rep_msg["rep"] == "error":
            raise HTTPException(rep_msg["status_code"], rep_msg["detail"])

        headers = None
        content_encoding = rep_msg.get("content_encoding")
        if content_encoding:
            headers = {"Content-Encoding": content_encoding}

        return Response(rep_bytes, media_type="application/octet-stream", headers=headers)

    async def get_tiles(
        self, pixelmap_id: UUID4, level: int, start_x: int, start_y: int, end_x: int, end_y: int, accept_encoding: str
    ):
        pixelmap = await self._get_pixelmap_from_cache(pixelmap_id)

        rep_msg, rep_bytes = await pixelmapd.get_tiles(
            case_id=None,
            pixelmap=pixelmap,
            level=level,
            start_x=start_x,
            start_y=start_y,
            end_x=end_x,
            end_y=end_y,
            accept_encoding=accept_encoding,
        )

        if rep_msg["rep"] == "error":
            raise HTTPException(rep_msg["status_code"], rep_msg["detail"])

        headers = None
        content_encoding = rep_msg.get("content_encoding")
        if content_encoding:
            headers = {"Content-Encoding": content_encoding}

        return Response(rep_bytes, media_type="application/octet-stream", headers=headers)

    async def delete_tile(self, pixelmap_id: UUID4, level: int, x: int, y: int):
        # check if locked
        await utils.check_pixelmap(self.conn, pixelmap_id, "lock")

        pixelmap = await self._get_pixelmap_from_cache(pixelmap_id)

        rep_msg = await pixelmapd.delete_tile(
            case_id=None,
            pixelmap=pixelmap,
            level=level,
            x=x,
            y=y,
        )

        if rep_msg["rep"] == "error":
            raise HTTPException(rep_msg["status_code"], rep_msg["detail"])

        return Response(status_code=204)

    async def _get_pixelmap_from_cache(self, pixelmap_id: UUID4):
        # check cache, if not in cache get pixelmap and add to cache
        cache_value = pixelmap_cache.get(str(pixelmap_id))
        if cache_value:
            pixelmap = cache_value
        else:
            pixelmap = await self.get(pixelmap_id)
            pixelmap_cache.add(str(pixelmap_id), pixelmap)
        return pixelmap

    async def _get_pixelmap_level(self, pixelmap: dict, level: int):
        levels = pixelmap["levels"]
        level_exists = list(filter(lambda x: x["slide_level"] == level, levels))

        if not level_exists:
            raise HTTPException(status_code=422, detail=f"The given Pixelmap level isn't valid (Level: {level}))")
        level = level_exists[0]
        return level

    async def _check_level_and_indexes(
        self,
        level: dict,
        start_x: int,
        start_y: int,
        end_x: int = None,
        end_y: int = None,
        tile_count: int = None,
    ):
        single_error_msg = "Given tile position not within level boundary."
        bulk_error_msg = "Given tile postions not within level boundary."

        # check range and tile position(s)
        if end_x and end_y:
            if start_x > end_x or start_y > end_y:
                raise HTTPException(status_code=422, detail="The given range is not valid (start must be <= end)")

            if tile_count > settings.v3_pixelmaps_post_limit:
                raise HTTPException(
                    413,
                    (
                        f"Number of posted tiles ({tile_count})"
                        f" exceeds server limit of {settings.v3_pixelmaps_post_limit}."
                    ),
                )
            await self._check_bulk_tile_positions(
                level=level, start_x=start_x, start_y=start_y, end_x=end_x, end_y=end_y, msg=bulk_error_msg
            )
        else:
            await self._check_single_tile_position(level=level, x=start_x, y=start_y, msg=single_error_msg)

    async def _check_single_tile_position(self, level: dict, x: int, y: int, msg: str):
        position_min_x = level.get("position_min_x")
        position_max_x = level.get("position_max_x")
        if (position_min_x and position_min_x > x) or (position_max_x and x > position_max_x):
            raise HTTPException(status_code=422, detail=msg)

        position_min_y = level.get("position_min_y")
        position_max_y = level.get("position_max_y")
        if (position_min_y and position_min_y > y) or (position_max_y and y > position_max_y):
            raise HTTPException(status_code=422, detail=msg)

    async def _check_bulk_tile_positions(
        self, level: dict, start_x: int, start_y: int, end_x: int, end_y: int, msg: str
    ):
        position_min_x = level.get("position_min_x")
        position_max_x = level.get("position_max_x")
        if (position_min_x and position_min_x > start_x) or (position_max_x and end_x > position_max_x):
            raise HTTPException(status_code=422, detail=msg)

        position_min_y = level.get("position_min_y")
        position_max_y = level.get("position_max_y")
        if (position_min_y and position_min_y > start_y) or (position_max_y and end_y > position_max_y):
            raise HTTPException(status_code=422, detail=msg)

    async def get_collection_items(self, collection_id: UUID4):
        sql = """
        SELECT
            id::TEXT
        FROM
            v3_pm_pixelmaps
        WHERE
            collection_ids && ARRAY[$1]::uuid[]
        """

        try:
            items = await self.conn.fetch(sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmap items could not be queried") from e

        return [item["id"] for item in items]

    async def update_collection_id(self, collection_id: UUID4, ids: List[UUID4]):
        sql = """
        WITH rows AS (
            UPDATE
                v3_pm_pixelmaps
            SET
                collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
            WHERE
                id = ANY($2::uuid[])
            RETURNING 1
        )
        SELECT count(*) FROM rows;
        """

        try:
            return await self.conn.fetchval(sql, collection_id, ids)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmap items could not be updated") from e

    async def remove_from_collection(self, pixelmap_id: UUID4, collection_id: UUID4):
        sql = """
        WITH ROWS AS (
            UPDATE
                v3_pm_pixelmaps
            SET
                collection_ids = ARRAY_REMOVE(collection_ids, $2::uuid)
            WHERE
                id = $1::uuid
                AND collection_ids && ARRAY[$2]::uuid[]
            RETURNING 1
        )
        SELECT
            count(*)
        FROM
            ROWS;
        """

        try:
            return await self.conn.fetchval(sql, pixelmap_id, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Pixelmap item could not be removed from Collection") from e

    async def create_materialized_view(self, job_id: str):
        # create view name
        view_name = f"{PIXELMAP_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}"

        try:
            await self.conn.execute("CALL create_pixelmap_view($1, $2)", job_id, view_name)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"Pixelmap view could not be created: {e}") from e
