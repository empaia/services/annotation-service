from typing import List

from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v3.annotation.primitives import PostPrimitive, Primitive
from annotation_service.singletons import logger


async def format_post_data(primitives: List[Primitive], allow_external_ids: bool, collection_id: UUID4 = None):
    p_type = primitives[0].type

    format_mapping = {
        "integer": _format_integer,
        "float": _format_float,
        "bool": _format_bool,
        "string": _format_string,
    }

    collection_ids = [collection_id] if collection_id else []

    return await format_mapping[p_type](primitives, allow_external_ids, collection_ids)


async def _format_integer(primitives: List[Primitive], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                p.id,
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                p.value,
                None,
                None,
                None,
                collection_ids,
            ]
            for p in primitives
        ]
    else:
        return [
            [
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                p.value,
                None,
                None,
                None,
                collection_ids,
            ]
            for p in primitives
        ]


async def _format_float(primitives: List[Primitive], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                p.id,
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                None,
                p.value,
                None,
                None,
                collection_ids,
            ]
            for p in primitives
        ]
    else:
        return [
            [
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                None,
                p.value,
                None,
                None,
                collection_ids,
            ]
            for p in primitives
        ]


async def _format_bool(primitives: List[Primitive], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                p.id,
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                None,
                None,
                p.value,
                None,
                collection_ids,
            ]
            for p in primitives
        ]
    else:
        return [
            [
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                None,
                None,
                p.value,
                None,
                collection_ids,
            ]
            for p in primitives
        ]


async def _format_string(primitives: List[Primitive], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                p.id,
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                None,
                None,
                None,
                p.value,
                collection_ids,
            ]
            for p in primitives
        ]
    else:
        return [
            [
                p.type,
                p.name,
                p.description if p.description is not None else None,
                p.creator_id,
                p.creator_type.value,
                p.reference_id if p.reference_id is not None else None,
                p.reference_type.value if p.reference_type is not None else None,
                False,
                None,
                None,
                None,
                p.value,
                collection_ids,
            ]
            for p in primitives
        ]


async def check_primitive(conn, primitive_id: UUID4, mode: str = None, primitive: PostPrimitive = None):
    if not await _check_if_primitive_exists(conn, primitive_id):
        raise HTTPException(404, "Primitive not found")
    if mode == "lock" or mode == "update":
        if await _check_if_locked(conn, primitive_id):
            raise HTTPException(423, "Primitive is locked")
    if mode == "update":
        sql = """
        SELECT
            *
        FROM
            v3_p_primitives
        WHERE
            id = $1;
        """

        try:
            p = await conn.fetchrow(sql, primitive_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Error while checking primitive") from e

        if p["type"] != primitive.type:
            raise HTTPException(405, "Primitive type not matching")
        if p["reference_id"] != primitive.reference_id:
            raise HTTPException(405, "Reference id not matching")
        if p["reference_type"] != primitive.reference_type:
            raise HTTPException(405, "Reference type not matching")
        if p["creator_id"] != primitive.creator_id:
            raise HTTPException(405, "Creator id not matching")
        if p["creator_type"] != primitive.creator_type:
            raise HTTPException(405, "Creator type not matching")


async def _check_if_primitive_exists(conn, primitive_id: UUID4):
    sql = """
    SELECT
        COUNT(1)
    FROM
        v3_p_primitives
    WHERE
        id = $1;
    """

    try:
        return await conn.fetchval(sql, primitive_id) == 1
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking primitive exists") from e


async def _check_if_locked(conn, primitive_id: UUID4):
    sql = """
    SELECT (
        EXISTS(
            SELECT
                1
            FROM
                v3_j_job_primitives
            WHERE
                primitive_id = $1
        )
        OR
        EXISTS (
            SELECT
                1
            FROM
                v3_j_job_collections
            WHERE
                collection_id IN (
                    SELECT
                        unnest(collection_ids)
                    FROM
                        v3_p_primitives
                    WHERE
                        id = $1
                )
        )
    );
    """

    try:
        return await conn.fetchval(sql, primitive_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking primitive locked") from e
