from typing import List

from asyncpg.exceptions import DataError, PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v3.annotation.classes import PostClass
from annotation_service.singletons import logger


async def format_post_data(classes: List[PostClass], allow_external_ids: bool, collection_id: UUID4 = None):
    collection_ids = [collection_id] if collection_id else []

    if allow_external_ids:
        return [
            [
                cl.id,
                cl.creator_id,
                cl.creator_type.value,
                cl.reference_id,
                cl.reference_type.value,
                cl.value,
                False,
                cl.type,
                collection_ids,
            ]
            for cl in classes
        ]
    else:
        return [
            [
                cl.creator_id,
                cl.creator_type.value,
                cl.reference_id,
                cl.reference_type.value,
                cl.value,
                False,
                cl.type,
                collection_ids,
            ]
            for cl in classes
        ]


async def format_class_value_data(classes: List[PostClass]):
    return [
        [
            cl.reference_id,
            cl.value,
        ]
        for cl in classes
    ]


async def check_if_classes_exist(conn, class_ids: List[UUID4]):
    if len(class_ids) == 0:
        return False
    sql = """
    SELECT
        COUNT(*)
    FROM
        v3_cl_classes
    WHERE
        id = ANY($1);
    """

    try:
        return await conn.fetchval(sql, class_ids) == len(class_ids)
    except DataError as e:
        logger.debug(e)
        raise HTTPException(422, e.args) from e
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking classes exist") from e


async def check_class(conn, class_id: UUID4, mode: str = None, _class: PostClass = None):
    if not await _check_if_class_exists(conn, class_id):
        raise HTTPException(404, "Class not found")
    if mode == "lock" or mode == "update":
        if await _check_if_locked(conn, class_id):
            raise HTTPException(423, "Class is locked")
    if mode == "update":
        sql = """
        SELECT
            *
        FROM
            v3_cl_classes
        WHERE
            id = $1;
        """

        try:
            cl = await conn.fetchrow(sql, class_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Error while checking class") from e

        if cl["reference_id"] != _class.reference_id:
            raise HTTPException(405, "Reference id not matching")
        if cl["reference_type"] != _class.reference_type:
            raise HTTPException(405, "Reference type not matching")
        if cl["creator_id"] != _class.creator_id:
            raise HTTPException(405, "Creator id not matching")
        if cl["creator_type"] != _class.creator_type:
            raise HTTPException(405, "Creator type not matching")


async def _check_if_class_exists(conn, class_id: UUID4):
    sql = """
    SELECT
        COUNT(1)
    FROM
        v3_cl_classes
    WHERE
        id = $1;
    """

    try:
        return await conn.fetchval(sql, class_id) == 1
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking class exists") from e


async def _check_if_locked(conn, class_id: UUID4):
    sql = """
    SELECT (
        EXISTS(
            SELECT
                1
            FROM
                v3_j_job_classes
            WHERE
                class_id = $1
        )
        OR
        EXISTS (
            SELECT
                1
            FROM
                v3_j_job_collections
            WHERE
                collection_id IN (
                    SELECT
                        unnest(collection_ids)
                    FROM
                        v3_cl_classes
                    WHERE
                        id = $1::uuid
                )
        )
    );
    """

    try:
        return await conn.fetchval(sql, str(class_id))
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking class locked") from e
