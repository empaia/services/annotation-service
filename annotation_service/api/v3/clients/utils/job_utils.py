from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v3.annotation.collections import CollectionItemType
from annotation_service.singletons import logger


async def get_all_collections_for_locking(conn, collection_id: UUID4):
    collections = []

    item_type_sql = """
    SELECT
        item_type
    FROM
        v3_c_collections
    WHERE
        id = $1;
    """

    try:
        item_type = await conn.fetchval(item_type_sql, collection_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Item type cound not be retrieved (collection lock)") from e

    collections.append((str(collection_id), item_type))

    if item_type == CollectionItemType.COLLECTION:
        item_ids_sql = """
        SELECT
            id,
            item_type
        FROM
            v3_c_collections
        WHERE
            collection_ids && ARRAY[$1]::uuid[];
        """

        try:
            items = await conn.fetch(item_ids_sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Items cound not be retrieved (collection lock)") from e

        collection_item_types = [(str(item["id"]), item["item_type"]) for item in items]

        if len(collection_item_types) > 0:
            for collection_item_type in collection_item_types:
                if collection_item_type[1] == CollectionItemType.COLLECTION:
                    child_items = await get_all_collections_for_locking(conn, collection_item_type[0])
                    collections.extend(child_items)
                else:
                    collections.append(collection_item_type)

    return collections
