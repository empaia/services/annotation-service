from typing import List, get_args

from asyncpg.exceptions import DataError, PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v3.annotation.annotations import (
    PostArrowAnnotation,
    PostCircleAnnotation,
    PostLineAnnotation,
    PostPointAnnotation,
    PostPolygonAnnotation,
    PostRectangleAnnotation,
)
from annotation_service.models.v3.annotation.classes import PostClass
from annotation_service.models.v3.annotation.collections import (
    Collection,
    CollectionItemType,
    PostCollection,
    SlideItem,
)
from annotation_service.models.v3.annotation.pixelmaps import (
    PostContinuousPixelmap,
    PostDiscretePixelmap,
    PostNominalPixelmap,
)
from annotation_service.models.v3.annotation.primitives import (
    PostBoolPrimitive,
    PostFloatPrimitive,
    PostIntegerPrimitive,
    PostStringPrimitive,
)
from annotation_service.models.v3.commons import Id
from annotation_service.singletons import logger


async def get_leaf_item_count(collection: Collection):
    if collection.item_type == CollectionItemType.COLLECTION:
        leaf_count = 0
        if collection.items is None:
            return 0
        for inner_collection in collection.items:
            leaf_count += await get_leaf_item_count(inner_collection)
        return leaf_count
    else:
        if collection.items is not None:
            return len(collection.items)
        else:
            return 0


async def check_item_type_post_put(item, item_type: CollectionItemType):
    item_type_mapping = {
        CollectionItemType.INTEGER: PostIntegerPrimitive,
        CollectionItemType.FLOAT: PostFloatPrimitive,
        CollectionItemType.BOOL: PostBoolPrimitive,
        CollectionItemType.STRING: PostStringPrimitive,
        CollectionItemType.POINT: PostPointAnnotation,
        CollectionItemType.LINE: PostLineAnnotation,
        CollectionItemType.ARROW: PostArrowAnnotation,
        CollectionItemType.CIRCLE: PostCircleAnnotation,
        CollectionItemType.RECTANGLE: PostRectangleAnnotation,
        CollectionItemType.POLYGON: PostPolygonAnnotation,
        CollectionItemType.CLASS: PostClass,
        CollectionItemType.CONTINUOUS: PostContinuousPixelmap,
        CollectionItemType.DISCRETE: PostDiscretePixelmap,
        CollectionItemType.NOMINAL: PostNominalPixelmap,
        CollectionItemType.WSI: SlideItem,
    }
    if item_type != CollectionItemType.COLLECTION:
        return isinstance(item, item_type_mapping[item_type])
    else:
        return isinstance(item, get_args(PostCollection))


async def check_type_ids(conn, ids: List[UUID4], item_type: CollectionItemType):
    type_table_mapping = {
        **dict.fromkeys(
            [
                CollectionItemType.POINT,
                CollectionItemType.LINE,
                CollectionItemType.ARROW,
                CollectionItemType.CIRCLE,
                CollectionItemType.RECTANGLE,
                CollectionItemType.POLYGON,
            ],
            "v3_a_annotations",
        ),
        **dict.fromkeys(
            [CollectionItemType.INTEGER, CollectionItemType.FLOAT, CollectionItemType.BOOL, CollectionItemType.STRING],
            "v3_p_primitives",
        ),
        CollectionItemType.CLASS: "v3_cl_classes",
        **dict.fromkeys(
            [CollectionItemType.CONTINUOUS, CollectionItemType.DISCRETE, CollectionItemType.NOMINAL],
            "v3_pm_pixelmaps",
        ),
        CollectionItemType.COLLECTION: "v3_c_collections",
    }
    sql = f"""
    SELECT
        COUNT(id)
    FROM
        {type_table_mapping[item_type]}
    WHERE
        id = ANY($1)
        AND type = $2;
    """

    try:
        return await conn.fetchval(sql, ids, item_type) == len(ids)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, f"Error while checking {item_type} ids") from e


async def check_if_collections_exist(conn, collection_ids: List[UUID4]):
    if len(collection_ids) == 0:
        return False

    sql = """
    SELECT
        COUNT(*)
    FROM
        v3_c_collections
    WHERE
        id = ANY($1);
    """

    try:
        return await conn.fetchval(sql, collection_ids) == len(collection_ids)
    except DataError as e:
        logger.debug(e)
        raise HTTPException(422, e.args) from e
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking collections exist") from e


async def check_collection(conn, collection_id: UUID4, mode: str = None, collection: PostCollection = None):
    if not await _check_if_collection_exists(conn, collection_id):
        raise HTTPException(404, "Collection not found")
    if mode == "lock" or mode == "update":
        if await _check_if_locked(conn, collection_id):
            raise HTTPException(423, "Collection is locked")
    if mode == "update":
        sql = """
        SELECT
            *
        FROM
            v3_c_collections
        WHERE
            id = $1;
        """

        try:
            c = await conn.fetchrow(sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Error while checking collection") from e

        if c["item_type"] != collection.item_type:
            raise HTTPException(405, "Primitive type not matching")
        if c["reference_id"] != collection.reference_id:
            raise HTTPException(405, "Reference id not matching")
        if c["reference_type"] != collection.reference_type:
            raise HTTPException(405, "Reference type not matching")
        if c["creator_id"] != collection.creator_id:
            raise HTTPException(405, "Creator id not matching")
        if c["creator_type"] != collection.creator_type:
            raise HTTPException(405, "Creator type not matching")


async def _check_if_collection_exists(conn, collection_id: UUID4):
    sql = """
    SELECT
        COUNT(1)
    FROM
        v3_c_collections
    WHERE
        id = $1;
    """

    try:
        return await conn.fetchval(sql, collection_id) == 1
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking collection exists") from e


async def _check_if_locked(conn, collection_id: UUID4):
    sql = """
    SELECT
        EXISTS(
            SELECT
                1
            FROM
                v3_j_job_collections
            WHERE
                collection_id = $1
        );
    """

    try:
        return await conn.fetchval(sql, collection_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking collection locked") from e


async def get_item_type(conn, collection_id: UUID4):
    sql = """
    SELECT
        item_type
    FROM
        v3_c_collections
    WHERE
        id = $1;
    """

    try:
        return await conn.fetchval(sql, collection_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while retrieving item type") from e


async def check_if_item_exists(conn, collection_id: UUID4, item_id: Id):
    sql = """
    SELECT
        COUNT(1)
    FROM
        v3_c_collection_slides
    WHERE
        collection_id = $1
        AND slide_id = $2;
    """

    try:
        return await conn.fetchval(sql, collection_id, item_id) == 1
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking item exists") from e
