from typing import List

from asyncpg import Box
from asyncpg.exceptions import DataError, PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v3.annotation.annotations import AnnotationQuery, PostAnnotation
from annotation_service.singletons import logger, settings

from ..helpers import geometry_helper as geoh


async def check_annotation_query(annotation_query: AnnotationQuery, limit: int, with_low_npp_centroids: bool):
    if limit is not None and limit > settings.v3_item_limit:
        raise HTTPException(413, f"Provided limit ({limit}) exceeds server limit of {settings.v3_item_limit}.")

    if with_low_npp_centroids and annotation_query.npp_viewing is None:
        raise HTTPException(
            400, "Key npp_viewing must be specified when query parameter with_low_npp_centroids set to true!"
        )


async def get_query_args(
    annotation_query: AnnotationQuery,
    query_type: str,
    with_classes: bool = False,
    with_low_npp_centroids: bool = False,
    skip: int = None,
    limit: int = None,
    collection_id: UUID4 = None,
):
    collection_ids = None
    if collection_id:
        collection_ids = [collection_id]
    viewport = None
    if annotation_query.viewport:
        viewport = Box(
            [annotation_query.viewport.x, annotation_query.viewport.y],
            [
                annotation_query.viewport.x + annotation_query.viewport.width,
                annotation_query.viewport.y + annotation_query.viewport.height,
            ],
        )

    npp_min = None
    npp_max = None
    if annotation_query.npp_viewing:
        npp_min = min(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])
        npp_max = max(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])

    if query_type == "class-values":
        return [
            annotation_query.references,
            annotation_query.creators,
            annotation_query.types,
            None,
            viewport,
            None,
            collection_ids,
            npp_min,
            npp_max,
        ]

    else:
        if annotation_query.class_values is not None:
            annotation_query.class_values = list(set(annotation_query.class_values))

        if query_type in ["query", "viewer"]:
            npp_min_ctr = None
            npp_max_ctr = None
            if with_low_npp_centroids or query_type == "viewer":
                npp_min_ctr = 0
                npp_max_ctr = min(annotation_query.npp_viewing[0], annotation_query.npp_viewing[1])

            if query_type == "query":
                return [
                    annotation_query.references,
                    annotation_query.creators,
                    annotation_query.types,
                    annotation_query.annotations,
                    viewport,
                    annotation_query.class_values,
                    collection_ids,
                    npp_min,
                    npp_max,
                    with_classes,
                    limit,
                    skip,
                    npp_min_ctr,
                    npp_max_ctr,
                    with_low_npp_centroids,
                ]
            if query_type == "viewer":
                return [
                    annotation_query.references,
                    annotation_query.creators,
                    annotation_query.types,
                    None,
                    viewport,
                    annotation_query.class_values,
                    collection_ids,
                    npp_min,
                    npp_max,
                    npp_min_ctr,
                    npp_max_ctr,
                ]

        if query_type in ["count", "position", "references"]:
            return [
                annotation_query.references,
                annotation_query.creators,
                annotation_query.types,
                annotation_query.annotations,
                viewport,
                annotation_query.class_values,
                collection_ids,
                npp_min,
                npp_max,
            ]


async def check_if_annotations_exist(conn, annot_ids: List[UUID4]):
    if len(annot_ids) == 0:
        return False

    sql = """
    SELECT
        COUNT(*)
    FROM
        v3_a_annotations
    WHERE
        id = ANY($1);
    """

    try:
        return await conn.fetchval(sql, annot_ids) == len(annot_ids)
    except DataError as e:
        logger.debug(e)
        raise HTTPException(422, e.args) from e
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotations exist") from e


async def check_annotation(conn, annotation_id: UUID4, mode: str = None, annotation: PostAnnotation = None):
    if not await _check_if_annotation_exists(conn, annotation_id):
        raise HTTPException(404, "Annotation not found")
    if mode == "lock":
        if await _check_if_locked(conn, annotation_id):
            raise HTTPException(423, "Annotation is locked")


async def _check_if_annotation_exists(conn, annotation_id: UUID4):
    sql = """
    SELECT
        COUNT(1)
    FROM
        v3_a_annotations
    WHERE
        id = $1;
    """

    try:
        return await conn.fetchval(sql, annotation_id) == 1
    except DataError as e:
        logger.debug(e)
        raise HTTPException(422, e.args) from e
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotation exists") from e


async def _check_if_locked(conn, annotation_id: UUID4):
    sql = """
    SELECT (
        EXISTS(
            SELECT
                1
            FROM
                v3_j_job_classes
            WHERE
                annot_id = $1
        )
        OR
        EXISTS (
            SELECT
                1
            FROM
                v3_j_job_collections
            WHERE
                collection_id IN (
                    SELECT
                        unnest(collection_ids)
                    FROM
                        v3_a_annotations
                    WHERE
                        id = $1
                )
        )
    );
    """

    try:
        return await conn.fetchval(sql, annotation_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking annotation locked") from e


async def format_post_data(annotations: List[PostAnnotation], allow_external_ids: bool, collection_id: UUID4 = None):
    annot_type = annotations[0].type

    format_mapping = {
        "point": _format_point_line_polygon,
        "line": _format_point_line_polygon,
        "arrow": _format_arrow,
        "circle": _format_circle,
        "rectangle": _format_rectangle,
        "polygon": _format_point_line_polygon,
    }

    collection_ids = [collection_id] if collection_id else []

    return await format_mapping[annot_type](annotations, allow_external_ids, collection_ids)


async def _format_point_line_polygon(
    annotations: List[PostAnnotation], allow_external_ids: bool, collection_ids: List[UUID4]
):
    if allow_external_ids:
        return [
            [
                a.id,
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                a.coordinates,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]
    else:
        return [
            [
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                a.coordinates,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]


async def _format_arrow(annotations: List[PostAnnotation], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                a.id,
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                None,
                a.head,
                a.tail,
                None,
                None,
                None,
                None,
                None,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]
    else:
        return [
            [
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                None,
                a.head,
                a.tail,
                None,
                None,
                None,
                None,
                None,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]


async def _format_circle(annotations: List[PostAnnotation], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                a.id,
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                None,
                None,
                None,
                a.center,
                a.radius,
                None,
                None,
                None,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]
    else:
        return [
            [
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                None,
                None,
                None,
                a.center,
                a.radius,
                None,
                None,
                None,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]


async def _format_rectangle(annotations: List[PostAnnotation], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                a.id,
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                None,
                None,
                None,
                None,
                None,
                a.upper_left,
                a.width,
                a.height,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]
    else:
        return [
            [
                a.type,
                a.name,
                a.description,
                a.creator_id,
                a.creator_type,
                a.reference_id,
                a.reference_type,
                a.npp_created,
                False,
                a.npp_viewing[0] if a.npp_viewing is not None else None,
                a.npp_viewing[1] if a.npp_viewing is not None else None,
                None,
                None,
                None,
                None,
                None,
                a.upper_left,
                a.width,
                a.height,
                a.centroid if a.centroid is not None else await geoh.get_centroid(a),
                await geoh.get_bounding_box(a),
                a.npp_viewing[0] if a.npp_viewing is not None else a.npp_created,
                a.npp_viewing[1] if a.npp_viewing is not None else a.npp_created,
                collection_ids,
                [],
            ]
            for a in annotations
        ]
