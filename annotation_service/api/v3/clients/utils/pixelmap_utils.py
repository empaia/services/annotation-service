from typing import List

from asyncpg.exceptions import PostgresError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from annotation_service.models.v3.annotation.pixelmaps import PostPixelmap
from annotation_service.singletons import logger


async def format_post_data(pixelmaps: List[PostPixelmap], allow_external_ids: bool, collection_id: UUID4 = None):
    annot_type = pixelmaps[0].type

    format_mapping = {
        "continuous_pixelmap": _format_continuous,
        "discrete_pixelmap": _format_discrete,
        "nominal_pixelmap": _format_nominal,
    }

    collection_ids = [collection_id] if collection_id else []

    return await format_mapping[annot_type](pixelmaps, allow_external_ids, collection_ids)


async def _format_continuous(pixelmaps: List[PostPixelmap], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                pm.id,
                pm.type,
                pm.name,
                pm.description,
                pm.creator_id,
                pm.creator_type,
                pm.reference_id,
                pm.reference_type,
                [level.model_dump() for level in pm.levels],
                pm.channel_count,
                pm.tilesize,
                [cm.model_dump() for cm in pm.channel_class_mapping] if pm.channel_class_mapping else None,
                pm.element_type,
                None,
                pm.min_value,
                None,
                pm.neutral_value,
                None,
                pm.max_value,
                None,
                collection_ids,
            ]
            for pm in pixelmaps
        ]
    else:
        return [
            [
                pm.type,
                pm.name,
                pm.description,
                pm.creator_id,
                pm.creator_type,
                pm.reference_id,
                pm.reference_type,
                [level.model_dump() for level in pm.levels],
                pm.channel_count,
                pm.tilesize,
                [cm.model_dump() for cm in pm.channel_class_mapping] if pm.channel_class_mapping else None,
                pm.element_type,
                None,
                pm.min_value,
                None,
                pm.neutral_value,
                None,
                pm.max_value,
                None,
                collection_ids,
            ]
            for pm in pixelmaps
        ]


async def _format_discrete(pixelmaps: List[PostPixelmap], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                pm.id,
                pm.type,
                pm.name,
                pm.description,
                pm.creator_id,
                pm.creator_type,
                pm.reference_id,
                pm.reference_type,
                [level.model_dump() for level in pm.levels],
                pm.channel_count,
                pm.tilesize,
                [cm.model_dump() for cm in pm.channel_class_mapping] if pm.channel_class_mapping else None,
                pm.element_type,
                pm.min_value,
                None,
                pm.neutral_value,
                None,
                pm.max_value,
                None,
                [em.model_dump() for em in pm.element_class_mapping] if pm.element_class_mapping else None,
                collection_ids,
            ]
            for pm in pixelmaps
        ]
    else:
        return [
            [
                pm.type,
                pm.name,
                pm.description,
                pm.creator_id,
                pm.creator_type,
                pm.reference_id,
                pm.reference_type,
                [level.model_dump() for level in pm.levels],
                pm.channel_count,
                pm.tilesize,
                [cm.model_dump() for cm in pm.channel_class_mapping] if pm.channel_class_mapping else None,
                pm.element_type,
                pm.min_value,
                None,
                pm.neutral_value,
                None,
                pm.max_value,
                None,
                [em.model_dump() for em in pm.element_class_mapping] if pm.element_class_mapping else None,
                collection_ids,
            ]
            for pm in pixelmaps
        ]


async def _format_nominal(pixelmaps: List[PostPixelmap], allow_external_ids: bool, collection_ids: List[UUID4]):
    if allow_external_ids:
        return [
            [
                pm.id,
                pm.type,
                pm.name,
                pm.description,
                pm.creator_id,
                pm.creator_type,
                pm.reference_id,
                pm.reference_type,
                [level.model_dump() for level in pm.levels],
                pm.channel_count,
                pm.tilesize,
                [cm.model_dump() for cm in pm.channel_class_mapping] if pm.channel_class_mapping else None,
                pm.element_type,
                None,
                None,
                pm.neutral_value,
                None,
                None,
                None,
                [em.model_dump() for em in pm.element_class_mapping],
                collection_ids,
            ]
            for pm in pixelmaps
        ]
    else:
        return [
            [
                pm.type,
                pm.name,
                pm.description,
                pm.creator_id,
                pm.creator_type,
                pm.reference_id,
                pm.reference_type,
                [level.model_dump() for level in pm.levels],
                pm.channel_count,
                pm.tilesize,
                [cm.model_dump() for cm in pm.channel_class_mapping] if pm.channel_class_mapping else None,
                pm.element_type,
                None,
                None,
                pm.neutral_value,
                None,
                None,
                None,
                [em.model_dump() for em in pm.element_class_mapping],
                collection_ids,
            ]
            for pm in pixelmaps
        ]


async def get_pixelmap_lock_state(conn, pixelmap_id: UUID4):
    return await _check_if_locked(conn, pixelmap_id)


async def check_pixelmap(conn, pixelmap_id: UUID4, mode: str = None):
    if not await _check_if_pixelmap_exists(conn, pixelmap_id):
        raise HTTPException(404, "Pixelmap not found")
    if mode == "lock" or mode == "update":
        if await _check_if_locked(conn, pixelmap_id):
            raise HTTPException(423, "Pixelmap is locked")


async def _check_if_pixelmap_exists(conn, pixelmap_id: UUID4):
    sql = """
    SELECT
        COUNT(1)
    FROM
        v3_pm_pixelmaps
    WHERE
        id = $1;
    """

    try:
        return await conn.fetchval(sql, pixelmap_id) == 1
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking pixelmap exists") from e


async def _check_if_locked(conn, pixelmap_id: UUID4):
    sql = """
    SELECT (
        EXISTS(
            SELECT
                1
            FROM
                v3_j_job_pixelmaps
            WHERE
                pixelmap_id = $1
        )
        OR
        EXISTS (
            SELECT
                1
            FROM
                v3_j_job_collections
            WHERE
                collection_id IN (
                    SELECT
                        unnest(collection_ids)
                    FROM
                        v3_pm_pixelmaps
                    WHERE
                        id = $1
                )
        )
    );
    """

    try:
        return await conn.fetchval(sql, pixelmap_id)
    except PostgresError as e:
        logger.debug(e)
        raise HTTPException(500, "Error while checking pixelmap locked") from e
