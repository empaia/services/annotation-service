from typing import List
from uuid import uuid4

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse, Response
from pydantic import UUID4

from annotation_service.models.v3.annotation.classes import ClassQuery, PostClass, PostClasses
from annotation_service.singletons import logger, settings

from .helpers.annotations import sql_helper_insert as sql_helper_insert_annot
from .helpers.classes import sql_helper_get, sql_helper_insert, sql_helper_query
from .helpers.sql_helper import get_existing_views_sql
from .utils import class_utils as utils

CLASS_MAT_VIEW_BASE_NAME = "mat_view_classes"


class ClassClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_classes(
        self, classes: PostClasses, external_ids: bool = False, raw: bool = False, collection_id: UUID4 = None
    ):
        if isinstance(classes, PostClass):
            return await self.create_new_classes([classes], external_ids, raw, collection_id)
        elif len(classes.items) > 0:
            if len(classes.items) > settings.v3_post_limit:
                raise HTTPException(
                    413,
                    f"Number of posted items ({len(classes.items)}) exceeds server limit of {settings.v3_post_limit}.",
                )
            return await self.create_new_classes(classes.items, external_ids, raw, collection_id)

        raise HTTPException(405, "No valid post data")

    async def create_new_classes(
        self, classes: List[PostClass], external_ids: bool = False, raw: bool = False, collection_id: UUID4 = None
    ):
        # create random name for temporary table
        temp_table_name = f"tmp_{uuid4().hex}"

        allow_external_ids = settings.allow_external_ids and external_ids

        columns, insert_sql = await sql_helper_insert.get_insert_sql(allow_external_ids, temp_table_name)
        logger.debug(insert_sql)

        update_annot_class_value_sql = await sql_helper_insert_annot.add_class_values_sql()

        new_classes = await utils.format_post_data(classes, allow_external_ids, collection_id)
        class_value_data = await utils.format_class_value_data(classes)

        try:
            async with self.conn.transaction():
                await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")
                await self.conn.execute(f"CREATE UNLOGGED TABLE IF NOT EXISTS {temp_table_name} (LIKE v3_cl_classes);")
                await self.conn.execute(f"TRUNCATE {temp_table_name};")
                if not settings.allow_external_ids or not external_ids:
                    await self.conn.execute(f"ALTER TABLE {temp_table_name} DROP COLUMN id;")
                await self.conn.copy_records_to_table(temp_table_name, records=new_classes, columns=columns)
                result = await self.conn.fetch(insert_sql)
                await self.conn.executemany(update_annot_class_value_sql, class_value_data)
        except UniqueViolationError as e:
            raise HTTPException(422, "Class with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Classes could not be created") from e
        finally:
            await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")

        if result is None:
            raise HTTPException(500, "Classes could not be created (result None)")

        items = [cl[0] for cl in result]

        if raw:
            return len(items), items
        if len(items) == 1:
            return JSONResponse(items[0], status_code=201)
        return JSONResponse({"item_count": len(items), "items": items}, status_code=201)

    async def get_all(self, with_unique_class_values: bool, skip: int, limit: int):
        sql = await sql_helper_get.get_all_sql(with_unique_class_values)
        logger.debug(sql)

        args = [limit, skip]

        try:
            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'classes' must be of type UUID4.") from e
            raise HTTPException(500, "Classes could not be queried (get all)") from e

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def get(self, class_id: UUID4):
        sql = await sql_helper_get.get_single_sql()
        logger.debug(sql)

        try:
            result = await self.conn.fetchrow(sql, class_id)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'classes' must be of type UUID4.") from e
            raise HTTPException(500, "Classes could not be queried (get)") from e

        if not result:
            raise HTTPException(404, f"No class with id '{class_id}' found")
        return result["class"]

    async def query(
        self,
        class_query: ClassQuery,
        with_unique_class_values: bool,
        skip: int,
        limit: int,
        raw: bool = False,
        collection_id: UUID4 = None,
    ):
        try:
            if not class_query.jobs:
                # if jobs not in query
                sql = await sql_helper_query.get_query_sql(with_unique_class_values, raw)
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [f"{CLASS_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in class_query.jobs]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

                sql = await sql_helper_query.get_query_sql(with_unique_class_values, raw, existing_views)

            logger.debug(sql)

            args = [
                class_query.references,
                class_query.creators,
                class_query.classes,
                [collection_id] if collection_id else None,
                limit,
                skip,
            ]

            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'classes' must be of type UUID4.") from e
            raise HTTPException(500, "Classes could not be queried (query)") from e

        if raw:
            if result is None:
                return 0, []
            return result["item_count"], result["items"]

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def unique_references_query(self, class_query: ClassQuery, collection_id: UUID4 = None):
        try:
            if not class_query.jobs:
                # if jobs not in query
                references_sql = await sql_helper_query.get_references_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [f"{CLASS_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in class_query.jobs]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(content={"annotation": []}, media_type="application/json")

                references_sql = await sql_helper_query.get_references_sql(existing_views)

            logger.debug(references_sql)

            args = [
                class_query.references,
                class_query.creators,
                class_query.classes,
                [collection_id] if collection_id else None,
            ]

            references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Classes could not be queried (class references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Classes cound not be queried (unique references None).")

    async def update(self, class_id: UUID4, put_class: PostClass):
        await utils.check_class(self.conn, class_id, "update", put_class)

        update_sql = await sql_helper_insert.get_update_sql()
        logger.debug(update_sql)

        try:
            await self.conn.execute(
                update_sql,
                class_id,
                put_class.creator_id,
                put_class.creator_type,
                put_class.reference_id,
                put_class.reference_type,
                put_class.value,
            )
            return await self.get(class_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class could not be updated") from e

    async def delete(self, class_id: UUID4):
        await utils.check_class(self.conn, class_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM
                v3_cl_classes
            WHERE
                id = $1
            RETURNING *
        )
        SELECT
            COUNT(*)
        FROM
            deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, class_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(class_id)})
        else:
            raise HTTPException(500, "Class could not be deleted (delete count 0)")

    async def get_collection_items(self, collection_id: UUID4):
        sql = """
        SELECT
            id::TEXT
        FROM
            v3_cl_classes
        WHERE
            collection_ids && ARRAY[$1]::uuid[]
        """

        try:
            items = await self.conn.fetch(sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class items could not be queried") from e

        return [item["id"] for item in items]

    async def remove_from_collection(self, class_id: UUID4, collection_id: UUID4):
        sql = """
        WITH ROWS AS (
            UPDATE
                v3_cl_classes
            SET
                collection_ids = ARRAY_REMOVE(collection_ids, $2::uuid)
            WHERE
                id = $1::uuid
                AND collection_ids && ARRAY[$2]::uuid[]
            RETURNING 1
        )
        SELECT
            count(*)
        FROM
            ROWS;
        """

        try:
            return await self.conn.fetchval(sql, class_id, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class item could not be removed from Collection") from e

    async def update_collection_id(self, collection_id: UUID4, ids: List[UUID4]):
        sql = """
        WITH rows AS (
            UPDATE
                v3_cl_classes
            SET
                collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
            WHERE
                id = ANY($2::uuid[])
            RETURNING 1
        )
        SELECT count(*) FROM rows;
        """

        try:
            return await self.conn.fetchval(sql, collection_id, ids)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Class items could not be updated") from e

    async def create_materialized_view(self, job_id: str):
        # create view name
        view_name = f"{CLASS_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}"

        try:
            await self.conn.execute("CALL create_class_view($1, $2)", job_id, view_name)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"Class view could not be created: {e}") from e
