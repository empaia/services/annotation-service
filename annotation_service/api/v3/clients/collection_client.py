from typing import Any, List, get_args
from uuid import uuid4

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse, Response
from pydantic import UUID4

from annotation_service.models.v3.annotation.annotations import (
    AnnotationQuery,
    PostArrowAnnotations,
    PostCircleAnnotations,
    PostLineAnnotations,
    PostPointAnnotations,
    PostPolygonAnnotations,
    PostRectangleAnnotations,
)
from annotation_service.models.v3.annotation.classes import ClassQuery, PostClassList
from annotation_service.models.v3.annotation.collections import (
    CollectionItemType,
    CollectionQuery,
    ItemQuery,
    PostCollection,
    PostItem,
    PostItemList,
    PostItems,
)
from annotation_service.models.v3.annotation.pixelmaps import (
    PixelmapQuery,
    PostContinuousPixelmaps,
    PostDiscretePixelmaps,
    PostNominalPixelmaps,
)
from annotation_service.models.v3.annotation.primitives import (
    PostBoolPrimitives,
    PostFloatPrimitives,
    PostIntegerPrimitives,
    PostStringPrimitives,
    PrimitiveQuery,
)
from annotation_service.models.v3.commons import Id, IdObject
from annotation_service.singletons import logger, settings

from .helpers.collections import sql_helper_get, sql_helper_insert, sql_helper_query
from .helpers.collections.items import sql_helper_insert as sql_helper_insert_items
from .helpers.sql_helper import get_existing_views_sql
from .utils import collection_utils as utils

COLL_MAT_VIEW_BASE_NAME = "mat_view_collections"

POST_TYPE_MAPPING = {
    "integer": PostIntegerPrimitives,
    "float": PostFloatPrimitives,
    "bool": PostBoolPrimitives,
    "string": PostStringPrimitives,
    "point": PostPointAnnotations,
    "line": PostLineAnnotations,
    "arrow": PostArrowAnnotations,
    "circle": PostCircleAnnotations,
    "rectangle": PostRectangleAnnotations,
    "polygon": PostPolygonAnnotations,
    "continuous_pixelmap": PostContinuousPixelmaps,
    "discrete_pixelmap": PostDiscretePixelmaps,
    "nominal_pixelmap": PostNominalPixelmaps,
}


ANNOTATION_TYPES = [
    CollectionItemType.POINT,
    CollectionItemType.LINE,
    CollectionItemType.ARROW,
    CollectionItemType.CIRCLE,
    CollectionItemType.RECTANGLE,
    CollectionItemType.POLYGON,
]

PRIMITIVE_TYPES = [
    CollectionItemType.INTEGER,
    CollectionItemType.FLOAT,
    CollectionItemType.BOOL,
    CollectionItemType.STRING,
]

PIXELMAP_TYPES = [
    CollectionItemType.CONTINUOUS,
    CollectionItemType.DISCRETE,
    CollectionItemType.NOMINAL,
]


class CollectionClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_collection(
        self,
        collection: PostCollection,
        clients,
        external_ids: bool = False,
        raw: bool = False,
        collection_id: UUID4 = None,
    ):
        async with self.conn.transaction():
            if isinstance(collection, get_args(PostCollection)):
                return await self.create_filled_collection(
                    collection, clients, external_ids, raw=raw, collection_id=collection_id
                )
            else:
                items = []
                for c in collection.items:
                    items.append(
                        await self.create_filled_collection(
                            c, clients, external_ids, raw=raw, collection_id=collection_id
                        )
                    )
                return len(items), items

    async def create_filled_collection(
        self,
        collection: PostCollection,
        clients,
        external_ids: bool = False,
        raw: bool = False,
        collection_id: UUID4 = None,
    ):
        item_count = len(collection.items) if collection.items is not None else 0
        if item_count == 0:
            return await self._create_collection(collection, external_ids, raw=raw, collection_id=collection_id)

        item_type = collection.item_type
        first_item = collection.items[0]
        if not isinstance(first_item, IdObject):
            if not await utils.check_item_type_post_put(first_item, item_type):
                raise HTTPException(405, "Item type and type of items not matching")

            leaf_item_count = await utils.get_leaf_item_count(collection)

            if leaf_item_count > settings.v3_post_limit:
                raise HTTPException(
                    413,
                    f"Number of posted items ({leaf_item_count}) exceeds server limit of {settings.v3_post_limit}.",
                )

            new_collection = await self._create_collection(
                collection, external_ids, raw=True, collection_id=collection_id
            )
            new_collection_id = new_collection["id"]

            if item_type in PRIMITIVE_TYPES:
                post_items = POST_TYPE_MAPPING[item_type](items=collection.items)
                i_count, items = await clients.primitive.add_primitives(
                    post_items, external_ids=external_ids, raw=True, collection_id=new_collection_id
                )
            elif item_type in ANNOTATION_TYPES:
                post_items = POST_TYPE_MAPPING[item_type](items=collection.items)
                i_count, items = await clients.annotation.add_annotations(
                    post_items, external_ids=external_ids, raw=True, collection_id=new_collection_id
                )
            elif item_type == CollectionItemType.CLASS:
                post_items = PostClassList(items=collection.items)
                i_count, items = await clients.aclass.add_classes(
                    post_items, external_ids=external_ids, raw=True, collection_id=new_collection_id
                )
            elif item_type in PIXELMAP_TYPES:
                post_items = POST_TYPE_MAPPING[item_type](items=collection.items)
                i_count, items = await clients.pixelmap.add_pixelmaps(
                    post_items, external_ids=external_ids, raw=True, collection_id=new_collection_id
                )
            elif item_type == CollectionItemType.WSI:
                ids = [item.id for item in collection.items]
                if len(ids) != len(set(ids)):
                    raise HTTPException(405, "Duplicate items found.")
                items = [{"id": item.id, "type": "wsi"} for item in collection.items]
                i_count = await self.set_slides(new_collection_id, ids)
            elif item_type == CollectionItemType.COLLECTION:
                items = []
                for item in collection.items:
                    items.append(
                        await self.create_filled_collection(
                            item, clients=clients, external_ids=external_ids, raw=True, collection_id=new_collection_id
                        )
                    )
                i_count = len(items)
            else:
                raise HTTPException(405, "Invalid type of items")
            if i_count != item_count:
                raise HTTPException(500, "Items could not be created (count not matching)")
        else:
            new_collection = await self._create_collection(
                collection, external_ids, raw=True, collection_id=collection_id
            )
            try:
                items = [
                    IdObject.model_validate(dict(t))
                    for t in {tuple(d.items()) for d in [item.model_dump() for item in collection.items]}
                ]
                if await self.add_items_by_id(new_collection["id"], items, item_type, clients):
                    items = [item.model_dump() for item in items]
                else:
                    raise HTTPException(500, "The resources could not be created (count not matching)")
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "The resources could not be created (items by id)") from e

        new_collection["item_count"] = len(items)
        new_collection["items"] = items

        if raw:
            return new_collection
        return JSONResponse(new_collection, status_code=201)

    async def _create_collection(
        self, collection: PostCollection, external_ids: bool = False, raw: bool = False, collection_id: UUID4 = None
    ):
        # create random name for temporary table
        temp_table_name = f"tmp_{uuid4().hex}"

        insert_tmptable_sql = await sql_helper_insert.get_insert_tmptable_sql(external_ids, temp_table_name)
        insert_sql = await sql_helper_insert.get_insert_sql(temp_table_name)
        logger.debug(insert_tmptable_sql)
        logger.debug(insert_sql)

        new_collection = [
            collection.type,
            collection.name,
            collection.description,
            collection.creator_id,
            collection.creator_type,
            collection.reference_id,
            collection.reference_type,
            collection.item_type,
            False,
            [collection_id] if collection_id else [],
        ]

        if settings.allow_external_ids and external_ids:
            new_collection.insert(0, collection.id)

        try:
            async with self.conn.transaction():
                await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")
                await self.conn.execute(
                    f"CREATE UNLOGGED TABLE IF NOT EXISTS {temp_table_name} (LIKE v3_c_collections);"
                )
                await self.conn.execute(f"TRUNCATE {temp_table_name};")
                await self.conn.execute(insert_tmptable_sql, *new_collection)
                result = await self.conn.fetchrow(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Collection with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collection could not be created") from e
        finally:
            await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")

        if result is None:
            raise HTTPException(500, "Collection could not be created (result None)")

        collection = result["json"]
        collection["item_count"] = 0
        collection["items"] = []

        if raw:
            return collection
        return JSONResponse(collection, status_code=201)

    async def get_all(self, skip: int, limit: int):
        raise HTTPException(501, "Requesting all collections is no longer supported!")

    async def get(
        self,
        collection_id: UUID4,
        shallow: bool,
        with_leaf_ids: bool,
        clients,
        raw: bool = False,
        is_item_query: bool = False,
    ):
        sql = await sql_helper_get.get_single_sql()
        logger.debug(sql)

        try:
            collection = await self.conn.fetchrow(sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collection could not be queried") from e

        if collection is None:
            raise HTTPException(404, f"No collection with id '{collection_id}' found")

        result = collection["collection"]

        logger.debug(result)

        item_count = result["item_count"]
        item_type = result["item_type"]

        items = []
        item_ids = None

        if item_count == 0:
            item_ids = [] if with_leaf_ids and item_type != CollectionItemType.COLLECTION else None
        else:
            if item_type == CollectionItemType.WSI and (not shallow or with_leaf_ids):
                select_slide_ids_sql = await sql_helper_insert_items.get_select_item_ids_sql()
                logger.debug(select_slide_ids_sql)

                try:
                    slides_ids_result = await self.conn.fetch(select_slide_ids_sql, collection_id)
                except PostgresError as e:
                    logger.debug(e)
                    raise HTTPException(500, "Slide ids could not be queried (get collection)") from e

                _slide_ids = [slide_id["slide_id"] for slide_id in slides_ids_result]

                if with_leaf_ids:
                    item_ids = _slide_ids

            if not shallow and item_type != CollectionItemType.COLLECTION:
                if item_count > settings.v3_item_limit:
                    raise HTTPException(
                        413, f"Number of items ({item_count}) exceeds server limit of {settings.v3_item_limit}."
                    )

            if (not shallow or with_leaf_ids) and item_type != CollectionItemType.COLLECTION:
                _items = []
                if item_type in ANNOTATION_TYPES:
                    if with_leaf_ids:
                        item_ids = await clients.annotation.get_collection_items(collection_id)
                    if not shallow:
                        _, _items = await clients.annotation.query(
                            annotation_query=AnnotationQuery(),
                            with_classes=False,
                            with_low_npp_centroids=False,
                            skip=None,
                            limit=None,
                            raw=True,
                            collection_id=collection_id,
                        )
                    if with_leaf_ids:
                        item_ids = await clients.annotation.get_collection_items(collection_id)
                elif item_type in PRIMITIVE_TYPES:
                    if with_leaf_ids:
                        item_ids = await clients.primitive.get_collection_items(collection_id)
                    if not shallow:
                        _, _items = await clients.primitive.query(
                            primitive_query=PrimitiveQuery(),
                            skip=None,
                            limit=None,
                            raw=True,
                            collection_id=collection_id,
                        )
                elif item_type == CollectionItemType.CLASS:
                    if with_leaf_ids:
                        item_ids = await clients.aclass.get_collection_items(collection_id)
                    if not shallow:
                        _, _items = await clients.aclass.query(
                            class_query=ClassQuery(),
                            with_unique_class_values=False,
                            skip=None,
                            limit=None,
                            raw=True,
                            collection_id=collection_id,
                        )
                elif item_type in PIXELMAP_TYPES:
                    if with_leaf_ids:
                        item_ids = await clients.pixelmaps.get_collection_items(collection_id)
                    if not shallow:
                        _, _items = await clients.pixelmap.query(
                            pixelmap_query=PixelmapQuery(),
                            skip=None,
                            limit=None,
                            raw=True,
                            collection_id=collection_id,
                        )
                elif item_type == CollectionItemType.WSI:
                    _items = [{"id": slide_id, "type": "wsi"} for slide_id in _slide_ids]

                items = _items
            elif item_type == CollectionItemType.COLLECTION:
                _item_ids = await self.get_collection_items(collection_id)

                collection_items = []
                for item_id in _item_ids:
                    collection_items.append(
                        await self.get(
                            collection_id=item_id["id"],
                            shallow=shallow,
                            with_leaf_ids=with_leaf_ids,
                            clients=clients,
                            raw=True,
                            is_item_query=is_item_query,
                        )
                    )
                items = collection_items

            if (not shallow or item_type == CollectionItemType.COLLECTION) and item_count != len(items):
                raise HTTPException(
                    500, "Number of items in collection and retrieved items not matching (get collection)"
                )

        if item_type == CollectionItemType.COLLECTION:
            result["items"] = items
        else:
            if is_item_query and (item_ids or item_ids == []):
                result["items"] = [{"id": item} for item in item_ids]
            else:
                result["items"] = [] if shallow else items
        if with_leaf_ids and (item_ids or item_ids == []) and not is_item_query:
            result["item_ids"] = item_ids

        if raw:
            return result
        return JSONResponse(result)

    async def query(
        self,
        collection_query: CollectionQuery,
        skip: int,
        limit: int,
        ids: List[UUID4] = None,
        raw: bool = False,
        collection_id: UUID4 = None,
    ):
        sql = await sql_helper_query.get_query_sql(raw)
        logger.debug(sql)

        args = [
            collection_query.references,
            collection_query.creators,
            collection_query.item_types,
            ids,
            limit,
            skip,
        ]

        try:
            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            raise HTTPException(500, "Collections could not be queried (query)") from e

        if raw:
            if result is None:
                return 0, []
            return result["item_count"], result["items"]

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def unique_references_query(
        self, collection_query: CollectionQuery, ids: List[UUID4] = None, collection_id: UUID4 = None
    ):
        try:
            if not collection_query.jobs:
                # if jobs not in query
                references_sql = await sql_helper_query.get_references_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{COLL_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in collection_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(
                        content={
                            "annotation": [],
                            "collection": [],
                            "wsi": [],
                            "contains_items_without_reference": False,
                        },
                        media_type="application/json",
                    )

                references_sql = await sql_helper_query.get_references_sql(existing_views)

            logger.debug(references_sql)

            args = [
                collection_query.references,
                collection_query.creators,
                collection_query.item_types,
                ids,
                [collection_id] if collection_id else None,
            ]

            references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be queried (collection references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Collections cound not be queried (unique references None).")

    async def update(self, collection_id: UUID4, collection: PostCollection):
        await utils.check_collection(self.conn, collection_id, "update", collection)

        update_sql = await sql_helper_insert.get_update_sql()
        logger.debug(update_sql)

        try:
            await self.conn.execute(
                update_sql,
                collection_id,
                collection.name,
                collection.description,
                collection.creator_id,
                collection.creator_type,
                collection.reference_id,
                collection.reference_type,
                collection.item_type,
            )
            return await self.get(collection_id=collection_id, shallow=True, with_leaf_ids=False, clients=None)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be updated") from e

    async def delete(self, collection_id: UUID4):
        await utils.check_collection(self.conn, collection_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM
                v3_c_collections
            WHERE
                id = $1
            RETURNING *
        )
        SELECT
            COUNT(*)
        FROM
            deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(collection_id)})
        else:
            raise HTTPException(500, "Collections could not be deleted (delete count 0)")

    async def get_collection_items(self, collection_id: UUID4, reference_ids: List[str] = None):
        sql = """
        SELECT
            id
        FROM
            v3_c_collections
        WHERE
            collection_ids && ARRAY[$1]::uuid[]
            {references_sql}
        """

        references_sql = ""
        if reference_ids:
            references_sql = " AND reference_id = ANY($2::text[])"

        sql = sql.format(references_sql=references_sql)

        logger.debug(sql)

        try:
            if reference_ids:
                items = await self.conn.fetch(sql, collection_id, reference_ids)
            else:
                items = await self.conn.fetch(sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collection items could not be queried") from e

        return items

    async def _update_collection_id(self, collection_id: UUID4, ids: List[UUID4]):
        sql = """
        WITH rows AS (
            UPDATE
                v3_c_collections
            SET
                collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
            WHERE
                id = ANY($2::uuid[])
            RETURNING 1
        )
        SELECT count(*) FROM rows;
        """

        try:
            return await self.conn.fetchval(sql, collection_id, ids)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collection items could not be updated") from e

    async def remove_from_collection(self, collection_item_id: UUID4, collection_id: UUID4):
        sql = """
        WITH ROWS AS (
            UPDATE
                v3_c_collections
            SET
                collection_ids = ARRAY_REMOVE(collection_ids, $2::uuid)
            WHERE
                id = $1::uuid
                AND collection_ids && ARRAY[$2]::uuid[]
            RETURNING 1
        )
        SELECT
            count(*)
        FROM
            ROWS;
        """

        try:
            return await self.conn.fetchval(sql, collection_item_id, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collection item could not be removed from Collection") from e

    async def create_materialized_view(self, job_id: str):
        # create view name
        view_name = f"{COLL_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}"

        try:
            await self.conn.execute("CALL create_collection_view($1, $2)", job_id, view_name)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"Collection view could not be created: {e}") from e

    # ITEMS

    async def set_slides(self, collection_id: UUID4, slides: List[str]):
        # create random name for temporary table
        temp_table_name = f"tmp_{uuid4().hex}"

        insert_slides_sql = await sql_helper_insert_items.get_insert_items_sql(temp_table_name)
        logger.debug(insert_slides_sql)

        columns = ["collection_id", "slide_id"]
        new_slides = [(collection_id, slide_id) for slide_id in slides]

        try:
            async with self.conn.transaction():
                await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")
                await self.conn.execute(
                    f"CREATE UNLOGGED TABLE IF NOT EXISTS {temp_table_name} (LIKE v3_c_collection_slides);"
                )
                await self.conn.execute(f"TRUNCATE {temp_table_name};")
                await self.conn.copy_records_to_table(temp_table_name, records=new_slides, columns=columns)
                return await self.conn.fetchval(insert_slides_sql)
        except UniqueViolationError as e:
            raise HTTPException(405, "Slide already in collection") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Slides could not be added to collection (insert)") from e
        finally:
            await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")

    async def add_items_by_id(
        self, collection_id: UUID4, items: List[IdObject], item_type: CollectionItemType, clients
    ):
        ids = []
        for item in items:
            ids.append(item.id)
        ids = list(set(ids))

        if item_type == CollectionItemType.WSI:
            count = await self.set_slides(collection_id, ids)
        else:
            if await utils.check_type_ids(self.conn, ids, item_type):
                count = await self.update_collection_id(collection_id, ids, item_type, clients)
            else:
                raise HTTPException(404, "Not all items found")

        if count == len(ids):
            return True
        else:
            return False

    async def update_collection_id(self, collection_id: UUID4, ids: List[Any], item_type: CollectionItemType, clients):
        type_mapping = {
            **dict.fromkeys(
                [
                    CollectionItemType.POINT,
                    CollectionItemType.LINE,
                    CollectionItemType.ARROW,
                    CollectionItemType.CIRCLE,
                    CollectionItemType.RECTANGLE,
                    CollectionItemType.POLYGON,
                ],
                clients.annotation.update_collection_id,
            ),
            **dict.fromkeys(
                [
                    CollectionItemType.INTEGER,
                    CollectionItemType.FLOAT,
                    CollectionItemType.BOOL,
                    CollectionItemType.STRING,
                ],
                clients.primitive.update_collection_id,
            ),
            CollectionItemType.CLASS: clients.aclass.update_collection_id,
            **dict.fromkeys(
                [
                    CollectionItemType.CONTINUOUS,
                    CollectionItemType.DISCRETE,
                    CollectionItemType.NOMINAL,
                ],
                clients.pixelmap.update_collection_id,
            ),
            CollectionItemType.COLLECTION: self._update_collection_id,
        }

        try:
            async with self.conn.transaction():
                return await type_mapping[item_type](collection_id, ids)
        except UniqueViolationError as e:
            raise HTTPException(405, "Item already in collection") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Items could not be added (items by id)") from e

    async def add_items(self, collection_id: UUID4, items: PostItems, clients, external_ids: bool = False):
        await utils.check_collection(self.conn, collection_id, "lock")

        if isinstance(items, PostItem.__args__):
            return await self.add_single_item(collection_id, items, clients, external_ids)
        else:
            return await self.add_item_list(collection_id, items, clients, external_ids)

    async def add_single_item(self, collection_id: UUID4, item: PostItem, clients, external_ids: bool = False):
        item_type = await utils.get_item_type(self.conn, collection_id)
        if isinstance(item, IdObject):
            if await self.add_item_by_id(collection_id, item, item_type, clients):
                return JSONResponse(status_code=201, content={"success": "Item added"})
            else:
                raise HTTPException(500, "Item could not be added (add item by id)")
        else:
            if await utils.check_item_type_post_put(item, item_type):
                if item_type in PRIMITIVE_TYPES:
                    _, new_items = await clients.primitive.add_primitives(item, external_ids, True, collection_id)
                    return new_items[0]
                elif item_type in ANNOTATION_TYPES:
                    _, new_items = await clients.annotation.add_annotations(item, external_ids, True, collection_id)
                    return new_items[0]
                elif item_type == CollectionItemType.CLASS:
                    _, new_items = await clients.aclass.add_classes(item, external_ids, True, collection_id)
                    return new_items[0]
                elif item_type in PIXELMAP_TYPES:
                    _, new_items = await clients.pixelmap.add_pixelmaps(item, external_ids, True, collection_id)
                    return new_items[0]
                elif item_type == CollectionItemType.WSI:
                    new_item = {"id": item.id}
                    await self.add_slide(collection_id, new_item)
                    return item
                elif item_type == CollectionItemType.COLLECTION:
                    new_item = await self.add_collection(item, clients, external_ids, True, collection_id)
                    return new_item
            else:
                raise HTTPException(405, "Item type and type of item not matching")

    async def add_item_by_id(self, collection_id: UUID4, item: PostItem, item_type: CollectionItemType, clients):
        if item_type == CollectionItemType.WSI:
            count = await self.set_slides(collection_id, [item.id])
        else:
            if await utils.check_type_ids(self.conn, [item.id], item_type):
                count = await self.add_items_by_id(collection_id, [item], item_type, clients)
            else:
                raise HTTPException(404, f"{item_type} not found")
        if count == 1:
            return True
        else:
            return False

    async def add_slide(self, collection_id: UUID4, item: Any):
        insert_sql = """
        INSERT INTO v3_c_collection_slides(
            collection_id,
            slide_id
        )
        VALUES($1, $2);
        """

        try:
            await self.conn.execute(insert_sql, collection_id, item["id"])
        except UniqueViolationError as e:
            raise HTTPException(405, "Slide already in collection") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Slide could not be added (insert)") from e

    async def add_item_list(self, collection_id: UUID4, items: PostItemList, clients, external_ids: bool = False):
        item_type = await utils.get_item_type(self.conn, collection_id)
        if len(items.items) == 0:
            raise HTTPException(404, "No items found")
        elif len(items.items) > settings.v3_post_limit:
            raise HTTPException(
                413,
                f"Number of posted items ({len(items.items)}) exceeds server limit of {settings.v3_post_limit}.",
            )

        first_item = items.items[0]
        if isinstance(first_item, IdObject):
            if await self.add_items_by_id(collection_id, items.items, item_type, clients):
                return JSONResponse(status_code=201, content={"success": "Items added"})
            else:
                raise HTTPException(500, "The resources could not be added (add items by id)")
        else:
            if await utils.check_item_type_post_put(first_item, item_type):
                new_items = []
                if item_type in PRIMITIVE_TYPES:
                    count, new_items = await clients.primitive.add_primitives(
                        items, external_ids=external_ids, raw=True, collection_id=collection_id
                    )
                elif item_type in ANNOTATION_TYPES:
                    count, new_items = await clients.annotation.add_annotations(
                        items, external_ids=external_ids, raw=True, collection_id=collection_id
                    )
                elif item_type == CollectionItemType.CLASS:
                    count, new_items = await clients.aclass.add_classes(
                        items, external_ids=external_ids, raw=True, collection_id=collection_id
                    )
                elif item_type in PIXELMAP_TYPES:
                    count, new_items = await clients.pixelmap.add_pixelmaps(
                        items, external_ids=external_ids, raw=True, collection_id=collection_id
                    )
                elif item_type == CollectionItemType.WSI:
                    ids = [item.id for item in items.items]
                    if len(ids) != len(set(ids)):
                        raise HTTPException(405, "Duplicate items found.")
                    new_items = [{"id": item_id, "type": "wsi"} for item_id in ids]
                    count = await self.set_slides(collection_id, ids)
                    if count != len(ids):
                        raise HTTPException(405, "Error while adding slides.")
                elif item_type == CollectionItemType.COLLECTION:
                    count, new_items = await self.add_collection(
                        items, clients, external_ids=external_ids, raw=True, collection_id=collection_id
                    )
                return JSONResponse({"item_count": count, "items": new_items}, status_code=201)
            else:
                raise HTTPException(405, "Item type and type of item not matching")

    async def query_items(
        self, collection_id: UUID4, item_query: ItemQuery, clients, skip: int, limit: int, raw: bool = False
    ):
        await utils.check_collection(self.conn, collection_id)

        collection = await self.get(
            collection_id=collection_id, shallow=True, with_leaf_ids=True, clients=clients, raw=True
        )
        item_type = collection["item_type"]
        item_count = collection["item_count"]

        if item_count == 0:
            if raw:
                return 0, []
            return JSONResponse({"item_count": 0, "items": []})
        elif item_count > settings.v3_item_limit and limit is None:
            raise HTTPException(
                413,
                f"Count of requested items ({item_count}) exceeds server limit of {settings.v3_item_limit}.",
            )

        if item_type in PRIMITIVE_TYPES:
            if item_query.references is not None:
                query = PrimitiveQuery(references=item_query.references, primitives=collection["item_ids"])
            else:
                query = PrimitiveQuery(primitives=collection["item_ids"])
            count, items = await clients.primitive.query(primitive_query=query, skip=skip, limit=limit, raw=True)
        elif item_type in ANNOTATION_TYPES:
            if item_query.references is not None:
                query = AnnotationQuery(
                    annotations=collection["item_ids"],
                    references=item_query.references,
                    viewport=item_query.viewport,
                    npp_viewing=item_query.npp_viewing,
                )
            else:
                query = AnnotationQuery(
                    annotations=collection["item_ids"],
                    viewport=item_query.viewport,
                    npp_viewing=item_query.npp_viewing,
                )
            count, items = await clients.annotation.query(
                annotation_query=query,
                with_classes=False,
                with_low_npp_centroids=False,
                skip=skip,
                limit=limit,
                raw=True,
            )
        elif item_type == CollectionItemType.CLASS:
            if item_query.references is not None:
                query = ClassQuery(references=item_query.references, classes=collection["item_ids"])
            else:
                query = ClassQuery(classes=collection["item_ids"])
            count, items = await clients.aclass.query(
                class_query=query,
                with_unique_class_values=False,
                skip=skip,
                limit=limit,
                raw=True,
            )
        elif item_type in PIXELMAP_TYPES:
            if item_query.references is not None:
                query = PixelmapQuery(references=item_query.references, pixelmaps=collection["item_ids"])
            else:
                query = PixelmapQuery(pixelmaps=collection["item_ids"])
            count, items = await clients.pixelmap.query(pixelmap_query=query, skip=skip, limit=limit, raw=True)
        elif item_type == CollectionItemType.WSI:
            select_slide_id_sql = """
            SELECT
                slide_id
            FROM
                v3_c_collection_slides
            WHERE
                collection_id = $1
            LIMIT $2 OFFSET $3;
            """

            try:
                result = await self.conn.fetch(select_slide_id_sql, collection_id, limit, skip)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Slide ids could not be queried (query items)") from e

            items = [{"id": r["slide_id"], "type": "wsi"} for r in result]
            count = len(items)

        elif item_type == CollectionItemType.COLLECTION:
            _item_ids = await self.get_collection_items(collection_id, reference_ids=item_query.references)

            collection_items = []
            for item_id in _item_ids:
                collection_items.append(
                    await self.get(
                        collection_id=item_id["id"],
                        shallow=True,
                        with_leaf_ids=True,
                        clients=clients,
                        raw=True,
                        is_item_query=True,
                    )
                )
            items = collection_items
            count = len(items)
        else:
            count = 0
            items = []

        if raw:
            return count, items

        return JSONResponse({"item_count": count, "items": items})

    async def _query_collections_as_items(
        self, collection_id: UUID4, references: List[str] = None, skip: int = None, limit: int = None
    ):
        sql = await sql_helper_insert_items.get_item_query_sql(references)

        args = [[collection_id], limit, skip]
        if references:
            args.append(references)

        try:
            result = await self.conn.fetch(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Collections could not be queried (item query)") from e

        items = [dict(r) for r in result]

        return len(items), items

    async def unique_references_query_items(self, collection_id: UUID4, item_query: ItemQuery, clients):
        await utils.check_collection(self.conn, collection_id)

        collection = await self.get(
            collection_id=collection_id, shallow=True, with_leaf_ids=True, clients=clients, raw=True
        )
        item_type = collection["item_type"]
        item_count = collection["item_count"]

        if item_count == 0:
            return JSONResponse(
                {"annotation": [], "collection": [], "wsi": [], "contains_items_without_reference": False}
            )

        if item_type in PRIMITIVE_TYPES:
            query = PrimitiveQuery(references=item_query.references, primitives=collection["item_ids"])
            return await clients.primitive.unique_references_query(query)
        elif item_type in ANNOTATION_TYPES:
            query = AnnotationQuery(
                annotations=collection["item_ids"],
                references=item_query.references,
                viewport=item_query.viewport,
                npp_viewing=item_query.npp_viewing,
            )
            return await clients.annotation.unique_references_query(query)
        elif item_type == CollectionItemType.CLASS:
            query = ClassQuery(references=item_query.references, classes=collection["item_ids"])
            return await clients.aclass.unique_references_query(query)
        elif item_type in PIXELMAP_TYPES:
            query = PixelmapQuery(references=item_query.references, pixelmaps=collection["item_ids"])
            return await clients.pixelmap.unique_references_query(query)
        elif item_type == CollectionItemType.COLLECTION:
            query = CollectionQuery(references=item_query.references)
            return await self.unique_references_query(collection_query=query, collection_id=collection_id)
        else:
            return JSONResponse(
                {"annotation": [], "collection": [], "wsi": [], "contains_items_without_reference": False}
            )

    async def delete_item(self, collection_id: UUID4, item_id: Id, clients):
        await utils.check_collection(self.conn, collection_id, "lock")

        item_type = await utils.get_item_type(self.conn, collection_id)

        delete_count = 0
        if item_type == CollectionItemType.WSI:
            if not await utils.check_if_item_exists(self.conn, collection_id, item_id):
                raise HTTPException(404, "Collection contains no item with given id")
            delete_sql = """
            WITH deleted AS (
                DELETE FROM
                    v3_c_collection_slides
                WHERE
                    collection_id = $1
                    AND slide_id = $2
                RETURNING *
            )
            SELECT
                COUNT(*)
            FROM
                deleted;
            """

            try:
                delete_count = await self.conn.fetchval(delete_sql, collection_id, item_id)
            except PostgresError as e:
                logger.debug(e)
                raise HTTPException(500, "Item could not be deleted") from e
        else:
            if item_type in PRIMITIVE_TYPES:
                delete_count = await clients.primitive.remove_from_collection(item_id, collection_id)
            elif item_type in ANNOTATION_TYPES:
                delete_count = await clients.annotation.remove_from_collection(item_id, collection_id)
            elif item_type == CollectionItemType.CLASS:
                delete_count = await clients.aclass.remove_from_collection(item_id, collection_id)
            elif item_type in PIXELMAP_TYPES:
                delete_count = await clients.pixelmap.remove_from_collection(item_id, collection_id)
            elif item_type == CollectionItemType.COLLECTION:
                delete_count = await self.remove_from_collection(item_id, collection_id)

        if delete_count > 0:
            return JSONResponse({"id": str(item_id)})
        else:
            raise HTTPException(404, f"Item {item_id} not in collection {collection_id}")
