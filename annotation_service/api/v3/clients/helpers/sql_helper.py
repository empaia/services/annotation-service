async def get_existing_views_sql():
    return """
    SELECT
        *
    FROM
        get_views_exists($1);
    """
