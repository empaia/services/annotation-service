from annotation_service.singletons import settings


async def get_insert_tmptable_sql(external_ids: bool, temp_table_name: str):
    if settings.allow_external_ids and external_ids:
        value_template = "($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)"
    else:
        value_template = "(gen_random_uuid(), $1, $2, $3, $4, $5, $6, $7, $8, $9, $10)"

    base_sql = f"""
    INSERT INTO {temp_table_name}(
        id,
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        item_type,
        is_locked,
        collection_ids
    )
    VALUES {value_template}
    """

    return base_sql


async def get_insert_sql(temp_table_name: str):
    return f"""
    INSERT INTO v3_c_collections SELECT * FROM {temp_table_name}
    RETURNING
    jsonb_build_object(
        'id', v3_c_collections.id,
        'type', v3_c_collections.type,
        'name', v3_c_collections.name,
        'description', v3_c_collections.description,
        'creator_id', v3_c_collections.creator_id,
        'creator_type', v3_c_collections.creator_type,
        'reference_id', v3_c_collections.reference_id,
        'reference_type', v3_c_collections.reference_type,
        'item_type', v3_c_collections.item_type,
        'is_locked', v3_c_collections.is_locked
    ) AS json;
    """


async def get_update_sql():
    return """
    UPDATE v3_c_collections
        SET name = $2,
            description = $3,
            creator_id = $4,
            creator_type = $5,
            reference_id = $6,
            reference_type = $7,
            item_type = $8
    WHERE id = $1;
    """


async def add_collection_id_sql():
    return """
    UPDATE v3_c_collections
        SET collection_ids = ARRAY_APPEND(collection_ids, $1)
    WHERE id = ANY($2);
    """
