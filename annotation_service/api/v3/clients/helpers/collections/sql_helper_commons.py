from typing import List


async def get_build_sql_str(raw: bool):
    if raw:
        build_sql_str = """
        SELECT
            item_count,
            items
        """
    else:
        build_sql_str = """
        SELECT
            json_build_object(
                'item_count', item_count,
                'items', items
            ) :: TEXT AS json
        """

    return build_sql_str


async def get_job_views_sql(views: List[str]):
    base_view_sql = """
    SELECT
        id,
        type,
        name,
        description,
        is_locked,
        item_type,
        creator_id,
        creator_type,
        reference_id,
        reference_type
    FROM
        {mat_view_name}
    WHERE
        (
            $1::TEXT[] IS NULL
            OR reference_id = ANY($1::TEXT[])
        )
        AND (
            $2::TEXT[] IS NULL
            OR creator_id = ANY($2::TEXT[])
        )
        AND (
            $3::TEXT[] IS NULL
            OR item_type = ANY($3::TEXT[])
        )
        AND (
            $4::uuid[] IS NULL
            OR id = ANY($4::uuid[])
        )
        AND (
            $5::uuid[] IS NULL
            OR collection_ids && $5::uuid[]
        )
    """

    view_sub_sql_queries = [base_view_sql.format(mat_view_name=view_name) for view_name in views]

    return " UNION ".join(view_sub_sql_queries)


async def get_references_base_sql():
    return """
    WITH collection_ids AS (
        SELECT
            *
        FROM
            get_collection_ids($1, $2, $3, $4, $5)
    ),
    ids_a_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS a_references
        FROM v3_c_collections AS c
        WHERE c.id in (SELECT * FROM collection_ids)
        and c.reference_type = 'annotation'
    ),
    ids_c_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS c_references
        FROM v3_c_collections AS c
        WHERE c.id in (SELECT * FROM collection_ids)
        and c.reference_type = 'collection'
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS w_references
        FROM v3_c_collections AS c
        WHERE c.id in (SELECT * FROM collection_ids)
        and c.reference_type = 'wsi'
    ),
    ids_n_refs AS (
        SELECT count(*) as n_count
        FROM v3_c_collections AS c
        WHERE c.id in (SELECT * FROM collection_ids)
        and c.reference_type IS NULL
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.a_references IS NULL THEN '[]'::jsonb
            ELSE ar.a_references
        END,
        'collection',
        CASE
            WHEN ar.c_references IS NULL THEN '[]'::jsonb
            ELSE ar.c_references
        END,
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END,
        'contains_items_without_reference',
        CASE
            WHEN ar.n_count > 0 THEN true
            ELSE false
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_a_refs, ids_c_refs, ids_w_refs, ids_n_refs
    ) AS ar
    """


async def get_references_jobs_sql():
    return """
    WITH collections AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS colls
    ),
    ids_a_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS a_references
        FROM collections AS c
        WHERE c.reference_type = 'annotation'
    ),
    ids_c_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS c_references
        FROM collections AS c
        WHERE c.reference_type = 'collection'
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(c.reference_id)) AS w_references
        FROM collections AS c
        WHERE c.reference_type = 'wsi'
    ),
    ids_n_refs AS (
        SELECT count(*) as n_count
        FROM collections AS c
        WHERE c.reference_type IS NULL
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.a_references IS NULL THEN '[]'::jsonb
            ELSE ar.a_references
        END,
        'collection',
        CASE
            WHEN ar.c_references IS NULL THEN '[]'::jsonb
            ELSE ar.c_references
        END,
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END,
        'contains_items_without_reference',
        CASE
            WHEN ar.n_count > 0 THEN true
            ELSE false
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_a_refs, ids_c_refs, ids_w_refs, ids_n_refs
    ) AS ar
    """
