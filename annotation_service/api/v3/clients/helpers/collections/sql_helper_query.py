from typing import List

from annotation_service.singletons import settings

from .sql_helper_commons import get_build_sql_str, get_job_views_sql, get_references_base_sql, get_references_jobs_sql


async def get_query_sql(raw: bool = False):
    base_sql = """
    WITH collection_ids AS (
        SELECT
            *
        FROM
            get_collection_ids($1, $2, $3, $4, $5)
    ),
    collection_count AS (
        SELECT
            COUNT(*)
        FROM
            collection_ids
    ),
    collections AS (
        SELECT
            v3cc.*,
            COUNT(v3ci.item_id) AS item_count,
            COALESCE(
                JSON_AGG(json_build_object('id', v3ci.item_id)) FILTER (
                    WHERE
                        v3ci.item_id IS NOT NULL
                ),
                '[]'
            ) AS items
        FROM
            v3_c_collections AS v3cc
            LEFT JOIN v3_c_collection_items AS v3ci ON v3ci.collection_id = v3cc.id
        WHERE
            v3cc.id IN (
                SELECT
                    *
                FROM
                    collection_ids
            )
        GROUP BY
            v3cc.id
        LIMIT $6 OFFSET $7
    )
    {build_sql_str}
    FROM
        (
            SELECT
                collection_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    {json_str} :: jsonb || json_strip_nulls(row_to_json(ROW)) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    *
                                FROM
                                    collections
                            ) ROW
                    )
                END items
            FROM
                collection_count
        ) AS sel;
    """

    build_sql_str = await get_build_sql_str(raw)
    limit_comparison_sql_str = (
        f"collection_count.count > {settings.v3_item_limit} AND ($6::int IS NULL OR $6::int > {settings.v3_item_limit})"
    )
    error_message = (
        f"'Count of requested items (%s) exceeds server limit of {settings.v3_item_limit}.', collection_count.count"
    )
    json_str = '\'{"description":null, "reference_id":null, "reference_type":null}\''

    return base_sql.format(
        build_sql_str=build_sql_str,
        limit_comparison_sql_str=limit_comparison_sql_str,
        error_message=error_message,
        json_str=json_str,
    )


async def get_references_sql(views: List[str] = None):
    base_sql = await get_references_jobs_sql() if views else await get_references_base_sql()

    if views:
        return base_sql.format(view_sql=await get_job_views_sql(views))
    else:
        return base_sql
