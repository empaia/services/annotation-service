from annotation_service.singletons import settings


async def get_single_sql():
    base_sql = """
    WITH empty_collection AS (
        SELECT
            id,
            TYPE,
            NAME,
            description,
            creator_id,
            creator_type,
            reference_id,
            reference_type,
            item_type,
            is_locked
        FROM
            v3_c_collections
        WHERE
            id = $1
    ),
    a_count AS (
        SELECT
            count(id) AS i_count,
            $1::uuid AS c_id
        FROM
            v3_a_annotations
        WHERE
            collection_ids && ARRAY [$1]::uuid[]
    ),
    p_count AS (
        SELECT
            count(id) AS i_count,
            $1::uuid AS c_id
        FROM
            v3_p_primitives
        WHERE
            collection_ids && ARRAY [$1]::uuid[]
    ),
    cl_count AS (
        SELECT
            count(id) AS i_count,
            $1::uuid AS c_id
        FROM
            v3_cl_classes
        WHERE
            collection_ids && ARRAY [$1]::uuid[]
    ),
    pm_count AS (
        SELECT
            count(id) AS i_count,
            $1::uuid AS c_id
        FROM
            v3_pm_pixelmaps
        WHERE
            collection_ids && ARRAY [$1]::uuid[]
    ),
    c_count AS (
        SELECT
            count(id) AS i_count,
            $1::uuid AS c_id
        FROM
            v3_c_collections
        WHERE
            collection_ids && ARRAY [$1]::uuid[]
    ),
    s_count AS (
        SELECT
            count(slide_id) AS i_count,
            $1::uuid AS c_id
        FROM
            v3_c_collection_slides
        WHERE
            collection_id = $1
    )
    SELECT
        '{"description":null, "reference_id":null, "reference_type":null}'::jsonb
        || json_strip_nulls(row_to_json(ROW))::jsonb AS collection
    FROM
        (
            SELECT
                c.*,
                CASE
                    WHEN c.item_type = ANY(ARRAY['point', 'line', 'arrow', 'circle', 'rectangle', 'polygon'])
                        THEN a_count.i_count
                    WHEN c.item_type = ANY(ARRAY['integer', 'float', 'bool', 'string'])
                        THEN p_count.i_count
                    WHEN c.item_type = 'class'
                        THEN cl_count.i_count
                    WHEN c.item_type = ANY(ARRAY['continuous_pixelmap', 'discrete_pixelmap', 'nominal_pixelmap'])
                        THEN pm_count.i_count
                    WHEN c.item_type = 'collection'
                        THEN c_count.i_count
                    WHEN c.item_type = 'wsi'
                        THEN s_count.i_count
                END AS item_count
            FROM
                empty_collection AS c
                JOIN a_count ON c.id = a_count.c_id
                JOIN p_count ON c.id = a_count.c_id
                JOIN cl_count ON c.id = a_count.c_id
                JOIN pm_count ON c.id = a_count.c_id
                JOIN c_count ON c.id = a_count.c_id
                JOIN s_count ON c.id = a_count.c_id
        ) AS ROW
    """

    return base_sql


async def get_all_sql():
    base_sql = """
    WITH c_count AS (
        SELECT
            count(*)
        FROM
            v3_c_collections
    )
    SELECT
        json_build_object('item_count', item_count, 'items', items) :: TEXT AS json
    FROM
        (
            SELECT
                c_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    {json_str} :: jsonb || json_strip_nulls(row_to_json(ROW)) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    v3cc.*,
                                    COUNT(v3ci.item_id) AS item_count,
                                    COALESCE(
                                        JSON_AGG(json_build_object('id', v3ci.item_id)) FILTER (
                                            WHERE
                                                v3ci.item_id IS NOT NULL
                                        ),
                                        '[]'
                                    ) AS items
                                FROM
                                    v3_c_collections AS v3cc
                                    LEFT JOIN v3_c_collection_items AS v3ci ON v3ci.collection_id = v3cc.id
                                GROUP BY
                                    v3cc.id
                                LIMIT $1 OFFSET $2
                            ) ROW
                    )
                END items
            FROM
                c_count
        ) sel;
    """

    limit_comparison_sql_str = (
        f"c_count.count > {settings.v3_item_limit} AND ($1::int IS NULL OR $1::int > {settings.v3_item_limit})"
    )

    error_message = f"'Count of requested items (%s) exceeds server limit of {settings.v3_item_limit}.', c_count.count"

    json_str = '\'{"description":null, "reference_id":null, "reference_type":null}\''

    return base_sql.format(
        limit_comparison_sql_str=limit_comparison_sql_str, error_message=error_message, json_str=json_str
    )
