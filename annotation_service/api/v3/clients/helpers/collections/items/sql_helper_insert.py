async def get_insert_items_tmptable_sql():
    return """
    INSERT INTO
        tmptable_v3_items(collection_id, item_id)
    VALUES
        ($1, $2);
    """


async def get_insert_items_sql(temp_table_name: str):
    return f"""
    WITH ROWS AS (
        INSERT INTO
            v3_c_collection_slides
        SELECT
            *
        FROM
            {temp_table_name}
        RETURNING 1
    )
    SELECT
        count(*)
    FROM
        ROWS;
    """


async def get_select_item_ids_sql():
    return """
    SELECT
        slide_id
    FROM
        v3_c_collection_slides
    WHERE
        collection_id = $1;
    """


async def get_item_query_sql(references: bool):
    base_sql = """
    SELECT
        id::TEXT,
        type,
        name,
        description,
        is_locked,
        item_type,
        creator_id,
        creator_type,
        reference_id,
        reference_type
    FROM
        v3_c_collections
    WHERE
        {references_sql}
        (
            $1::uuid[] IS NULL
            OR collection_ids && $1::uuid[]
        )
    LIMIT $2 OFFSET $3
    """

    if references:
        references_sql = """
        (
            $4::TEXT[] IS NULL
            OR reference_id = ANY($4::TEXT[])
        )
        AND
        """

        return base_sql.format(references_sql=references_sql)
    else:
        return base_sql.format(references_sql="")
