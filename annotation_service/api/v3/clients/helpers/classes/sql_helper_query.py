from typing import List

from annotation_service.singletons import settings

from .sql_helper_commons import (
    get_base_query_jobs_sql,
    get_base_query_sql,
    get_build_sql_str,
    get_job_views_sql,
    get_references_base_sql,
    get_references_jobs_sql,
)


async def get_query_sql(with_unique_class_values: bool, raw: bool = False, views: List[str] = None):
    base_sql = await get_base_query_jobs_sql() if views else await get_base_query_sql()

    build_sql_str = await get_build_sql_str(raw, with_unique_class_values, views)
    limit_comparison_sql_str = (
        f"class_count.count > {settings.v3_item_limit} AND ($5::int IS NULL OR $5::int > {settings.v3_item_limit})"
    )
    error_message = (
        f"'Count of requested items (%s) exceeds server limit of {settings.v3_item_limit}.', class_count.count"
    )

    if views:
        view_sql = await get_job_views_sql(views)

        return base_sql.format(
            view_sql=view_sql,
            build_sql_str=build_sql_str,
            limit_comparison_sql_str=limit_comparison_sql_str,
            error_message=error_message,
        )
    else:
        return base_sql.format(
            build_sql_str=build_sql_str,
            limit_comparison_sql_str=limit_comparison_sql_str,
            error_message=error_message,
        )


async def get_references_sql(views: List[str] = None):
    base_sql = await get_references_jobs_sql() if views else await get_references_base_sql()

    if views:
        return base_sql.format(view_sql=await get_job_views_sql(views))
    else:
        return base_sql
