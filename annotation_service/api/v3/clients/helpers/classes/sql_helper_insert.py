async def get_insert_sql(allow_external_ids: bool, temp_table_name: str):
    id_column = "id" if allow_external_ids else "gen_random_uuid()"

    base_sql = f"""
    INSERT INTO
        v3_cl_classes
    SELECT
        {id_column},
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        value,
        is_locked,
        type,
        collection_ids
    FROM
        {temp_table_name}
    RETURNING jsonb_build_object(
        'id', v3_cl_classes.id,
        'type', v3_cl_classes.type,
        'creator_id', v3_cl_classes.creator_id,
        'creator_type', v3_cl_classes.creator_type,
        'reference_id', v3_cl_classes.reference_id,
        'reference_type', v3_cl_classes.reference_type,
        'value', v3_cl_classes.value,
        'is_locked', v3_cl_classes.is_locked
    );
    """

    columns_to_copy = [
        "creator_id",
        "creator_type",
        "reference_id",
        "reference_type",
        "value",
        "is_locked",
        "type",
        "collection_ids",
    ]

    if allow_external_ids:
        columns_to_copy.insert(0, "id")

    return columns_to_copy, base_sql


async def get_update_sql():
    return """
    UPDATE v3_cl_classes
        SET creator_id = $2,
            creator_type = $3,
            reference_id = $4,
            reference_type = $5,
            value = $6
    WHERE id=$1;
    """


async def add_collection_id_sql():
    return """
    UPDATE v3_cl_classes
        SET collection_ids = ARRAY_APPEND(collection_ids, §1)
    WHERE id = ANY($2);
    """
