from annotation_service.singletons import settings


async def get_single_sql():
    base_sql = """
    SELECT
        row_to_json(ROW) :: jsonb AS class
    FROM
        (
            SELECT
                v3cl.id,
                v3cl.type,
                v3cl.value,
                v3cl.creator_id,
                v3cl.creator_type,
                v3cl.reference_id,
                v3cl.reference_type,
                v3cl.is_locked
            FROM
                v3_cl_classes AS v3cl
            WHERE
                v3cl.id = $1
        ) AS ROW;
    """

    return base_sql


async def get_all_sql(with_unique_class_values: bool = False):
    base_sql = """
    WITH cl_count AS (
        SELECT
            count(*)
        FROM
            v3_cl_classes
    )
    SELECT
        json_build_object(
            'item_count', item_count,
            'items', items
            {unique_values_str}
        ) :: TEXT AS json
    FROM
        (
            SELECT
                cl_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    row_to_json(ROW) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    classes.*
                                FROM
                                    (
                                        SELECT
                                            v3cl.id,
                                            v3cl.type,
                                            v3cl.value,
                                            v3cl.creator_id,
                                            v3cl.creator_type,
                                            v3cl.reference_id,
                                            v3cl.reference_type,
                                            v3cl.is_locked
                                        FROM
                                            v3_cl_classes AS v3cl
                                        LIMIT
                                            $1 OFFSET $2
                                    ) AS classes
                            ) ROW
                    )
                END items
            FROM
                cl_count
        ) sel;
    """

    limit_comparison_sql_str = (
        f"cl_count.count > {settings.v3_item_limit} AND ($1::int IS NULL OR $1::int > {settings.v3_item_limit})"
    )
    error_message = f"'Count of requested items (%s) exceeds server limit of {settings.v3_item_limit}.', cl_count.count"
    unique_values_str = ""
    if with_unique_class_values:
        unique_values_str = """
        , 'unique_class_values',
        (
            SELECT
                array_agg(vals.value)
            FROM
                (
                    SELECT
                        DISTINCT VALUE
                    FROM
                        v3_cl_classes
                ) AS vals
        )
        """

    return base_sql.format(
        limit_comparison_sql_str=limit_comparison_sql_str,
        error_message=error_message,
        unique_values_str=unique_values_str,
    )
