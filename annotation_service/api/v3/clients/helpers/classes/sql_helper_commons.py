from typing import List


async def get_base_query_sql():
    return """
    WITH class_ids AS (
        SELECT
            *
        FROM
            get_class_ids($1, $2, $3, $4)
    ),
    class_count AS (
        SELECT
            COUNT(*)
        FROM
            class_ids
    ),
    classes AS (
        SELECT
            v3cl.id,
            v3cl.type,
            v3cl.creator_id,
            v3cl.creator_type,
            v3cl.reference_id,
            v3cl.reference_type,
            v3cl.is_locked,
            v3cl.value
        FROM
            v3_cl_classes AS v3cl
        WHERE
            v3cl.id IN (
                SELECT
                    *
                FROM
                    class_ids
            )
        LIMIT $5 OFFSET $6
    )
    {build_sql_str}
    FROM
        (
            SELECT
                class_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    row_to_json(ROW) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    *
                                FROM
                                    classes
                            ) ROW
                    )
                END items
            FROM
                class_count
        ) AS sel;
    """


async def get_base_query_jobs_sql():
    return """
    WITH classes AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS cls
    ),
    class_count AS (
        SELECT
            COUNT(*)
        FROM
            classes
    ),
    classes_limit AS (
        SELECT
            *
        FROM
            classes
        LIMIT
            $5 OFFSET $6
    )
    {build_sql_str}
    FROM
        (
            SELECT
                class_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    row_to_json(ROW) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    *
                                FROM
                                    classes_limit
                            ) ROW
                    )
                END items
            FROM
                class_count
        ) AS sel;
    """


async def get_class_query_jobs_for_annots_sql(classes_queried: bool):
    if classes_queried:
        return """
        SELECT
            *
        FROM
            (
                {class_view_sql}
            ) AS cls
        """
    else:
        return """
        SELECT
            NULL::uuid AS id,
            NULL::TEXT AS type,
            NULL::uuid AS reference_id,
            NULL::TEXT AS reference_type,
            NULL::TEXT AS creator_id,
            NULL::TEXT AS creator_type,
            NULL::boolean AS is_locked,
            NULL::TEXT AS value
        """


async def get_build_sql_str(raw: bool, with_unique_class_values: bool, use_views: bool):
    if raw:
        build_sql_str = """
        SELECT
            item_count,
            items
            {unique_values_str}
        """
    else:
        build_sql_str = """
        SELECT
            json_build_object(
                'item_count', item_count,
                'items', items
                {unique_values_str}
            ) :: TEXT AS json
        """

    if use_views:
        unique_class_value_sql = """
        SELECT
            array_agg(vals.value)
        FROM
            (
                SELECT
                    DISTINCT value
                FROM
                    classes
            ) AS vals
        """
    else:
        unique_class_value_sql = """
        SELECT
            array_agg(vals.value)
        FROM
            (
                SELECT
                    DISTINCT value
                FROM
                    v3_cl_classes AS v3cl
                WHERE
                    v3cl.id IN (
                        SELECT
                            *
                        FROM
                            class_ids
                    )
            ) AS vals
        """

    unique_values_str = ""
    if with_unique_class_values:
        unique_values_str = f", ({unique_class_value_sql})"
        if not raw:
            unique_values_str = ", 'unique_class_values'" + unique_values_str

    return build_sql_str.format(unique_values_str=unique_values_str)


async def get_job_views_sql(views: List[str]):
    base_view_sql = """
    SELECT
        id,
        type,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        is_locked,
        value
    FROM
        {mat_view_name}
    WHERE
        (
            $1::uuid[] IS NULL
            OR reference_id = ANY($1::uuid[])
        )
        AND (
            $2::TEXT[] IS NULL
            OR creator_id = ANY($2::TEXT[])
        )
        AND (
            $3::uuid[] IS NULL
            OR id = ANY($3::uuid[])
        )
        AND (
            $4::uuid[] IS NULL
            OR collection_ids && $4::uuid[]
        )
    """

    view_sub_sql_queries = [base_view_sql.format(mat_view_name=view_name) for view_name in views]

    return " UNION ".join(view_sub_sql_queries)


async def get_class_job_views_for_annots_sql(views: List[str]):
    base_view_sql = """
    SELECT
        id,
        type,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        is_locked,
        value
    FROM
        {mat_view_name}
    WHERE
        (
            $2::TEXT[] IS NULL
            OR creator_id = ANY($2::TEXT[])
        )
        AND (
            $6::TEXT[] IS NULL
            OR value = ANY($6::TEXT[])
        )
    """

    view_sub_sql_queries = [base_view_sql.format(mat_view_name=view_name) for view_name in views]

    return " UNION ".join(view_sub_sql_queries)


async def get_references_base_sql():
    return """
    WITH class_ids AS (
        SELECT
            *
        FROM
            get_class_ids($1, $2, $3, $4)
    ),
    ids_refs AS (
        SELECT JSONB_AGG(DISTINCT(cl.reference_id)) AS references
        FROM v3_cl_classes AS cl
        WHERE cl.id in (SELECT * FROM class_ids)
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.references IS NULL THEN '[]'::jsonb
            ELSE ar.references
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_refs
    ) AS ar
    """


async def get_references_jobs_sql():
    return """
    WITH classes AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS cls
    ),
    ids_refs AS (
        SELECT JSONB_AGG(DISTINCT(cl.reference_id)) AS references
        FROM classes AS cl
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.references IS NULL THEN '[]'::jsonb
            ELSE ar.references
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_refs
    ) AS ar
    """
