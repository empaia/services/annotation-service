from typing import List

from annotation_service.singletons import settings

from .sql_helper_commons import (
    get_base_query_jobs_sql,
    get_base_query_sql,
    get_build_sql_str,
    get_job_views_sql,
    get_references_base_sql,
    get_references_jobs_sql,
)


async def get_query_sql(raw: bool = False, views: List[str] = None):
    base_sql = await get_base_query_jobs_sql() if views else await get_base_query_sql()

    build_sql_str = await get_build_sql_str(raw)
    limit_comparison_sql_str = (
        f"pixelmap_count.count > {settings.v3_item_limit} AND ($7::int IS NULL OR $7::int > {settings.v3_item_limit})"
    )
    error_message = (
        f"'Count of requested items (%s) exceeds server limit of {settings.v3_item_limit}.', pixelmap_count.count"
    )
    json_str = "'{\"description\":null}'"

    if views:
        view_sql = await get_job_views_sql(views)

        return base_sql.format(
            view_sql=view_sql,
            build_sql_str=build_sql_str,
            limit_comparison_sql_str=limit_comparison_sql_str,
            error_message=error_message,
            json_str=json_str,
        )
    else:
        return base_sql.format(
            build_sql_str=build_sql_str,
            limit_comparison_sql_str=limit_comparison_sql_str,
            error_message=error_message,
            json_str=json_str,
        )


async def get_references_sql(views: List[str] = None):
    base_sql = await get_references_jobs_sql() if views else await get_references_base_sql()

    if views:
        return base_sql.format(view_sql=await get_job_views_sql(views))
    else:
        return base_sql
