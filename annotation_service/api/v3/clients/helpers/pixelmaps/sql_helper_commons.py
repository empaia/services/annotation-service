from typing import List


async def get_base_query_sql():
    return """
    WITH pixelmap_ids AS (
        SELECT
            *
        FROM
            get_pixelmap_ids($1, $2, $3, $4, $5)
    ),
    pixelmap_count AS (
        SELECT
            COUNT(*)
        FROM
            pixelmap_ids
    ),
    pixelmaps AS (
        SELECT
            v3pm.id,
            v3pm.type,
            v3pm.name,
            v3pm.description,
            v3pm.creator_id,
            v3pm.creator_type,
            v3pm.reference_id,
            v3pm.reference_type,
            v3pm.levels,
            v3pm.channel_count,
            v3pm.tilesize,
            v3pm.channel_class_mapping,
            EXTRACT(epoch FROM v3pm.created_at)::int as created_at,
            EXTRACT(epoch FROM v3pm.updated_at)::int as updated_at,
            v3pm.element_type,
            to_json(v3pm.min_value_int) AS min_value,
            to_json(v3pm.min_value_float) AS min_value,
            to_json(v3pm.neutral_value_int) AS neutral_value,
            to_json(v3pm.neutral_value_float) AS neutral_value,
            to_json(v3pm.max_value_int) AS max_value,
            to_json(v3pm.max_value_float) AS max_value,
            v3pm.element_class_mapping
        FROM
            v3_pm_pixelmaps AS v3pm
        WHERE
            v3pm.id IN (
                SELECT
                    *
                FROM
                    pixelmap_ids
            )
        LIMIT $6 OFFSET $7
    )
    {build_sql_str}
    FROM
        (
            SELECT
                pixelmap_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    {json_str} :: jsonb || json_strip_nulls(row_to_json(ROW)) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    *
                                FROM
                                    pixelmaps
                            ) ROW
                    )
                END items
            FROM
                pixelmap_count
        ) AS sel;
    """


async def get_base_query_jobs_sql():
    return """
    WITH pixelmaps AS (
        SELECT
            id,
            type,
            name,
            description,
            creator_id,
            creator_type,
            reference_id,
            reference_type,
            levels,
            channel_count,
            tilesize,
            channel_class_mapping,
            EXTRACT(epoch FROM created_at)::int as created_at,
            EXTRACT(epoch FROM updated_at)::int as updated_at,
            element_type,
            to_json(min_value_int) AS min_value,
            to_json(min_value_float) AS min_value,
            to_json(neutral_value_int) AS neutral_value,
            to_json(neutral_value_float) AS neutral_value,
            to_json(max_value_int) AS max_value,
            to_json(max_value_float) AS max_value,
            element_class_mapping
        FROM
            (
                {view_sql}
            ) AS pixmaps
    ),
    pixelmap_count AS (
        SELECT
            COUNT(*)
        FROM
            pixelmaps
    ),
    pixelmaps_limit AS (
        SELECT
            *
        FROM
            pixelmaps
        LIMIT
            $6 OFFSET $7
    )
    {build_sql_str}
    FROM
        (
            SELECT
                pixelmap_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    {json_str} :: jsonb || json_strip_nulls(row_to_json(ROW)) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    *
                                FROM
                                    pixelmaps_limit
                            ) ROW
                    )
                END items
            FROM
                pixelmap_count
        ) AS sel;
    """


async def get_build_sql_str(raw: bool):
    if raw:
        build_sql_str = """
        SELECT
            item_count,
            items
        """
    else:
        build_sql_str = """
        SELECT
            json_build_object(
                'item_count', item_count,
                'items', items
            ) :: TEXT AS json
        """

    return build_sql_str


async def get_job_views_sql(views: List[str]):
    base_view_sql = """
    SELECT
        id,
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        levels,
        channel_count,
        tilesize,
        channel_class_mapping,
        created_at,
        updated_at,
        element_type,
        min_value_int,
        min_value_float,
        neutral_value_int,
        neutral_value_float,
        max_value_int,
        max_value_float,
        element_class_mapping
    FROM
        {mat_view_name}
    WHERE
        (
            $1::TEXT[] IS NULL
            OR reference_id = ANY($1::TEXT[])
        )
        AND (
            $2::TEXT[] IS NULL
            OR creator_id = ANY($2::TEXT[])
        )
        AND (
            $3::TEXT[] IS NULL
            OR type = ANY($3::TEXT[])
        )
        AND (
            $4::uuid[] IS NULL
            OR id = ANY($4::uuid[])
        )
        AND (
            $5::uuid[] IS NULL
            OR collection_ids && $5::uuid[]
        )
    """

    view_sub_sql_queries = [base_view_sql.format(mat_view_name=view_name) for view_name in views]

    return " UNION ".join(view_sub_sql_queries)


async def get_references_base_sql():
    return """
    WITH pixelmap_ids AS (
        SELECT
            *
        FROM
            get_pixelmap_ids($1, $2, $3, $4, $5)
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(pm.reference_id)) AS w_references
        FROM v3_pm_pixelmaps AS pm
        WHERE pm.id in (SELECT * FROM pixelmap_ids)
        and pm.reference_type = 'wsi'
    )
    SELECT jsonb_build_object(
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_w_refs
    ) AS ar
    """


async def get_references_jobs_sql():
    return """
    WITH pixelmaps AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS pixmaps
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(pm.reference_id)) AS w_references
        FROM pixelmaps AS pm
        WHERE pm.reference_type = 'wsi'
    )
    SELECT jsonb_build_object(
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_w_refs
    ) AS ar
    """
