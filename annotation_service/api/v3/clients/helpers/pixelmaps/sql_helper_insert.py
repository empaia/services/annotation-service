async def get_insert_sql(allow_external_ids: bool, temp_table_name: str):
    id_column = "id" if allow_external_ids else "gen_random_uuid()"

    base_sql = f"""
    INSERT INTO
        v3_pm_pixelmaps
    SELECT
        {id_column},
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        levels,
        channel_count,
        tilesize,
        channel_class_mapping,
        element_type,
        min_value_int,
        min_value_float,
        neutral_value_int,
        neutral_value_float,
        max_value_int,
        max_value_float,
        element_class_mapping,
        current_timestamp,
        current_timestamp,
        collection_ids
    FROM
        {temp_table_name}
    RETURNING '{{"description":null}}' :: jsonb || jsonb_strip_nulls(
            jsonb_build_object(
                'id', v3_pm_pixelmaps.id,
                'type', v3_pm_pixelmaps.type,
                'name', v3_pm_pixelmaps.name,
                'description', v3_pm_pixelmaps.description,
                'creator_id', v3_pm_pixelmaps.creator_id,
                'creator_type', v3_pm_pixelmaps.creator_type,
                'reference_id', v3_pm_pixelmaps.reference_id,
                'reference_type', v3_pm_pixelmaps.reference_type,
                'levels', v3_pm_pixelmaps.levels,
                'channel_count', v3_pm_pixelmaps.channel_count,
                'tilesize', v3_pm_pixelmaps.tilesize,
                'channel_class_mapping', v3_pm_pixelmaps.channel_class_mapping,
                'element_type', v3_pm_pixelmaps.element_type,
                'min_value', coalesce(v3_pm_pixelmaps.min_value_int, v3_pm_pixelmaps.min_value_float),
                'neutral_value', coalesce(v3_pm_pixelmaps.neutral_value_int, v3_pm_pixelmaps.neutral_value_float),
                'max_value', coalesce(v3_pm_pixelmaps.max_value_int, v3_pm_pixelmaps.max_value_float),
                'element_class_mapping', v3_pm_pixelmaps.element_class_mapping,
                'created_at', EXTRACT(epoch FROM v3_pm_pixelmaps.created_at) :: INT,
                'updated_at', EXTRACT(epoch FROM v3_pm_pixelmaps.updated_at) :: INT
            )
        );
    """

    columns_to_copy = [
        "type",
        "name",
        "description",
        "creator_id",
        "creator_type",
        "reference_id",
        "reference_type",
        "levels",
        "channel_count",
        "tilesize",
        "channel_class_mapping",
        "element_type",
        "min_value_int",
        "min_value_float",
        "neutral_value_int",
        "neutral_value_float",
        "max_value_int",
        "max_value_float",
        "element_class_mapping",
        "collection_ids",
    ]

    if allow_external_ids:
        columns_to_copy.insert(0, "id")

    return columns_to_copy, base_sql


async def get_update_sql():
    return """
    UPDATE v3_pm_pixelmaps
        SET name = $2,
            description = $3,
            channel_class_mapping = $4,
            min_value_int = $5,
            min_value_float = $6,
            neutral_value_int = $7,
            neutral_value_float = $8,
            max_value_int = $9,
            max_value_float = $10,
            element_class_mapping = $11
    WHERE id = $1;
    """
