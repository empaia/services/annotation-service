async def get_single_sql():
    base_sql = """
    SELECT
        '{"description":null}' :: jsonb ||
        json_strip_nulls(row_to_json(ROW)) :: jsonb AS pixelmap
    FROM
        (
            SELECT
                v3pm.id,
                v3pm.type,
                v3pm.name,
                v3pm.description,
                v3pm.creator_id,
                v3pm.creator_type,
                v3pm.reference_id,
                v3pm.reference_type,
                v3pm.levels,
                v3pm.channel_count,
                v3pm.tilesize,
                v3pm.channel_class_mapping,
                EXTRACT(epoch FROM v3pm.created_at)::int as created_at,
                EXTRACT(epoch FROM v3pm.updated_at)::int as updated_at,
                v3pm.element_type,
                to_json(v3pm.min_value_int) AS min_value,
                to_json(v3pm.min_value_float) AS min_value,
                to_json(v3pm.neutral_value_int) AS neutral_value,
                to_json(v3pm.neutral_value_float) AS neutral_value,
                to_json(v3pm.max_value_int) AS max_value,
                to_json(v3pm.max_value_float) AS max_value,
                v3pm.element_class_mapping
            FROM
                v3_pm_pixelmaps AS v3pm
            WHERE
                v3pm.id = $1
        ) AS ROW;
    """

    return base_sql
