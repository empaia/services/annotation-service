from asyncpg import Box
from shapely.geometry import Polygon

from annotation_service.models.v3.annotation.annotations import Annotation


async def get_centroid(annotation: Annotation):
    annot_type = annotation.type
    if annot_type == "point":
        return annotation.coordinates
    elif annot_type == "line":
        return [
            round((annotation.coordinates[0][0] + annotation.coordinates[1][0]) / 2),
            round((annotation.coordinates[0][1] + annotation.coordinates[1][1]) / 2),
        ]
    elif annot_type == "arrow":
        return [
            round((annotation.head[0] + annotation.tail[0]) / 2),
            round((annotation.head[1] + annotation.tail[1]) / 2),
        ]
    elif annot_type == "circle":
        return annotation.center
    elif annot_type == "rectangle":
        return [
            round((annotation.upper_left[0] * 2 + annotation.width) / 2),
            round((annotation.upper_left[1] * 2 + annotation.height) / 2),
        ]
    elif annot_type == "polygon":
        centroid = Polygon(annotation.coordinates).centroid.coords
        return [round(centroid[0][0]), round(centroid[0][1])]


async def get_bounding_box(annotation: Annotation):
    annot_type = annotation.type
    if annot_type == "point":
        return Box(
            [annotation.coordinates[0], annotation.coordinates[1]],
            [annotation.coordinates[0], annotation.coordinates[1]],
        )
    elif annot_type == "line":
        return Box(
            [annotation.coordinates[0][0], annotation.coordinates[0][1]],
            [annotation.coordinates[1][0], annotation.coordinates[1][1]],
        )
    elif annot_type == "arrow":
        return Box([annotation.head[0], annotation.head[1]], [annotation.tail[0], annotation.tail[1]])
    elif annot_type == "circle":
        return Box(
            [annotation.center[0] - annotation.radius, annotation.center[1] - annotation.radius],
            [annotation.center[0] + annotation.radius, annotation.center[1] + annotation.radius],
        )
    elif annot_type == "rectangle":
        return Box(
            [annotation.upper_left[0], annotation.upper_left[1]],
            [annotation.upper_left[0] + annotation.width, annotation.upper_left[1] + annotation.height],
        )
    elif annot_type == "polygon":
        x_coords = [x for x in [p[0] for p in annotation.coordinates]]
        y_coords = [y for y in [p[1] for p in annotation.coordinates]]
        return Box([min(x_coords), min(y_coords)], [max(x_coords), max(y_coords)])
