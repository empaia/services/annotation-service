from annotation_service.models.v3.annotation.annotations import AnnotationType

TYPE_COLUMN_NAMES_RESPONSE = {
    **dict.fromkeys(
        [AnnotationType.POINT, AnnotationType.LINE, AnnotationType.POLYGON],
        "'coordinates', v3_a_annotations.coordinates",
    ),
    AnnotationType.ARROW: "'head', v3_a_annotations.head, 'tail', v3_a_annotations.tail",
    AnnotationType.CIRCLE: "'center', v3_a_annotations.center, 'radius', v3_a_annotations.radius",
    AnnotationType.RECTANGLE: """
    'upper_left', v3_a_annotations.upper_left,
    'width', v3_a_annotations.width,
    'height', v3_a_annotations.height
    """,
}


async def get_insert_sql(annotation_type: AnnotationType, allow_external_ids: bool, temp_table_name: str):
    id_column = "id" if allow_external_ids else "gen_random_uuid()"

    base_sql = f"""
    INSERT INTO
        v3_a_annotations
    SELECT
        {id_column},
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        npp_created,
        is_locked,
        current_timestamp,
        current_timestamp,
        npp_viewing_min,
        npp_viewing_max,
        coordinates,
        head,
        tail,
        center,
        radius,
        upper_left,
        width,
        height,
        centroid,
        bounding_box,
        npp_viewing_min_query,
        npp_viewing_max_query,
        collection_ids,
        class_values
    FROM
        {temp_table_name}
    RETURNING '{{"description":null, "npp_viewing":null}}' :: jsonb || jsonb_strip_nulls(
            jsonb_build_object(
                'id', v3_a_annotations.id,
                'type', v3_a_annotations.type,
                'name', v3_a_annotations.name,
                'description', v3_a_annotations.description,
                'creator_id', v3_a_annotations.creator_id,
                'creator_type', v3_a_annotations.creator_type,
                'reference_id', v3_a_annotations.reference_id,
                'reference_type', v3_a_annotations.reference_type,
                'npp_created', v3_a_annotations.npp_created,
                'npp_viewing',
                NULLIF(
                    to_jsonb(
                        array_remove(
                            ARRAY[v3_a_annotations.npp_viewing_min, v3_a_annotations.npp_viewing_max], NULL
                        )
                    ),
                    '[]'
                ),
                'is_locked', v3_a_annotations.is_locked,
                'created_at', EXTRACT(epoch FROM v3_a_annotations.created_at) :: INT,
                'updated_at', EXTRACT(epoch FROM v3_a_annotations.updated_at) :: INT,
                {TYPE_COLUMN_NAMES_RESPONSE[annotation_type]},
                'centroid', v3_a_annotations.centroid
            )
        );
    """

    columns_to_copy = [
        "type",
        "name",
        "description",
        "creator_id",
        "creator_type",
        "reference_id",
        "reference_type",
        "npp_created",
        "is_locked",
        "npp_viewing_min",
        "npp_viewing_max",
        "coordinates",
        "head",
        "tail",
        "center",
        "radius",
        "upper_left",
        "width",
        "height",
        "centroid",
        "bounding_box",
        "npp_viewing_min_query",
        "npp_viewing_max_query",
        "collection_ids",
        "class_values",
    ]

    if allow_external_ids:
        columns_to_copy.insert(0, "id")

    return columns_to_copy, base_sql


async def add_collection_id_sql():
    return """
    UPDATE v3_a_annotations
        SET collection_ids = ARRAY_APPEND(collection_ids, $1)
    WHERE id = ANY($2);
    """


async def add_class_values_sql():
    return """
    UPDATE
        v3_a_annotations
    SET
        class_values = ARRAY_APPEND(class_values, ref_class_values.class_value)
    FROM
        (
            VALUES
                ($1::uuid, $2)
        ) AS ref_class_values (reference_id, class_value)
    WHERE
        v3_a_annotations.id = ref_class_values.reference_id;
    """
