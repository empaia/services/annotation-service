from typing import List

from annotation_service.singletons import settings

from ..classes import sql_helper_commons as class_sql_helper
from .sql_helper_commons import (
    get_base_query_jobs_sql,
    get_base_query_sql,
    get_build_sql_str,
    get_count_base_sql,
    get_count_jobs_sql,
    get_job_views_sql,
    get_position_base_sql,
    get_position_jobs_sql,
    get_references_base_sql,
    get_references_jobs_sql,
    get_values_base_sql,
    get_values_jobs_sql,
    get_viewer_base_sql,
    get_viewer_jobs_sql,
)


async def get_query_sql(
    with_centroids: bool, raw: bool = False, views: List[str] = None, class_views: List[str] = None
):
    base_sql = await get_base_query_jobs_sql() if views else await get_base_query_sql()

    build_sql_str = await get_build_sql_str(raw, with_centroids)
    limit_comparison_sql_str = (
        f"annotation_count.count > {settings.v3_item_limit} AND "
        f"($11::int IS NULL OR $11::int > {settings.v3_item_limit})"
    )
    error_message = (
        f"'Count of requested items (%s) exceeds server limit of {settings.v3_item_limit}.', annotation_count.count"
    )

    if views:
        view_sql = await get_job_views_sql(views)
        class_sql = await class_sql_helper.get_class_query_jobs_for_annots_sql(
            classes_queried=True if class_views else False
        )
        if class_views:
            class_view_sql = await class_sql_helper.get_class_job_views_for_annots_sql(class_views)
            class_sql = class_sql.format(class_view_sql=class_view_sql)

        return base_sql.format(
            class_sql=class_sql,
            view_sql=view_sql,
            build_sql_str=build_sql_str,
            limit_comparison_sql_str=limit_comparison_sql_str,
            error_message=error_message,
        )
    else:
        return base_sql.format(
            build_sql_str=build_sql_str, limit_comparison_sql_str=limit_comparison_sql_str, error_message=error_message
        )


async def get_references_sql(views: List[str] = None):
    base_sql = await get_references_jobs_sql() if views else await get_references_base_sql()

    if views:
        return base_sql.format(view_sql=await get_job_views_sql(views))
    else:
        return base_sql


async def get_viewer_sql(views: List[str] = None):
    base_sql = await get_viewer_jobs_sql() if views else await get_viewer_base_sql()

    if views:
        return base_sql.format(view_sql=await get_job_views_sql(views))
    else:
        return base_sql


async def get_position_sql(views: List[str] = None):
    base_sql = await get_position_jobs_sql() if views else await get_position_base_sql()

    if views:
        return base_sql.format(view_sql=await get_job_views_sql(views))
    else:
        return base_sql


async def get_count_sql(views: List[str] = None):
    base_sql = await get_count_jobs_sql() if views else await get_count_base_sql()

    if views:
        return base_sql.format(view_sql=await get_job_views_sql(views))
    else:
        return base_sql


async def get_values_sql(views: List[str] = None, class_views: List[str] = None):
    base_sql = await get_values_jobs_sql() if views else await get_values_base_sql()

    if views:
        view_sql = await get_job_views_sql(views)
        class_sql = await class_sql_helper.get_class_query_jobs_for_annots_sql(classes_queried=True)
        if class_views:
            class_view_sql = await class_sql_helper.get_class_job_views_for_annots_sql(class_views)
            class_sql = class_sql.format(class_view_sql=class_view_sql)

        return base_sql.format(
            class_sql=class_sql,
            view_sql=view_sql,
        )
    else:
        return base_sql
