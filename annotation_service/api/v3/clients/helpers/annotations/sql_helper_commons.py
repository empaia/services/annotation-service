from typing import List


async def get_base_query_sql():
    return """
    WITH annotation_ids AS (
        SELECT
            *
        FROM
            get_annotation_ids($1, $2, $3, $4, $5, $6, $7)
    ),
    annotation_ids_npp AS (
        SELECT
            v3aa.id
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($8, v3aa.npp_viewing_min_query) <= LEAST($9, v3aa.npp_viewing_max_query)
    ),
    annotation_count AS (
        SELECT
            COUNT(*)
        FROM
            annotation_ids_npp
    ),
    annotations AS (
        SELECT
            annots.*,
            CASE
                WHEN $10 IS TRUE THEN COALESCE(
                    json_agg(
                        json_build_object(
                            'id', cl.id,
                            'type', cl.type,
                            'creator_id', cl.creator_id,
                            'creator_type', cl.creator_type,
                            'reference_id', cl.reference_id,
                            'reference_type', cl.reference_type,
                            'value', cl.value,
                            'is_locked', cl.is_locked
                        )
                    ) FILTER (
                        WHERE
                            cl.id IS NOT NULL
                    ),
                    '[]'
                )
            END classes
        FROM
            (
                SELECT
                    v3aa.id,
                    v3aa.type,
                    v3aa.name,
                    v3aa.description,
                    v3aa.creator_id,
                    v3aa.creator_type,
                    v3aa.reference_id,
                    v3aa.reference_type,
                    v3aa.npp_created,
                    NULLIF(
                        to_jsonb(
                            array_remove(
                                ARRAY [ v3aa.npp_viewing_min,
                                v3aa.npp_viewing_max ],
                                NULL
                            )
                        ),
                        '[]'
                    ) AS npp_viewing,
                    v3aa.is_locked,
                    v3aa.created_at,
                    v3aa.updated_at,
                    v3aa.coordinates,
                    v3aa.head,
                    v3aa.tail,
                    v3aa.center,
                    v3aa.radius,
                    v3aa.upper_left,
                    v3aa.width,
                    v3aa.height,
                    v3aa.centroid
                FROM
                    v3_a_annotations AS v3aa
                WHERE
                    v3aa.id IN (
                        SELECT
                            *
                        FROM
                            annotation_ids_npp
                    )
                ORDER BY
                    v3aa.updated_at DESC,
                    v3aa.id ASC
                LIMIT
                    $11 OFFSET $12
            ) AS annots
            LEFT JOIN v3_cl_classes AS cl ON $10
            AND cl.reference_id = annots.id
            AND (
                $2 IS NULL
                OR cl.creator_id = annots.creator_id
            )
        GROUP BY
            annots.id,
            annots.type,
            annots.name,
            annots.description,
            annots.creator_id,
            annots.creator_type,
            annots.reference_id,
            annots.reference_type,
            annots.npp_created,
            annots.npp_viewing,
            annots.is_locked,
            annots.created_at,
            annots.updated_at,
            annots.coordinates,
            annots.head,
            annots.tail,
            annots.center,
            annots.radius,
            annots.upper_left,
            annots.width,
            annots.height,
            annots.centroid
        ORDER BY
            annots.updated_at DESC,
            annots.id ASC
    ),
    centroids_below_npp AS (
        SELECT
            v3aa.centroid
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($13, v3aa.npp_viewing_min_query) <= LEAST($14, v3aa.npp_viewing_max_query)
            AND v3aa.id NOT IN (
                SELECT
                    *
                FROM
                    annotation_ids_npp
            )
    )
    {build_sql_str}
    FROM
        (
            SELECT
                annotation_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(json_strip_nulls(row_to_json(ROW)) :: jsonb),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    annotations.id,
                                    annotations.type,
                                    annotations.name,
                                    annotations.description,
                                    annotations.creator_id,
                                    annotations.creator_type,
                                    annotations.reference_id,
                                    annotations.reference_type,
                                    annotations.npp_created,
                                    annotations.npp_viewing,
                                    annotations.is_locked,
                                    EXTRACT(
                                        epoch
                                        FROM
                                            annotations.created_at
                                    ) :: INT AS created_at,
                                    EXTRACT(
                                        epoch
                                        FROM
                                            annotations.updated_at
                                    ) :: INT AS updated_at,
                                    annotations.coordinates,
                                    annotations.head,
                                    annotations.tail,
                                    annotations.center,
                                    annotations.radius,
                                    annotations.upper_left,
                                    annotations.width,
                                    annotations.height,
                                    annotations.centroid,
                                    CASE
                                        WHEN $10 IS TRUE THEN annotations.classes
                                    END classes
                                FROM
                                    annotations
                            ) ROW
                        )
                END items,
                CASE
                    WHEN $15 IS TRUE
                        THEN (
                            SELECT
                                COALESCE(
                                    jsonb_agg(ctr.centroid), '[]'::jsonb
                                )
                            FROM
                                centroids_below_npp AS ctr
                        )
                END centroids
            FROM
                annotation_count
        ) AS sel;
    """


async def get_base_query_jobs_sql():
    return """
    WITH annotations AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS annots
    ),
    annotations_npp AS (
        SELECT
            *
        FROM
            annotations
        WHERE
            GREATEST($8, npp_viewing_min_query) <= LEAST($9, npp_viewing_max_query)
    ),
    annotation_count AS (
        SELECT
            COUNT(*)
        FROM
            annotations_npp
    ),
    tmp_classes AS (
        {class_sql}
    ),
    annotations_limit AS (
        SELECT
            annots.*,
            CASE
                WHEN $10 IS TRUE THEN COALESCE(
                    json_agg(
                        json_build_object(
                            'id', cl.id,
                            'type', cl.type,
                            'creator_id', cl.creator_id,
                            'creator_type', cl.creator_type,
                            'reference_id', cl.reference_id,
                            'reference_type', cl.reference_type,
                            'value', cl.value,
                            'is_locked', cl.is_locked
                        )
                    ) FILTER (
                        WHERE
                            cl.id IS NOT NULL
                    ),
                    '[]'
                )
            END classes
        FROM
            (
                SELECT
                    id,
                    type,
                    name,
                    description,
                    creator_id,
                    creator_type,
                    reference_id,
                    reference_type,
                    npp_created,
                    npp_viewing,
                    is_locked,
                    created_at,
                    updated_at,
                    coordinates,
                    head,
                    tail,
                    center,
                    radius,
                    upper_left,
                    width,
                    height,
                    centroid
                FROM
                    annotations_npp
                ORDER BY
                    updated_at DESC,
                    id ASC
                LIMIT
                    $11 OFFSET $12
            ) AS annots
            LEFT JOIN tmp_classes AS cl ON $10
            AND cl.reference_id = annots.id
            AND (
                $2 IS NULL
                OR cl.creator_id = annots.creator_id
            )
        GROUP BY
            annots.id,
            annots.type,
            annots.name,
            annots.description,
            annots.creator_id,
            annots.creator_type,
            annots.reference_id,
            annots.reference_type,
            annots.npp_created,
            annots.npp_viewing,
            annots.is_locked,
            annots.created_at,
            annots.updated_at,
            annots.coordinates,
            annots.head,
            annots.tail,
            annots.center,
            annots.radius,
            annots.upper_left,
            annots.width,
            annots.height,
            annots.centroid
        ORDER BY
            annots.updated_at DESC,
            annots.id ASC
    ),
    centroids_below_npp AS (
        SELECT
            *
        FROM
            annotations
        WHERE
            GREATEST($13, npp_viewing_min_query) <= LEAST($14, npp_viewing_max_query)
            AND id NOT IN (
                SELECT
                    id
                FROM
                    annotations_npp
            )
    )
    {build_sql_str}
    FROM
        (
            SELECT
                annotation_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(json_strip_nulls(row_to_json(ROW)) :: jsonb),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    annotations_limit.id,
                                    annotations_limit.type,
                                    annotations_limit.name,
                                    annotations_limit.description,
                                    annotations_limit.creator_id,
                                    annotations_limit.creator_type,
                                    annotations_limit.reference_id,
                                    annotations_limit.reference_type,
                                    annotations_limit.npp_created,
                                    annotations_limit.npp_viewing,
                                    annotations_limit.is_locked,
                                    EXTRACT(
                                        epoch
                                        FROM
                                            annotations_limit.created_at
                                    ) :: INT AS created_at,
                                    EXTRACT(
                                        epoch
                                        FROM
                                            annotations_limit.updated_at
                                    ) :: INT AS updated_at,
                                    annotations_limit.coordinates,
                                    annotations_limit.head,
                                    annotations_limit.tail,
                                    annotations_limit.center,
                                    annotations_limit.radius,
                                    annotations_limit.upper_left,
                                    annotations_limit.width,
                                    annotations_limit.height,
                                    annotations_limit.centroid,
                                    CASE
                                        WHEN $10 IS TRUE THEN annotations_limit.classes
                                    END classes
                                FROM
                                    annotations_limit
                            ) ROW
                        )
                END items,
                CASE
                    WHEN $15 IS TRUE
                        THEN (
                            SELECT
                                COALESCE(
                                    jsonb_agg(ctr.centroid), '[]'::jsonb
                                )
                            FROM
                                centroids_below_npp AS ctr
                        )
                END centroids
            FROM
                annotation_count
        ) AS sel;
    """


async def get_build_sql_str(raw: bool, with_centroids: bool):
    if raw:
        build_sql_str = """
        SELECT
            item_count,
            items
        """
    else:
        build_sql_str = """
        SELECT
            json_build_object(
                'item_count', item_count,
                'items', items
                {centroid_build_sql}
            ) :: TEXT AS json
        """

        centroid_build_sql = ""
        if with_centroids:
            centroid_build_sql = ",'low_npp_centroids', centroids"
        build_sql_str = build_sql_str.format(centroid_build_sql=centroid_build_sql)

    return build_sql_str


async def get_job_views_sql(views: List[str]):
    base_view_sql = """
    SELECT
        id,
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        npp_created,
        NULLIF(
            to_jsonb(
                array_remove(ARRAY[npp_viewing_min, npp_viewing_max], NULL)
            ), '[]'
        ) AS npp_viewing,
        is_locked,
        created_at,
        updated_at,
        coordinates,
        head,
        tail,
        center,
        radius,
        upper_left,
        width,
        height,
        centroid,
        npp_viewing_min_query,
        npp_viewing_max_query
    FROM
        {mat_view_name}
    WHERE
        (
            $1::TEXT[] IS NULL
            OR reference_id = ANY($1::TEXT[])
        )
        AND (
            $2::TEXT[] IS NULL
            OR creator_id = ANY($2::TEXT[])
        )
        AND (
            $3::TEXT[] IS NULL
            OR TYPE = ANY($3::TEXT[])
        )
        AND (
            $4::uuid[] IS NULL
            OR id = ANY($4::uuid[])
        )
        AND (
            $5::Box IS NULL
            OR bounding_box ?# $5::Box
        )
        AND (
            $6::TEXT[] IS NULL
            OR class_values && $6::TEXT[]
        )
        AND (
            $7::uuid[] IS NULL
            OR collection_ids && $7::uuid[]
        )
    """

    view_sub_sql_queries = [base_view_sql.format(mat_view_name=view_name) for view_name in views]

    return " UNION ".join(view_sub_sql_queries)


async def get_references_base_sql():
    return """
    WITH annotation_ids AS (
        SELECT
            *
        FROM
            get_annotation_ids($1, $2, $3, $4, $5, $6, $7)
    ),
    annotation_ids_npp AS (
        SELECT
            v3aa.id
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($8, v3aa.npp_viewing_min_query) <= LEAST($9, v3aa.npp_viewing_max_query)
    )
    SELECT
        jsonb_build_object(
            'wsi',
            CASE
                WHEN refs.references IS NULL THEN '[]' :: jsonb
                ELSE refs.references
            END
        ) AS u_refs
    FROM
        (
            SELECT
                JSONB_AGG(DISTINCT(a.reference_id)) AS references
            FROM
                v3_a_annotations AS a
            WHERE
                a.id IN (
                    (
                        SELECT
                            *
                        FROM
                            annotation_ids_npp
                    )
                )
        ) AS refs;
    """


async def get_references_jobs_sql():
    return """
    WITH annotations AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS annots
        WHERE
            GREATEST($8, annots.npp_viewing_min_query) <= LEAST($9, annots.npp_viewing_max_query)
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(a.reference_id)) AS w_references
        FROM annotations AS a
        WHERE a.reference_type = 'wsi'
    )
    SELECT jsonb_build_object(
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_w_refs
    ) AS ar
    """


async def get_count_base_sql():
    return """
    WITH annotation_ids AS (
        SELECT
            *
        FROM
            get_annotation_ids($1, $2, $3, $4, $5, $6, $7)
    ),
    annotation_ids_npp AS (
        SELECT
            v3aa.id
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($8, v3aa.npp_viewing_min_query) <= LEAST($9, v3aa.npp_viewing_max_query)
    ),
    annotation_count AS (
        SELECT
            COUNT(*)
        FROM
            annotation_ids_npp
    )
    SELECT
        jsonb_build_object('item_count', item_count) AS count
    FROM
        (
            SELECT
                annotation_count.count AS item_count
            FROM
                annotation_count
        ) AS sel;
    """


async def get_count_jobs_sql():
    return """
    WITH annotations AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS annots
        WHERE
            GREATEST($8, annots.npp_viewing_min_query) <= LEAST($9, annots.npp_viewing_max_query)
    ),
    annotation_count AS (
        SELECT
            COUNT(*)
        FROM
            annotations
    )
    SELECT
        jsonb_build_object('item_count', item_count) AS count
    FROM
        (
            SELECT
                annotation_count.count AS item_count
            FROM
                annotation_count
        ) AS sel;
    """


async def get_values_base_sql():
    return """
    WITH annotation_ids AS (
        SELECT
            *
        FROM
            get_annotation_ids($1, $2, $3, $4, $5, $6, $7)
    ),
    annotation_ids_npp AS (
        SELECT
            v3aa.id
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($8, v3aa.npp_viewing_min_query) <= LEAST($9, v3aa.npp_viewing_max_query)
    )
    SELECT
        json_build_object('values', json_agg(v.value)) AS json
    FROM
        (
            SELECT
                DISTINCT cl.value
            FROM
                (
                    SELECT
                        v3aa.id,
                        v3aa.creator_id
                    FROM
                        v3_a_annotations AS v3aa
                    WHERE
                        v3aa.id IN (
                            SELECT
                                *
                            FROM
                                annotation_ids_npp
                        )
                ) AS annots
                JOIN v3_cl_classes AS cl ON cl.reference_id = annots.id
                AND (
                    $2 IS NULL
                    OR cl.creator_id = annots.creator_id
                )
        ) AS v;
    """


async def get_values_jobs_sql():
    return """
    WITH annotations AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS annots
        WHERE
            GREATEST($8, annots.npp_viewing_min_query) <= LEAST($9, annots.npp_viewing_max_query)
    ),
    tmp_classes AS (
        {class_sql}
    )SELECT
        json_build_object('values', json_agg(v.value)) AS json
    FROM
        (
            SELECT
                DISTINCT cl.value
            FROM
                (
                    SELECT
                        id,
                        creator_id
                    FROM
                        annotations
                ) AS annots
                JOIN tmp_classes AS cl ON cl.reference_id = annots.id
                AND (
                    $2 IS NULL
                    OR cl.creator_id = annots.creator_id
                )
        ) AS v;
    """


async def get_viewer_base_sql():
    return """
    WITH annotation_ids AS (
        SELECT
            *
        FROM
            get_annotation_ids($1, $2, $3, $4, $5, $6, $7)
    ),
    annotation_ids_npp AS (
        SELECT
            v3aa.id
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($8, v3aa.npp_viewing_min_query) <= LEAST($9, v3aa.npp_viewing_max_query)
    ),
    centroids_below_npp_ids AS (
        SELECT
            v3aa.id
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($10, v3aa.npp_viewing_min_query) <= LEAST($11, v3aa.npp_viewing_max_query)
            AND v3aa.id NOT IN (
                SELECT
                    *
                FROM
                    annotation_ids_npp
            )
    )
    SELECT
        jsonb_agg(ids.id) FILTER (
            WHERE
                ids.id IN (
                    SELECT
                        *
                    FROM
                        annotation_ids_npp
                )
        ) AS annotations,
        jsonb_agg(ids.centroid) FILTER (
            WHERE
                ids.id IN (
                    SELECT
                        *
                    FROM
                        centroids_below_npp_ids
                )
        ) AS low_npp_centroids
    FROM
        (
            SELECT
                v3aa.id,
                v3aa.centroid
            FROM
                v3_a_annotations AS v3aa
            WHERE
                v3aa.id IN (
                    SELECT
                        *
                    FROM
                        annotation_ids
                )
        ) AS ids;
    """


async def get_viewer_jobs_sql():
    return """
    WITH annotations AS (
        SELECT
            id,
            centroid,
            npp_viewing_min_query,
            npp_viewing_max_query
        FROM
            (
                {view_sql}
            ) AS annots
    ),
    annotations_npp AS (
        SELECT
            id
        FROM
            annotations
        WHERE
            GREATEST($8, npp_viewing_min_query) <= LEAST($9, npp_viewing_max_query)
    ),
    centroids_below_npp AS (
        SELECT
            id
        FROM
            annotations
        WHERE
            GREATEST($10, npp_viewing_min_query) <= LEAST($11, npp_viewing_max_query)
            AND id NOT IN (
                SELECT
                    id
                FROM
                    annotations_npp
            )
    )
    SELECT
        jsonb_agg(ids.id) FILTER (
            WHERE
                ids.id IN (
                    SELECT
                        *
                    FROM
                        annotations_npp
                )
        ) AS annotations,
        jsonb_agg(ids.centroid) FILTER (
            WHERE
                ids.id IN (
                    SELECT
                        *
                    FROM
                        centroids_below_npp
                )
        ) AS low_npp_centroids
    FROM
        (
            SELECT
                id,
                centroid
            FROM
                annotations
        ) AS ids;
    """


async def get_position_base_sql():
    return """
    WITH annotation_ids AS (
        SELECT
            *
        FROM
            get_annotation_ids($1, $2, $3, $4, $5, $6, $7)
    ),
    annotation_ids_npp AS (
        SELECT
            v3aa.id
        FROM
            v3_a_annotations AS v3aa
        WHERE
            v3aa.id IN (
                SELECT
                    *
                FROM
                    annotation_ids
            )
            AND GREATEST($8, v3aa.npp_viewing_min_query) <= LEAST($9, v3aa.npp_viewing_max_query)
    )
    SELECT
        id,
        position
    FROM
        (
            SELECT
                v3aa.id,
                row_number() OVER(
                    ORDER BY
                        v3aa.updated_at DESC,
                        v3aa.id ASC
                ) AS position
            FROM
                v3_a_annotations AS v3aa
            WHERE
                v3aa.id IN (
                    SELECT
                        *
                    FROM
                        annotation_ids_npp
                )
            ORDER BY
                v3aa.updated_at DESC,
                v3aa.id ASC
        ) AS id_position
    WHERE
        id = $10;
    """


async def get_position_jobs_sql():
    return """
    WITH annotations AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS annots
        WHERE
            GREATEST($8, annots.npp_viewing_min_query) <= LEAST($9, annots.npp_viewing_max_query)
    )
    SELECT
        id,
        position
    FROM
        (
            SELECT
                id,
                row_number() OVER(
                    ORDER BY
                        updated_at DESC,
                        id ASC
                ) AS position
            FROM
                annotations
            ORDER BY
                updated_at DESC,
                id ASC
        ) AS id_position
    WHERE
        id = $10;
    """
