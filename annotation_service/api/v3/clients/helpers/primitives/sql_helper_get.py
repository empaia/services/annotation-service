from annotation_service.singletons import settings


async def get_single_sql():
    base_sql = """
    SELECT
        '{"description":null, "reference_id":null, "reference_type":null}' :: jsonb ||
        json_strip_nulls(row_to_json(ROW)) :: jsonb AS primitive
    FROM
        (
            SELECT
                v3pp.id,
                v3pp.type,
                v3pp.name,
                v3pp.description,
                v3pp.creator_id,
                v3pp.creator_type,
                v3pp.reference_id,
                v3pp.reference_type,
                v3pp.is_locked,
                to_json(v3pp.ivalue) AS value,
                to_json(v3pp.fvalue) AS value,
                to_json(v3pp.bvalue) AS value,
                to_json(v3pp.svalue) AS value
            FROM
                v3_p_primitives AS v3pp
            WHERE
                v3pp.id = $1
        ) AS ROW;
    """

    return base_sql


async def get_all_sql():
    base_sql = """
    WITH p_count AS (
        SELECT
            count(*)
        FROM
            v3_p_primitives
    )
    SELECT
        json_build_object('item_count', item_count, 'items', items) :: TEXT AS json
    FROM
        (
            SELECT
                p_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    {json_str} :: jsonb || json_strip_nulls(row_to_json(ROW)) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    primitives.*
                                FROM
                                    (
                                        SELECT
                                            v3pp.id,
                                            v3pp.type,
                                            v3pp.name,
                                            v3pp.description,
                                            v3pp.creator_id,
                                            v3pp.creator_type,
                                            v3pp.reference_id,
                                            v3pp.reference_type,
                                            v3pp.is_locked,
                                            to_json(v3pp.ivalue) AS VALUE,
                                            to_json(v3pp.fvalue) AS VALUE,
                                            to_json(v3pp.bvalue) AS VALUE,
                                            to_json(v3pp.svalue) AS VALUE
                                        FROM
                                            v3_p_primitives AS v3pp
                                        LIMIT
                                            $1 OFFSET $2
                                    ) AS primitives
                            ) ROW
                    )
                END items
            FROM
                p_count
        ) sel;
    """

    limit_comparison_sql_str = (
        f"p_count.count > {settings.v3_item_limit} AND ($1::int IS NULL OR $1::int > {settings.v3_item_limit})"
    )

    error_message = f"'Count of requested items (%s) exceeds server limit of {settings.v3_item_limit}.', p_count.count"

    json_str = '\'{"description":null, "reference_id":null, "reference_type":null}\''

    return base_sql.format(
        limit_comparison_sql_str=limit_comparison_sql_str, error_message=error_message, json_str=json_str
    )
