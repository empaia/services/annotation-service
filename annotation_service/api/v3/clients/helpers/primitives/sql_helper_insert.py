from annotation_service.models.v3.annotation.primitives import PrimitiveType

TYPE_COLUMN_NAMES = {
    PrimitiveType.INTEGER: "ivalue",
    PrimitiveType.FLOAT: "fvalue",
    PrimitiveType.BOOL: "bvalue",
    PrimitiveType.STRING: "svalue",
}


async def get_insert_sql(primitive_type: PrimitiveType, allow_external_ids: bool, temp_table_name: str):
    id_column = "id" if allow_external_ids else "gen_random_uuid()"

    type_column_names_response = {
        PrimitiveType.INTEGER: "'value', v3_p_primitives.ivalue",
        PrimitiveType.FLOAT: "'value', v3_p_primitives.fvalue",
        PrimitiveType.BOOL: "'value', v3_p_primitives.bvalue",
        PrimitiveType.STRING: "'value', v3_p_primitives.svalue",
    }

    base_sql = f"""
    INSERT INTO
        v3_p_primitives
    SELECT
        {id_column},
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        is_locked,
        ivalue,
        fvalue,
        bvalue,
        svalue,
        collection_ids
    FROM
        {temp_table_name}
    RETURNING '{{"description":null, "reference_id":null, "reference_type":null}}' || jsonb_strip_nulls(
        jsonb_build_object(
            'id', v3_p_primitives.id,
            'name', v3_p_primitives.name,
            'description', v3_p_primitives.description,
            'creator_id', v3_p_primitives.creator_id,
            'creator_type', v3_p_primitives.creator_type,
            'reference_id', v3_p_primitives.reference_id,
            'reference_type', v3_p_primitives.reference_type,
            'type', v3_p_primitives.type,
            'is_locked', v3_p_primitives.is_locked,
            {type_column_names_response[primitive_type]}
        )
    );
    """

    columns_to_copy = [
        "type",
        "name",
        "description",
        "creator_id",
        "creator_type",
        "reference_id",
        "reference_type",
        "is_locked",
        "ivalue",
        "fvalue",
        "bvalue",
        "svalue",
        "collection_ids",
    ]

    if allow_external_ids:
        columns_to_copy.insert(0, "id")

    return columns_to_copy, base_sql


async def get_update_sql(primitive_type: PrimitiveType):
    base_sql = """
    UPDATE v3_p_primitives
        SET name = $2,
            description = $3,
            creator_id = $4,
            creator_type = $5,
            reference_id = $6,
            reference_type = $7,
            type = $8,
            {type_column_name} = $9
    WHERE id = $1;
    """

    return base_sql.format(type_column_name=TYPE_COLUMN_NAMES[primitive_type])


async def add_collection_id_sql():
    return """
    UPDATE v3_p_primitives
        SET collection_ids = ARRAY_APPEND(collection_ids, $1)
    WHERE id = ANY($2);
    """
