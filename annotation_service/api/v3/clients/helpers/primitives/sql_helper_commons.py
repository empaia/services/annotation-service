from typing import List


async def get_base_query_sql():
    return """
    WITH primitive_ids AS (
        SELECT
            *
        FROM
            get_primitive_ids($1, $2, $3, $4, $5, $6)
    ),
    primitive_count AS (
        SELECT
            COUNT(*)
        FROM
            primitive_ids
    ),
    primitives AS (
        SELECT
            v3pp.id,
            v3pp.type,
            v3pp.name,
            v3pp.description,
            v3pp.creator_id,
            v3pp.creator_type,
            v3pp.reference_id,
            v3pp.reference_type,
            v3pp.is_locked,
            to_json(v3pp.ivalue) AS value,
            to_json(v3pp.fvalue) AS value,
            to_json(v3pp.bvalue) AS value,
            to_json(v3pp.svalue) AS value
        FROM
            v3_p_primitives AS v3pp
        WHERE
            v3pp.id IN (
                SELECT
                    *
                FROM
                    primitive_ids
            )
        LIMIT $7 OFFSET $8
    )
    {build_sql_str}
    FROM
        (
            SELECT
                primitive_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    {json_str} :: jsonb || json_strip_nulls(row_to_json(ROW)) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    *
                                FROM
                                    primitives
                            ) ROW
                    )
                END items
            FROM
                primitive_count
        ) AS sel;
    """


async def get_base_query_jobs_sql():
    return """
    WITH primitives AS (
        SELECT
            id,
            type,
            name,
            description,
            creator_id,
            creator_type,
            reference_id,
            reference_type,
            is_locked,
            to_json(ivalue) AS value,
            to_json(fvalue) AS value,
            to_json(bvalue) AS value,
            to_json(svalue) AS value
        FROM
            (
                {view_sql}
            ) AS prims
    ),
    primitive_count AS (
        SELECT
            COUNT(*)
        FROM
            primitives
    ),
    primitives_limit AS (
        SELECT
            *
        FROM
            primitives
        LIMIT
            $7 OFFSET $8
    )
    {build_sql_str}
    FROM
        (
            SELECT
                primitive_count.count AS item_count,
                CASE
                    WHEN {limit_comparison_sql_str}
                        THEN raise_exception(NULL :: jsonb, format({error_message}), 'EM001')
                    ELSE (
                        SELECT
                            COALESCE(
                                jsonb_agg(
                                    {json_str} :: jsonb || json_strip_nulls(row_to_json(ROW)) :: jsonb
                                ),
                                '[]' :: jsonb
                            )
                        FROM
                            (
                                SELECT
                                    *
                                FROM
                                    primitives_limit
                            ) ROW
                    )
                END items
            FROM
                primitive_count
        ) AS sel;
    """


async def get_build_sql_str(raw: bool):
    if raw:
        build_sql_str = """
        SELECT
            item_count,
            items
        """
    else:
        build_sql_str = """
        SELECT
            json_build_object(
                'item_count', item_count,
                'items', items
            ) :: TEXT AS json
        """

    return build_sql_str


async def get_job_views_sql(views: List[str]):
    base_view_sql = """
    SELECT
        id,
        type,
        name,
        description,
        creator_id,
        creator_type,
        reference_id,
        reference_type,
        is_locked,
        ivalue,
        fvalue,
        bvalue,
        svalue
    FROM
        {mat_view_name}
    WHERE
        (
            $1::TEXT[] IS NULL
            OR reference_id = ANY($1::TEXT[])
            OR (
                CASE
                    WHEN $2 IS TRUE THEN reference_id IS NULL
                END
            )
        )
        AND (
            $3::TEXT[] IS NULL
            OR creator_id = ANY($3::TEXT[])
        )
        AND (
            $4::TEXT[] IS NULL
            OR TYPE = ANY($4::TEXT[])
        )
        AND (
            $5::uuid[] IS NULL
            OR id = ANY($5::uuid[])
        )
        AND (
            $6::uuid[] IS NULL
            OR collection_ids && $6::uuid[]
        )
    """

    view_sub_sql_queries = [base_view_sql.format(mat_view_name=view_name) for view_name in views]

    return " UNION ".join(view_sub_sql_queries)


async def get_references_base_sql():
    return """
    WITH primitive_ids AS (
        SELECT
            *
        FROM
            get_primitive_ids($1, $2, $3, $4, $5, $6)
    ),
    ids_a_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS a_references
        FROM v3_p_primitives AS p
        WHERE p.id in (SELECT * FROM primitive_ids)
        and p.reference_type = 'annotation'
    ),
    ids_c_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS c_references
        FROM v3_p_primitives AS p
        WHERE p.id in (SELECT * FROM primitive_ids)
        and p.reference_type = 'collection'
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS w_references
        FROM v3_p_primitives AS p
        WHERE p.id in (SELECT * FROM primitive_ids)
        and p.reference_type = 'wsi'
    ),
    ids_n_refs AS (
        SELECT count(*) as n_count
        FROM v3_p_primitives AS p
        WHERE p.id in (SELECT * FROM primitive_ids)
        and p.reference_type IS NULL
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.a_references IS NULL THEN '[]'::jsonb
            ELSE ar.a_references
        END,
        'collection',
        CASE
            WHEN ar.c_references IS NULL THEN '[]'::jsonb
            ELSE ar.c_references
        END,
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END,
        'contains_items_without_reference',
        CASE
            WHEN ar.n_count > 0 THEN true
            ELSE false
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_a_refs, ids_c_refs, ids_w_refs, ids_n_refs
    ) AS ar
    """


async def get_references_jobs_sql():
    return """
    WITH primitives AS (
        SELECT
            *
        FROM
            (
                {view_sql}
            ) AS prims
    ),
    ids_a_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS a_references
        FROM primitives AS p
        WHERE p.reference_type = 'annotation'
    ),
    ids_c_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS c_references
        FROM primitives AS p
        WHERE p.reference_type = 'collection'
    ),
    ids_w_refs AS (
        SELECT JSONB_AGG(DISTINCT(p.reference_id)) AS w_references
        FROM primitives AS p
        WHERE p.reference_type = 'wsi'
    ),
    ids_n_refs AS (
        SELECT count(*) as n_count
        FROM primitives AS p
        WHERE p.reference_type IS NULL
    )
    SELECT jsonb_build_object(
        'annotation',
        CASE
            WHEN ar.a_references IS NULL THEN '[]'::jsonb
            ELSE ar.a_references
        END,
        'collection',
        CASE
            WHEN ar.c_references IS NULL THEN '[]'::jsonb
            ELSE ar.c_references
        END,
        'wsi',
        CASE
            WHEN ar.w_references IS NULL THEN '[]'::jsonb
            ELSE ar.w_references
        END,
        'contains_items_without_reference',
        CASE
            WHEN ar.n_count > 0 THEN true
            ELSE false
        END
    ) AS u_refs
    FROM
    (
        SELECT * FROM ids_a_refs, ids_c_refs, ids_w_refs, ids_n_refs
    ) AS ar
    """
