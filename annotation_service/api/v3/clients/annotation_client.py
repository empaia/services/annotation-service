from typing import List
from uuid import uuid4

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse, Response
from pydantic import UUID4

from annotation_service.models.v3.annotation.annotations import (
    AnnotationQuery,
    AnnotationViewerQuery,
    PostAnnotation,
    PostAnnotations,
)
from annotation_service.singletons import logger, settings

from .class_client import CLASS_MAT_VIEW_BASE_NAME
from .helpers.annotations import sql_helper_get, sql_helper_insert, sql_helper_query
from .helpers.sql_helper import get_existing_views_sql
from .utils import annotation_utils as utils

ANNOT_MAT_VIEW_BASE_NAME = "mat_view_annotations"


class AnnotationClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_annotations(
        self, annotations: PostAnnotations, external_ids: bool = False, raw: bool = False, collection_id: UUID4 = None
    ):
        if isinstance(annotations, PostAnnotation.__args__):
            return await self._create_annotations([annotations], external_ids, raw, collection_id)
        elif len(annotations.items) > 0:
            if len(annotations.items) > settings.v3_post_limit:
                raise HTTPException(
                    413,
                    (
                        f"Number of posted items ({len(annotations.items)})"
                        f" exceeds server limit of {settings.v3_post_limit}."
                    ),
                )
            return await self._create_annotations(annotations.items, external_ids, raw, collection_id)

        raise HTTPException(405, "No valid post data")

    async def _create_annotations(
        self,
        annotations: List[PostAnnotation],
        external_ids: bool = False,
        raw: bool = False,
        collection_id: UUID4 = None,
    ):
        # create random name for temporary table
        temp_table_name = f"tmp_{uuid4().hex}"

        allow_external_ids = settings.allow_external_ids and external_ids

        columns, insert_sql = await sql_helper_insert.get_insert_sql(
            annotations[0].type, allow_external_ids, temp_table_name
        )
        logger.debug(insert_sql)

        new_annotations = await utils.format_post_data(annotations, allow_external_ids, collection_id)

        alter_table_sql = f"""
        ALTER TABLE {temp_table_name} DROP COLUMN created_at;
        ALTER TABLE {temp_table_name} DROP COLUMN updated_at;
        """

        if not allow_external_ids:
            alter_table_sql += f"\nALTER TABLE {temp_table_name} DROP COLUMN id;"

        try:
            async with self.conn.transaction():
                await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")
                await self.conn.execute(
                    f"CREATE UNLOGGED TABLE IF NOT EXISTS {temp_table_name} (LIKE v3_a_annotations);"
                )
                await self.conn.execute(f"TRUNCATE {temp_table_name};")
                await self.conn.execute(alter_table_sql)
                await self.conn.copy_records_to_table(temp_table_name, records=new_annotations, columns=columns)
                result = await self.conn.fetch(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Annotation with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be created") from e
        finally:
            await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")

        if result is None:
            raise HTTPException(500, "Annotations could not be created (result None)")

        items = [a[0] for a in result]

        if raw:
            return len(items), items
        if len(items) == 1:
            return JSONResponse(items[0], status_code=201)
        return JSONResponse({"item_count": len(items), "items": items}, status_code=201)

    async def get_all(self, with_classes: bool, skip, limit):
        sql = await sql_helper_get.get_all_sql(with_classes)
        logger.debug(sql)

        args = [limit, skip]

        try:
            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            raise HTTPException(500, "Annotations could not be queried (get all)") from e

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def get(self, annot_id: UUID4, with_classes: bool):
        sql = await sql_helper_get.get_single_sql(with_classes)
        logger.debug(sql)

        try:
            result = await self.conn.fetchrow(sql, annot_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (get)") from e

        if not result:
            raise HTTPException(404, f"No annotation with id '{annot_id}' found")
        return result["annotation"]

    async def query(
        self,
        annotation_query: AnnotationQuery,
        with_classes: bool,
        with_low_npp_centroids: bool,
        skip: int,
        limit: int,
        raw: bool = False,
        collection_id: UUID4 = None,
    ):
        await utils.check_annotation_query(annotation_query, limit, with_low_npp_centroids)

        try:
            if not annotation_query.jobs:
                # if jobs not in query
                sql = await sql_helper_query.get_query_sql(with_low_npp_centroids, raw)
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{ANNOT_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]
                existing_class_views = None
                if with_classes:
                    # if with_classes is True
                    # get existing class views for queried job ids
                    class_view_names = [
                        f"{CLASS_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_query.jobs
                    ]
                    result = await self.conn.fetch(existing_views_sql, class_view_names)
                    existing_class_views = result[0]["get_views_exists"]

                if not existing_views or (with_classes and not existing_class_views):
                    return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

                sql = await sql_helper_query.get_query_sql(
                    with_low_npp_centroids, raw, existing_views, existing_class_views
                )

            logger.debug(sql)

            args = await utils.get_query_args(
                annotation_query=annotation_query,
                query_type="query",
                with_classes=with_classes,
                with_low_npp_centroids=with_low_npp_centroids,
                skip=skip,
                limit=limit,
                collection_id=collection_id,
            )

            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'annotations' must be of type UUID4.") from e
            raise HTTPException(500, "Annotations could not be queried (query)") from e

        if raw:
            if result is None:
                return 0, []
            return result["item_count"], result["items"]

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def query_position(self, annot_id: UUID4, annotation_query: AnnotationQuery):
        await utils.check_annotation(self.conn, annot_id)

        try:
            if not annotation_query.jobs:
                # if jobs not in query
                position_sql = await sql_helper_query.get_position_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{ANNOT_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    raise HTTPException(404, "Annotation not found in queried jobs")

                position_sql = await sql_helper_query.get_position_sql(existing_views)

            logger.debug(position_sql)

            args = await utils.get_query_args(annotation_query=annotation_query, query_type="position")
            args.append(annot_id)

            result = await self.conn.fetchrow(position_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation position could not be queried") from e

        if result is None:
            raise HTTPException(500, "Annotation position could not be queried (result None)")

        return JSONResponse({"id": str(result["id"]), "position": result["position"] - 1})

    async def count_query(self, annotation_query: AnnotationQuery):
        try:
            if not annotation_query.jobs:
                # if jobs not in query
                count_sql = await sql_helper_query.get_count_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{ANNOT_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(content={"item_count": 0}, media_type="application/json")

                count_sql = await sql_helper_query.get_count_sql(existing_views)

            logger.debug(count_sql)

            args = await utils.get_query_args(annotation_query=annotation_query, query_type="count")

            count_result = await self.conn.fetchrow(count_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (count)") from e

        if count_result is not None:
            return JSONResponse(count_result["count"])
        else:
            raise HTTPException(500, "Annotations cound not be queried (count None).")

    async def unique_classes_query(self, annotation_query: AnnotationQuery):
        try:
            if not annotation_query.jobs:
                # if jobs not in query
                values_sql = await sql_helper_query.get_values_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{ANNOT_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]
                # get existing class views for queried job ids
                class_view_names = [
                    f"{CLASS_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, class_view_names)
                existing_class_views = result[0]["get_views_exists"]

                if not existing_views or not existing_class_views:
                    return JSONResponse(content={"unique_class_values": []}, media_type="application/json")

                values_sql = await sql_helper_query.get_values_sql(existing_views, existing_class_views)

            logger.debug(values_sql)

            args = await utils.get_query_args(annotation_query=annotation_query, query_type="class-values")

            values_result = await self.conn.fetchrow(values_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (class values)") from e

        if values_result is not None:
            unique_class_values = values_result["json"]["values"] if values_result["json"]["values"] is not None else []
            result_dict = dict()
            result_dict["unique_class_values"] = unique_class_values
            return JSONResponse(result_dict)
        else:
            raise HTTPException(500, "Annotations cound not be queried (class values None).")

    async def unique_references_query(self, annotation_query: AnnotationQuery):
        try:
            if not annotation_query.jobs:
                # if jobs not in query
                references_sql = await sql_helper_query.get_references_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{ANNOT_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(content={"wsi": []}, media_type="application/json")

                references_sql = await sql_helper_query.get_references_sql(existing_views)

            logger.debug(references_sql)

            args = await utils.get_query_args(annotation_query=annotation_query, query_type="references")

            references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (annotation references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Annotations cound not be queried (unique references None).")

    async def viewer_query(self, annotation_viewer_query: AnnotationViewerQuery):
        if annotation_viewer_query.npp_viewing is None:
            raise HTTPException(400, "Key npp_viewing must be specified!")

        try:
            if not annotation_viewer_query.jobs:
                # if jobs not in query
                viewer_sql = await sql_helper_query.get_viewer_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{ANNOT_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in annotation_viewer_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(
                        content={"annotations": [], "low_npp_centroids": []}, media_type="application/json"
                    )

                viewer_sql = await sql_helper_query.get_viewer_sql(existing_views)

            logger.debug(viewer_sql)

            args = await utils.get_query_args(annotation_query=annotation_viewer_query, query_type="viewer")

            viewer_result = await self.conn.fetch(viewer_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotations could not be queried (viewer query)") from e

        if viewer_result is not None:
            result_dict = dict()
            result_dict["annotations"] = (
                viewer_result[0]["annotations"] if viewer_result[0]["annotations"] is not None else []
            )
            result_dict["low_npp_centroids"] = (
                viewer_result[0]["low_npp_centroids"] if viewer_result[0]["low_npp_centroids"] is not None else []
            )
            return JSONResponse(result_dict)
        else:
            raise HTTPException(500, "Annotations cound not be queried (class values None).")

    async def delete(self, annotation_id: UUID4):
        await utils.check_annotation(self.conn, annotation_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM
                v3_a_annotations
            WHERE
                id = $1
            RETURNING *
        )
        SELECT
            COUNT(*)
        FROM
            deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, annotation_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(annotation_id)})
        else:
            raise HTTPException(500, "Annotation could not be deleted (delete count 0)")

    async def get_collection_items(self, collection_id: UUID4):
        sql = """
        SELECT
            id::TEXT
        FROM
            v3_a_annotations
        WHERE
            collection_ids && ARRAY[$1]::uuid[]
        """

        try:
            items = await self.conn.fetch(sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation items could not be queried") from e

        return [item["id"] for item in items]

    async def update_collection_id(self, collection_id: UUID4, ids: List[UUID4]):
        sql = """
        WITH rows AS (
            UPDATE
                v3_a_annotations
            SET
                collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
            WHERE
                id = ANY($2::uuid[])
            RETURNING 1
        )
        SELECT count(*) FROM rows;
        """

        try:
            return await self.conn.fetchval(sql, collection_id, ids)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation items could not be updated") from e

    async def remove_from_collection(self, annotation_id: UUID4, collection_id: UUID4):
        sql = """
        WITH ROWS AS (
            UPDATE
                v3_a_annotations
            SET
                collection_ids = ARRAY_REMOVE(collection_ids, $2::uuid)
            WHERE
                id = $1::uuid
                AND collection_ids && ARRAY[$2]::uuid[]
            RETURNING 1
        )
        SELECT
            count(*)
        FROM
            ROWS;
        """

        try:
            return await self.conn.fetchval(sql, annotation_id, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Annotation item could not be removed from Collection") from e

    async def create_materialized_view(self, job_id: str):
        # create view name
        view_name = f"{ANNOT_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}"

        try:
            await self.conn.execute("CALL create_annotation_view($1, $2)", job_id, view_name)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"Annotation view could not be created: {e}") from e
