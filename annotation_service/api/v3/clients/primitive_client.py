from typing import List
from uuid import uuid4

from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse, Response
from pydantic import UUID4

from annotation_service.models.v3.annotation.primitives import PostPrimitive, PostPrimitives, Primitive, PrimitiveQuery
from annotation_service.singletons import logger, settings

from .helpers.primitives import sql_helper_get, sql_helper_insert, sql_helper_query
from .helpers.sql_helper import get_existing_views_sql
from .utils import primitive_utils as utils

PRIMITIVE_MAT_VIEW_BASE_NAME = "mat_view_primitives"


class PrimitiveClient:
    def __init__(self, conn):
        self.conn = conn

    async def add_primitives(
        self, primitives: PostPrimitives, external_ids: bool = False, raw: bool = False, collection_id: UUID4 = None
    ):
        if isinstance(primitives, PostPrimitive.__args__):
            return await self.create_primitives([primitives], external_ids, raw, collection_id)
        elif len(primitives.items) > 0:
            if len(primitives.items) > settings.v3_post_limit:
                raise HTTPException(
                    413,
                    (
                        f"Number of posted items ({len(primitives.items)})"
                        f" exceeds server limit of {settings.v3_post_limit}."
                    ),
                )
            return await self.create_primitives(primitives.items, external_ids, raw, collection_id)
        raise HTTPException(405, "No valid post data")

    async def create_primitives(
        self, primitives: List[Primitive], external_ids: bool = False, raw: bool = False, collection_id: UUID4 = None
    ):
        # create random name for temporary table
        temp_table_name = f"tmp_{uuid4().hex}"

        allow_external_ids = settings.allow_external_ids and external_ids

        columns, insert_sql = await sql_helper_insert.get_insert_sql(
            primitives[0].type, allow_external_ids, temp_table_name
        )
        logger.debug(insert_sql)

        new_primitives = await utils.format_post_data(primitives, allow_external_ids, collection_id)

        try:
            async with self.conn.transaction():
                await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")
                await self.conn.execute(
                    f"CREATE UNLOGGED TABLE IF NOT EXISTS {temp_table_name} (LIKE v3_p_primitives);"
                )
                await self.conn.execute(f"TRUNCATE {temp_table_name};")
                if not settings.allow_external_ids or not external_ids:
                    await self.conn.execute(f"ALTER TABLE {temp_table_name} DROP COLUMN id;")
                await self.conn.copy_records_to_table(temp_table_name, records=new_primitives, columns=columns)
                result = await self.conn.fetch(insert_sql)
        except UniqueViolationError as e:
            raise HTTPException(422, "Primitive with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            logger.debug(e)
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive could not be created") from e
        finally:
            await self.conn.execute(f"DROP TABLE IF EXISTS {temp_table_name};")

        if result is None:
            raise HTTPException(500, "Primitives could not be created (result None)")

        items = [p[0] for p in result]

        if raw:
            return len(items), items
        if len(items) == 1:
            return JSONResponse(items[0], status_code=201)
        return JSONResponse({"item_count": len(items), "items": items}, status_code=201)

    async def get_all(self, skip: int, limit: int):
        sql = await sql_helper_get.get_all_sql()
        logger.debug(sql)

        args = [limit, skip]

        try:
            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'primitives' must be of type UUID4.") from e
            raise HTTPException(500, "Primitives could not be queried (get all)") from e

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def get(self, primitive_id: UUID4):
        sql = await sql_helper_get.get_single_sql()
        logger.debug(sql)

        try:
            result = await self.conn.fetchrow(sql, primitive_id)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'primitives' must be of type UUID4.") from e
            raise HTTPException(500, "Primitives could not be queried (get)") from e

        if not result:
            raise HTTPException(404, f"No primitive with id '{primitive_id}' found")
        return result["primitive"]

    async def query(
        self, primitive_query: PrimitiveQuery, skip: int, limit: int, raw: bool = False, collection_id: UUID4 = None
    ):
        try:
            if not primitive_query.jobs:
                # if jobs not in query
                sql = await sql_helper_query.get_query_sql(raw)
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{PRIMITIVE_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in primitive_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

                sql = await sql_helper_query.get_query_sql(raw, existing_views)

            logger.debug(sql)

            include_null_refereneces = (
                True if primitive_query.references and None in primitive_query.references else False
            )

            args = [
                primitive_query.references,
                include_null_refereneces,
                primitive_query.creators,
                primitive_query.types,
                primitive_query.primitives,
                [collection_id] if collection_id else None,
                limit,
                skip,
            ]

            result = await self.conn.fetchrow(sql, *args)
        except PostgresError as e:
            logger.debug(e)
            if e.as_dict()["sqlstate"] == "EM001":
                raise HTTPException(413, e.as_dict()["message"]) from e
            if e.as_dict()["sqlstate"] == "22000":
                raise HTTPException(422, "ERROR: Value for 'primitives' must be of type UUID4.") from e
            raise HTTPException(500, "Primitives could not be queried (query)") from e

        if raw:
            if result is None:
                return 0, []
            return result["item_count"], result["items"]

        if result is None:
            return JSONResponse(content={"item_count": 0, "items": []}, media_type="application/json")

        return Response(content=result["json"], media_type="application/json")

    async def unique_references_query(self, primitive_query: PrimitiveQuery, collection_id: UUID4 = None):
        try:
            if not primitive_query.jobs:
                # if jobs not in query
                references_sql = await sql_helper_query.get_references_sql()
            else:
                # if jobs in query
                # get existing views for queried job ids
                existing_views_sql = await get_existing_views_sql()
                view_names = [
                    f"{PRIMITIVE_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}" for job_id in primitive_query.jobs
                ]
                result = await self.conn.fetch(existing_views_sql, view_names)
                existing_views = result[0]["get_views_exists"]

                if not existing_views:
                    return JSONResponse(
                        content={
                            "annotation": [],
                            "collection": [],
                            "wsi": [],
                            "contains_items_without_reference": False,
                        },
                        media_type="application/json",
                    )

                references_sql = await sql_helper_query.get_references_sql(existing_views)

            logger.debug(references_sql)

            include_null_refereneces = (
                True if primitive_query.references and None in primitive_query.references else False
            )

            args = [
                primitive_query.references,
                include_null_refereneces,
                primitive_query.creators,
                primitive_query.types,
                primitive_query.primitives,
                [collection_id] if collection_id else None,
            ]

            references_result = await self.conn.fetchrow(references_sql, *args)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitives could not be queried (primitive references)") from e

        if references_result is not None:
            references = references_result["u_refs"] if references_result["u_refs"] is not None else []
            return JSONResponse(references)
        else:
            raise HTTPException(500, "Primitives cound not be queried (unique references None).")

    async def update(self, primitive_id: UUID4, primitive: PostPrimitive):
        await utils.check_primitive(self.conn, primitive_id, "update", primitive)

        update_sql = await sql_helper_insert.get_update_sql(primitive.type)
        logger.debug(update_sql)

        try:
            await self.conn.execute(
                update_sql,
                primitive_id,
                primitive.name,
                primitive.description,
                primitive.creator_id,
                primitive.creator_type,
                primitive.reference_id,
                primitive.reference_type,
                primitive.type,
                primitive.value,
            )
            return await self.get(primitive_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive could not be updated") from e

    async def delete(self, primitive_id: UUID4):
        await utils.check_primitive(self.conn, primitive_id, "lock")

        delete_sql = """
        WITH deleted AS (
            DELETE FROM
                v3_p_primitives
            WHERE
                id = $1
            RETURNING *
        )
        SELECT
            COUNT(*)
        FROM
            deleted;
        """

        try:
            delete_count = await self.conn.fetchval(delete_sql, primitive_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive could not be deleted") from e

        if delete_count > 0:
            return JSONResponse({"id": str(primitive_id)})
        else:
            raise HTTPException(500, "Primitive could not be deleted (delete count 0)")

    async def get_collection_items(self, collection_id: UUID4):
        sql = """
        SELECT
            id::TEXT
        FROM
            v3_p_primitives
        WHERE
            collection_ids && ARRAY[$1]::uuid[]
        """

        try:
            items = await self.conn.fetch(sql, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive items could not be queried") from e

        return [item["id"] for item in items]

    async def update_collection_id(self, collection_id: UUID4, ids: List[UUID4]):
        sql = """
        WITH rows AS (
            UPDATE
                v3_p_primitives
            SET
                collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
            WHERE
                id = ANY($2::uuid[])
            RETURNING 1
        )
        SELECT count(*) FROM rows;
        """

        try:
            return await self.conn.fetchval(sql, collection_id, ids)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive items could not be updated") from e

    async def remove_from_collection(self, primitive_id: UUID4, collection_id: UUID4):
        sql = """
        WITH ROWS AS (
            UPDATE
                v3_p_primitives
            SET
                collection_ids = ARRAY_REMOVE(collection_ids, $2::uuid)
            WHERE
                id = $1::uuid
                AND collection_ids && ARRAY[$2]::uuid[]
            RETURNING 1
        )
        SELECT
            count(*)
        FROM
            ROWS;
        """

        try:
            return await self.conn.fetchval(sql, primitive_id, collection_id)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Primitive item could not be removed from Collection") from e

    async def create_materialized_view(self, job_id: str):
        # create view name
        view_name = f"{PRIMITIVE_MAT_VIEW_BASE_NAME}_{job_id.replace('-', '')}"

        try:
            await self.conn.execute("CALL create_primitive_view($1, $2)", job_id, view_name)
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, f"Primitive view could not be created: {e}") from e
