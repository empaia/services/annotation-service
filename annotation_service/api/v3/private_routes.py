from annotation_service.models.v3.commons import Id

from .db import db_clients


def add_routes_private(app, late_init, api_v3_integration):
    @app.put(
        "/db/{job_id}/aggregate-job",
        tags=["DB"],
        summary="Aggregate job data to improve queries",
        responses={
            200: {"model": Id},
        },
    )
    async def _(job_id: Id, payload=api_v3_integration.global_depends()):
        """
        Aggregates all data for given job ID.

        The job ID is returned.
        """
        async with late_init.pool.acquire() as conn:
            clients = await db_clients(conn=conn)
            return await clients.job.aggregate_job(job_id=job_id, clients=clients)
