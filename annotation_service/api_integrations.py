import importlib

from .api.v1.integrations.default import Default as Default_v1
from .api.v3.integrations.default import Default as Default_v3


def get_api_v1_integration(settings, logger):
    if settings.api_v1_integration:
        module_name, class_name = settings.api_v1_integration.split(":")
        module = importlib.import_module(module_name)
        IntegrationClass = getattr(module, class_name)
        return IntegrationClass(settings=settings, logger=logger)

    return Default_v1(settings=settings, logger=logger)


def get_api_v3_integration(settings, logger):
    if settings.api_v3_integration:
        module_name, class_name = settings.api_v3_integration.split(":")
        module = importlib.import_module(module_name)
        IntegrationClass = getattr(module, class_name)
        return IntegrationClass(settings=settings, logger=logger)

    return Default_v3(settings=settings, logger=logger)
