from typing import OrderedDict


class SimpleCache:
    def __init__(self):
        self.cache: OrderedDict = {}
        self.max_size = 100

    def get(self, key):
        if key in self.cache:
            return self.cache[key]
        else:
            return None

    def add(self, key, value):
        if key not in self.cache and len(self.cache) >= self.max_size:
            self.cache.popitem(last=False)

        self.cache[key] = value

    def update(self, key, value):
        if key in self.cache:
            self.cache[key] = value

    def delete(self, key):
        if key in self.cache:
            del self.cache[key]
