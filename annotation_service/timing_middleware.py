import time
from datetime import datetime

from fastapi import Request

DATE_FORMAT = "%Y-%m-%d %H:%M:%S"


def add_timing(app, logger):
    @app.middleware("http")
    async def add_logging(request: Request, call_next):
        times = []

        start_time = time.time()
        start_time_formatted = f"REQUEST STARTED: {datetime.fromtimestamp(start_time).strftime(DATE_FORMAT)}"

        response = await call_next(request)

        end_time = time.time()
        end_time_formatted = f"REQUEST FINISHED: {datetime.fromtimestamp(end_time).strftime(DATE_FORMAT)}"
        elapsed_time = end_time - start_time
        elapsed_time_formatted = "ELAPSED TIME: {:8.4f} sec".format(elapsed_time)
        times.extend([elapsed_time_formatted, start_time_formatted, end_time_formatted])

        logger.info(" - ".join(times))

        return response
