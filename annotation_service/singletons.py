import logging

from annotation_service.pixelmapd_manager.pixelmapd_manager import PixelmapdManager

from .settings import Settings
from .simple_cache import SimpleCache

settings = Settings()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

pixelmap_cache = SimpleCache()
pixelmapd = PixelmapdManager(settings=settings, logger=logger)
