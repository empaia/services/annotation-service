from contextlib import asynccontextmanager

import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api.root import add_routes_root
from .api.v1.routes import add_routes_v1
from .api.v3.routes import add_private_routes_v3, add_routes_v3
from .api_integrations import get_api_v1_integration, get_api_v3_integration
from .file_profiling import add_profiling as add_file_profiler
from .late_init import LateInit
from .profiler import add_profiler
from .singletons import logger, pixelmapd, settings
from .timing_middleware import add_timing

DESCRIPTION = """
API for the generic EMPAIA annotation service.

The Annotation Service stores and delivers
- Annotations on Whole Slide Images
- Classifications (Classes) for annotations
- Primitive Data
  - integer values
  - float values
  - bool values
  - string values
- Collections to group elements of same type

Data can / must reference each other to increase semantic meaning.
"""


openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

late_init = LateInit()


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
    await pixelmapd.initialize_connection()
    yield


app = FastAPI(
    debug=settings.debug,
    title="Annotation Service API",
    version=version,
    description=DESCRIPTION,
    root_path=settings.root_path,
    openapi_url=openapi_url,
    lifespan=lifespan,
)


api_v1_integration = get_api_v1_integration(settings=settings, logger=logger)
api_v3_integration = get_api_v3_integration(settings=settings, logger=logger)

app_v1 = FastAPI(openapi_url=openapi_url)
app_v3 = FastAPI(openapi_url=openapi_url)
app_v3_private = FastAPI(openapi_url=openapi_url)

if settings.cors_allow_origins:
    for app_obj in [app_v1, app_v3]:
        app_obj.add_middleware(
            CORSMiddleware,
            allow_origins=settings.cors_allow_origins,
            allow_credentials=settings.cors_allow_credentials,
            allow_methods=["*"],
            allow_headers=["*"],
        )

add_routes_root(app, late_init)
add_routes_v1(app_v1, late_init, api_v1_integration)
add_routes_v3(app_v3, late_init, api_v3_integration)
add_private_routes_v3(app_v3_private, late_init, api_v3_integration)

app.mount("/v1", app_v1)
app.mount("/v3", app_v3)
app.mount("/private/v3", app_v3_private)


if settings.enable_profiler:
    add_profiler(app_v1)
    add_profiler(app_v3)
    add_profiler(app_v3_private)

if settings.enable_file_profiler:
    add_file_profiler(app_v1)
    add_file_profiler(app_v3)
    add_file_profiler(app_v3_private)

if settings.enable_timing:
    add_timing(app_v1, logger)
    add_timing(app_v3, logger)
    add_timing(app_v3_private, logger)
