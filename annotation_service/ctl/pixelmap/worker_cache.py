import time

EXPIRATION_TIME_SECONDS = 300


class WorkerCache:
    def __init__(self):
        self._sockets = {}

    def add_socket(self, pixelmap_id, socket, port):
        expiration_time = time.time() + EXPIRATION_TIME_SECONDS
        self._sockets[pixelmap_id] = (socket, port, expiration_time)

    def get_socket(self, pixelmap_id):
        socket, port, expiration_time = self._sockets.get(pixelmap_id, (None, None, None))
        if socket:
            expiration_time = time.time() + EXPIRATION_TIME_SECONDS
            self._sockets[pixelmap_id] = (socket, port, expiration_time)

        return (socket, port)

    def get_expired_sockets(self):
        expired_items = {}
        for key in self._sockets.keys():
            socket, port, expiration_time = self._sockets[key]
            if time.time() > expiration_time:
                expired_items[key] = (socket, port)
        return expired_items

    def clear_cache(self, pixelmap_id):
        self._sockets.pop(pixelmap_id)
