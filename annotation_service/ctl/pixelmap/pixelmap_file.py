import gzip
from pathlib import Path

import h5py
import numpy as np

COMPRESSION = "lzf"
ELEMENT_SIZE_MAPPING = {
    **dict.fromkeys(["float64", "int64", "uint64"], 8),
    **dict.fromkeys(["float32", "int32", "uint32"], 4),
    **dict.fromkeys(["int16", "uint16"], 2),
    **dict.fromkeys(["int8", "uint8"], 1),
}


class PixelmapFile:
    def __init__(self, data_path: str, case_id: str, gzip_compresslevel: int, pixelmap: dict):
        self._data_path = data_path
        self._case_id = case_id
        self._gzip_compresslevel = gzip_compresslevel
        self._pixelmap = pixelmap
        self._pixelmap_id = pixelmap["id"]

        self._full_data_path = Path(data_path) / Path("hdf5")
        if case_id:
            self._full_data_path = self._full_data_path / Path(case_id)
        self._full_data_path.mkdir(parents=True, exist_ok=True)

        self._file_path = Path(self._full_data_path) / Path(f"{self._pixelmap_id}.hdf5")
        # Changed mode from "w" to "a" to allow appending to existing files
        self._file = h5py.File(self._file_path, mode="a")

        self._groups = {}
        for level in pixelmap["levels"]:
            slide_level = level["slide_level"]
            slide_level = str(slide_level)
            group = self._file.require_group(slide_level)
            self._groups[slide_level] = group

    def delete(self):
        rep_msg = {
            "rep": "success",
            "status_code": 204,
        }

        self._file.close()
        self._file_path.unlink()

        return rep_msg

    def get_tile(self, level, x, y, accept_encoding):
        rep_msg = {
            "rep": "success",
            "status_code": 200,
            "content_encoding": None,
        }
        rep_bytes = None

        group: h5py.Group = self._groups[str(level)]
        tile_name = f"{x}_{y}"

        try:
            dataset: np.array = group[tile_name]
            data = dataset[...]
            rep_bytes = data.tobytes()
        except KeyError:
            rep_msg = {
                "rep": "error",
                "status_code": 404,
                "content_encoding": None,
                "detail": "Tile not found",
            }
            return rep_msg, None

        compress = accept_encoding is not None and "gzip" in [x.strip() for x in accept_encoding.split(",")]
        if compress:
            rep_bytes = gzip.compress(rep_bytes, compresslevel=self._gzip_compresslevel)
            rep_msg["content_encoding"] = "gzip"

        return rep_msg, rep_bytes

    def get_tiles(self, level, start_x, start_y, end_x, end_y, accept_encoding):
        rep_msg = {
            "rep": "success",
            "status_code": 200,
            "content_encoding": None,
        }

        tiles = []
        for row in range(start_y, end_y + 1):
            for col in range(start_x, end_x + 1):
                tile_rep_msg, tile_rep_bytes = self.get_tile(
                    level=level,
                    x=col,
                    y=row,
                    accept_encoding=None,
                )

                if tile_rep_msg["rep"] == "error":
                    return tile_rep_msg, None

                tiles.append(tile_rep_bytes)

        rep_bytes = b"".join(tiles)

        compress = accept_encoding is not None and "gzip" in [x.strip() for x in accept_encoding.split(",")]
        if compress:
            rep_bytes = gzip.compress(rep_bytes, compresslevel=self._gzip_compresslevel)
            rep_msg["content_encoding"] = "gzip"

        return rep_msg, rep_bytes

    def create_tile(self, level, x, y, content_encoding, req_bytes):
        rep_msg = {
            "rep": "success",
            "status_code": 200,
        }

        decompress = content_encoding is not None and "gzip" in [x.strip() for x in content_encoding.split(",")]
        if decompress:
            try:
                req_bytes = gzip.decompress(req_bytes)
            except gzip.BadGzipFile:
                rep_msg = {
                    "rep": "error",
                    "status_code": 422,
                    "detail": "Not a gzipped file.",
                }
                return rep_msg

        dtype = self._pixelmap["element_type"]
        correct_size, detail = self._check_size(req_bytes=req_bytes, tile_count=1)
        if not correct_size:
            rep_msg = {
                "rep": "error",
                "status_code": 422,
                "detail": detail,
            }
            return rep_msg

        group: h5py.Group = self._groups[str(level)]
        tile_name = f"{x}_{y}"
        data = np.frombuffer(req_bytes, dtype=dtype)

        # Use require_dataset to either get existing dataset or create a new one
        # This handles both creation and update scenarios
        dataset = group.require_dataset(
            name=tile_name,
            shape=data.shape,
            dtype=dtype,
            compression=COMPRESSION,
            exact=True,  # Enforce same shape/dtype for existing datasets
        )

        # Update the data
        dataset[...] = data

        return rep_msg

    def create_tiles(self, level, start_x, start_y, end_x, end_y, content_encoding, req_bytes):
        rep_msg = {
            "rep": "success",
            "status_code": 200,
        }

        decompress = content_encoding is not None and "gzip" in [x.strip() for x in content_encoding.split(",")]
        if decompress:
            try:
                req_bytes = gzip.decompress(req_bytes)
            except gzip.BadGzipFile:
                rep_msg = {
                    "rep": "error",
                    "status_code": 422,
                    "detail": "Not a gzipped file.",
                }
                return rep_msg

        tile_count = (end_x - start_x + 1) * (end_y - start_y + 1)
        correct_size, detail = self._check_size(req_bytes=req_bytes, tile_count=tile_count)
        if not correct_size:
            rep_msg = {
                "rep": "error",
                "status_code": 422,
                "detail": detail,
            }
            return rep_msg

        tile_length = self._get_tile_length()

        start_idx = 0
        current_x = start_x
        current_y = start_y

        for _ in range(tile_count):
            tile = req_bytes[start_idx : start_idx + tile_length]

            tile_rep_msg = self.create_tile(
                level=level,
                x=current_x,
                y=current_y,
                content_encoding=None,
                req_bytes=tile,
            )

            if tile_rep_msg["rep"] == "error":
                return tile_rep_msg

            start_idx += tile_length
            if current_x == end_x:
                current_x = start_x
                current_y += 1
            else:
                current_x += 1

        return rep_msg

    def delete_tile(self, level, x, y):
        rep_msg = {
            "rep": "success",
            "status_code": 204,
        }

        group: h5py.Group = self._groups[str(level)]
        tile_name = f"{x}_{y}"
        try:
            del group[tile_name]
        except KeyError:
            rep_msg = {
                "rep": "error",
                "status_code": 404,
                "detail": "Tile not found.",
            }

        return rep_msg

    def close(self):
        self._file.close()

    def _get_tile_length(self):
        element_type = self._pixelmap["element_type"]
        channel_count = self._pixelmap["channel_count"]
        tilesize = self._pixelmap["tilesize"]

        element_type_size = ELEMENT_SIZE_MAPPING[element_type]
        tile_length = (tilesize**2) * channel_count * element_type_size

        return tile_length

    def _check_size(self, req_bytes, tile_count):
        tile_length = self._get_tile_length()

        expected_length = tile_length * tile_count
        is_correct = len(req_bytes) == expected_length
        if is_correct:
            return True, None

        tilesize = self._pixelmap["tilesize"]
        channel_count = self._pixelmap["channel_count"]
        element_type = self._pixelmap["element_type"]

        detail = (
            "Expected length: "
            f"{tilesize}x{tilesize}x{channel_count}, "
            f"{element_type}, number of tiles: {tile_count} -> {expected_length} "
            f"but got {len(req_bytes)}"
        )
        return False, detail
