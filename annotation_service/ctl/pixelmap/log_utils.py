import sys


def tprint(msg: str):
    """like print, but won't get newlines confused with multiple threads"""
    sys.stdout.write(f"{msg}\n")
    sys.stdout.flush()
