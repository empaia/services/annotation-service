#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_05(conn):
    # ALTER TABLE p_primitives
    # add column ivalue integer
    # add column fvalue float
    # add column bvalue bool
    # add column svalue text
    await conn.execute(
        """
        ALTER TABLE p_primitives ADD COLUMN IF NOT EXISTS ivalue integer;
        ALTER TABLE p_primitives ADD COLUMN IF NOT EXISTS fvalue float;
        ALTER TABLE p_primitives ADD COLUMN IF NOT EXISTS bvalue bool;
        ALTER TABLE p_primitives ADD COLUMN IF NOT EXISTS svalue text;
        """
    )

    # Migration of primitive type tables
    # INTEGER
    await conn.execute(
        """
        UPDATE p_primitives
        SET ivalue=subquery.value
        FROM
        (
            SELECT
            p.id,
            ip.value
            FROM p_primitives AS p
            JOIN p_integer_primitives AS ip
            ON ip.primitive_id=p.id
        ) AS subquery
        WHERE p_primitives.id=subquery.id;
        """
    )

    # FLOAT
    await conn.execute(
        """
        UPDATE p_primitives
        SET fvalue=subquery.value
        FROM
        (
            SELECT
            p.id,
            fp.value
            FROM p_primitives AS p
            JOIN p_float_primitives AS fp
            ON fp.primitive_id=p.id
        ) AS subquery
        WHERE p_primitives.id=subquery.id;
        """
    )

    # BOOL
    await conn.execute(
        """
        UPDATE p_primitives
        SET bvalue=subquery.value
        FROM
        (
            SELECT
            p.id,
            bp.value
            FROM p_primitives AS p
            JOIN p_bool_primitives AS bp
            ON bp.primitive_id=p.id
        ) AS subquery
        WHERE p_primitives.id=subquery.id;
        """
    )

    # STRING
    await conn.execute(
        """
        UPDATE p_primitives
        SET svalue=subquery.value
        FROM
        (
            SELECT
            p.id,
            sp.value
            FROM p_primitives AS p
            JOIN p_string_primitives AS sp
            ON sp.primitive_id=p.id
        ) AS subquery
        WHERE p_primitives.id=subquery.id;
        """
    )

    # DROP primitive type tables and indexer
    # INDEXER
    await conn.execute(
        """
        DROP INDEX p_integer_primitives_prim;
        DROP INDEX p_float_primitives_prim;
        DROP INDEX p_bool_primitives_prim;
        DROP INDEX p_string_primitives_prim;
        """
    )

    # TABLES
    await conn.execute(
        """
        DROP TABLE p_integer_primitives;
        DROP TABLE p_float_primitives;
        DROP TABLE p_bool_primitives;
        DROP TABLE p_string_primitives;
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
