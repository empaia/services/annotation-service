#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################

from asyncpg.exceptions import UniqueViolationError


async def step_02(conn):
    # Alter tables c_collections
    # reference_id and reference_type are no longer obligatory
    await conn.execute(
        """
        ALTER TABLE c_collections ALTER COLUMN reference_id DROP NOT NULL;
        """
    )

    await conn.execute(
        """
        ALTER TABLE c_collections ALTER COLUMN reference_type DROP NOT NULL;
        """
    )

    # Alter tables p_primitives
    # reference_id and reference_type are no longer obligatory
    await conn.execute(
        """
        ALTER TABLE p_primitives ALTER COLUMN reference_id DROP NOT NULL;
        """
    )

    await conn.execute(
        """
        ALTER TABLE p_primitives ALTER COLUMN reference_type DROP NOT NULL;
        """
    )

    # Add extension "pgcrypto" to create uuid type 4
    try:
        async with conn.transaction():
            await conn.execute(
                """
                CREATE EXTENSION IF NOT EXISTS "pgcrypto";
                """
            )
    except UniqueViolationError as e:
        print("WARNING:", e)

    # Alter tables to use more performant function to create uuids, change serial to bigserial
    await conn.execute(
        """
        ALTER TABLE a_annotations ALTER COLUMN id SET DEFAULT gen_random_uuid();
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE a_point_annotations_id_seq AS bigint;
        ALTER TABLE a_point_annotations ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE a_line_annotations_id_seq AS bigint;
        ALTER TABLE a_line_annotations ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE a_arrow_annotations_id_seq AS bigint;
        ALTER TABLE a_arrow_annotations ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE a_circle_annotations_id_seq AS bigint;
        ALTER TABLE a_circle_annotations ALTER id TYPE bigint;
        ALTER TABLE a_circle_annotations ALTER COLUMN radius TYPE integer;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE a_rectangle_annotations_id_seq AS bigint;
        ALTER TABLE a_rectangle_annotations ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        DROP TABLE a_polygon_annotations;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE a_polygon_points_id_seq AS bigint;
        ALTER TABLE a_polygon_points ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        ALTER TABLE cl_classes ALTER COLUMN id SET DEFAULT gen_random_uuid();
        """
    )

    await conn.execute(
        """
        ALTER TABLE c_collections ALTER COLUMN id SET DEFAULT gen_random_uuid();
        """
    )

    await conn.execute(
        """
        ALTER TABLE p_primitives ALTER COLUMN id SET DEFAULT gen_random_uuid();
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE p_integer_primitives_id_seq AS bigint;
        ALTER TABLE p_integer_primitives ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE p_float_primitives_id_seq AS bigint;
        ALTER TABLE p_float_primitives ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE p_bool_primitives_id_seq AS bigint;
        ALTER TABLE p_bool_primitives ALTER id TYPE bigint;
        """
    )

    await conn.execute(
        """
        ALTER SEQUENCE p_string_primitives_id_seq AS bigint;
        ALTER TABLE p_string_primitives ALTER id TYPE bigint;
        """
    )

    # rename resolution to npp_created, add npp_viewing_min, npp_viewing_max
    await conn.execute(
        """
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS npp_viewing_min float;
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS npp_viewing_max float;
        ALTER TABLE a_annotations RENAME COLUMN resolution TO npp_created;
        """
    )

    # add NOT NULL contraint for npp_created (set NULL values in npp_created to 1.0)
    await conn.execute(
        """
        UPDATE a_annotations
        SET npp_created = 1.0
        WHERE npp_created IS NULL;

        ALTER TABLE a_annotations ALTER COLUMN npp_created SET NOT NULL;
        """
    )

    # add job_ tables
    await conn.execute(
        """
        CREATE TABLE j_job_annotations (
            job_id text,
            annotation_id uuid REFERENCES a_annotations(id),
            PRIMARY KEY (job_id, annotation_id)
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE j_job_classes (
            job_id text,
            class_id uuid REFERENCES cl_classes(id),
            PRIMARY KEY (job_id, class_id)
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE j_job_collections (
            job_id text,
            collection_id uuid REFERENCES c_collections(id),
            PRIMARY KEY (job_id, collection_id)
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE j_job_primitives (
            job_id text,
            primitive_id uuid REFERENCES p_primitives(id),
            PRIMARY KEY (job_id, primitive_id)
        );
        """
    )

    # add indexer
    await conn.execute(
        """
        CREATE INDEX a_annotations_creator ON a_annotations USING btree (creator_id);
        CREATE INDEX a_annotations_reference ON a_annotations USING btree (reference_id);
        CREATE INDEX a_annotations_npp_created ON a_annotations USING btree (npp_created);
        CREATE INDEX a_annotations_npp_min ON a_annotations USING btree (npp_viewing_min);
        CREATE INDEX a_annotations_npp_max ON a_annotations USING btree (npp_viewing_max);
        CREATE INDEX a_annotations_type ON a_annotations USING btree (type);

        CREATE INDEX a_arrow_annotations_annot ON a_arrow_annotations USING btree (annot_id);

        CREATE INDEX a_circle_annotations_annot ON a_circle_annotations USING btree (annot_id);

        CREATE INDEX a_line_annotations_annot ON a_line_annotations USING btree (annot_id);

        CREATE INDEX a_point_annotations_annot ON a_point_annotations USING btree (annot_id);

        CREATE INDEX a_rectangle_annotations_annot ON a_rectangle_annotations USING btree (annot_id);

        CREATE INDEX a_polygon_points_annot ON a_polygon_points USING btree (annot_id);

        CREATE INDEX cl_classes_creator ON cl_classes USING btree (creator_id);
        CREATE INDEX cl_classes_reference ON cl_classes USING btree (reference_id);

        CREATE INDEX c_collections_creator ON c_collections USING btree (creator_id);
        CREATE INDEX c_collections_reference ON c_collections USING btree (reference_id);
        CREATE INDEX c_collections_type ON c_collections USING btree (item_type);

        CREATE INDEX c_collection_items_coll ON c_collection_items USING btree (collection_id);

        CREATE INDEX p_primitives_creator ON p_primitives USING btree (creator_id);
        CREATE INDEX p_primitives_reference ON p_primitives USING btree (reference_id);
        CREATE INDEX p_primitives_type ON p_primitives USING btree (type);

        CREATE INDEX p_integer_primitives_prim ON p_integer_primitives USING btree (primitive_id);

        CREATE INDEX p_float_primitives_prim ON p_float_primitives USING btree (primitive_id);

        CREATE INDEX p_bool_primitives_prim ON p_bool_primitives USING btree (primitive_id);

        CREATE INDEX p_string_primitives_prim ON p_string_primitives USING btree (primitive_id);

        CREATE INDEX j_job_annotations_job ON j_job_annotations USING btree (job_id);

        CREATE INDEX j_job_classes_job ON j_job_classes USING btree (job_id);

        CREATE INDEX j_job_collections_job ON j_job_collections USING btree (job_id);

        CREATE INDEX j_job_primitives_job ON j_job_primitives USING btree (job_id);
        """
    )

    # add indexer for class values
    await conn.execute(
        """
        CREATE INDEX cl_classes_value ON cl_classes USING btree (value);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
