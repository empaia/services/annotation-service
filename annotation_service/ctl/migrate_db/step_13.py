#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_13(conn):
    # Add columns for collection ids to v3 data tables
    async with conn.transaction():
        await conn.execute(
            """
            ALTER TABLE v3_a_annotations ADD COLUMN IF NOT EXISTS collection_ids uuid[] DEFAULT ARRAY[]::uuid[];
            ALTER TABLE v3_cl_classes ADD COLUMN IF NOT EXISTS collection_ids uuid[] DEFAULT ARRAY[]::uuid[];
            ALTER TABLE v3_c_collections ADD COLUMN IF NOT EXISTS collection_ids uuid[] DEFAULT ARRAY[]::uuid[];
            ALTER TABLE v3_p_primitives ADD COLUMN IF NOT EXISTS collection_ids uuid[] DEFAULT ARRAY[]::uuid[];
            """
        )

        # migrate collection items
        await migrate_collection_items(conn)

        # Add NOT NULL constarints on columns collection_ids
        await conn.execute(
            """
            ALTER TABLE v3_a_annotations ALTER COLUMN collection_ids SET NOT NULL;
            ALTER TABLE v3_cl_classes ALTER COLUMN collection_ids SET NOT NULL;
            ALTER TABLE v3_c_collections ALTER COLUMN collection_ids SET NOT NULL;
            ALTER TABLE v3_p_primitives ALTER COLUMN collection_ids SET NOT NULL;
            """
        )

        # Create indices on collection_ids
        await conn.execute(
            """
            CREATE INDEX v3_a_annotations_colids ON v3_a_annotations USING GIN (collection_ids);
            CREATE INDEX v3_cl_classes_colids ON v3_cl_classes USING GIN (collection_ids);
            CREATE INDEX v3_c_collections_colids ON v3_c_collections USING GIN (collection_ids);
            CREATE INDEX v3_p_primitives_colids ON v3_p_primitives USING GIN (collection_ids);
            """
        )

        # Rename collection items table and recreate index
        await conn.execute(
            """
            DROP INDEX v3_c_collection_items_coll;
            ALTER TABLE v3_c_collection_items RENAME TO v3_c_collection_slides;
            ALTER TABLE v3_c_collection_slides RENAME COLUMN item_id TO slide_id;
            CREATE INDEX v3_c_collection_slides_cid ON v3_c_collection_slides USING BTREE (collection_id);
            """
        )

    # Add column for class values to v3 annotation table
    async with conn.transaction():
        await conn.execute(
            """
            ALTER TABLE v3_a_annotations ADD COLUMN IF NOT EXISTS class_values TEXT[] DEFAULT ARRAY[]::TEXT[];
            """
        )

        # migrate collection items
        await migrate_annotation_class_values(conn)

        # Add NOT NULL constarint on column annotation class_values
        await conn.execute(
            """
            ALTER TABLE v3_a_annotations ALTER COLUMN class_values SET NOT NULL;
            """
        )

        # Create index on annotation class_values
        await conn.execute(
            """
            CREATE INDEX v3_a_annotations_clvals ON v3_a_annotations USING GIN (class_values);
            """
        )

    # Create function to retrieve existing view names from provided view name list
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_views_exists(
                _view_names TEXT[]
            )
            RETURNS TEXT[]
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            DECLARE exist TEXT[];
            BEGIN
                SELECT
                    array_agg(matviewname) INTO exist
                FROM
                    pg_matviews
                WHERE
                    matviewname = ANY(_view_names);
            RETURN exist;
            END $func$;
            """
        )

    # Alter annotation functions

    # DROP old functions
    async with conn.transaction():
        await conn.execute(
            """
            DROP FUNCTION IF EXISTS get_locked_annotation_ids;
            DROP FUNCTION IF EXISTS get_annotation_ids_for_jobs_classes;
            DROP FUNCTION IF EXISTS get_annotation_ids;
            """
        )

    # Function to retrieve locked annotation collections
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_annotation_collection_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                collection_id uuid[]
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    ARRAY_AGG(v3cc.collection_id) AS ids
                FROM
                    v3_j_job_collections AS v3cc
                WHERE
                    v3cc.job_id = ANY(_jobs)
                    AND v3cc.item_type = ANY(ARRAY['point', 'line', 'arrow', 'circle', 'rectangle', 'polygon']);
            END $func$;
            """
        )

    # Function to retrieve directly locked annotations
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_annotation_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                annot_id uuid
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3jjcl.annot_id
                FROM
                    v3_j_job_classes AS v3jjcl
                WHERE
                    v3jjcl.job_id = ANY(_jobs);
            END $func$;
            """
        )

    # Procedure to create materialized view for specific job id for annotations
    async with conn.transaction():
        await conn.execute(
            """
            DROP PROCEDURE IF EXISTS create_annotation_view;

            CREATE OR REPLACE PROCEDURE create_annotation_view(
                _job_id TEXT,
                _view_name TEXT
            )
            LANGUAGE plpgsql
            AS $$
            BEGIN
                execute FORMAT('
                DROP INDEX IF EXISTS %1$s_id;
                DROP INDEX IF EXISTS %1$s_cr;
                DROP INDEX IF EXISTS %1$s_ref;
                DROP INDEX IF EXISTS %1$s_type;
                DROP INDEX IF EXISTS %1$s_nppmin;
                DROP INDEX IF EXISTS %1$s_nppmax;
                DROP INDEX IF EXISTS %1$s_bb;
                DROP INDEX IF EXISTS %1$s_colids;
                DROP INDEX IF EXISTS %1$s_clvals;
                DROP MATERIALIZED VIEW IF EXISTS %1$s;
                CREATE MATERIALIZED VIEW %1$s AS (
                    WITH annotations AS (
                        SELECT
                            *
                        FROM
                            v3_a_annotations
                        WHERE
                            collection_ids && (
                                SELECT
                                    *
                                FROM
                                    get_locked_annotation_collection_ids(ARRAY[%2$L]::TEXT[])
                            )
                            OR id IN (
                                SELECT
                                    *
                                FROM
                                    get_locked_annotation_ids(ARRAY[%2$L]::TEXT[])
                            )
                    ),
                    class_refs_and_values AS (
                        SELECT
                            reference_id,
                            array_agg(value) AS class_values
                        FROM
                            v3_cl_classes
                        WHERE
                            collection_ids && (
                                SELECT
                                    *
                                FROM
                                    get_locked_class_collection_ids(ARRAY[%2$L]::TEXT[])
                            )
                            OR id IN (
                                SELECT
                                    *
                                FROM
                                    get_locked_class_ids(ARRAY[%2$L]::TEXT[])
                            )
                        GROUP BY
                            reference_id
                    )
                    SELECT
                        annotations.id,
                        annotations.type,
                        annotations.name,
                        annotations.description,
                        annotations.creator_id,
                        annotations.creator_type,
                        annotations.reference_id,
                        annotations.reference_type,
                        annotations.npp_created,
                        annotations.is_locked,
                        annotations.created_at,
                        annotations.updated_at,
                        annotations.npp_viewing_min,
                        annotations.npp_viewing_max,
                        annotations.coordinates,
                        annotations.head,
                        annotations.tail,
                        annotations.center,
                        annotations.radius,
                        annotations.upper_left,
                        annotations.width,
                        annotations.height,
                        annotations.centroid,
                        annotations.bounding_box,
                        annotations.npp_viewing_min_query,
                        annotations.npp_viewing_max_query,
                        annotations.collection_ids,
                        class_refs_and_values.class_values
                    FROM
                        annotations
                        LEFT JOIN class_refs_and_values
                        ON annotations.id = class_refs_and_values.reference_id
                );
                CREATE INDEX %1$s_id ON %1$s USING BTREE (id);
                CREATE INDEX %1$s_cr ON %1$s USING BTREE (creator_id);
                CREATE INDEX %1$s_ref ON %1$s USING BTREE (reference_id);
                CREATE INDEX %1$s_type ON %1$s USING BTREE (type);
                CREATE INDEX %1$s_nppmin ON %1$s USING BTREE (npp_viewing_min_query);
                CREATE INDEX %1$s_nppmax ON %1$s USING BTREE (npp_viewing_max_query);
                CREATE INDEX %1$s_bb ON %1$s USING GIST (bounding_box);
                CREATE INDEX %1$s_colids ON %1$s USING GIN (collection_ids);
                CREATE INDEX %1$s_clvals ON %1$s USING GIN (class_values);'
                , _view_name, _job_id);
            END;
            $$
            """
        )

    # Function to query annotations
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_annotation_ids(
                _references TEXT[],
                _creators TEXT[],
                _types TEXT[],
                _ids uuid[],
                _viewport BOX,
                _class_values TEXT[],
                _collection_ids uuid[]
            )
            RETURNS TABLE (
                annotation_id uuid
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3aa.id
                FROM
                    v3_a_annotations AS v3aa
                WHERE
                    (
                        _references IS NULL
                        OR v3aa.reference_id = ANY(_references)
                    )
                    AND (
                        _creators IS NULL
                        OR v3aa.creator_id = ANY(_creators)
                    )
                    AND (
                        _types IS NULL
                        OR v3aa.type = ANY(_types)
                    )
                    AND (
                        _ids IS NULL
                        OR v3aa.id = ANY(_ids)
                    )
                    AND (
                        _viewport IS NULL
                        OR v3aa.bounding_box ?# _viewport
                    )
                    AND (
                        _class_values IS NULL
                        OR v3aa.class_values && _class_values
                    )
                    AND (
                        _collection_ids IS NULL
                        OR v3aa.collection_ids && _collection_ids
                    );
            END
            $func$;
            """
        )

    # Alter classes query functions

    # DROP old functions
    async with conn.transaction():
        await conn.execute(
            """
            DROP FUNCTION IF EXISTS get_locked_class_ids;
            DROP FUNCTION IF EXISTS get_class_ids;
            """
        )

    # Function to retrieve locked class collections
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_class_collection_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                collection_id uuid[]
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    ARRAY_AGG(v3cc.collection_id) AS ids
                FROM
                    v3_j_job_collections AS v3cc
                WHERE
                    v3cc.job_id = ANY(_jobs)
                    AND v3cc.item_type = 'class';
            END $func$;
            """
        )

    # Function to retrieve directly locked classes
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_class_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                class_id uuid
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3jjcl.class_id::uuid
                FROM
                    v3_j_job_classes AS v3jjcl
                WHERE
                    v3jjcl.job_id = ANY(_jobs)
                    AND v3jjcl.class_id <> '';
            END $func$;
            """
        )

    # Procedure to create materialized view for specific job id for classes
    async with conn.transaction():
        await conn.execute(
            """
            DROP PROCEDURE IF EXISTS create_class_view;

            CREATE OR REPLACE PROCEDURE create_class_view(
                _job_id TEXT,
                _view_name TEXT
            )
            LANGUAGE plpgsql
            AS $$
            BEGIN
                execute FORMAT('
                DROP INDEX IF EXISTS %1$s_id;
                DROP INDEX IF EXISTS %1$s_cr;
                DROP INDEX IF EXISTS %1$s_ref;
                DROP INDEX IF EXISTS %1$s_val;
                DROP INDEX IF EXISTS %1$s_colids;
                DROP MATERIALIZED VIEW IF EXISTS %1$s;
                CREATE MATERIALIZED VIEW %1$s AS (
                    WITH classes AS (
                        SELECT
                            *
                        FROM
                            v3_cl_classes
                        WHERE
                            collection_ids && (
                                SELECT
                                    *
                                FROM
                                    get_locked_class_collection_ids(ARRAY[%2$L]::TEXT[])
                            )
                            OR id IN (
                                SELECT
                                    *
                                FROM
                                    get_locked_class_ids(ARRAY[%2$L]::TEXT[])
                            )
                    )
                    SELECT
                        *
                    FROM
                        classes
                );
                CREATE INDEX %1$s_id ON %1$s USING BTREE (id);
                CREATE INDEX %1$s_cr ON %1$s USING BTREE (creator_id);
                CREATE INDEX %1$s_ref ON %1$s USING BTREE (reference_id);
                CREATE INDEX %1$s_val ON %1$s USING BTREE (value);
                CREATE INDEX %1$s_colids ON %1$s USING GIN (collection_ids);'
                , _view_name, _job_id);
            END;
            $$
            """
        )

    # Function to query classes without job filter
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_class_ids(
                _references uuid[],
                _creators TEXT[],
                _ids uuid[],
                _collection_ids uuid[]
            )
            RETURNS TABLE (
                class_id uuid
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3cl.id
                FROM
                    v3_cl_classes AS v3cl
                WHERE
                    (
                        _references IS NULL
                        OR v3cl.reference_id = ANY(_references)
                    )
                    AND (
                        _creators IS NULL
                        OR v3cl.creator_id = ANY(_creators)
                    )
                    AND (
                        _ids IS NULL
                        OR v3cl.id = ANY(_ids)
                    )
                    AND (
                        _collection_ids IS NULL
                        OR v3cl.collection_ids && _collection_ids
                    );
            END
            $func$;
            """
        )

    # Alter primitive query functions

    # DROP old functions
    async with conn.transaction():
        await conn.execute(
            """
            DROP FUNCTION IF EXISTS get_locked_primitive_ids;
            DROP FUNCTION IF EXISTS get_primitive_ids;
            """
        )

    # Funtion to retrieve locked primitive collections
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_primitive_collection_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                collection_id uuid[]
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    ARRAY_AGG(v3cc.collection_id) AS ids
                FROM
                    v3_j_job_collections AS v3cc
                WHERE
                    v3cc.job_id = ANY(_jobs)
                    AND v3cc.item_type = ANY(ARRAY['integer', 'float', 'bool', 'string']);
            END $func$;
            """
        )

    # Function to retrieve directly locked primitives
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_primitive_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                primitive_id uuid
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3jjp.primitive_id
                FROM
                    v3_j_job_primitives AS v3jjp
                WHERE
                    v3jjp.job_id = ANY(_jobs);
            END $func$;
            """
        )

    # Procedure to create materialized view for specific job id for primitives
    async with conn.transaction():
        await conn.execute(
            """
            DROP PROCEDURE IF EXISTS create_primitive_view;

            CREATE OR REPLACE PROCEDURE create_primitive_view(
                _job_id TEXT,
                _view_name TEXT
            )
            LANGUAGE plpgsql
            AS $$
            BEGIN
                execute FORMAT('
                DROP INDEX IF EXISTS %1$s_id;
                DROP INDEX IF EXISTS %1$s_cr;
                DROP INDEX IF EXISTS %1$s_ref;
                DROP INDEX IF EXISTS %1$s_type;
                DROP INDEX IF EXISTS %1$s_colids;
                DROP MATERIALIZED VIEW IF EXISTS %1$s;
                CREATE MATERIALIZED VIEW %1$s AS (
                    WITH primitives AS (
                        SELECT
                            *
                        FROM
                            v3_p_primitives
                        WHERE
                            collection_ids && (
                                SELECT
                                    *
                                FROM
                                    get_locked_primitive_collection_ids(ARRAY[%2$L]::TEXT[])
                            )
                            OR id IN (
                                SELECT
                                    *
                                FROM
                                    get_locked_primitive_ids(ARRAY[%2$L]::TEXT[])
                            )
                    )
                    SELECT
                        *
                    FROM
                        primitives
                );
                CREATE INDEX %1$s_id ON %1$s USING BTREE (id);
                CREATE INDEX %1$s_cr ON %1$s USING BTREE (creator_id);
                CREATE INDEX %1$s_ref ON %1$s USING BTREE (reference_id);
                CREATE INDEX %1$s_type ON %1$s USING BTREE (type);
                CREATE INDEX %1$s_colids ON %1$s USING GIN (collection_ids);'
                , _view_name, _job_id);
            END;
            $$
            """
        )

    # Function to query primitives without job filter
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_primitive_ids(
                _references TEXT[],
                _null_references BOOLEAN,
                _creators TEXT[],
                _types TEXT[],
                _ids uuid[],
                _collection_ids uuid[]
            )
            RETURNS TABLE (
                primitive_id uuid
            )
            LANGUAGE plpgsql
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3pp.id
                FROM
                    v3_p_primitives AS v3pp
                WHERE
                    (
                        _references IS NULL
                        OR v3pp.reference_id = ANY(_references)
                        OR (
                            CASE
                                WHEN _null_references IS TRUE THEN v3pp.reference_id IS NULL
                            END
                        )
                    )
                    AND (
                        _creators IS NULL
                        OR v3pp.creator_id = ANY(_creators)
                    )
                    AND (
                        _types IS NULL
                        OR v3pp.type = ANY(_types)
                    )
                    AND (
                        _ids IS NULL
                        OR v3pp.id = ANY(_ids)
                    )
                    AND (
                        _collection_ids IS NULL
                        OR v3pp.collection_ids && _collection_ids
                    );
            END
            $func$;
            """
        )

    # Alter collections query functions

    # DROP old functions
    async with conn.transaction():
        await conn.execute(
            """
            DROP FUNCTION IF EXISTS get_locked_collection_ids;
            DROP FUNCTION IF EXISTS get_collection_ids;
            """
        )

    # Function to retrieve locked collections
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_locked_collection_ids(
            _jobs TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        STABLE
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                ids.*
            FROM
                (
                    SELECT
                        v3jjc.collection_id
                    FROM
                        v3_j_job_collections AS v3jjc
                    WHERE
                        v3jjc.job_id = ANY(_jobs)
                ) AS ids;
        END $func$;
        """
    )

    # Procedure to create materialized view for specific job id for collections
    async with conn.transaction():
        await conn.execute(
            """
            DROP PROCEDURE IF EXISTS create_collection_view;

            CREATE OR REPLACE PROCEDURE create_collection_view(
                _job_id TEXT,
                _view_name TEXT
            )
            LANGUAGE plpgsql
            AS $$
            BEGIN
                execute FORMAT('
                DROP INDEX IF EXISTS %1$s_id;
                DROP INDEX IF EXISTS %1$s_cr;
                DROP INDEX IF EXISTS %1$s_ref;
                DROP INDEX IF EXISTS %1$s_itype;
                DROP INDEX IF EXISTS %1$s_colids;
                DROP MATERIALIZED VIEW IF EXISTS %1$s;
                CREATE MATERIALIZED VIEW %1$s AS (
                    WITH collections AS (
                        SELECT
                            *
                        FROM
                            v3_c_collections
                        WHERE
                            id IN (
                                SELECT
                                    *
                                FROM
                                    get_locked_collection_ids(ARRAY[%2$L]::TEXT[])
                            )
                    )
                    SELECT
                        *
                    FROM
                        collections
                );
                CREATE INDEX %1$s_id ON %1$s USING BTREE (id);
                CREATE INDEX %1$s_cr ON %1$s USING BTREE (creator_id);
                CREATE INDEX %1$s_ref ON %1$s USING BTREE (reference_id);
                CREATE INDEX %1$s_itype ON %1$s USING BTREE (item_type);
                CREATE INDEX %1$s_colids ON %1$s USING GIN (collection_ids);'
                , _view_name, _job_id);
            END;
            $$
            """
        )

    # Function to query collections without job filter
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_collection_ids(
            _references TEXT[],
            _creators TEXT[],
            _item_types TEXT[],
            _ids uuid[],
            _collection_ids uuid[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        STABLE
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                v3cc.id
            FROM
                v3_c_collections AS v3cc
            WHERE
                (
                    _references IS NULL
                    OR v3cc.reference_id = ANY(_references)
                )
                AND (
                    _creators IS NULL
                    OR v3cc.creator_id = ANY(_creators)
                )
                AND (
                    _item_types IS NULL
                    OR v3cc.item_type = ANY(_item_types)
                )
                AND (
                    _ids IS NULL
                    OR v3cc.id = ANY(_ids)
                )
                AND (
                    _collection_ids IS NULL
                    OR v3cc.collection_ids && _collection_ids
                );
        END
        $func$;
        """
    )

    async with conn.transaction():
        await aggregate_jobs(conn)


# MIGRATION FUNCTIONS


async def migrate_collection_items(conn):
    print("\n***\nSTARTING COLLECTION MIGRATION\n")

    item_type_mapping_update = {
        **dict.fromkeys(
            ["point", "line", "arrow", "circle", "rectangle", "polygon"],
            update_collection_annotations,
        ),
        **dict.fromkeys(
            ["integer", "float", "bool", "string"],
            update_collection_primitives,
        ),
        "class": update_collection_classes,
        "collection": update_collection_collections,
    }

    # get collection count
    collection_count_sql = """
    SELECT
        COUNT(id)
    FROM
        v3_c_collections;
    """

    collection_count = await conn.fetchval(collection_count_sql)
    print(f"COLLECTION TO MIGRATE FOUND: {collection_count}")

    collection_sql = """
    SELECT
        id,
        item_type
    FROM
        v3_c_collections
    ORDER BY
        id ASC
    LIMIT $1 OFFSET $2;
    """

    collection_items_sql = """
    SELECT
        item_id::uuid
    FROM
        v3_c_collection_items
    WHERE
        collection_id = $1;
    """

    for i in range(collection_count):
        collection = await conn.fetchrow(collection_sql, 1, i)

        collection_id = collection["id"]
        item_type = collection["item_type"]

        if item_type == "wsi":
            continue

        items = await conn.fetch(collection_items_sql, collection_id)

        print(f"\nMIGRATE COLLECTION {i + 1}/{collection_count}")

        item_ids = [item["item_id"] for item in items]

        print(f"COLLECTION {collection_id} - ITEM TYPE: {item_type}, ITEMS: {len(item_ids)}")

        update_count = await item_type_mapping_update[item_type](conn, collection_id, item_ids)

        print(f"-> UPDATED ITEMS: {update_count}, EXPECTED: {len(item_ids)}")

        if update_count != len(item_ids):
            print("-> NOT ALL ITEMS WERE UPDATE! ITEM COULD BE DELETED BUT NOT REMOVED FROM COLLECTION")

        delete_count = await delete_collection_items(conn, collection_id, item_ids)

        print(f"-> DELETED ITEMS: {delete_count}, EXPECTED: {len(item_ids)}")

        if delete_count != len(item_ids):
            print("-> NOT ALL ITEMS WERE DELETED!")

    print("\nCOLLECTION MIGRATION FINISHED")


async def update_collection_annotations(conn, collection_id, ids):
    sql = """
    WITH rows AS (
        UPDATE
            v3_a_annotations
        SET
            collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
        WHERE
            id = ANY($2::uuid[])
        RETURNING 1
    )
    SELECT count(*) FROM rows;
    """

    return await conn.fetchval(sql, collection_id, ids)


async def update_collection_classes(conn, collection_id, ids):
    sql = """
    WITH rows AS (
        UPDATE
            v3_cl_classes
        SET
            collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
        WHERE
            id = ANY($2::uuid[])
        RETURNING 1
    )
    SELECT count(*) FROM rows;
    """

    return await conn.fetchval(sql, collection_id, ids)


async def update_collection_collections(conn, collection_id, ids):
    sql = """
    WITH rows AS (
        UPDATE
            v3_c_collections
        SET
            collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
        WHERE
            id = ANY($2::uuid[])
        RETURNING 1
    )
    SELECT count(*) FROM rows;
    """

    return await conn.fetchval(sql, collection_id, ids)


async def update_collection_primitives(conn, collection_id, ids):
    sql = """
    WITH rows AS (
        UPDATE
            v3_p_primitives
        SET
            collection_ids = ARRAY_APPEND(collection_ids, $1::uuid)
        WHERE
            id = ANY($2::uuid[])
        RETURNING 1
    )
    SELECT count(*) FROM rows;
    """

    return await conn.fetchval(sql, collection_id, ids)


async def delete_collection_items(conn, collection_id, ids):
    sql = """
    WITH ROWS AS (
        DELETE FROM
            v3_c_collection_items
        WHERE
            collection_id = $1
            AND item_id = ANY($2::TEXT[])
        RETURNING 1
    )
    SELECT
        count(*)
    FROM
        ROWS;
    """
    item_ids = [str(item_id) for item_id in ids]
    return await conn.fetchval(sql, collection_id, item_ids)


async def migrate_annotation_class_values(conn):
    print("\n***\nSTARTING CLASS VALUE MIGRATION")

    # get class count
    class_count_sql = """
    SELECT
        COUNT(id)
    FROM
        v3_cl_classes;
    """

    class_count = await conn.fetchval(class_count_sql)
    print(f"CLASSES TO MIGRATE FOUND: {class_count}")

    ref_val_sql = """
    SELECT
        reference_id,
        value
    FROM
        v3_cl_classes
    ORDER BY
        id ASC
    LIMIT $1 OFFSET $2;
    """

    update_sql = """
    UPDATE
        v3_a_annotations
    SET
        class_values = ARRAY_APPEND(class_values, ref_class_values.class_value)
    FROM
        (
            VALUES
                ($1::uuid, $2)
        ) AS ref_class_values (reference_id, class_value)
    WHERE
        v3_a_annotations.id = ref_class_values.reference_id;
    """

    limit = 1000
    batch_count = class_count // limit + 1

    for i in range(batch_count):
        offset = i * limit

        print(f"MIGRATE CLASSES {offset + 1} - {offset + limit} / {class_count}")

        classes = await conn.fetch(ref_val_sql, limit, offset)

        update_data = [[cl[0], cl[1]] for cl in classes]

        await conn.executemany(update_sql, update_data)

    print("CLASS VALUE MIGRATION FINISHED")


async def aggregate_jobs(conn):
    print("\n***\nSTARTING JOB AGGREGATION\n")

    # get all job_ids
    job_sql = """
    SELECT
        jobs.job_id
    FROM
        (
            SELECT
                job_id
            FROM
                v3_j_job_classes
            UNION
            SELECT
                job_id
            FROM
                v3_j_job_collections
            UNION
            SELECT
                job_id
            FROM
                v3_j_job_primitives
            UNION
            SELECT
                job_id
            FROM
                v3_j_job_slides
        ) AS jobs
    """

    job_ids = await conn.fetch(job_sql)
    job_count = len(job_ids)
    print(f"JOBS TO AGGREGATE FOUND: {job_count}")

    for idx, job in enumerate(job_ids):
        print(f"\nAGGREGATE JOB: {idx + 1}/{job_count}")

        job_id = job["job_id"]

        print("CREATE ANNOTATION VIEW")
        await conn.execute(
            "CALL create_annotation_view($1, $2)", job_id, f"mat_view_annotations_{job_id.replace('-', '')}"
        )

        print("CREATE CLASS VIEW")
        await conn.execute("CALL create_class_view($1, $2)", job_id, f"mat_view_classes_{job_id.replace('-', '')}")

        print("CREATE COLLECTION VIEW")
        await conn.execute(
            "CALL create_collection_view($1, $2)", job_id, f"mat_view_collections_{job_id.replace('-', '')}"
        )

        print("CREATE PRIMITIVE VIEW")
        await conn.execute(
            "CALL create_primitive_view($1, $2)", job_id, f"mat_view_primitives_{job_id.replace('-', '')}"
        )

    print("\nJOB AGGREGATION FINISHED\n")


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
