#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_14(conn):
    # Add pixelmap table
    await conn.execute(
        """
        CREATE TABLE IF NOT EXISTS public.v3_pm_pixelmaps (
            id uuid NOT NULL DEFAULT gen_random_uuid(),
            TYPE TEXT NOT NULL,
            NAME TEXT NOT NULL,
            description TEXT,
            creator_id TEXT NOT NULL,
            creator_type TEXT NOT NULL,
            reference_id TEXT NOT NULL,
            reference_type TEXT NOT NULL,
            levels jsonb[] NOT NULL,
            channel_count INTEGER NOT NULL,
            tilesize INTEGER NOT NULL,
            channel_class_mapping jsonb[],
            element_type TEXT NOT NULL,
            min_value_int BIGINT,
            min_value_float DOUBLE PRECISION,
            neutral_value_int BIGINT,
            neutral_value_float DOUBLE PRECISION,
            max_value_int BIGINT,
            max_value_float DOUBLE PRECISION,
            element_class_mapping jsonb[],
            created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
            updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
            collection_ids uuid[] NOT NULL DEFAULT ARRAY[]::uuid[],
            CONSTRAINT v3_pm_pixelmaps_pkey PRIMARY KEY (id)
        ) TABLESPACE pg_default;

        CREATE INDEX IF NOT EXISTS v3_pm_pixelmaps_creator ON public.v3_pm_pixelmaps USING btree (
            creator_id COLLATE pg_catalog."default" ASC NULLS LAST
        ) TABLESPACE pg_default;

        CREATE INDEX IF NOT EXISTS v3_pm_pixelmaps_reference ON public.v3_pm_pixelmaps USING btree (
            reference_id COLLATE pg_catalog."default" ASC NULLS LAST
        ) TABLESPACE pg_default;

        CREATE INDEX IF NOT EXISTS v3_pm_pixelmaps_type ON public.v3_pm_pixelmaps USING btree (
            TYPE COLLATE pg_catalog."default" ASC NULLS LAST
        ) TABLESPACE pg_default;

        CREATE INDEX IF NOT EXISTS v3_pm_pixelmaps_colids ON public.v3_pm_pixelmaps USING gin (
            collection_ids
        ) TABLESPACE pg_default;
        """
    )

    # Add pixelmap lock table
    await conn.execute(
        """
        CREATE TABLE IF NOT EXISTS public.v3_j_job_pixelmaps (
            job_id TEXT NOT NULL,
            pixelmap_id uuid NOT NULL,
            CONSTRAINT v3_j_job_pixelmaps_pkey PRIMARY KEY (job_id, pixelmap_id)
        ) TABLESPACE pg_default;

        CREATE INDEX IF NOT EXISTS v3_j_job_pixelmaps_job ON public.v3_j_job_pixelmaps USING btree (
            job_id COLLATE pg_catalog."default" ASC NULLS LAST
        ) TABLESPACE pg_default;

        CREATE INDEX IF NOT EXISTS v3_j_job_pixelmaps_sid ON public.v3_j_job_pixelmaps USING btree (
            pixelmap_id ASC NULLS LAST
        ) TABLESPACE pg_default;
        """
    )

    # Function to query pixelmaps without job filter
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_pixelmap_ids(
                _references TEXT[],
                _creators TEXT[],
                _types TEXT[],
                _ids uuid[],
                _collection_ids uuid[]
            )
            RETURNS TABLE (
                pixelmap_id uuid
            )
            LANGUAGE plpgsql
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3s.id
                FROM
                    v3_pm_pixelmaps AS v3s
                WHERE
                    (
                        _references IS NULL
                        OR v3s.reference_id = ANY(_references)
                    )
                    AND (
                        _creators IS NULL
                        OR v3s.creator_id = ANY(_creators)
                    )
                    AND (
                        _types IS NULL
                        OR v3s.type = ANY(_types)
                    )
                    AND (
                        _ids IS NULL
                        OR v3s.id = ANY(_ids)
                    )
                    AND (
                        _collection_ids IS NULL
                        OR v3s.collection_ids && _collection_ids
                    );
            END
            $func$;
            """
        )

    # Funtion to retrieve locked pixelmap collections
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_pixelmap_collection_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                collection_id uuid[]
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    ARRAY_AGG(v3cc.collection_id) AS ids
                FROM
                    v3_j_job_collections AS v3cc
                WHERE
                    v3cc.job_id = ANY(_jobs)
                    AND v3cc.item_type = ANY(ARRAY['continuous_pixelmap', 'discrete_pixelmap', 'nominal_pixelmap']);
            END $func$;
            """
        )

    # Function to retrieve directly locked pixelmaps
    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_locked_pixelmap_ids(
                _jobs TEXT[]
            )
            RETURNS TABLE (
                pixelmap_id uuid
            )
            LANGUAGE plpgsql
            STABLE
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    v3jjs.pixelmap_id
                FROM
                    v3_j_job_pixelmaps AS v3jjs
                WHERE
                    v3jjs.job_id = ANY(_jobs);
            END $func$;
            """
        )

    # Procedure to create materialized view for specific job id for pixelmaps
    async with conn.transaction():
        await conn.execute(
            """
            DROP PROCEDURE IF EXISTS create_pixelmap_view;

            CREATE OR REPLACE PROCEDURE create_pixelmap_view(
                _job_id TEXT,
                _view_name TEXT
            )
            LANGUAGE plpgsql
            AS $$
            BEGIN
                execute FORMAT('
                DROP INDEX IF EXISTS %1$s_id;
                DROP INDEX IF EXISTS %1$s_cr;
                DROP INDEX IF EXISTS %1$s_ref;
                DROP INDEX IF EXISTS %1$s_type;
                DROP INDEX IF EXISTS %1$s_colids;
                DROP MATERIALIZED VIEW IF EXISTS %1$s;
                CREATE MATERIALIZED VIEW %1$s AS (
                    WITH pixelmaps AS (
                        SELECT
                            *
                        FROM
                            v3_pm_pixelmaps
                        WHERE
                            collection_ids && (
                                SELECT
                                    *
                                FROM
                                    get_locked_pixelmap_collection_ids(ARRAY[%2$L]::TEXT[])
                            )
                            OR id IN (
                                SELECT
                                    *
                                FROM
                                    get_locked_pixelmap_ids(ARRAY[%2$L]::TEXT[])
                            )
                    )
                    SELECT
                        *
                    FROM
                        pixelmaps
                );
                CREATE INDEX %1$s_id ON %1$s USING BTREE (id);
                CREATE INDEX %1$s_cr ON %1$s USING BTREE (creator_id);
                CREATE INDEX %1$s_ref ON %1$s USING BTREE (reference_id);
                CREATE INDEX %1$s_type ON %1$s USING BTREE (type);
                CREATE INDEX %1$s_colids ON %1$s USING GIN (collection_ids);'
                , _view_name, _job_id);
            END;
            $$
            """
        )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
