#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################

from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    # Add extension to create uuid values
    try:
        async with conn.transaction():
            await conn.execute(
                """
                CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
                """
            )
    except UniqueViolationError as e:
        print("WARNING:", e)

    # annotations
    await conn.execute(
        """
        CREATE TABLE a_annotations (
            id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
            type text NOT NULL,
            name text,
            description text,
            creator_id text NOT NULL,
            creator_type text NOT NULL,
            reference_id text NOT NULL,
            reference_type text NOT NULL,
            resolution float,
            is_locked bool,
            created_at timestamp NOT NULL,
            updated_at timestamp NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE a_point_annotations (
            id serial PRIMARY KEY,
            annot_id uuid REFERENCES a_annotations(id) ON DELETE CASCADE,
            x integer NOT NULL,
            y integer NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE a_line_annotations (
            id serial PRIMARY KEY,
            annot_id uuid REFERENCES a_annotations(id) ON DELETE CASCADE,
            x_one integer NOT NULL,
            y_one integer NOT NULL,
            x_two integer NOT NULL,
            y_two integer NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE a_arrow_annotations (
            id serial PRIMARY KEY,
            annot_id uuid REFERENCES a_annotations(id) ON DELETE CASCADE,
            head_x integer NOT NULL,
            head_y integer NOT NULL,
            tail_x integer NOT NULL,
            tail_y integer NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE a_circle_annotations (
            id serial PRIMARY KEY,
            annot_id uuid REFERENCES a_annotations(id) ON DELETE CASCADE,
            x integer NOT NULL,
            y integer NOT NULL,
            radius float NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE a_rectangle_annotations (
            id serial PRIMARY KEY,
            annot_id uuid REFERENCES a_annotations(id) ON DELETE CASCADE,
            x integer NOT NULL,
            y integer NOT NULL,
            width integer NOT NULL,
            height integer NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE a_polygon_annotations (
            id serial PRIMARY KEY,
            annot_id uuid REFERENCES a_annotations(id) ON DELETE CASCADE
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE a_polygon_points (
            id serial PRIMARY KEY,
            x integer NOT NULL,
            y integer NOT NULL,
            annot_id uuid REFERENCES a_annotations(id) ON DELETE CASCADE
        );
        """
    )

    # classes
    await conn.execute(
        """
        CREATE TABLE cl_classes (
            id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
            creator_id text NOT NULL,
            creator_type text NOT NULL,
            reference_id uuid NOT NULL,
            reference_type text NOT NULL,
            value text NOT NULL,
            is_locked bool NOT NULL
        );
        """
    )

    # primitives
    await conn.execute(
        """
        CREATE TABLE p_primitives (
            id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
            type text NOT NULL,
            name text NOT NULL,
            description text,
            creator_id text NOT NULL,
            creator_type text NOT NULL,
            reference_id text NOT NULL,
            reference_type text NOT NULL,
            is_locked bool NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE p_integer_primitives (
            id serial PRIMARY KEY,
            primitive_id uuid REFERENCES p_primitives(id) ON DELETE CASCADE,
            value integer NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE p_float_primitives (
            id serial PRIMARY KEY,
            primitive_id uuid REFERENCES p_primitives(id) ON DELETE CASCADE,
            value float NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE p_bool_primitives (
            id serial PRIMARY KEY,
            primitive_id uuid REFERENCES p_primitives(id) ON DELETE CASCADE,
            value bool NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE p_string_primitives (
            id serial PRIMARY KEY,
            primitive_id uuid REFERENCES p_primitives(id) ON DELETE CASCADE,
            value text NOT NULL
        );
        """
    )

    # collections
    await conn.execute(
        """
        CREATE TABLE c_collections (
            id uuid DEFAULT uuid_generate_v4 () PRIMARY KEY,
            name text,
            description text,
            creator_id text NOT NULL,
            creator_type text NOT NULL,
            reference_id text NOT NULL,
            reference_type text NOT NULL,
            item_type text NOT NULL,
            is_locked bool NOT NULL
        );
        """
    )

    await conn.execute(
        """
        CREATE TABLE c_collection_items (
            collection_id uuid REFERENCES c_collections(id) ON DELETE CASCADE,
            item_id text,
            PRIMARY KEY (collection_id, item_id)
        );
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
