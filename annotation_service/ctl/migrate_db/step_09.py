#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_09(conn):
    # Add indexes
    await conn.execute(
        """
        CREATE INDEX a_annotations_updated ON public.a_annotations USING btree (updated_at);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
