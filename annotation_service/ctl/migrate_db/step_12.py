#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_12(conn):
    # add item_type to collection locking table
    await conn.execute(
        """
        ALTER TABLE v3_j_job_collections ADD COLUMN IF NOT EXISTS item_type TEXT;
        """
    )

    # add index
    await conn.execute(
        """
        CREATE INDEX IF NOT EXISTS v3_j_job_collections_type ON public.v3_j_job_collections USING btree (item_type);
        """
    )

    # update collection locking table with item_types
    await conn.execute(
        """
        UPDATE
            v3_j_job_collections
        SET
            item_type = subquery.item_type
        FROM
            (
                SELECT
                    v3_c.id AS id,
                    v3_c.item_type
                FROM
                    v3_c_collections AS v3_c
            ) AS subquery
        WHERE
            v3_j_job_collections.collection_id = subquery.id;
        """
    )

    await conn.execute(
        """
        ALTER TABLE v3_j_job_collections ALTER COLUMN item_type SET NOT NULL;
        """
    )

    # ANNOTATIONS

    # create function to fetch ids of locked annotations for given job ids
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_locked_annotation_ids(
            _jobs TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                item_ids.*
            FROM
                (
                    (
                        SELECT
                            v3jjcl.annot_id
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.job_id = ANY(_jobs)
                    )
                    UNION
                    (
                        SELECT
                            v3cci.item_id :: uuid
                        FROM
                            v3_c_collection_items AS v3cci
                        WHERE
                            v3cci.collection_id IN (
                                SELECT
                                    v3cc.collection_id
                                FROM
                                    v3_j_job_collections AS v3cc
                                WHERE
                                    v3cc.job_id = ANY(_jobs)
                                    AND v3cc.item_type = ANY(
                                        ARRAY['point', 'line', 'arrow', 'circle', 'rectangle', 'polygon']
                                    )
                            )
                    )
                ) AS item_ids;
        END $func$;
        """
    )

    # create function to fetch ids of annotations for given job ids and class values
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_annotation_ids_for_jobs_classes(
            _jobs TEXT[],
            _class_values TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            WITH annotation_ids AS (
                SELECT
                    v3aa.id
                FROM
                    v3_a_annotations AS v3aa
                WHERE (
                    _jobs IS NULL
                    OR v3aa.id IN (
                        SELECT
                            *
                        FROM
                            get_locked_annotation_ids(_jobs)
                    )
                )
            ),
            class_reference_ids AS (
                SELECT
                    v3cl.reference_id
                FROM
                    v3_cl_classes AS v3cl
                WHERE
                    (
                        _jobs IS NULL
                        OR v3cl.id IN (
                            SELECT
                                *
                            FROM
                                get_locked_class_ids(_jobs)
                        )
                    )
                    AND (
                        _class_values IS NULL
                        OR v3cl.value = ANY(_class_values)
                    )
            ),
            annotation_ids_without_class_values AS (
                SELECT
                    *
                FROM
                    annotation_ids
            ),
            annotation_ids_with_class_values AS (
                SELECT
                    annotation_ids.*
                FROM
                    annotation_ids
                    JOIN class_reference_ids ON	annotation_ids.id = class_reference_ids.reference_id
            )
            SELECT
                *
            FROM
                annotation_ids_with_class_values
            WHERE
                (_class_values IS NOT NULL) = TRUE
            UNION ALL
            SELECT
                *
            FROM
                annotation_ids_without_class_values
            WHERE
                (_class_values IS NOT NULL) <> TRUE;
        END $func$;
        """
    )

    # create function to fetch ids of queried annotations
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_annotation_ids(
            _references TEXT[],
            _creators TEXT[],
            _types TEXT[],
            _ids uuid[],
            _jobs TEXT[],
            _viewport BOX,
            _class_values TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                v3aa.id
            FROM
                v3_a_annotations AS v3aa
            WHERE
                (
                    _references IS NULL
                    OR v3aa.reference_id = ANY(_references)
                )
                AND (
                    _creators IS NULL
                    OR v3aa.creator_id = ANY(_creators)
                )
                AND (
                    _types IS NULL
                    OR v3aa.type = ANY(_types)
                )
                AND (
                    _ids IS NULL
                    OR v3aa.id = ANY(_ids)
                )
                AND (
                    _viewport IS NULL
                    OR v3aa.bounding_box ?# _viewport
                )
                AND v3aa.id IN (
                    SELECT
                        *
                    FROM
                        get_annotation_ids_for_jobs_classes(
                            _jobs,
                            _class_values
                        )
                );
        END $func$;
        """
    )

    # CLASSES

    # create function to fetch ids of locked classes for given job ids
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_locked_class_ids(
            _jobs TEXT []
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                item_ids.*
            FROM
                (
                    (
                        SELECT
                            v3jjcl.class_id :: uuid
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.job_id = ANY(_jobs) AND
                            v3jjcl.class_id != ''
                    )
                    UNION
                    (
                        SELECT
                            v3cci.item_id :: uuid
                        FROM
                            v3_c_collection_items AS v3cci
                        WHERE
                            v3cci.collection_id IN (
                                SELECT
                                    v3cc.collection_id
                                FROM
                                    v3_j_job_collections AS v3cc
                                WHERE
                                    v3cc.job_id = ANY(_jobs)
                                    AND v3cc.item_type = 'class'
                            )
                    )
                ) AS item_ids;
        END $func$;
        """
    )

    # create function to fetch ids of queried classes
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_class_ids(
            _references uuid[],
            _creators TEXT[],
            _ids uuid[],
            _jobs TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                v3cl.id
            FROM
                v3_cl_classes AS v3cl
            WHERE
                (
                    _references IS NULL
                    OR v3cl.reference_id = ANY(_references)
                )
                AND (
                    _creators IS NULL
                    OR v3cl.creator_id = ANY(_creators)
                )
                AND (
                    _ids IS NULL
                    OR v3cl.id = ANY(_ids)
                )
                AND (
                    _jobs IS NULL
                    OR v3cl.id IN (
                        SELECT
                            *
                        FROM
                            get_locked_class_ids(_jobs)
                    )
                );
        END
        $func$;
        """
    )

    # PRIMITIVES

    # create function to fetch ids of locked primitives jor given job ids
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_locked_primitive_ids(
            _jobs TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                item_ids.*
            FROM
                (
                    (
                        SELECT
                            v3jjp.primitive_id
                        FROM
                            v3_j_job_primitives AS v3jjp
                        WHERE
                            v3jjp.job_id = ANY(_jobs)
                    )
                    UNION
                    (
                        SELECT
                            v3cci.item_id :: uuid
                        FROM
                            v3_c_collection_items AS v3cci
                        WHERE
                            v3cci.collection_id IN (
                                SELECT
                                    v3cc.collection_id
                                FROM
                                    v3_j_job_collections AS v3cc
                                WHERE
                                    v3cc.job_id = ANY(_jobs)
                                    AND v3cc.item_type = ANY(ARRAY['integer', 'float', 'bool', 'string'])
                            )
                    )
                ) AS item_ids;
        END $func$;
        """
    )

    # create function to fetch ids of queried primitives
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_primitive_ids(
            _references TEXT[],
            _null_references BOOLEAN,
            _creators TEXT[],
            _types TEXT[],
            _ids uuid[],
            _jobs TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                v3pp.id
            FROM
                v3_p_primitives AS v3pp
            WHERE
                (
                    _references IS NULL
                    OR v3pp.reference_id = ANY(_references)
                    OR (
                        CASE
                            WHEN _null_references IS TRUE THEN v3pp.reference_id IS NULL
                        END
                    )
                )
                AND (
                    _creators IS NULL
                    OR v3pp.creator_id = ANY(_creators)
                )
                AND (
                    _types IS NULL
                    OR v3pp.type = ANY(_types)
                )
                AND (
                    _ids IS NULL
                    OR v3pp.id = ANY(_ids)
                )
                AND (
                    _jobs IS NULL
                    OR v3pp.id IN (
                        SELECT
                            *
                        FROM
                            get_locked_primitive_ids(_jobs)
                    )
                );
        END
        $func$;
        """
    )

    # COLLECTIONS

    # create function to fetch ids of locked collections jor given job ids
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_locked_collection_ids(
            _jobs TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                ids.*
            FROM
                (
                    SELECT
                        v3jjc.collection_id
                    FROM
                        v3_j_job_collections AS v3jjc
                    WHERE
                        v3jjc.job_id = ANY(_jobs)
                ) AS ids;
        END $func$;
        """
    )

    # create function to fetch ids of queried collections
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION get_collection_ids(
            _references TEXT[],
            _creators TEXT[],
            _item_types TEXT[],
            _ids uuid[],
            _jobs TEXT[]
        )
        RETURNS TABLE (
            item_id uuid
        )
        LANGUAGE plpgsql
        PARALLEL SAFE
        AS $func$
        BEGIN
            RETURN QUERY
            SELECT
                v3cc.id
            FROM
                v3_c_collections AS v3cc
            WHERE
                (
                    _references IS NULL
                    OR v3cc.reference_id = ANY(_references)
                )
                AND (
                    _creators IS NULL
                    OR v3cc.creator_id = ANY(_creators)
                )
                AND (
                    _item_types IS NULL
                    OR v3cc.item_type = ANY(_item_types)
                )
                AND (
                    _ids IS NULL
                    OR v3cc.id = ANY(_ids)
                )
                AND (
                    _jobs IS NULL
                    OR v3cc.id IN (
                        SELECT
                            *
                        FROM
                            get_locked_collection_ids(_jobs)
                    )
                );
        END
        $func$;
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
