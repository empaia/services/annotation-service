#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_06(conn):
    # add job_slides table
    await conn.execute(
        """
        CREATE TABLE j_job_slides (
            job_id text,
            slide_id text,
            PRIMARY KEY (job_id, slide_id)
        );
        """
    )

    # add indexer
    await conn.execute(
        """
        CREATE INDEX j_job_slides_job ON j_job_slides USING btree (job_id);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
