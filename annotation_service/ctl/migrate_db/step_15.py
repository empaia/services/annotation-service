from logging import Logger
from pathlib import Path

from annotation_service.api.v3.db import db_clients
from annotation_service.ctl.pixelmapd import Settings
from annotation_service.pixelmapd_manager.pixelmapd_manager import PixelmapdManager
from annotation_service.settings import Settings as AnnotSettings


async def step_15(conn):
    annot_settings = AnnotSettings()
    settings = Settings()

    annot_data_path = annot_settings.pixelmaps_data_path
    data_path = settings.data_path

    assert annot_data_path
    assert data_path

    annot_data_path = Path(annot_data_path)
    data_path = Path(data_path)

    logger = Logger("migration_step_15")
    pixelmapd_manager = PixelmapdManager(settings=annot_settings, logger=logger)
    await pixelmapd_manager.initialize_connection()
    clients = await db_clients(conn=conn)
    client = clients.pixelmap

    for path in annot_data_path.iterdir():
        if not path.is_dir():
            continue

        if path.name == "hdf5":
            continue

        pixelmap_id = path.name
        pixelmap = await client.get(pixelmap_id=pixelmap_id)

        for level_path in path.iterdir():
            if not level_path.is_dir():
                continue

            level = int(level_path.name)

            for tile_path in level_path.iterdir():
                if not tile_path.is_file():
                    continue

                x, y = tile_path.name.split("_")
                x, y = int(x), int(y)

                with open(tile_path, "rb") as f:
                    req_bytes = f.read()

                await pixelmapd_manager.create_tile(
                    case_id=None,
                    pixelmap=pixelmap,
                    level=level,
                    x=x,
                    y=y,
                    content_encoding=None,
                    req_bytes=req_bytes,
                )
