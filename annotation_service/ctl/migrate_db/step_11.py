#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_11(conn):
    async with conn.transaction():
        # Drop foreign key constaints in locking tables
        await conn.execute(
            """
            ALTER TABLE v3_j_job_classes DROP CONSTRAINT v3_j_job_classes_annot_id_fkey;
            ALTER TABLE v3_j_job_collections DROP CONSTRAINT v3_j_job_collections_collection_id_fkey;
            ALTER TABLE v3_j_job_primitives DROP CONSTRAINT v3_j_job_primitives_primitive_id_fkey;
            """
        )

        # Create missing indexes for locking tables for faster queries
        await conn.execute(
            """
            CREATE INDEX IF NOT EXISTS v3_j_job_classes_cid ON v3_j_job_classes USING btree (class_id);
            CREATE INDEX IF NOT EXISTS v3_j_job_collections_colid ON v3_j_job_collections USING btree (collection_id);
            CREATE INDEX IF NOT EXISTS v3_j_job_primitives_pid ON v3_j_job_primitives USING btree (primitive_id);
            CREATE INDEX IF NOT EXISTS v3_j_job_slides_sid ON v3_j_job_slides USING btree (slide_id);
            """
        )

        # Drop annotation locking table (not used any longer)
        await conn.execute(
            """
            DROP INDEX IF EXISTS v3_j_job_annotations_job;
            DROP TABLE IF EXISTS v3_j_job_annotations;
            """
        )

        # Create v3 job_id look up function (used in following function v3_clean_up_class_locking_table)
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION get_rows_for_job_id(
                _job_id TEXT
            )
            RETURNS TABLE (
                job_id TEXT,
                class_id TEXT,
                annot_id uuid,
                value TEXT
            )
            LANGUAGE plpgsql
            PARALLEL SAFE
            AS $func$
            BEGIN
                RETURN QUERY
                SELECT
                    *
                FROM
                    v3_j_job_classes AS v3jjcl
                WHERE
                    v3jjcl.job_id = _job_id;
            END
            $func$;
            """
        )

        # Create v3 class lock table clean up function (called after locking annotations or classes)
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION v3_clean_up_class_locking_table(
                _job_id TEXT
            )
            RETURNS void
            LANGUAGE plpgsql
            AS $func$
            BEGIN
                WITH ids AS (
                    SELECT
                        array_agg(annot_id) AS annot_ids
                    FROM
                        (
                            SELECT
                                annot_id,
                                job_id,
                                array_agg(VALUE) AS vals,
                                array_agg(class_id) AS cl_ids
                            FROM
                                get_rows_for_job_id(_job_id)
                            GROUP BY
                                annot_id,
                                job_id
                            HAVING
                                count(annot_id) > 1
                        ) AS tmp
                    WHERE
                        '' = ANY (tmp.vals)
                        AND '' = ANY (tmp.cl_ids)
                )
                DELETE FROM
                    v3_j_job_classes
                WHERE
                    annot_id = ANY (
                        SELECT
                            unnest(annot_ids)
                        FROM
                            ids
                    )
                    AND job_id = _job_id
                    AND VALUE = ''
                    AND class_id = '';
            END
            $func$;
            """
        )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
