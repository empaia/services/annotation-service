#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_16(conn):
    # Add pixelmap table
    await conn.execute(
        """
        CREATE TABLE IF NOT EXISTS public.v3_pm_info (
            id uuid NOT NULL,
            info jsonb,
            CONSTRAINT v3_pm_info_pkey PRIMARY KEY (id)
        ) TABLESPACE pg_default;
        """
    )
