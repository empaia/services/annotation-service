#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_07(conn):
    # add type column to table cl_classes
    await conn.execute(
        """
        ALTER TABLE cl_classes ADD COLUMN IF NOT EXISTS type text;
        """
    )

    # Set value of column type in cl_classes
    await conn.execute(
        """
        UPDATE cl_classes
        SET type='class'
        WHERE type IS NULL;
        """
    )

    # set type not null
    await conn.execute(
        """
        ALTER TABLE cl_classes ALTER COLUMN type SET NOT NULL;
        """
    )

    # add type column to table c_collections
    await conn.execute(
        """
        ALTER TABLE c_collections ADD COLUMN IF NOT EXISTS type text;
        """
    )

    # Set value of column type in c_collections
    await conn.execute(
        """
        UPDATE c_collections
        SET type='collection'
        WHERE type IS NULL;
        """
    )

    # set type not null
    await conn.execute(
        """
        ALTER TABLE c_collections ALTER COLUMN type SET NOT NULL;
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
