#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_04(conn):
    # ALTER TABLE a_annotations
    # create column for bounding box
    await conn.execute(
        """
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS bounding_box box;
        """
    )

    # set bounding box for point annotations
    await conn.execute(
        """
        UPDATE a_annotations
        SET bounding_box=Box((coordinates[1], coordinates[2], coordinates[1], coordinates[2])::text)
        WHERE type='point';
        """
    )

    # set bounding box for line annotations
    await conn.execute(
        """
        UPDATE a_annotations
        SET bounding_box=Box((coordinates[1][1], coordinates[1][2], coordinates[2][1], coordinates[2][2])::text)
        WHERE type='line';
        """
    )

    # set bounding box for arrow annotations
    await conn.execute(
        """
        UPDATE a_annotations
        SET bounding_box=Box((head[1], head[2], tail[1], tail[2])::text)
        WHERE type='arrow';
        """
    )

    # set bounding box for circle annotations
    await conn.execute(
        """
        UPDATE a_annotations
        SET bounding_box=Box((center[1] - radius, center[2] - radius, center[1] + radius, center[2] + radius)::text)
        WHERE type='circle';
        """
    )

    # set bounding box for rectangle annotations
    await conn.execute(
        """
        UPDATE a_annotations
        SET bounding_box=Box((upper_left[1], upper_left[2], upper_left[1] + width, upper_left[2] + height)::text)
        WHERE type='rectangle';
        """
    )

    # set bounding box for polygon annotations
    await conn.execute(
        """
        UPDATE a_annotations
        SET bounding_box=Box(
            Polygon(
                array_to_string(
                    ARRAY
                    (
                        SELECT UNNEST(t.c::int[])
                        FROM
                        (
                            SELECT coordinates::text AS c
                        ) AS t
                    ),','
                )
            )
        )
        WHERE type='polygon';
        """
    )

    # drop centroid coordinates contraints
    await conn.execute(
        """
        ALTER TABLE a_annotations ALTER COLUMN centroid_x DROP NOT NULL;
        ALTER TABLE a_annotations ALTER COLUMN centroid_y DROP NOT NULL;
        """
    )

    # drop centroid coordinates indexer
    await conn.execute(
        """
        DROP INDEX a_annotations_cx;
        DROP INDEX a_annotations_cy;
        """
    )

    # delete centroid coordinates columns
    await conn.execute(
        """
        ALTER TABLE a_annotations DROP COLUMN IF EXISTS centroid_x;
        ALTER TABLE a_annotations DROP COLUMN IF EXISTS centroid_y;
        """
    )

    # set bounding box not null
    await conn.execute(
        """
        ALTER TABLE a_annotations ALTER COLUMN bounding_box SET NOT NULL;
        """
    )

    # create index on bounding box column
    await conn.execute(
        """
        CREATE INDEX a_annotations_bb ON a_annotations USING GIST (bounding_box);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
