#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_03(conn):
    # ALTER TABLE a_annotations
    # add column coordinates integer[]
    # add column head integer[]
    # add column tail integer[]
    # add column center integer[]
    # add column radius integer
    # add column upper_left integer[]
    # add column width integer
    # add column height integer
    # add column centroid integer[]
    # add column centroid_x integer
    # add column centroid_y integer
    await conn.execute(
        """
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS coordinates integer[];
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS head integer[];
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS tail integer[];
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS center integer[];
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS radius integer;
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS upper_left integer[];
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS width integer;
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS height integer;
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS centroid integer[];
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS centroid_x integer;
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS centroid_y integer;
        """
    )

    # Migration of annotation type tables
    # POINT
    await conn.execute(
        """
        UPDATE a_annotations
        SET coordinates=subquery.coordinates,
            centroid=subquery.centroid,
            centroid_x=subquery.centroid_x,
            centroid_y=subquery.centroid_y
        FROM
        (
            SELECT
            a.id AS id,
            ARRAY[pa.x, pa.y] AS coordinates,
            ARRAY[pa.x, pa.y] AS centroid,
            pa.x AS centroid_x,
            pa.y AS centroid_y
            FROM a_annotations AS a
            JOIN a_point_annotations AS pa
            ON pa.annot_id=a.id
        ) AS subquery
        WHERE a_annotations.id=subquery.id;
        """
    )

    # LINE
    await conn.execute(
        """
        UPDATE a_annotations
        SET coordinates=subquery.coordinates,
            centroid=subquery.centroid,
            centroid_x=subquery.centroid_x,
            centroid_y=subquery.centroid_y
        FROM
        (
            SELECT
            a.id AS id,
            ARRAY[[la.x_one, la.y_one],[la.x_two, la.y_two]] AS coordinates,
            ARRAY[(la.x_one+la.x_two)/2::int, (la.y_one+la.y_two)/2::int] AS centroid,
            (la.x_one+la.x_two)/2::int AS centroid_x,
            (la.y_one+la.y_two)/2::int AS centroid_y
            FROM a_annotations AS a
            JOIN a_line_annotations AS la
            ON la.annot_id=a.id
        ) AS subquery
        WHERE a_annotations.id=subquery.id;
        """
    )

    # ARROW
    await conn.execute(
        """
        UPDATE a_annotations
        SET head=subquery.head,
            tail=subquery.tail,
            centroid=subquery.centroid,
            centroid_x=subquery.centroid_x,
            centroid_y=subquery.centroid_y
        FROM
        (
            SELECT
            a.id AS id,
            ARRAY[aa.head_x, aa.head_y] AS head,
            ARRAY[aa.tail_x, aa.tail_y] AS tail,
            ARRAY[(aa.head_x+aa.tail_x)/2::int, (aa.head_y+aa.tail_y)/2::int] AS centroid,
            (aa.head_x+aa.tail_x)/2::int AS centroid_x,
            (aa.head_y+aa.tail_y)/2::int AS centroid_y
            FROM a_annotations AS a
            JOIN a_arrow_annotations AS aa
            ON aa.annot_id=a.id
        ) AS subquery
        WHERE a_annotations.id=subquery.id;
        """
    )

    # CIRCLE
    await conn.execute(
        """
        UPDATE a_annotations
        SET center=subquery.center,
            radius=subquery.radius,
            centroid=subquery.centroid,
            centroid_x=subquery.centroid_x,
            centroid_y=subquery.centroid_y
        FROM
        (
            SELECT
            a.id AS id,
            ARRAY[ca.x, ca.y] AS center,
            ca.radius AS radius,
            ARRAY[ca.x, ca.y] AS centroid,
            ca.x AS centroid_x,
            ca.y AS centroid_y
            FROM a_annotations AS a
            JOIN a_circle_annotations AS ca
            ON ca.annot_id=a.id
        ) AS subquery
        WHERE a_annotations.id=subquery.id;
        """
    )

    # RECTANGLE
    await conn.execute(
        """
        UPDATE a_annotations
        SET upper_left=subquery.upper_left,
            width=subquery.width,
            height=subquery.height,
            centroid=subquery.centroid,
            centroid_x=subquery.centroid_x,
            centroid_y=subquery.centroid_y
        FROM
        (
            SELECT
            a.id AS id,
            ARRAY[ra.x, ra.y] AS upper_left,
            ra.width AS width,
            ra.height AS height,
            ARRAY[(ra.x*2+ra.width)/2::int, (ra.y*2+ra.height)/2::int] AS centroid,
            (ra.x*2+ra.width)/2::int AS centroid_x,
            (ra.y*2+ra.height)/2::int AS centroid_y
            FROM a_annotations AS a
            JOIN a_rectangle_annotations AS ra
            ON ra.annot_id=a.id
        ) AS subquery
        WHERE a_annotations.id=subquery.id;
        """
    )

    # POLYGON --> before migration: delete last point if last = first
    await conn.execute(
        """
        DELETE FROM a_polygon_points a USING
        (
            SELECT MIN(id) as min_id, MAX(id) as max_id, x, y, annot_id
            FROM a_polygon_points
            GROUP BY x,y,annot_id HAVING COUNT(*) > 1
        ) b
        WHERE a.x=b.x AND a.y=b.y AND a.id <> b.min_id AND a.id = b.max_id
        """
    )

    await conn.execute(
        """
        UPDATE a_annotations
        SET coordinates=subquery.coordinates,
            centroid=subquery.centroid,
            centroid_x=subquery.centroid_x,
            centroid_y=subquery.centroid_y
        FROM
        (
            SELECT
            a.id AS id,
            (
                SELECT array_agg(tmp.c)
                FROM
                (
                    SELECT annot_id, array[x, y] AS c
                    FROM a_polygon_points
                    ORDER BY id
                ) AS tmp
                WHERE tmp.annot_id=a.id
                GROUP BY tmp.annot_id
            ) AS coordinates,
            ARRAY[AVG(pp.x)::int, AVG(pp.y)::int] AS centroid,
            AVG(pp.x)::int AS centroid_x,
            AVG(pp.y)::int AS centroid_y
            FROM a_annotations AS a
            JOIN a_polygon_points AS pp
            ON pp.annot_id=a.id
            GROUP BY a.id
        ) AS subquery
        WHERE a_annotations.id=subquery.id;
        """
    )

    # add NOT NULL contraint for centroid
    await conn.execute(
        """
        ALTER TABLE a_annotations ALTER COLUMN centroid SET NOT NULL;
        ALTER TABLE a_annotations ALTER COLUMN centroid_x SET NOT NULL;
        ALTER TABLE a_annotations ALTER COLUMN centroid_y SET NOT NULL;
        """
    )

    # add new indexer for centroid
    await conn.execute(
        """
        CREATE INDEX a_annotations_cx ON a_annotations USING btree (centroid_x);
        CREATE INDEX a_annotations_cy ON a_annotations USING btree (centroid_y);
        """
    )

    # DROP annotation type tables and indexer
    # INDEXER
    await conn.execute(
        """
        DROP INDEX a_arrow_annotations_annot;
        DROP INDEX a_circle_annotations_annot;
        DROP INDEX a_line_annotations_annot;
        DROP INDEX a_polygon_points_annot;
        DROP INDEX a_point_annotations_annot;
        DROP INDEX a_rectangle_annotations_annot;
        """
    )

    # TABLES
    await conn.execute(
        """
        DROP TABLE a_arrow_annotations;
        DROP TABLE a_circle_annotations;
        DROP TABLE a_line_annotations;
        DROP TABLE a_polygon_points;
        DROP TABLE a_point_annotations;
        DROP TABLE a_rectangle_annotations;
        """
    )

    # extend table j_job_classes
    await conn.execute(
        """
        ALTER TABLE j_job_classes ADD COLUMN IF NOT EXISTS annot_id uuid;
        ALTER TABLE j_job_classes ADD COLUMN IF NOT EXISTS value text;
        """
    )

    # update of j_job_classes table
    await conn.execute(
        """
        UPDATE j_job_classes
        SET annot_id=subquery.annot_id,
            value=subquery.value
        FROM
        (
            SELECT id, reference_id AS annot_id, value
            FROM cl_classes
        ) AS subquery
        WHERE j_job_classes.class_id=subquery.id;
        """
    )

    # change constraints
    await conn.execute(
        """
        ALTER TABLE j_job_classes ADD FOREIGN KEY (annot_id) REFERENCES a_annotations(id);
        ALTER TABLE j_job_classes ALTER COLUMN annot_id SET NOT NULL;
        ALTER TABLE j_job_classes ALTER COLUMN value SET NOT NULL;
        ALTER TABLE j_job_classes DROP CONSTRAINT j_job_classes_pkey;
        ALTER TABLE j_job_classes ALTER COLUMN class_id DROP NOT NULL;
        ALTER TABLE j_job_classes DROP CONSTRAINT j_job_classes_class_id_fkey;
        ALTER TABLE j_job_classes ALTER COLUMN class_id TYPE text;
        ALTER TABLE j_job_classes ADD PRIMARY KEY (job_id, annot_id, class_id);
        """
    )

    # migrate job annotation data
    await conn.execute(
        """
        INSERT INTO j_job_classes (job_id, class_id, annot_id, value)
        (
            SELECT jja.job_id, '' AS class_id, jja.annotation_id, '' AS value
            FROM j_job_annotations AS jja
            LEFT JOIN j_job_classes AS jjc
            ON jja.annotation_id=jjc.annot_id AND jja.job_id=jjc.job_id
            WHERE jjc.class_id IS NULL
        )
        """
    )

    # add new indexer for annot_id and value
    await conn.execute(
        """
        CREATE INDEX j_job_classes_aid ON j_job_classes USING btree (annot_id);
        CREATE INDEX j_job_classes_val ON j_job_classes USING btree (value);
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
