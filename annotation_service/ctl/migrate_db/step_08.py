#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################


async def step_08(conn):
    # add npp_viewing_min_query, npp_viewing_max_query
    await conn.execute(
        """
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS npp_viewing_min_query float;
        ALTER TABLE a_annotations ADD COLUMN IF NOT EXISTS npp_viewing_max_query float;
        """
    )

    await conn.execute(
        """
        UPDATE a_annotations
        SET npp_viewing_min_query = npp_created,
            npp_viewing_max_query = npp_created
        WHERE npp_viewing_min IS NULL AND npp_viewing_max IS NULL;

        UPDATE a_annotations
        SET npp_viewing_min_query = npp_viewing_min,
            npp_viewing_max_query = npp_viewing_max
        WHERE npp_viewing_min IS NOT NULL AND npp_viewing_max IS NOT NULL;

        ALTER TABLE a_annotations ALTER COLUMN npp_viewing_min_query SET NOT NULL;
        ALTER TABLE a_annotations ALTER COLUMN npp_viewing_max_query SET NOT NULL;
        """
    )

    # Add indexes
    await conn.execute(
        """
        CREATE INDEX a_annotations_npp_min_q ON a_annotations USING btree (npp_viewing_min_query);
        CREATE INDEX a_annotations_npp_max_q ON a_annotations USING btree (npp_viewing_max_query);
        """
    )

    # Add custom exception function
    await conn.execute(
        """
        CREATE OR REPLACE FUNCTION raise_exception(anyelement, text, text) RETURNS anyelement AS $$
        BEGIN
            RAISE EXCEPTION '%',$2 USING ERRCODE = $3;
            RETURN $1;
        END;
        $$ LANGUAGE plpgsql VOLATILE;
        """
    )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
