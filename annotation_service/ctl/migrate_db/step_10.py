#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################

from asyncpg.exceptions import UniqueViolationError


async def step_10(conn):
    # Add extension and functions
    # may exist, if steps 01 - 09 were executed

    # Add extension "pgcrypto" to create uuid type 4
    try:
        async with conn.transaction():
            await conn.execute(
                """
                CREATE EXTENSION IF NOT EXISTS "pgcrypto";
                """
            )
    except UniqueViolationError as e:
        print("WARNING:", e)

    # Add error function

    async with conn.transaction():
        await conn.execute(
            """
            CREATE OR REPLACE FUNCTION raise_exception(anyelement, TEXT, TEXT) RETURNS anyelement AS $$
            BEGIN
                RAISE EXCEPTION '%',$2 USING ERRCODE = $3;
                RETURN $1;
            END;
            $$ LANGUAGE plpgsql VOLATILE;
            """
        )

    # Create v3 tables

    # Annotations v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_a_annotations (
                id uuid NOT NULL DEFAULT gen_random_uuid(),
                TYPE TEXT COLLATE pg_catalog."default" NOT NULL,
                NAME TEXT COLLATE pg_catalog."default",
                description TEXT COLLATE pg_catalog."default",
                creator_id TEXT COLLATE pg_catalog."default" NOT NULL,
                creator_type TEXT COLLATE pg_catalog."default" NOT NULL,
                reference_id TEXT COLLATE pg_catalog."default" NOT NULL,
                reference_type TEXT COLLATE pg_catalog."default" NOT NULL,
                npp_created DOUBLE PRECISION NOT NULL,
                is_locked BOOLEAN,
                created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
                updated_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
                npp_viewing_min DOUBLE PRECISION,
                npp_viewing_max DOUBLE PRECISION,
                coordinates INTEGER [ ],
                head INTEGER [ ],
                tail INTEGER [ ],
                center INTEGER [ ],
                radius INTEGER,
                upper_left INTEGER [ ],
                width INTEGER,
                height INTEGER,
                centroid INTEGER [ ] NOT NULL,
                bounding_box box NOT NULL,
                npp_viewing_min_query DOUBLE PRECISION NOT NULL,
                npp_viewing_max_query DOUBLE PRECISION NOT NULL,
                CONSTRAINT v3_a_annotations_pkey PRIMARY KEY (id)
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_bb ON public.v3_a_annotations USING gist (
                bounding_box
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_creator ON public.v3_a_annotations USING btree (
                creator_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_npp_created ON public.v3_a_annotations USING btree (
                npp_created ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_npp_max ON public.v3_a_annotations USING btree (
                npp_viewing_max ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_npp_max_q ON public.v3_a_annotations USING btree (
                npp_viewing_max_query ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_npp_min ON public.v3_a_annotations USING btree (
                npp_viewing_min ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_npp_min_q ON public.v3_a_annotations USING btree (
                npp_viewing_min_query ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_reference ON public.v3_a_annotations USING btree (
                reference_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_type ON public.v3_a_annotations USING btree (
                TYPE COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_a_annotations_updated ON public.v3_a_annotations USING btree (
                updated_at ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Classes v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_cl_classes (
                id uuid NOT NULL DEFAULT gen_random_uuid(),
                creator_id TEXT COLLATE pg_catalog."default" NOT NULL,
                creator_type TEXT COLLATE pg_catalog."default" NOT NULL,
                reference_id uuid NOT NULL,
                reference_type TEXT COLLATE pg_catalog."default" NOT NULL,
                VALUE TEXT COLLATE pg_catalog."default" NOT NULL,
                is_locked BOOLEAN NOT NULL,
                TYPE TEXT COLLATE pg_catalog."default" NOT NULL,
                CONSTRAINT v3_cl_classes_pkey PRIMARY KEY (id)
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_cl_classes_creator ON public.v3_cl_classes USING btree (
                creator_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_cl_classes_reference ON public.v3_cl_classes USING btree (
                reference_id ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_cl_classes_value ON public.v3_cl_classes USING btree (
                VALUE COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Primitives v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_p_primitives (
                id uuid NOT NULL DEFAULT gen_random_uuid(),
                TYPE TEXT COLLATE pg_catalog."default" NOT NULL,
                NAME TEXT COLLATE pg_catalog."default" NOT NULL,
                description TEXT COLLATE pg_catalog."default",
                creator_id TEXT COLLATE pg_catalog."default" NOT NULL,
                creator_type TEXT COLLATE pg_catalog."default" NOT NULL,
                reference_id TEXT COLLATE pg_catalog."default",
                reference_type TEXT COLLATE pg_catalog."default",
                is_locked BOOLEAN NOT NULL,
                ivalue INTEGER,
                fvalue DOUBLE PRECISION,
                bvalue BOOLEAN,
                svalue TEXT COLLATE pg_catalog."default",
                CONSTRAINT v3_p_primitives_pkey PRIMARY KEY (id)
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_p_primitives_creator ON public.v3_p_primitives USING btree (
                creator_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_p_primitives_reference ON public.v3_p_primitives USING btree (
                reference_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_p_primitives_type ON public.v3_p_primitives USING btree (
                TYPE COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Collections v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_c_collections (
                id uuid NOT NULL DEFAULT gen_random_uuid(),
                NAME TEXT COLLATE pg_catalog."default",
                description TEXT COLLATE pg_catalog."default",
                creator_id TEXT COLLATE pg_catalog."default" NOT NULL,
                creator_type TEXT COLLATE pg_catalog."default" NOT NULL,
                reference_id TEXT COLLATE pg_catalog."default",
                reference_type TEXT COLLATE pg_catalog."default",
                item_type TEXT COLLATE pg_catalog."default" NOT NULL,
                is_locked BOOLEAN NOT NULL,
                TYPE TEXT COLLATE pg_catalog."default" NOT NULL,
                CONSTRAINT v3_c_collections_pkey PRIMARY KEY (id)
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_c_collections_creator ON public.v3_c_collections USING btree (
                creator_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_c_collections_reference ON public.v3_c_collections USING btree (
                reference_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_c_collections_type ON public.v3_c_collections USING btree (
                item_type COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Collection Items v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_c_collection_items (
                collection_id uuid NOT NULL,
                item_id TEXT COLLATE pg_catalog."default" NOT NULL,
                CONSTRAINT v3_c_collection_items_pkey PRIMARY KEY (collection_id, item_id),
                CONSTRAINT v3_c_collection_items_collection_id_fkey FOREIGN KEY (collection_id)
                    REFERENCES public.v3_c_collections (id) MATCH SIMPLE
                    ON UPDATE NO ACTION
                    ON DELETE CASCADE
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_c_collection_items_coll ON public.v3_c_collection_items USING btree (
                collection_id ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Jobs Lock Annotations v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_j_job_annotations (
                job_id TEXT COLLATE pg_catalog."default" NOT NULL,
                annotation_id uuid NOT NULL,
                CONSTRAINT v3_j_job_annotations_pkey PRIMARY KEY (job_id, annotation_id),
                CONSTRAINT v3_j_job_annotations_annotation_id_fkey FOREIGN KEY (annotation_id)
                    REFERENCES public.v3_a_annotations (id) MATCH SIMPLE
                    ON UPDATE NO ACTION
                    ON DELETE NO ACTION
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_j_job_annotations_job ON public.v3_j_job_annotations USING btree (
                job_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Jobs Lock Classes v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_j_job_classes (
                job_id TEXT COLLATE pg_catalog."default" NOT NULL,
                class_id TEXT COLLATE pg_catalog."default" NOT NULL,
                annot_id uuid NOT NULL,
                VALUE TEXT COLLATE pg_catalog."default" NOT NULL,
                CONSTRAINT v3_j_job_classes_pkey PRIMARY KEY (job_id, annot_id, class_id),
                CONSTRAINT v3_j_job_classes_annot_id_fkey FOREIGN KEY (annot_id)
                    REFERENCES public.v3_a_annotations (id) MATCH SIMPLE
                    ON UPDATE NO ACTION
                    ON DELETE NO ACTION
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_j_job_classes_aid ON public.v3_j_job_classes USING btree (
                annot_id ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_j_job_classes_job ON public.v3_j_job_classes USING btree (
                job_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_j_job_classes_val ON public.v3_j_job_classes USING btree (
                VALUE COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Jobs Lock Primitives v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_j_job_primitives (
                job_id TEXT COLLATE pg_catalog."default" NOT NULL,
                primitive_id uuid NOT NULL,
                CONSTRAINT v3_j_job_primitives_pkey PRIMARY KEY (job_id, primitive_id),
                CONSTRAINT v3_j_job_primitives_primitive_id_fkey FOREIGN KEY (primitive_id)
                    REFERENCES public.v3_p_primitives (id) MATCH SIMPLE
                    ON UPDATE NO ACTION
                    ON DELETE NO ACTION
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_j_job_primitives_job ON public.v3_j_job_primitives USING btree (
                job_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Jobs Lock Collections v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_j_job_collections (
                job_id TEXT COLLATE pg_catalog."default" NOT NULL,
                collection_id uuid NOT NULL,
                CONSTRAINT v3_j_job_collections_pkey PRIMARY KEY (job_id, collection_id),
                CONSTRAINT v3_j_job_collections_collection_id_fkey FOREIGN KEY (collection_id)
                    REFERENCES public.v3_c_collections (id) MATCH SIMPLE
                    ON UPDATE NO ACTION
                    ON DELETE NO ACTION
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_j_job_collections_job ON public.v3_j_job_collections USING btree (
                job_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )

    # Jobs Lock Slides v3
    async with conn.transaction():
        await conn.execute(
            """
            CREATE TABLE IF NOT EXISTS public.v3_j_job_slides (
                job_id TEXT COLLATE pg_catalog."default" NOT NULL,
                slide_id TEXT COLLATE pg_catalog."default" NOT NULL,
                CONSTRAINT v3_j_job_slides_pkey PRIMARY KEY (job_id, slide_id)
            ) TABLESPACE pg_default;

            CREATE INDEX IF NOT EXISTS v3_j_job_slides_job ON public.v3_j_job_slides USING btree (
                job_id COLLATE pg_catalog."default" ASC NULLS LAST
            ) TABLESPACE pg_default;
            """
        )


#######################################################################
# NO MORE CHANGES ON DATABASE IN THIS STEP! BEGIN NEW MIGRATION STEP! #
#######################################################################
