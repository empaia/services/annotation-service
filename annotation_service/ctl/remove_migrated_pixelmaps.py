import shutil
import uuid
from pathlib import Path

from annotation_service.ctl.pixelmapd import Settings
from annotation_service.settings import Settings as AnnotSettings


def run_remove_migrated_pixelmaps():
    annot_settings = AnnotSettings()
    settings = Settings()

    annot_data_path = annot_settings.pixelmaps_data_path
    data_path = settings.data_path

    assert annot_data_path
    assert data_path

    annot_data_path = Path(annot_data_path)
    data_path = Path(data_path)

    migrated_pixelmaps = []
    hdf5_path = data_path / Path("hdf5")

    for path in hdf5_path.iterdir():
        if not path.is_file():
            continue

        if not path.suffix == ".hdf5":
            continue

        pixelmap_id = path.stem
        uuid.UUID(pixelmap_id)

        migrated_pixelmaps.append(pixelmap_id)

    for path in annot_data_path.iterdir():
        if not path.is_dir():
            continue

        if path.name == "hdf5":
            continue

        pixelmap_id = path.name

        if pixelmap_id not in migrated_pixelmaps:
            continue

        shutil.rmtree(path)
