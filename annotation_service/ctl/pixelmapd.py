import json
from multiprocessing import Process

import zmq
from pydantic_settings import BaseSettings, SettingsConfigDict

from annotation_service.ctl.pixelmap.log_utils import tprint

from .pixelmap.pixelmap_file import PixelmapFile
from .pixelmap.worker_cache import WorkerCache

POLLER_TIMEOUT_SECONDS = 15


class Settings(BaseSettings):
    data_path: str = "/app/data/pixelmaps/"
    port: int = 5556
    gzip_compresslevel: int = 1

    model_config = SettingsConfigDict(env_prefix="PMD_", env_file=".env", extra="ignore")


def run_pixelmapd():
    settings = Settings()
    data_path = settings.data_path

    context: zmq.Context = zmq.Context()
    client_socket: zmq.Socket = context.socket(zmq.ROUTER)
    client_socket.bind(f"tcp://*:{settings.port}")

    poller: zmq.Poller = zmq.Poller()
    poller.register(socket=client_socket, flags=zmq.POLLIN)

    worker_cache = WorkerCache()

    while True:
        sockets = poller.poll(POLLER_TIMEOUT_SECONDS)
        if not sockets:
            _cleanup_expired_workers(worker_cache=worker_cache)
            continue

        for socket, _ in sockets:
            req = socket.recv_multipart()
            client_id = req[0]
            req_msg = json.loads(req[1])

            if req_msg.get("req") == "ALIVE":
                rep_msg = {"is_alive": True}
                client_socket.send_multipart([client_id, json.dumps(rep_msg).encode("ascii")])
                continue

            if socket == client_socket:
                _handle_client_request(
                    data_path=data_path,
                    gzip_compresslevel=settings.gzip_compresslevel,
                    worker_cache=worker_cache,
                    context=context,
                    poller=poller,
                    req=req,
                    req_msg=req_msg,
                )
            else:
                client_socket.send_multipart(req)


def _handle_client_request(
    data_path: str,
    gzip_compresslevel: int,
    worker_cache: WorkerCache,
    context: zmq.Context,
    poller: zmq.Poller,
    req,
    req_msg,
):
    case_id = req_msg.get("case_id")
    pixelmap = req_msg.get("pixelmap")
    pixelmap_id = pixelmap["id"]
    assert isinstance(pixelmap_id, str)

    socket, port = worker_cache.get_socket(pixelmap_id=pixelmap_id)
    if socket is None:
        socket: zmq.Socket = context.socket(zmq.DEALER)
        port = socket.bind_to_random_port("tcp://0.0.0.0")
        poller.register(socket=socket, flags=zmq.POLLIN)

        worker_cache.add_socket(pixelmap_id=pixelmap_id, socket=socket, port=port)

        process = Process(
            target=_worker_process,
            args=(data_path, port, case_id, gzip_compresslevel, pixelmap),
        )
        process.start()

    socket.send_multipart(req)


def _cleanup_expired_workers(worker_cache: WorkerCache):
    expired_sockets = worker_cache.get_expired_sockets()
    for pixelmap_id in expired_sockets.keys():
        exp_socket: zmq.Socket
        exp_socket, _ = expired_sockets[pixelmap_id]
        exp_socket.send_string("SIGNAL", zmq.SNDMORE)
        exp_socket.send_json({"req": "KILL_PROC"})
        worker_cache.clear_cache(pixelmap_id=pixelmap_id)


def _worker_process(data_path: str, port: int, case_id: str, gzip_compresslevel: int, pixelmap: dict):
    context: zmq.Context = zmq.Context()
    socket: zmq.Socket = context.socket(zmq.DEALER)
    socket.connect(f"tcp://127.0.0.1:{port}")

    pixelmap_id = pixelmap["id"]

    print("New Worker for Pixelmap:", pixelmap_id)

    pixelmap_file = PixelmapFile(
        data_path=data_path,
        case_id=case_id,
        gzip_compresslevel=gzip_compresslevel,
        pixelmap=pixelmap,
    )

    while True:
        raw_data = socket.recv_multipart()
        client_id = raw_data[0]
        req_msg = json.loads(raw_data[1])
        req = req_msg["req"]

        if client_id == "SIGNAL" and req == "KILL_PROC":
            socket.close()
            break

        try:
            if req == "GET_TILE":
                level, x, y, accept_encoding = req_msg["level"], req_msg["x"], req_msg["y"], req_msg["accept_encoding"]
                rep_msg, rep_bytes = pixelmap_file.get_tile(level=level, x=x, y=y, accept_encoding=accept_encoding)
                _send_response(
                    socket=socket,
                    client_id=client_id,
                    rep_msg=rep_msg,
                    rep_bytes=rep_bytes,
                )
            elif req == "GET_TILES":
                level, start_x, start_y, end_x, end_y, accept_encoding = (
                    req_msg["level"],
                    req_msg["start_x"],
                    req_msg["start_y"],
                    req_msg["end_x"],
                    req_msg["end_y"],
                    req_msg["accept_encoding"],
                )
                rep_msg, rep_bytes = pixelmap_file.get_tiles(
                    level=level,
                    start_x=start_x,
                    start_y=start_y,
                    end_x=end_x,
                    end_y=end_y,
                    accept_encoding=accept_encoding,
                )
                _send_response(
                    socket=socket,
                    client_id=client_id,
                    rep_msg=rep_msg,
                    rep_bytes=rep_bytes,
                )
            elif req == "CREATE_TILE":
                req_bytes = raw_data[2]
                level, x, y, content_encoding = (
                    req_msg["level"],
                    req_msg["x"],
                    req_msg["y"],
                    req_msg["content_encoding"],
                )
                rep_msg = pixelmap_file.create_tile(
                    level=level, x=x, y=y, content_encoding=content_encoding, req_bytes=req_bytes
                )
                _send_response(
                    socket=socket,
                    client_id=client_id,
                    rep_msg=rep_msg,
                )
            elif req == "CREATE_TILES":
                req_bytes = raw_data[2]
                level, start_x, start_y, end_x, end_y, content_encoding = (
                    req_msg["level"],
                    req_msg["start_x"],
                    req_msg["start_y"],
                    req_msg["end_x"],
                    req_msg["end_y"],
                    req_msg["content_encoding"],
                )
                rep_msg = pixelmap_file.create_tiles(
                    level=level,
                    start_x=start_x,
                    start_y=start_y,
                    end_x=end_x,
                    end_y=end_y,
                    content_encoding=content_encoding,
                    req_bytes=req_bytes,
                )
                _send_response(
                    socket=socket,
                    client_id=client_id,
                    rep_msg=rep_msg,
                )
            elif req == "DELETE_TILE":
                level, x, y = req_msg["level"], req_msg["x"], req_msg["y"]
                rep_msg = pixelmap_file.delete_tile(level=level, x=x, y=y)
                _send_response(
                    socket=socket,
                    client_id=client_id,
                    rep_msg=rep_msg,
                )
            elif req == "DELETE":
                rep_msg = pixelmap_file.delete()
                _send_response(
                    socket=socket,
                    client_id=client_id,
                    rep_msg=rep_msg,
                )
            else:
                rep_msg = {
                    "rep": "error",
                    "status_code": 422,
                    "detail": f"Invalid request '{req}'",
                }
                _send_response(
                    socket=socket,
                    client_id=client_id,
                    rep_msg=rep_msg,
                )
        except Exception as e:
            tprint(str(e))
            rep_msg = {
                "rep": "error",
                "status_code": 500,
                "detail": "Unexpected error while accessing a pixelmap.",
            }
            _send_response(
                socket=socket,
                client_id=client_id,
                rep_msg=rep_msg,
            )

    pixelmap_file.close()


def _send_response(socket, client_id, rep_msg, rep_bytes=None):
    rep_parts = [client_id, json.dumps(rep_msg).encode("ascii")]
    if rep_bytes is not None:
        rep_parts.append(rep_bytes)
    socket.send_multipart(rep_parts)
