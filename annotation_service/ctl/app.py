import asyncio

import typer

from .drop_db import run_drop_db
from .migrate_db import run_migrate_db
from .pixelmapd import run_pixelmapd
from .remove_migrated_pixelmaps import run_remove_migrated_pixelmaps

app = typer.Typer()


@app.command()
def migrate_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_migrate_db())


@app.command()
def drop_db():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_drop_db())


@app.command()
def pixelmapd():
    run_pixelmapd()


@app.command()
def remove_migrated_pixelmaps():
    run_remove_migrated_pixelmaps()
