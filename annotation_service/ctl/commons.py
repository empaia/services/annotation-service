import asyncpg

from ..db import set_type_codecs
from ..settings import Settings


async def connect_db() -> asyncpg.Connection:
    settings = Settings()
    conn = await asyncpg.connect(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
    await set_type_codecs(conn=conn)
    return conn
