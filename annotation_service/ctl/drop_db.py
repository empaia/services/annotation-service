from asyncpg.exceptions import UndefinedTableError

from .commons import connect_db

TABLES = [
    "migration_steps",
    "annot_migration_steps",
    # v1
    "j_job_slides",
    "j_job_primitives",
    "j_job_collections",
    "j_job_classes",
    "j_job_annotations",
    "c_collection_items",
    "c_collections",
    "p_primitives",
    "cl_classes",
    "a_annotations",
    # v3
    "v3_j_job_slides",
    "v3_j_job_primitives",
    "v3_j_job_collections",
    "v3_j_job_classes",
    "v3_c_collection_slides",
    "v3_c_collections",
    "v3_p_primitives",
    "v3_cl_classes",
    "v3_a_annotations",
]


async def run_drop_db():
    conn = await connect_db()

    for table in TABLES:
        try:
            await conn.execute(f"DROP TABLE {table};")
        except UndefinedTableError as e:
            print(e)
