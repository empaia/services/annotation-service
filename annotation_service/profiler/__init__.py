import json

from fastapi import Request
from pyinstrument import Profiler
from pyinstrument.renderers import JSONRenderer

from .profiler import parse_timings


def add_profiler(app):
    @app.middleware("http")
    async def add_process_time_header(request: Request, call_next):
        profiler = Profiler(interval=0.0000001, async_mode="enabled")
        profiler.start()
        response = await call_next(request)
        profiler.stop()
        output = await parse_timings(profiler.output(JSONRenderer(show_all=True)))
        response.headers["Server-Timings"] = output
        return response
