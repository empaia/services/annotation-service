import json

IGNORED_FUNCTIONS = ["db_clients", "set_type_codecs", "add_process_time_header"]
INCLUDED_LIBRARY_FUNCTIONS = [
    "run_endpoint_function",
    "fetch",
    "fetchrow",
    "fetchval",
    "execute",
    "executemany",
    "jsonable_encoder",
    "render",
    "validate",
]


async def parse_timings(timings_str):
    timings = json.loads(timings_str)
    parsed_timings = {}
    parsed_timings["duration"] = timings["duration"]
    parsed_timings["cpu_time"] = timings["cpu_time"]
    parsed_timings["frames"] = await parse_frame(timings["root_frame"])
    return str(parsed_timings)


async def parse_frame(frame):
    parsed_frames = []
    children = frame["children"]
    for child in children:
        if child["is_application_code"]:
            if child["function"] in IGNORED_FUNCTIONS:
                continue
            new_frame = await get_function_timings(child)
            parsed_frames.append(new_frame)
            parsed_frames.extend(await parse_frame(child))
        else:
            if child["function"] in INCLUDED_LIBRARY_FUNCTIONS:
                new_frame = await get_function_timings(child)
                parsed_frames.append(new_frame)
            parsed_frames.extend(await parse_frame(child))
    return parsed_frames


async def get_function_timings(frame):
    return {frame["function"]: {"time": frame["time"], "await_time": frame["await_time"]}}
