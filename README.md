# Annotation Service (AS)

Leightweight annotation server to manage annotations, classes, collections ans primitives.

***

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone with `git clone --recurse-submodules https://gitlab.com/empaia/services/annotation-service.git`

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd annotation-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Create a `.env` file:

```bash
cp sample.env .env # modify if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build
```

If only DB server (incl. pgAdmin) should be started via compose

```bash
docker-compose up --build -d pgadmin

./run.sh --port=5000 --reload
```

If only DB server (without pgAdmin) should be started via compose

```bash
docker-compose up --build -d annot-db

./run.sh --port=5000 --reload
```

### Stop and Remove

```bash
docker-compose down -v
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black .
pycodestyle annotation_service tests
pylint annotation_service tests
pytest tests/root -v --maxfail=1
pytest tests/v1 -v --maxfail=1
pytest tests/v3 -v --maxfail=1
pytest tests/separate_apis -v --maxfail=1
```

### Recommended VSCode Settings

In `.vscode/settings.json`.

```json
{
    "python.linting.pylintEnabled": true,
    "python.linting.pycodestyleEnabled": false,
    "python.formatting.provider": "black"
}
```

## Usage

OpenAPI documentation:

* `http://localhost:5000/docs`
* `http://localhost:5000/v1/docs`
* `http://localhost:5000/v2/docs`
