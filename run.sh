#!/usr/bin/env bash

annotctl migrate-db && uvicorn annotation_service.app:app $@
