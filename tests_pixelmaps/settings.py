from pydantic_settings import BaseSettings, SettingsConfigDict


class PytestSettings(BaseSettings):
    annot_url: str
    model_config = SettingsConfigDict(env_file=".env", env_prefix="PYTEST_", extra="ignore")
