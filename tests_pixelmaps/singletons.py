from .settings import PytestSettings

pytest_settings = PytestSettings()
annot_url = pytest_settings.annot_url.rstrip("/")
