import numpy as np

from annotation_service.models.v3.annotation.pixelmaps import ContinuousPixelmap, DiscretePixelmap

from .commons import (
    delete_pixelmap,
    delete_tile,
    get_continuous_pixelmap,
    get_discrete_pixelmap,
    get_pixelmap,
    get_pixelmap_info,
    get_tile,
    get_tiles,
    post_pixelmap,
    put_tile,
    put_tiles,
    slice_bulk_data,
)


def test_continuous_pixelmaps():
    level = {"slide_level": 1, "position_min_x": 0, "position_max_x": 2, "position_min_y": 0, "position_max_y": 2}
    pixelmap = get_continuous_pixelmap(tilesize=1024, level=level)

    # POST
    data = post_pixelmap(pixelmap)
    post_response = ContinuousPixelmap(**data)
    assert post_response.id
    assert post_response.name == pixelmap["name"]

    pixelmap_id = str(post_response.id)

    # GET
    data = get_pixelmap(pixelmap_id)
    get_response = ContinuousPixelmap(**data)
    assert get_response.id == post_response.id
    assert get_response.name == pixelmap["name"]

    # CHECK CORRECT BYTE ORDER OF numpy.tobytes()
    tile_data = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data[0] = 1.0
    first_row = np.frombuffer(tile_data.tobytes()[: 1024 * 4], dtype=pixelmap["element_type"])
    assert np.array_equal(first_row, tile_data[0])

    # PUT TILE
    tile_data = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data[42, 42] = 1.0
    put_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes())

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=get_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (get_response.channel_count, get_response.tilesize, get_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # PUT TILE OVERWRITE
    tile_data[42, 42] = 0.5
    put_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes())

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=get_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (get_response.channel_count, get_response.tilesize, get_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # DELETE TILE
    delete_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, expected_status_code=404)

    # PUT TILE AFTER DELETE
    tile_data[42, 42] = 0.2
    put_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes())

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=get_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (get_response.channel_count, get_response.tilesize, get_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # PUT TILES BULK
    tile_data_1_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_1[1, 1] = 1.0
    tile_data_2_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_1[2, 1] = 1.0
    tile_data_1_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_2[1, 2] = 1.0
    tile_data_2_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_2[2, 2] = 1.0

    tiles = [tile_data_1_1, tile_data_2_1, tile_data_1_2, tile_data_2_2]
    byte_data = b"".join([tile.tobytes() for tile in tiles])
    put_tiles(pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2, data=byte_data)

    # GET TILES BULK
    tile_response = get_tiles(pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2)

    retrieved_tiles = slice_bulk_data(
        pixelmap=get_response.model_dump(), tiles_data=tile_response, start_x=1, start_y=1, end_x=2, end_y=2
    )

    assert retrieved_tiles[0][0][1, 1] == tile_data_1_1[1, 1]
    assert retrieved_tiles[1][0][2, 1] == tile_data_2_1[2, 1]
    assert retrieved_tiles[2][0][1, 2] == tile_data_1_2[1, 2]
    assert retrieved_tiles[3][0][2, 2] == tile_data_2_2[2, 2]

    get_pixelmap_info(pixelmap_id=pixelmap_id)

    delete_pixelmap(pixelmap_id=str(get_response.id))


def test_discrete_pixelmaps():
    level = {"slide_level": 1, "position_min_x": 0, "position_max_x": 2, "position_min_y": 0, "position_max_y": 2}
    pixelmap = get_discrete_pixelmap(tilesize=1024, level=level)

    # POST
    data = post_pixelmap(pixelmap)
    post_response = DiscretePixelmap(**data)
    assert post_response.id
    assert post_response.name == pixelmap["name"]

    pixelmap_id = str(post_response.id)

    # GET
    data = get_pixelmap(pixelmap_id)
    get_response = DiscretePixelmap(**data)
    assert get_response.id == post_response.id
    assert get_response.name == pixelmap["name"]

    # PUT TILE
    tile_data = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data[42, 42] = 10
    put_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes())

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=get_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (get_response.channel_count, get_response.tilesize, get_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # PUT TILE OVERWRITE
    tile_data[42, 42] = 5
    put_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes())

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=get_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (get_response.channel_count, get_response.tilesize, get_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # DELETE TILE
    delete_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, expected_status_code=404)

    # PUT TILE AFTER DELETE
    tile_data[42, 42] = 2
    put_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, data=tile_data.tobytes())

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=get_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (get_response.channel_count, get_response.tilesize, get_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # PUT TILES BULK
    tile_data_1_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_1[1, 1] = -10
    tile_data_2_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_1[2, 1] = -5
    tile_data_1_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_2[1, 2] = 5
    tile_data_2_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_2[2, 2] = 10

    tiles = [tile_data_1_1, tile_data_2_1, tile_data_1_2, tile_data_2_2]
    byte_data = b"".join([tile.tobytes() for tile in tiles])
    put_tiles(pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2, data=byte_data)

    # GET TILES BULK
    tile_response = get_tiles(pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2)

    retrieved_tiles = slice_bulk_data(
        pixelmap=get_response.model_dump(), tiles_data=tile_response, start_x=1, start_y=1, end_x=2, end_y=2
    )

    assert retrieved_tiles[0][0][1, 1] == tile_data_1_1[1, 1]
    assert retrieved_tiles[1][0][2, 1] == tile_data_2_1[2, 1]
    assert retrieved_tiles[2][0][1, 2] == tile_data_1_2[1, 2]
    assert retrieved_tiles[3][0][2, 2] == tile_data_2_2[2, 2]

    get_pixelmap_info(pixelmap_id=pixelmap_id)

    delete_pixelmap(pixelmap_id=pixelmap_id)


def test_compression():
    level = {"slide_level": 1, "position_min_x": 0, "position_max_x": 2, "position_min_y": 0, "position_max_y": 2}
    pixelmap = get_continuous_pixelmap(tilesize=1024, level=level)

    # POST
    data = post_pixelmap(pixelmap)
    post_response = ContinuousPixelmap(**data)
    assert post_response.id
    assert post_response.name == pixelmap["name"]

    pixelmap_id = str(post_response.id)

    # PUT TILE
    tile_data = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data[42, 42] = 1.0
    put_tile(
        pixelmap_id=pixelmap_id,
        level=1,
        tile_x=0,
        tile_y=0,
        data=tile_data.tobytes(),
        compress=True,
        content_encoding="gzip",
    )

    # GET TILE
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0, accept_encoding="gzip")
    tile_response_data = np.frombuffer(tile_response, dtype=post_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (post_response.channel_count, post_response.tilesize, post_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # GET TILE WITHOUT COMPRESSION
    tile_response = get_tile(pixelmap_id=pixelmap_id, level=1, tile_x=0, tile_y=0)
    tile_response_data = np.frombuffer(tile_response, dtype=post_response.element_type)
    tile_response_data = tile_response_data.reshape(
        (post_response.channel_count, post_response.tilesize, post_response.tilesize)
    )
    first_channel = tile_response_data[0]
    assert first_channel[42, 42] == tile_data[42, 42]

    # PUT TILE WHERE COMPRESSION DOES NOT MATCH HEADER
    put_tile(
        pixelmap_id=pixelmap_id,
        level=1,
        tile_x=0,
        tile_y=0,
        data=tile_data.tobytes(),
        expected_status_code=422,
        compress=False,
        content_encoding="gzip",
    )

    put_tile(
        pixelmap_id=pixelmap_id,
        level=1,
        tile_x=0,
        tile_y=0,
        data=tile_data.tobytes(),
        expected_status_code=422,
        compress=True,
        content_encoding=None,
    )

    # PUT TILES BULK
    tile_data_1_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_1[1, 1] = 1.0
    tile_data_2_1 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_1[2, 1] = 1.0
    tile_data_1_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_1_2[1, 2] = 1.0
    tile_data_2_2 = np.zeros((pixelmap["tilesize"], pixelmap["tilesize"]), dtype=pixelmap["element_type"])
    tile_data_2_2[2, 2] = 1.0

    tiles = [tile_data_1_1, tile_data_2_1, tile_data_1_2, tile_data_2_2]
    byte_data = b"".join([tile.tobytes() for tile in tiles])
    put_tiles(
        pixelmap_id=pixelmap_id,
        level=1,
        start_x=1,
        start_y=1,
        end_x=2,
        end_y=2,
        data=byte_data,
        compress=True,
        content_encoding="gzip",
    )

    # GET TILES BULK
    tile_response = get_tiles(
        pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2, accept_encoding="gzip"
    )

    retrieved_tiles = slice_bulk_data(
        pixelmap=post_response.model_dump(), tiles_data=tile_response, start_x=1, start_y=1, end_x=2, end_y=2
    )

    assert retrieved_tiles[0][0][1, 1] == tile_data_1_1[1, 1]
    assert retrieved_tiles[1][0][2, 1] == tile_data_2_1[2, 1]
    assert retrieved_tiles[2][0][1, 2] == tile_data_1_2[1, 2]
    assert retrieved_tiles[3][0][2, 2] == tile_data_2_2[2, 2]

    # GET TILES BULK WITHOUT COMPRESSION
    tile_response = get_tiles(pixelmap_id=pixelmap_id, level=1, start_x=1, start_y=1, end_x=2, end_y=2)

    retrieved_tiles = slice_bulk_data(
        pixelmap=post_response.model_dump(), tiles_data=tile_response, start_x=1, start_y=1, end_x=2, end_y=2
    )

    assert retrieved_tiles[0][0][1, 1] == tile_data_1_1[1, 1]
    assert retrieved_tiles[1][0][2, 1] == tile_data_2_1[2, 1]
    assert retrieved_tiles[2][0][1, 2] == tile_data_1_2[1, 2]
    assert retrieved_tiles[3][0][2, 2] == tile_data_2_2[2, 2]

    # PUT TILES WHERE COMPRESSION DOES NOT MATCH HEADER
    put_tiles(
        pixelmap_id=pixelmap_id,
        level=1,
        start_x=1,
        start_y=1,
        end_x=2,
        end_y=2,
        data=byte_data,
        expected_status_code=422,
        compress=False,
        content_encoding="gzip",
    )

    put_tiles(
        pixelmap_id=pixelmap_id,
        level=1,
        start_x=1,
        start_y=1,
        end_x=2,
        end_y=2,
        data=byte_data,
        expected_status_code=422,
        compress=True,
        content_encoding=None,
    )

    delete_pixelmap(pixelmap_id=pixelmap_id)
