import gzip
import uuid

import numpy as np
import requests

from annotation_service.models.v3.annotation.pixelmaps import PixelmapReferenceType
from annotation_service.models.v3.commons import DataCreatorType

from ..singletons import annot_url


def get_continuous_pixelmap(
    tilesize: int,
    level: dict,
    element_type: str = "float32",
    channel_count: int = 1,
    min_value: float = 0.0,
    max_value: float = 1.0,
    neutral_value=None,
    reference_id: str = str(uuid.uuid4()),
    creator_id: str = str(uuid.uuid4()),
    creator_type: str = DataCreatorType.JOB,
):
    return {
        "type": "continuous_pixelmap",
        "name": "CONTINUOUS PIXELMAP",
        "tilesize": tilesize,
        "element_type": element_type,
        "channel_count": channel_count,
        "levels": [level],
        "reference_id": reference_id,
        "reference_type": PixelmapReferenceType.WSI,
        "creator_id": creator_id,
        "creator_type": creator_type,
        "description": None,
        "min_value": min_value,
        "max_value": max_value,
        "neutral_value": neutral_value,
    }


def get_discrete_pixelmap(
    tilesize: int,
    level: dict,
    element_type: str = "int8",
    channel_count: int = 1,
    min_value: int = -10,
    max_value: int = 10,
    neutral_value: int = None,
    reference_id: str = str(uuid.uuid4()),
    creator_id: str = str(uuid.uuid4()),
    creator_type: str = DataCreatorType.JOB,
):
    return {
        "type": "discrete_pixelmap",
        "name": "DISCRETE PIXELMAP",
        "tilesize": tilesize,
        "element_type": element_type,
        "channel_count": channel_count,
        "levels": [level],
        "reference_id": reference_id,
        "reference_type": PixelmapReferenceType.WSI,
        "creator_id": creator_id,
        "creator_type": creator_type,
        "description": None,
        "min_value": min_value,
        "max_value": max_value,
        "neutral_value": neutral_value,
    }


def post_pixelmap(pixelmap: dict):
    url = f"{annot_url}/v3/pixelmaps"
    r = requests.post(url, json=pixelmap)
    assert r.status_code == 201
    return r.json()


def get_pixelmap(pixelmap_id: str):
    url = f"{annot_url}/v3/pixelmaps/{pixelmap_id}"
    r = requests.get(url)
    assert r.status_code == 200
    return r.json()


def get_pixelmap_info(pixelmap_id: str):
    url = f"{annot_url}/v3/pixelmaps/{pixelmap_id}/info"
    r = requests.get(url)
    assert r.status_code == 404


def delete_pixelmap(pixelmap_id: str):
    url = f"{annot_url}/v3/pixelmaps/{pixelmap_id}"
    r = requests.delete(url)
    assert r.status_code == 200
    return r.json()


def put_tile(
    pixelmap_id: str,
    level: int,
    tile_x: int,
    tile_y: int,
    data: bytes,
    expected_status_code: int = 204,
    compress=False,
    content_encoding=None,
):
    url = f"{annot_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
    headers = {"Content-Encoding": content_encoding}
    if compress:
        data = gzip.compress(data=data, compresslevel=1)

    r = requests.put(url, data=data, headers=headers)
    assert r.status_code == expected_status_code


def get_tile(
    pixelmap_id: str, level: int, tile_x: int, tile_y: int, expected_status_code: int = 200, accept_encoding=None
):
    url = f"{annot_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
    headers = {"Accept-Encoding": accept_encoding}
    r = requests.get(url, headers=headers)
    assert r.status_code == expected_status_code
    data = r.content

    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding == accept_encoding

    return data


def delete_tile(pixelmap_id: str, level: int, tile_x: int, tile_y: int):
    url = f"{annot_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
    r = requests.delete(url)
    assert r.status_code == 204


def put_tiles(
    pixelmap_id: str,
    level: int,
    start_x: int,
    start_y: int,
    end_x: int,
    end_y: int,
    data: bytes,
    expected_status_code: int = 204,
    compress=False,
    content_encoding=None,
):
    url = (
        f"{annot_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position"
        f"/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
    )
    headers = {"Content-Encoding": content_encoding}
    if compress:
        data = gzip.compress(data=data, compresslevel=1)

    r = requests.put(url, data=data, headers=headers)
    assert r.status_code == expected_status_code


def get_tiles(pixelmap_id: str, level: int, start_x: int, start_y: int, end_x: int, end_y: int, accept_encoding=None):
    url = (
        f"{annot_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position"
        f"/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
    )
    headers = None
    headers = {"Accept-Encoding": accept_encoding}
    r = requests.get(url, headers=headers)
    assert r.status_code == 200
    data = r.content

    content_encoding = r.headers.get("Content-Encoding")
    assert content_encoding == accept_encoding

    return data


def slice_bulk_data(pixelmap: dict, tiles_data: bytes, start_x: int, start_y: int, end_x: int, end_y: int):
    retrieved_tiles = []

    tiles = np.frombuffer(tiles_data, dtype=pixelmap["element_type"])
    tile_length = (pixelmap["tilesize"] ** 2) * pixelmap["channel_count"]
    tile_count = (end_x - start_x + 1) * (end_y - start_y + 1)

    start_idx = 0
    current_x = start_x
    current_y = start_y
    for _ in range(tile_count):
        tile = tiles[start_idx : start_idx + tile_length]

        tile = tile.reshape((pixelmap["channel_count"], pixelmap["tilesize"], pixelmap["tilesize"]))

        start_idx += tile_length
        if current_x == end_x:
            current_x = start_x
            current_y += 1
        else:
            current_x += 1
        retrieved_tiles.append(tile)

    return retrieved_tiles
