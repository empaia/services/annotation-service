import docker
import requests

from annotation_service.models.commons import ServiceStatusEnum

from ..commons.setup_tests import Up, docker_build, get_base_url


def test_build():
    client = docker.from_env()
    docker_build(client)


def test_run():
    client = docker.from_env()
    with Up(client) as up:
        base_url = get_base_url(up.annot_container)

        response = requests.get(f"{base_url}/alive")
        assert response.status_code == 200
        status = response.json()
        assert status["status"] == ServiceStatusEnum.OK
        assert status["version"]
