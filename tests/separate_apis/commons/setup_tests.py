from socket import socket
from time import sleep

from docker.errors import NotFound
from docker.models.containers import Container
from sqlalchemy.engine import create_engine

NET = "annot-tests"
DB_HOST = "annot-db-tests"
ANNOT_IMAGE = "annot:tests"
PSQL_VOL = "psql-test-data"
WAIT_INTERVAL = 0.5
ITEM_LIMIT = 100
POST_LIMIT = 100


def get_environment(allow_external_ids: bool = False):
    env = {
        "ANNOT_API_V1_INTEGRATION": "annotation_service.api.v1.integrations.disable_auth:DisableAuth",
        "ANNOT_API_V3_INTEGRATION": "annotation_service.api.v3.integrations.disable_auth:DisableAuth",
        "ANNOT_DB_HOST": DB_HOST,
        "ANNOT_V1_ITEM_LIMIT": ITEM_LIMIT,
        "ANNOT_V1_POST_LIMIT": POST_LIMIT,
        "ANNOT_V3_ITEM_LIMIT": ITEM_LIMIT,
        "ANNOT_V3_POST_LIMIT": POST_LIMIT,
    }

    if allow_external_ids:
        env["ANNOT_ALLOW_EXTERNAL_IDS"] = True

    return env


def free_port():
    """
    port 0 requests a random free port from the OS
    this is a work around, because setting port 0 in docker run does not work as expected
    in very rare cases this work around could result in a race condition
    """
    with socket() as s:
        s.bind(("", 0))
        return s.getsockname()[1]


def get_port(container_port, container):
    print("CONTAINER_PORTS:", container.ports)
    return container.ports[f"{container_port}/tcp"][0]["HostPort"]


def get_base_url(container):
    return f"http://localhost:{get_port(5000, container)}"


def clean(client):
    for container in client.containers.list():
        if ANNOT_IMAGE in container.image.tags:
            container.remove(force=True)

    try:
        container = client.containers.get(DB_HOST)
        container.remove(force=True)
    except NotFound:
        pass

    for net in client.networks.list():
        if net.name == NET:
            net.remove()

    try:
        volume = client.volumes.get(PSQL_VOL)
        volume.remove()
    except NotFound:
        pass


def network(client):
    return client.networks.create(name=NET)


def docker_build(client):
    client.images.build(path=".", tag=ANNOT_IMAGE, network_mode="host")


def wait_for_log(container: Container, log_msg: str, timeout: int = 30):
    stdout, stderr = logs(container)
    slept = 0
    while log_msg not in stdout and log_msg not in stderr:
        stdout, stderr = logs(container)
        slept += WAIT_INTERVAL
        if slept > timeout:
            raise TimeoutError((stdout, stderr))
        sleep(WAIT_INTERVAL)


def annot_run(client, allow_external_ids: bool = False):
    container = client.containers.run(
        ANNOT_IMAGE,
        "run.sh --host=0.0.0.0 --port=5000",
        network=NET,
        environment=get_environment(allow_external_ids),
        detach=True,
        stdout=True,
        stderr=True,
        ports={"5000/tcp": ("localhost", free_port())},
    )
    wait_for_log(container, "Application startup complete.")
    container.reload()
    for line in logs(container):
        print(line)
    assert container.status == "running"
    net = client.networks.get("bridge")
    net.connect(container)
    return container


def db_run(client):
    container = client.containers.run(
        "registry.gitlab.com/empaia/integration/ci-docker-images/custom-postgres:latest",
        network=NET,
        name=DB_HOST,
        environment={"POSTGRES_USER": "admin", "POSTGRES_PASSWORD": "secret", "POSTGRES_DB": "annot"},
        detach=True,
        stdout=True,
        stderr=True,
        ports={"5432/tcp": ("localhost", free_port())},
        volumes={PSQL_VOL: {"bind": "/var/lib/postgresql/data", "mode": "rw"}},
    )
    wait_for_log(container, "[1] LOG:  database system is ready to accept connections")
    container.reload()
    for line in logs(container):
        print(line)
    assert container.status == "running"
    return container


def get_db(container):
    return create_engine(f"postgresql+psycopg2://admin:secret@localhost:{get_port(5432, container)}/annot")


def logs(container):
    stdout = container.logs(stdout=True, stderr=False)
    stderr = container.logs(stdout=False, stderr=True)
    return stdout.decode("utf-8"), stderr.decode("utf-8")


class Up:
    def __init__(self, client, allow_external_ids: bool = False):
        self.client = client
        clean(client)
        docker_build(client)
        network(client)
        self.db_container = db_run(client)
        self.annot_container = annot_run(client, allow_external_ids)

    def __enter__(self):
        return self

    def __exit__(self, _type, value, traceback):
        clean(self.client)
