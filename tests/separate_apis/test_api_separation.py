import docker
import requests

from .commons.setup_tests import Up, get_base_url


def test_annotations():
    client = docker.from_env()
    with Up(client) as up:
        base_url = get_base_url(up.annot_container)
        base_url_v1 = f"{base_url}/v1"
        base_url_v3 = f"{base_url}/v3"

        # post annot v1
        annot = {
            "name": "Name",
            "description": "Desc",
            "creator_id": "User1",
            "creator_type": "user",
            "reference_id": "WSI1",
            "reference_type": "wsi",
            "npp_created": 500.00,
            "type": "point",
            "coordinates": [1000, 2000],
        }

        r = requests.post(f"{base_url_v1}/annotations", json=annot)
        assert r.status_code == 201
        response = r.json()
        annot_id = response["id"]

        # get v1
        r = requests.get(f"{base_url_v1}/annotations/{str(annot_id)}")
        assert r.status_code == 200

        # get v3
        r = requests.get(f"{base_url_v3}/annotations/{str(annot_id)}")
        assert r.status_code == 404

        # post annot v3
        r = requests.post(f"{base_url_v3}/annotations", json=annot)
        assert r.status_code == 201
        response = r.json()
        annot_id = response["id"]

        # get v1
        r = requests.get(f"{base_url_v1}/annotations/{str(annot_id)}")
        assert r.status_code == 404

        # get v3
        r = requests.get(f"{base_url_v3}/annotations/{str(annot_id)}")
        assert r.status_code == 200

        # get all v1
        r = requests.get(f"{base_url_v1}/annotations")
        assert r.status_code == 200
        response = r.json()
        assert response["item_count"] == 1
        assert len(response["items"]) == 1

        # get all v3
        r = requests.get(f"{base_url_v3}/annotations")
        assert r.status_code == 200
        response = r.json()
        assert response["item_count"] == 1
        assert len(response["items"]) == 1
