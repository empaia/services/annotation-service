import requests

from annotation_service.models.v3.commons import IdObject

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_count_for_id, lock_for_job
from .commons.utils_create import (
    create_arrow,
    create_circle,
    create_line,
    create_point,
    create_polygon,
    create_rectangle,
)


def annotations_delete(up):
    base_url = get_base_url(up.annot_container)

    delete_point(base_url, up)
    delete_line(base_url, up)
    delete_arrow(base_url, up)
    delete_circle(base_url, up)
    delete_rectangle(base_url, up)
    delete_polygon(base_url, up)
    delete_locked(base_url, up)

    clear_database(up)


def delete_point(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)

    response = requests.delete(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(a_id)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, a_id, "annotations") == 0


def delete_line(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_line(conn)

    response = requests.delete(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(a_id)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, a_id, "annotations") == 0


def delete_arrow(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_arrow(conn)

    response = requests.delete(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(a_id)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, a_id, "annotations") == 0


def delete_circle(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_circle(conn)

    response = requests.delete(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(a_id)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, a_id, "annotations") == 0


def delete_rectangle(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_rectangle(conn)

    response = requests.delete(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(a_id)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, a_id, "annotations") == 0


def delete_polygon(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_polygon(conn)

    response = requests.delete(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(a_id)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, a_id, "annotations") == 0


def delete_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
    lock_for_job(base_url, "job1", a_id, "annotations")

    response = requests.delete(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 423

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
