from uuid import uuid4

import requests

from annotation_service.models.v3.annotation.annotations import AnnotationListResponse, PointAnnotation

from ..commons.setup_tests import POST_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, get_count
from .commons.utils_requests import post_annotation

CR_ID = uuid4()
REF_ID = uuid4()
POST_COUNT = 100

POINT_ANNOT = {
    "name": "Name",
    "description": "desc",
    "creator_id": str(CR_ID),
    "creator_type": "job",
    "reference_id": str(REF_ID),
    "reference_type": "wsi",
    "npp_created": 45.67,
    "type": "point",
    "coordinates": [100, 200],
}


def annotations_post(up):
    base_url = get_base_url(up.annot_container)
    post_point(base_url, up)
    post_line(base_url, up)
    post_arrow(base_url, up)
    post_circle(base_url, up)
    post_rectangle(base_url, up)
    post_polygon(base_url, up)
    post_external_id(base_url)
    post_point_without_optional(base_url, up)
    post_line_without_optional(base_url, up)
    post_arrow_without_optional(base_url, up)
    post_circle_without_optional(base_url, up)
    post_rectangle_without_optional(base_url, up)
    post_polygon_without_optional(base_url, up)
    post_point_centroid(base_url, up)
    post_line_centroid(base_url, up)
    post_arrow_centroid(base_url, up)
    post_circle_centroid(base_url, up)
    post_rectangle_centroid(base_url, up)
    post_polygon_centroid(base_url, up)
    post_polygon_many_single(base_url, up)
    post_polygon_many(base_url, up)
    post_polygon_many_over_limit(base_url)
    post_check_validation_error_type(base_url)
    post_check_validation_error_reference(base_url)
    post_invalid_type_in_list(base_url)

    clear_database(up)


def post_point(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "point",
        "coordinates": [100, 200],
    }
    post_annotation(up, base_url, annot, "point")


def post_line(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "line",
        "coordinates": [[100, 200], [300, 400]],
    }
    post_annotation(up, base_url, annot, "line")


def post_arrow(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "arrow",
        "head": [100, 200],
        "tail": [300, 400],
    }
    post_annotation(up, base_url, annot, "arrow")


def post_circle(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "circle",
        "center": [100, 200],
        "radius": 100,
    }
    post_annotation(up, base_url, annot, "circle")


def post_rectangle(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "rectangle",
        "upper_left": [100, 200],
        "width": 200,
        "height": 300,
    }
    post_annotation(up, base_url, annot, "rectangle")


def post_polygon(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "polygon",
        "coordinates": [[100, 200], [300, 400], [500, 600]],
    }
    post_annotation(up, base_url, annot, "polygon")


def post_external_id(base_url):
    annot = {
        "name": "Nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "type": "point",
        "npp_created": 45.67,
        "coordinates": [100, 200],
    }
    annot["id"] = str(uuid4())
    params = {"external_ids": True}
    post_response = requests.post(f"{base_url}/annotations", params=params, json=annot)
    assert post_response.status_code == 201

    response_json = post_response.json()
    PointAnnotation.model_validate(response_json)
    assert response_json["id"] != annot["id"]


def post_point_without_optional(base_url, up):
    annot = {
        "name": "Nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "type": "point",
        "npp_created": 45.67,
        "coordinates": [100, 200],
    }
    post_annotation(up, base_url, annot, "point")


def post_line_without_optional(base_url, up):
    annot = {
        "name": "Nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "type": "line",
        "npp_created": 45.67,
        "coordinates": [[100, 200], [300, 400]],
    }
    post_annotation(up, base_url, annot, "line")


def post_arrow_without_optional(base_url, up):
    annot = {
        "name": "Nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "type": "arrow",
        "npp_created": 45.67,
        "head": [100, 200],
        "tail": [300, 400],
    }
    post_annotation(up, base_url, annot, "arrow")


def post_circle_without_optional(base_url, up):
    annot = {
        "name": "Nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "type": "circle",
        "npp_created": 45.67,
        "center": [100, 200],
        "radius": 100,
    }
    post_annotation(up, base_url, annot, "circle")


def post_rectangle_without_optional(base_url, up):
    annot = {
        "name": "Nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "type": "rectangle",
        "npp_created": 45.67,
        "upper_left": [100, 200],
        "width": 200,
        "height": 300,
    }
    post_annotation(up, base_url, annot, "rectangle")


def post_polygon_without_optional(base_url, up):
    annot = {
        "name": "Nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "type": "polygon",
        "npp_created": 45.67,
        "coordinates": [[100, 200], [300, 400], [500, 600]],
    }
    post_annotation(up, base_url, annot, "polygon")


def post_point_centroid(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "point",
        "coordinates": [100, 200],
        "centroid": [100, 100],
    }
    post_annotation(up, base_url, annot, "point")


def post_line_centroid(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "line",
        "coordinates": [[100, 200], [300, 400]],
        "centroid": [100, 100],
    }
    post_annotation(up, base_url, annot, "line")


def post_arrow_centroid(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "arrow",
        "head": [100, 200],
        "tail": [300, 400],
        "centroid": [100, 100],
    }
    post_annotation(up, base_url, annot, "arrow")


def post_circle_centroid(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "circle",
        "center": [100, 200],
        "radius": 100,
        "centroid": [100, 100],
    }
    post_annotation(up, base_url, annot, "circle")


def post_rectangle_centroid(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "rectangle",
        "upper_left": [100, 200],
        "width": 200,
        "height": 300,
        "centroid": [100, 100],
    }
    post_annotation(up, base_url, annot, "rectangle")


def post_polygon_centroid(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "polygon",
        "coordinates": [[100, 200], [200, 200], [200, 100]],
        "centroid": [100, 100],
    }
    post_annotation(up, base_url, annot, "polygon")


def post_polygon_many_single(base_url, up):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "polygon",
        "coordinates": [[100, 200], [300, 400], [500, 600]],
    }

    with get_db(up.db_container).begin() as conn:
        count_before = get_count(conn, "annotations")

    for _ in range(POST_COUNT):
        post_annotation(up, base_url, annot, "polygon")

    with get_db(up.db_container).begin() as conn:
        count_after = get_count(conn, "annotations")
        assert count_after - count_before == POST_COUNT


def post_polygon_many(base_url, up):
    annots = []
    for i in range(POST_COUNT):
        annots.append(
            {
                "name": f"Nice annotation_{i}",
                "description": "Very nice annotation",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "polygon",
                "coordinates": [[100, 200], [300, 400], [500, 600]],
            }
        )
    post_annots = {"items": annots}

    with get_db(up.db_container).begin() as conn:
        count_before = get_count(conn, "annotations")

    post_response = requests.post(f"{base_url}/annotations", json=post_annots)
    assert post_response.status_code == 201
    response = post_response.json()
    AnnotationListResponse.model_validate(response)
    assert len(response["items"]) == POST_COUNT

    with get_db(up.db_container).begin() as conn:
        count_after = get_count(conn, "annotations")
        assert count_after - count_before == POST_COUNT

    # check for correct order of response
    for i in range(POST_COUNT):
        assert annots[i]["name"] == response["items"][i]["name"]


def post_polygon_many_over_limit(base_url):
    annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "polygon",
        "coordinates": [[100, 200], [300, 400], [500, 600]],
    }

    post_count = POST_LIMIT + 1

    annots = []
    for _ in range(post_count):
        annots.append(annot)
    post_annots = {"items": annots}
    post_response = requests.post(f"{base_url}/annotations", json=post_annots)
    assert post_response.status_code == 413


def post_check_validation_error_type(base_url):
    POINT_ANNOT["type"] = "line"
    post_response = requests.post(f"{base_url}/annotations", json=POINT_ANNOT)
    assert post_response.status_code == 422


def post_check_validation_error_reference(base_url):
    POINT_ANNOT["type"] = "point"
    POINT_ANNOT["reference_type"] = "no wsi"
    post_response = requests.post(f"{base_url}/annotations", json=POINT_ANNOT)
    assert post_response.status_code == 422


def post_invalid_type_in_list(base_url):
    POINT_ANNOT["reference_type"] = "wsi"
    point_annot_invalid = {
        "name": "Name",
        "description": "desc",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "line",
        "coordinates": [100, 200],
    }

    annots = [POINT_ANNOT, point_annot_invalid]
    post_annots = {"items": annots}
    post_response = requests.post(f"{base_url}/annotations", json=post_annots)
    assert post_response.status_code == 422
