from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_job_count, get_job_count_for_item, lock_for_job
from .commons.utils_count import get_job_count_in_classes
from .commons.utils_create import create_point

CR_ID = "User 1"
CR_TYPE = "job"
REF_ID = "Slide 1"
NPP_C = 10.0
NPP_V = None


def annotations_put(up):
    base_url = get_base_url(up.annot_container)
    lock_annotation(base_url, up)

    clear_database(up)


def lock_annotation(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, [100, 200])

    job_id_one = "job1"
    job_id_two = "job2"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_one, a_id, "annotations")
        assert get_job_count_for_item(conn, a_id, "annotations") == 1
        assert get_job_count_in_classes(conn, a_id) == 1

        lock_for_job(base_url, job_id_one, a_id, "annotations")
        assert get_job_count_for_item(conn, a_id, "annotations") == 1
        assert get_job_count_in_classes(conn, a_id) == 1

        lock_for_job(base_url, job_id_two, a_id, "annotations")
        assert get_job_count_for_item(conn, a_id, "annotations") == 2
        assert get_job_count_in_classes(conn, a_id) == 2
        assert get_job_count(conn, a_id, job_id_one, "annotations") == 1
        assert get_job_count(conn, a_id, job_id_two, "annotations") == 1
