from uuid import uuid4

import requests

from annotation_service.models.v3.annotation.annotations import (
    ArrowAnnotation,
    CircleAnnotation,
    LineAnnotation,
    PointAnnotation,
    PolygonAnnotation,
    RectangleAnnotation,
)
from annotation_service.models.v3.annotation.classes import Class

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database
from .commons.utils_check import check_base_annotation, check_class
from .commons.utils_count import get_class_count
from .commons.utils_create import (
    create_arrow,
    create_circle,
    create_line,
    create_point,
    create_point_with_classes,
    create_point_with_classes_different_creator,
    create_polygon,
    create_rectangle,
    get_large_polygons,
)

CR_ID = "User 1"
CR_TYPE = "job"
REF_ID = "WSI 1"
NPP_C = 10.0
NPP_V = None
POINT = [100, 200]
LINE = [[100, 100], [200, 200]]
HEAD = [100, 200]
TAIL = [300, 400]
RADIUS = 100
WIDTH = 200
HEIGHT = 250
POLYGON = [[100, 100], [200, 100], [200, 200], [100, 200]]


def annotations_get_by_id(up):
    base_url = get_base_url(up.annot_container)
    get_point(base_url, up)
    get_line(base_url, up)
    get_arrow(base_url, up)
    get_circle(base_url, up)
    get_rectangle(base_url, up)
    get_polygon(base_url, up)
    get_large_polygon(base_url, up)
    get_with_classes(base_url, up)
    get_with_classes_different_creator(base_url, up)
    get_non_existent_annotation(base_url)

    clear_database(up)


def get_point(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    annot = response.json()
    PointAnnotation.model_validate(annot)
    check_base_annotation(annot, a_id, "point", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot["coordinates"] == POINT


def get_line(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_line(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, LINE)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    annot = response.json()
    LineAnnotation.model_validate(annot)
    check_base_annotation(annot, a_id, "line", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot["coordinates"] == LINE


def get_arrow(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_arrow(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, HEAD, TAIL)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    annot = response.json()
    ArrowAnnotation.model_validate(annot)
    check_base_annotation(annot, a_id, "arrow", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot["head"] == HEAD
    assert annot["tail"] == TAIL


def get_circle(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_circle(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT, RADIUS)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    annot = response.json()
    CircleAnnotation.model_validate(annot)
    check_base_annotation(annot, a_id, "circle", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot["center"] == POINT
    assert annot["radius"] == RADIUS


def get_rectangle(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_rectangle(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT, WIDTH, HEIGHT)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    annot = response.json()
    RectangleAnnotation.model_validate(annot)
    check_base_annotation(annot, a_id, "rectangle", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot["upper_left"] == POINT
    assert annot["width"] == WIDTH
    assert annot["height"] == HEIGHT


def get_polygon(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_polygon(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POLYGON)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    annot = response.json()
    PolygonAnnotation.model_validate(annot)
    check_base_annotation(annot, a_id, "polygon", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot["coordinates"] == POLYGON


def get_large_polygon(base_url, up):
    points = get_large_polygons()["ONE"]["coordinates"]

    with get_db(up.db_container).begin() as conn:
        a_id = create_polygon(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, points)

    response = requests.get(f"{base_url}/annotations/{a_id}")
    assert response.status_code == 200
    annot = response.json()
    PolygonAnnotation.model_validate(annot)
    check_base_annotation(annot, a_id, "polygon", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot["coordinates"] == points


def get_with_classes(base_url, up):
    class_count = 2
    with get_db(up.db_container).begin() as conn:
        a_id = create_point_with_classes(conn, class_count, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT)

    request_params_with = {"with_classes": True}
    response_with = requests.get(f"{base_url}/annotations/{a_id}", params=request_params_with)
    assert response_with.status_code == 200
    annot_with = response_with.json()
    PointAnnotation.model_validate(annot_with)
    check_base_annotation(annot_with, a_id, "point", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot_with["coordinates"] == POINT
    assert len(annot_with["classes"]) == class_count
    for cl in annot_with["classes"]:
        Class.model_validate(cl)
        check_class(cl, CR_ID, CR_TYPE, a_id)

    response_without_classes = requests.get(f"{base_url}/annotations/{a_id}")
    assert response_without_classes.status_code == 200
    annot_without = response_without_classes.json()
    PointAnnotation.model_validate(annot_without)
    check_base_annotation(annot_without, a_id, "point", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot_without["coordinates"] == POINT
    assert "classes" not in annot_without


def get_with_classes_different_creator(base_url, up):
    class_count = 2
    with get_db(up.db_container).begin() as conn:
        a_id = create_point_with_classes_different_creator(
            conn, class_count, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT
        )
        assert get_class_count(conn, a_id) == class_count * 2

    request_params_with = {"with_classes": True}
    response_with = requests.get(f"{base_url}/annotations/{a_id}", params=request_params_with)
    assert response_with.status_code == 200
    annot_with = response_with.json()
    PointAnnotation.model_validate(annot_with)
    check_base_annotation(annot_with, a_id, "point", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot_with["coordinates"] == POINT
    assert len(annot_with["classes"]) == class_count * 2
    for cl in annot_with["classes"]:
        Class.model_validate(cl)

    response_without_classes = requests.get(f"{base_url}/annotations/{a_id}")
    assert response_without_classes.status_code == 200
    annot_without = response_without_classes.json()
    PointAnnotation.model_validate(annot_without)
    check_base_annotation(annot_without, a_id, "point", CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
    assert annot_without["coordinates"] == POINT
    assert "classes" not in annot_without


def get_non_existent_annotation(base_url):
    response = requests.get(f"{base_url}/annotations/{uuid4()}")
    assert response.status_code == 404
