import requests
from sqlalchemy.sql import text

from annotation_service.models.v3.annotation.jobs import JobLock
from annotation_service.models.v3.commons import UniqueReferences

from ..commons.setup_tests import get_db

TYPE_TABLE_MAPPING = {
    "annotations": "v3_a_annotations",
    "classes": "v3_cl_classes",
    "collections": "v3_c_collections",
    "primitives": "v3_p_primitives",
}


TYPE_JOB_TABLE_MAPPING = {
    "annotations": "v3_j_job_classes",
    "classes": "v3_j_job_classes",
    "collections": "v3_j_job_collections",
    "primitives": "",
    "slides": "v3_j_job_slides",
}


TYPE_JOB_COLUMN_MAPPING = {
    "annotations": "annot_id",
    "classes": "class_id",
    "collections": "collection_id",
    "primitives": "primitive_id",
    "slides": "slide_id",
}

BASE_ITEM_COUNT_SQL = """
SELECT COUNT(*) as count
FROM {table_name}
WHERE {column_name}=:item_id;
"""

BASE_ITEM_JOB_COUNT_SQL = """
SELECT COUNT(*) as count
FROM {table_name}
WHERE {column_name}=:item_id AND job_id=:job_id;
"""

BASE_ITEM_COUNT_FOR_JOB = """
SELECT COUNT(*) as count
FROM {table_name}
WHERE job_id=:job_id;
"""


def clear_database(up):
    with get_db(up.db_container).begin() as conn:
        conn.execute(
            text(
                """
                TRUNCATE TABLE v3_j_job_slides;
                TRUNCATE TABLE v3_j_job_primitives CASCADE;
                TRUNCATE TABLE v3_j_job_collections CASCADE;
                TRUNCATE TABLE v3_j_job_classes CASCADE;
                TRUNCATE TABLE v3_c_collection_slides CASCADE;
                TRUNCATE TABLE v3_c_collections CASCADE;
                TRUNCATE TABLE v3_p_primitives CASCADE;
                TRUNCATE TABLE v3_cl_classes CASCADE;
                TRUNCATE TABLE v3_a_annotations CASCADE;
                """
            )
        )


def lock_for_job(base_url, job_id, element_id, route):
    response = requests.put(f"{base_url}/jobs/{job_id}/lock/{route}/{element_id}")
    assert response.status_code == 200
    result = response.json()
    JobLock.model_validate(result)


def aggregate_job(base_url, job_id):
    response = requests.put(f"{base_url[:-3]}/private/v3/db/{job_id}/aggregate-job")
    assert response.status_code == 200


def get_count(conn, i_type):
    return conn.execute(
        text(
            f"""
            SELECT COUNT(*) as count
            FROM {TYPE_TABLE_MAPPING[i_type]};
            """
        )
    ).first()["count"]


def get_count_for_id(conn, i_id, i_type):
    return conn.execute(
        text(
            f"""
            SELECT COUNT(*) as count
            FROM {TYPE_TABLE_MAPPING[i_type]}
            WHERE id=:id;
            """
        ),
        id=i_id,
    ).first()["count"]


def get_job_count_for_item(conn, item_id, i_type):
    type_job_count_mapping = {
        "annotations": get_job_count_for_annotation,
        "classes": get_job_count_for_class,
        "collections": get_job_count_for_collection,
        "primitives": get_job_count_for_primitive,
        "slides": get_job_count_for_slide,
    }

    return type_job_count_mapping[i_type](conn, str(item_id))


def get_job_count(conn, item_id, job_id, i_type):
    type_job_count_mapping = {
        "annotations": get_count_for_job_annotation,
        "classes": get_count_for_job_class,
        "collections": get_count_for_job_collection,
        "primitives": get_count_for_job_primitive,
        "slides": get_count_for_job_slide,
    }

    return type_job_count_mapping[i_type](conn, job_id, str(item_id))


def get_item_count_for_job(conn, job_id, i_type):
    type_job_count_mapping = {
        "annotations": get_annotation_count_for_job,
        "classes": get_class_count_for_job,
        "collections": get_collection_count_for_job,
        "primitives": get_primitive_count_for_job,
        "slides": get_slide_count_for_job,
    }

    return type_job_count_mapping[i_type](conn, job_id)


def query_unique_references(base_url, query, route):
    r = requests.put(f"{base_url}/{route}/query/unique-references", json=query)
    assert r.status_code == 200
    result = r.json()
    UniqueReferences.model_validate(result)
    return result


def query_unique_references_items(base_url, collection_id, query):
    r = requests.put(f"{base_url}/collections/{collection_id}/items/query/unique-references", json=query)
    assert r.status_code == 200
    result = r.json()
    UniqueReferences.model_validate(result)
    return result


def get_count_for_job_collection(conn, job_id, c_id):
    return conn.execute(
        text(
            """
            SELECT
                count(v3jjc.job_id) AS count
            FROM
                v3_j_job_collections AS v3jjc
            WHERE
                v3jjc.collection_id=:c_id
                AND v3jjc.job_id=:job_id;
            """
        ),
        c_id=c_id,
        job_id=job_id,
    ).first()["count"]


def get_job_count_for_collection(conn, c_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(v3jjc.job_id) AS count
            FROM
                v3_j_job_collections AS v3jjc
            WHERE
                v3jjc.collection_id=:c_id;
            """
        ),
        c_id=c_id,
    ).first()["count"]


def get_collection_count_for_job(conn, job_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(v3jjc.collection_id) AS count
            FROM
                v3_j_job_collections AS v3jjc
            WHERE
                v3jjc.job_id=:job_id;
            """
        ),
        job_id=job_id,
    ).first()["count"]


# ANNOTATIONS


def get_count_for_job_annotation(conn, job_id, a_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjcl.job_id
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.annot_id=:a_id
                            AND v3jjcl.job_id=:job_id
                    )
                    UNION
                    (
                        SELECT
                            job_id
                        FROM
                            v3_j_job_collections
                        WHERE
                            collection_id IN (
                                SELECT
                                    unnest(collection_ids)
                                FROM
                                    v3_a_annotations
                                WHERE
                                    id=:a_id
                            )
                            AND job_id=:job_id
                    )
                ) AS jobs;
            """
        ),
        a_id=a_id,
        job_id=job_id,
    ).first()["count"]


def get_job_count_for_annotation(conn, a_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjcl.job_id
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.annot_id=:a_id
                    )
                    UNION
                    (
                        SELECT
                            job_id
                        FROM
                            v3_j_job_collections
                        WHERE
                            collection_id IN (
                                SELECT
                                    unnest(collection_ids)
                                FROM
                                    v3_a_annotations
                                WHERE
                                    id=:a_id
                            )
                    )
                ) AS jobs;
            """
        ),
        a_id=a_id,
    ).first()["count"]


def get_annotation_count_for_job(conn, job_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(annotations.*) AS COUNT
            FROM
                (
                    (
                        SELECT
                            v3jjcl.annot_id
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.job_id=:job_id
                    )
                    UNION
                    (
                        WITH annotation_collections_jobs AS (
                            SELECT
                                array_agg(collection_id) AS ids
                            FROM
                                v3_j_job_collections
                            WHERE
                                item_type = ANY(
                                    ARRAY ['point', 'line', 'arrow', 'circle', 'rectangle', 'polygon']
                                )
                                AND job_id=:job_id
                        )
                        SELECT
                            id
                        FROM
                            v3_a_annotations
                        WHERE
                            collection_ids && (
                                SELECT
                                    ids
                                FROM
                                    annotation_collections_jobs
                            )
                    )
                ) AS annotations;
            """
        ),
        job_id=job_id,
    ).first()["count"]


# CLASSES


def get_count_for_job_class(conn, job_id, cl_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjcl.job_id
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.class_id=:cl_id
                            AND v3jjcl.job_id=:job_id
                    )
                    UNION
                    (
                        SELECT
                            job_id
                        FROM
                            v3_j_job_collections
                        WHERE
                            collection_id IN (
                                SELECT
                                    unnest(collection_ids)
                                FROM
                                    v3_cl_classes
                                WHERE
                                    id=:cl_id
                            )
                            AND job_id=:job_id
                    )
                ) AS jobs;
            """
        ),
        cl_id=cl_id,
        job_id=job_id,
    ).first()["count"]


def get_job_count_for_class(conn, cl_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjcl.job_id
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.class_id=:cl_id
                    )
                    UNION
                    (
                        SELECT
                            job_id
                        FROM
                            v3_j_job_collections
                        WHERE
                            collection_id IN (
                                SELECT
                                    unnest(collection_ids)
                                FROM
                                    v3_cl_classes
                                WHERE
                                    id=:cl_id
                            )
                    )
                ) AS jobs;
            """
        ),
        cl_id=cl_id,
    ).first()["count"]


def get_class_count_for_job(conn, job_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(classes.*) AS COUNT
            FROM
                (
                    (
                        SELECT
                            v3jjcl.class_id
                        FROM
                            v3_j_job_classes AS v3jjcl
                        WHERE
                            v3jjcl.job_id=:job_id
                    )
                    UNION
                    (
                        WITH class_collections_jobs AS (
                            SELECT
                                array_agg(collection_id) AS ids
                            FROM
                                v3_j_job_collections
                            WHERE
                                item_type = 'class'
                                AND job_id=:job_id
                        )
                        SELECT
                            id::TEXT
                        FROM
                            v3_cl_classes
                        WHERE
                            collection_ids && (
                                SELECT
                                    ids
                                FROM
                                    class_collections_jobs
                            )
                    )
                ) AS classes;
            """
        ),
        job_id=job_id,
    ).first()["count"]


# PRIMITIVES


def get_count_for_job_primitive(conn, job_id, p_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjp.job_id
                        FROM
                            v3_j_job_primitives AS v3jjp
                        WHERE
                            v3jjp.primitive_id=:p_id
                            AND v3jjp.job_id=:job_id
                    )
                    UNION
                    (
                        SELECT
                            job_id
                        FROM
                            v3_j_job_collections
                        WHERE
                            collection_id IN (
                                SELECT
                                    unnest(collection_ids)
                                FROM
                                    v3_p_primitives
                                WHERE
                                    id=:p_id
                            )
                            AND job_id=:job_id
                    )
                ) AS jobs;
            """
        ),
        p_id=p_id,
        job_id=job_id,
    ).first()["count"]


def get_job_count_for_primitive(conn, p_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjp.job_id
                        FROM
                            v3_j_job_primitives AS v3jjp
                        WHERE
                            v3jjp.primitive_id=:p_id
                    )
                    UNION
                    (
                        SELECT
                            job_id
                        FROM
                            v3_j_job_collections
                        WHERE
                            collection_id IN (
                                SELECT
                                    unnest(collection_ids)
                                FROM
                                    v3_p_primitives
                                WHERE
                                    id=:p_id
                            )
                    )
                ) AS jobs;
            """
        ),
        p_id=p_id,
    ).first()["count"]


def get_primitive_count_for_job(conn, job_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(primitives.*) AS COUNT
            FROM
                (
                    (
                        SELECT
                            v3jjp.primitive_id
                        FROM
                            v3_j_job_primitives AS v3jjp
                        WHERE
                            v3jjp.job_id=:job_id
                    )
                    UNION
                    (
                        WITH primitive_collections_jobs AS (
                            SELECT
                                array_agg(collection_id) AS ids
                            FROM
                                v3_j_job_collections
                            WHERE
                                item_type = ANY(
                                    ARRAY ['integer', 'float', 'bool', 'string']
                                )
                                AND job_id=:job_id
                        )
                        SELECT
                            id
                        FROM
                            v3_p_primitives
                        WHERE
                            collection_ids && (
                                SELECT
                                    ids
                                FROM
                                    primitive_collections_jobs
                            )
                    )
                ) AS primitives;
            """
        ),
        job_id=job_id,
    ).first()["count"]


# SLIDES


def get_count_for_job_slide(conn, job_id, s_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjs.job_id
                        FROM
                            v3_j_job_slides AS v3jjs
                        WHERE
                            v3jjs.slide_id=:s_id
                            AND v3jjs.job_id=:job_id
                    )
                    UNION
                    (
                        WITH slide_collections_jobs AS (
                            SELECT
                                collection_id,
                                job_id
                            FROM
                                v3_j_job_collections
                            WHERE
                                item_type = 'wsi'
                                AND job_id=:job_id
                        )
                        SELECT
                            scj.job_id
                        FROM
                            v3_c_collection_slides AS v3cc,
                            slide_collections_jobs AS scj
                        WHERE
                            v3cc.slide_id=:s_id
                            AND v3cc.collection_id IN (
                                SELECT
                                    collection_id
                                FROM
                                    slide_collections_jobs
                            )
                    )
                ) AS jobs;
            """
        ),
        s_id=s_id,
        job_id=job_id,
    ).first()["count"]


def get_job_count_for_slide(conn, s_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(jobs.*) AS count
            FROM
                (
                    (
                        SELECT
                            v3jjs.job_id
                        FROM
                            v3_j_job_slides AS v3jjs
                        WHERE
                            v3jjs.slide_id=:s_id
                    )
                    UNION
                    (
                        WITH slide_collections_jobs AS (
                            SELECT
                                collection_id,
                                job_id
                            FROM
                                v3_j_job_collections
                            WHERE
                                item_type = 'wsi'
                        )
                        SELECT
                            scj.job_id
                        FROM
                            v3_c_collection_slides AS v3cc,
                            slide_collections_jobs AS scj
                        WHERE
                            v3cc.slide_id=:s_id
                            AND v3cc.collection_id IN (
                                SELECT
                                    collection_id
                                FROM
                                    slide_collections_jobs
                            )
                    )
                ) AS jobs;
            """
        ),
        s_id=s_id,
    ).first()["count"]


def get_slide_count_for_job(conn, job_id):
    return conn.execute(
        text(
            """
            SELECT
                COUNT(slides.*) AS COUNT
            FROM
                (
                    (
                        SELECT
                            v3jjs.slide_id
                        FROM
                            v3_j_job_slides AS v3jjs
                        WHERE
                            v3jjs.job_id=:job_id
                    )
                    UNION
                    (
                        WITH slide_collections_jobs AS (
                            SELECT
                                collection_id,
                                job_id
                            FROM
                                v3_j_job_collections
                            WHERE
                                item_type = 'wsi'
                                AND job_id=:job_id
                        )
                        SELECT
                            v3cc.slide_id
                        FROM
                            v3_c_collection_slides AS v3cc
                        WHERE
                            v3cc.collection_id IN (
                                SELECT
                                    collection_id
                                FROM
                                    slide_collections_jobs
                            )
                    )
                ) AS slides;
            """
        ),
        job_id=job_id,
    ).first()["count"]
