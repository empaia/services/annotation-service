from uuid import uuid4

from ..commons.setup_tests import get_base_url
from ..commons.utils import clear_database
from .commons.utils_requests import post_external, post_many_external

CR_ID = uuid4()
REF_ID = uuid4()
POST_COUNT = 10


def annotations_post_external(up):
    base_url = get_base_url(up.annot_container)

    post_point(base_url)
    post_line(base_url)
    post_arrow(base_url)
    post_circle(base_url)
    post_rectangle(base_url)
    post_polygon(base_url)
    post_polygon_many(base_url, up)
    clear_database(up)


def post_point(base_url):
    annot = {
        "id": str(uuid4()),
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "point",
        "coordinates": [100, 200],
    }
    post_external(base_url, annot, "annotations")


def post_line(base_url):
    annot = {
        "id": str(uuid4()),
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "line",
        "coordinates": [[100, 200], [300, 400]],
    }
    post_external(base_url, annot, "annotations")


def post_arrow(base_url):
    annot = {
        "id": str(uuid4()),
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "arrow",
        "head": [100, 200],
        "tail": [300, 400],
    }
    post_external(base_url, annot, "annotations")


def post_circle(base_url):
    annot = {
        "id": str(uuid4()),
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "circle",
        "center": [100, 200],
        "radius": 100,
    }
    post_external(base_url, annot, "annotations")


def post_rectangle(base_url):
    annot = {
        "id": str(uuid4()),
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "rectangle",
        "upper_left": [100, 200],
        "width": 200,
        "height": 300,
    }
    post_external(base_url, annot, "annotations")


def post_polygon(base_url):
    annot = {
        "id": str(uuid4()),
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "npp_viewing": [1.0, 100],
        "type": "polygon",
        "coordinates": [[100, 200], [300, 400], [500, 600]],
    }
    post_external(base_url, annot, "annotations")


def post_polygon_many(base_url, up):
    annots = []
    for _ in range(10):
        a = {
            "id": str(uuid4()),
            "name": "Nice annotation",
            "description": "Very nice annotation",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(REF_ID),
            "reference_type": "wsi",
            "npp_created": 45.67,
            "type": "polygon",
            "coordinates": [[100, 200], [300, 400], [500, 600]],
        }
        annots.append(a)

    post_annots = {"items": annots}
    post_many_external(up, base_url, post_annots, "annotations", POST_COUNT)
