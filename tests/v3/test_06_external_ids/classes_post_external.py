from uuid import uuid4

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_requests import post_external, post_many_external

CR_ID = uuid4()
REF_ID = uuid4()
POST_COUNT = 10


def classes_post_external(up):
    base_url = get_base_url(up.annot_container)

    post(base_url, up)
    post_many(base_url, up)

    clear_database(up)


def post(base_url, up):
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)

    new_class = {
        "id": str(uuid4()),
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(annot_id),
        "reference_type": "annotation",
    }
    post_external(base_url, new_class, "classes")


def post_many(base_url, up):
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)

    classes = []
    for _ in range(POST_COUNT):
        cl = {
            "id": str(uuid4()),
            "type": "class",
            "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(annot_id),
            "reference_type": "annotation",
        }
        classes.append(cl)

    class_post = {"items": classes}
    post_many_external(up, base_url, class_post, "classes", POST_COUNT)
