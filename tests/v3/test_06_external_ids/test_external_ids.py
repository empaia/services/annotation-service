import docker

from ..commons.setup_tests import Up
from ..commons.utils import clear_database
from .annotations_post_external import annotations_post_external
from .classes_post_external import classes_post_external
from .collections_add_item_external import collections_add_item_external
from .collections_add_items_external import collections_add_items_external
from .collections_post_external import collections_post_external
from .primitives_post_external import primitives_post_external


def test_external_ids():
    client = docker.from_env()
    with Up(client, allow_external_ids=True) as up:
        annotations_post_external(up)
        classes_post_external(up)
        primitives_post_external(up)
        collections_post_external(up)
        collections_add_item_external(up)
        collections_add_items_external(up)

        clear_database(up)
