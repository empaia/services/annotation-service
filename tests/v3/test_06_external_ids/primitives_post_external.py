from uuid import uuid4

from ..commons.setup_tests import get_base_url
from ..commons.utils import clear_database
from .commons.utils_requests import post_external, post_many_external

CR_ID = uuid4()
REF_ID = uuid4()
POST_COUNT = 10


def primitives_post_external(up):
    base_url = get_base_url(up.annot_container)

    post_integer(base_url)
    post_float(base_url)
    post_bool(base_url)
    post_string(base_url)
    post_many(base_url, up)

    clear_database(up)


def post_integer(base_url):
    primitive = {
        "id": str(uuid4()),
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_external(base_url, primitive, "primitives")


def post_float(base_url):
    primitive = {
        "id": str(uuid4()),
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_external(base_url, primitive, "primitives")


def post_bool(base_url):
    primitive = {
        "id": str(uuid4()),
        "name": "Nice",
        "description": "Nice",
        "value": True,
        "type": "bool",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_external(base_url, primitive, "primitives")


def post_string(base_url):
    primitive = {
        "id": str(uuid4()),
        "name": "Nice",
        "description": "Nice",
        "value": "value",
        "type": "string",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_external(base_url, primitive, "primitives")


def post_many(base_url, up):
    primitives = []
    for _ in range(POST_COUNT):
        p = {
            "id": str(uuid4()),
            "name": "Nice",
            "description": "Nice",
            "value": 42,
            "type": "integer",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(REF_ID),
            "reference_type": "wsi",
        }
        primitives.append(p)

    primitive_post = {"items": primitives}
    post_many_external(up, base_url, primitive_post, "primitives", POST_COUNT)
