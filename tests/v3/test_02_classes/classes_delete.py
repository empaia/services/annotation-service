import requests

from annotation_service.models.v3.commons import IdObject

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_count_for_id, lock_for_job
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_create import create_class

CR_ID = "User 1"
CR_TYPE = "job"


def classes_delete(up):
    base_url = get_base_url(up.annot_container)

    delete(base_url, up)
    delete_locked(base_url, up)

    clear_database(up)


def delete(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE)

    response = requests.delete(f"{base_url}/classes/{cl_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(cl_id)

    response = requests.get(f"{base_url}/classes/{cl_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, cl_id, "classes") == 0


def delete_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE)
    lock_for_job(base_url, "job1", cl_id, "classes")

    response = requests.delete(f"{base_url}/classes/{cl_id}")
    assert response.status_code == 423

    response = requests.get(f"{base_url}/classes/{cl_id}")
    assert response.status_code == 200
