import requests

from annotation_service.models.v3.annotation.classes import ClassList


def query_classes(base_url, query, i_count, i_len, params=None, ucv_count=None):
    response = requests.put(f"{base_url}/classes/query", json=query, params=params)
    assert response.status_code == 200
    result = response.json()
    ClassList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len

    if params and "with_unique_class_values" in params and params["with_unique_class_values"] and ucv_count:
        assert len(result["unique_class_values"]) == ucv_count

    return result


def get_classes(base_url, i_count, i_len):
    response = requests.get(f"{base_url}/classes")
    assert response.status_code == 200
    result = response.json()
    ClassList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len

    return result
