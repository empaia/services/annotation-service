from typing import List

from pydantic import UUID4
from sqlalchemy.sql import text


def create_class(
    conn,
    a_id,
    cr_id="User",
    cr_type="user",
    value="value",
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    result = conn.execute(
        text(
            """
            INSERT INTO v3_cl_classes(
                type,
                creator_id,
                creator_type,
                reference_id,
                reference_type,
                value,
                is_locked,
                collection_ids
            )
            VALUES(
                :type,
                :creator_id,
                :creator_type,
                :reference_id,
                :reference_type,
                :value,
                :is_locked,
                :collection_ids
            ) RETURNING id;
            """
        ),
        type="class",
        creator_id=cr_id,
        creator_type=cr_type,
        reference_id=a_id,
        reference_type="annotation",
        value=value,
        is_locked=is_locked,
        collection_ids=collection_ids if collection_ids else [],
    ).first()

    return result["id"]
