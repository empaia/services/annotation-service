from uuid import uuid4

import requests

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, lock_for_job
from .commons.utils_create import create_integer_collection, create_shallow

ITEM_COUNT = 10


def collections_delete_item(up):
    base_url = get_base_url(up.annot_container)

    delete_item(base_url, up)
    delete_locked(base_url, up)
    delete_empty(base_url, up)

    clear_database(up)


def delete_item(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_integer_collection(conn, i_count=ITEM_COUNT)

    c_response = requests.get(f"{base_url}/collections/{c_id}")
    assert c_response.status_code == 200
    c_response = c_response.json()
    first_item_id = c_response["items"][0]["id"]
    response = requests.delete(f"{base_url}/collections/{c_id}/items/{first_item_id}")
    assert response.status_code == 200
    c_response = requests.get(f"{base_url}/collections/{c_id}")
    assert c_response.status_code == 200
    c_response = c_response.json()
    assert c_response["item_count"] == ITEM_COUNT - 1
    assert len(c_response["items"]) == ITEM_COUNT - 1


def delete_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_integer_collection(conn, i_count=ITEM_COUNT)
    lock_for_job(base_url, "job_id_1", c_id, "collections")

    c_response = requests.get(f"{base_url}/collections/{c_id}")
    assert c_response.status_code == 200
    c_response = c_response.json()
    first_item_id = c_response["items"][0]["id"]
    response = requests.delete(f"{base_url}/collections/{c_id}/items/{first_item_id}")
    assert response.status_code == 423
    c_response = requests.get(f"{base_url}/collections/{c_id}")
    assert c_response.status_code == 200
    c_response = c_response.json()
    assert c_response["item_count"] == ITEM_COUNT
    assert len(c_response["items"]) == ITEM_COUNT


def delete_empty(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    response = requests.delete(f"{base_url}/collections/{c_id}/items/{uuid4()}")
    assert response.status_code == 404
