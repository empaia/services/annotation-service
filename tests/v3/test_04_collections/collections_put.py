from uuid import uuid4

import requests
from sqlalchemy.sql import text

from annotation_service.models.v3.annotation.collections import Collection

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_item_count_for_job, get_job_count, get_job_count_for_item, lock_for_job
from .commons.utils_check import NAME, check_collection_post_put
from .commons.utils_create import (
    create_class_collection,
    create_integer_collection,
    create_nested_collection,
    create_point_collection,
    create_shallow,
    create_wsi_collection,
)

ITEM_COUNT = 10


def collections_put(up):
    base_url = get_base_url(up.annot_container)

    put(base_url, up)
    put_locked(base_url, up)
    put_not_found(base_url)
    put_creator_error(base_url, up)
    put_reference_error(base_url, up)
    put_creator_type_error(base_url, up)
    put_reference_type_error(base_url, up)
    put_item_type_error(base_url, up)
    lock_point(base_url, up)
    lock_integer(base_url, up)
    lock_class(base_url, up)
    lock_wsi(base_url, up)
    lock_nested(base_url, up)

    clear_database(up)


def put(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User",
        "creator_type": "user",
        "reference_id": "WSI",
        "reference_type": "wsi",
        "item_type": "integer",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, result["id"], new_collection)


def put_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")
    lock_for_job(base_url, "job_id_1", c_id, "collections")

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User",
        "creator_type": "user",
        "reference_id": "WSI",
        "reference_type": "wsi",
        "item_type": "integer",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 423

    with get_db(up.db_container).begin() as conn:
        result = conn.execute(
            text(
                """
                SELECT *
                FROM v3_c_collections
                WHERE id=:c_id
                """
            ),
            c_id=c_id,
        ).first()

        assert result["name"] == NAME


def put_not_found(base_url):
    c_id = uuid4()

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User",
        "creator_type": "user",
        "reference_id": "WSI",
        "reference_type": "wsi",
        "item_type": "integer",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 404


def put_creator_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User 2",
        "creator_type": "user",
        "reference_id": "WSI",
        "reference_type": "wsi",
        "item_type": "integer",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 405


def put_reference_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User",
        "creator_type": "user",
        "reference_id": "WSI 2",
        "reference_type": "wsi",
        "item_type": "integer",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 405


def put_creator_type_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User",
        "creator_type": "job",
        "reference_id": "WSI",
        "reference_type": "wsi",
        "item_type": "integer",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 405


def put_reference_type_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User",
        "creator_type": "user",
        "reference_id": "WSI",
        "reference_type": "annotation",
        "item_type": "integer",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 405


def put_item_type_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    new_collection = {
        "type": "collection",
        "name": "Nice Collection 2",
        "description": "Nice Collection 2",
        "creator_id": "User",
        "creator_type": "user",
        "reference_id": "WSI",
        "reference_type": "wsi",
        "item_type": "float",
    }

    response = requests.put(f"{base_url}/collections/{c_id}", json=new_collection)
    assert response.status_code == 405


def lock_point(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_point_collection(conn, ITEM_COUNT)

    job_id_1 = "job1a"
    job_id_2 = "job2a"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_1, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        # double lock - Error
        response = requests.put(f"{base_url}/jobs/{job_id_1}/lock/collections/{c_id}")
        assert response.status_code == 400
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        lock_for_job(base_url, job_id_2, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 2
        assert get_job_count(conn, c_id, job_id_1, "collections") == 1
        assert get_job_count(conn, c_id, job_id_2, "collections") == 1
        assert get_item_count_for_job(conn, job_id_1, "annotations") == ITEM_COUNT
        assert get_item_count_for_job(conn, job_id_2, "annotations") == ITEM_COUNT


def lock_integer(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_integer_collection(conn, ITEM_COUNT)

    job_id_1 = "job1p"
    job_id_2 = "job2p"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_1, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        # double lock - Error
        response = requests.put(f"{base_url}/jobs/{job_id_1}/lock/collections/{c_id}")
        assert response.status_code == 400
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        lock_for_job(base_url, job_id_2, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 2
        assert get_job_count(conn, c_id, job_id_1, "collections") == 1
        assert get_job_count(conn, c_id, job_id_2, "collections") == 1
        assert get_item_count_for_job(conn, job_id_1, "primitives") == ITEM_COUNT
        assert get_item_count_for_job(conn, job_id_2, "primitives") == ITEM_COUNT


def lock_class(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_class_collection(conn, ITEM_COUNT)

    job_id_1 = "job1cl"
    job_id_2 = "job2cl"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_1, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        # double lock - Error
        response = requests.put(f"{base_url}/jobs/{job_id_1}/lock/collections/{c_id}")
        assert response.status_code == 400
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        lock_for_job(base_url, job_id_2, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 2
        assert get_job_count(conn, c_id, job_id_1, "collections") == 1
        assert get_job_count(conn, c_id, job_id_2, "collections") == 1
        assert get_item_count_for_job(conn, job_id_1, "classes") == ITEM_COUNT
        assert get_item_count_for_job(conn, job_id_2, "classes") == ITEM_COUNT


def lock_wsi(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_wsi_collection(conn, ITEM_COUNT)

    job_id_1 = "job1s"
    job_id_2 = "job2s"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_1, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        # double lock - Error
        response = requests.put(f"{base_url}/jobs/{job_id_1}/lock/collections/{c_id}")
        assert response.status_code == 400
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        lock_for_job(base_url, job_id_2, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 2
        assert get_job_count(conn, c_id, job_id_1, "collections") == 1
        assert get_job_count(conn, c_id, job_id_2, "collections") == 1
        assert get_item_count_for_job(conn, job_id_1, "slides") == ITEM_COUNT
        assert get_item_count_for_job(conn, job_id_2, "slides") == ITEM_COUNT


def lock_nested(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_nested_collection(conn, ITEM_COUNT)

    job_id_1 = "job1nc"
    job_id_2 = "job2nc"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_1, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        # double lock - Error
        response = requests.put(f"{base_url}/jobs/{job_id_1}/lock/collections/{c_id}")
        assert response.status_code == 400
        assert get_job_count_for_item(conn, c_id, "collections") == 1

        lock_for_job(base_url, job_id_2, c_id, "collections")
        assert get_job_count_for_item(conn, c_id, "collections") == 2
        assert get_job_count(conn, c_id, job_id_1, "collections") == 1
        assert get_job_count(conn, c_id, job_id_2, "collections") == 1
        assert get_item_count_for_job(conn, job_id_1, "collections") == ITEM_COUNT + 1
        assert get_item_count_for_job(conn, job_id_2, "collections") == ITEM_COUNT + 1
        assert get_item_count_for_job(conn, job_id_1, "primitives") == ITEM_COUNT * ITEM_COUNT
        assert get_item_count_for_job(conn, job_id_2, "primitives") == ITEM_COUNT * ITEM_COUNT
