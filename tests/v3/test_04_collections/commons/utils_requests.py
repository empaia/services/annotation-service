import requests

from annotation_service.models.v3.annotation.annotations import AnnotationListResponse, PointAnnotation
from annotation_service.models.v3.annotation.classes import Class, ClassListResponse
from annotation_service.models.v3.annotation.collections import (
    Collection,
    CollectionList,
    ItemQueryList,
    SlideItem,
    SlideItemList,
)
from annotation_service.models.v3.annotation.primitives import IntegerPrimitive, PrimitiveList

POST_SINGLE_TYPE_MAPPING = {
    "integer": IntegerPrimitive,
    "point": PointAnnotation,
    "class": Class,
    "wsi": SlideItem,
    "collection": Collection,
}

POST_LIST_TYPE_MAPPING = {
    "integers": PrimitiveList,
    "points": AnnotationListResponse,
    "classes": ClassListResponse,
    "wsis": SlideItemList,
    "collections": CollectionList,
}


def query_collections(base_url, query, i_count, i_len, params=None):
    response = requests.put(f"{base_url}/collections/query", json=query, params=params)
    assert response.status_code == 200
    result = response.json()
    CollectionList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
    return result


def get_collections(base_url, i_count, i_len, ci_count, ci_len, params=None):
    response = requests.get(f"{base_url}/collections", params=params)
    assert response.status_code == 200
    result = response.json()
    CollectionList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
    for coll in result["items"]:
        if len(coll["items"]) > 0:
            assert coll["item_count"] == ci_count
            assert len(coll["items"]) == ci_len
            for item in coll["items"]:
                assert "id" in item
    return result


def query_items(base_url, c_id, query, i_count, i_len, params=None):
    response = requests.put(f"{base_url}/collections/{c_id}/items/query", json=query, params=params)
    assert response.status_code == 200
    result = response.json()
    ItemQueryList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
    return result


def post_item(base_url, c_id, item, i_type):
    response = requests.post(f"{base_url}/collections/{c_id}/items", json=item)
    assert response.status_code == 201
    result = response.json()
    if i_type != "id":
        POST_SINGLE_TYPE_MAPPING[i_type].model_validate(result)
        assert "id" in result
    c_response = requests.get(f"{base_url}/collections/{c_id}")
    assert c_response.status_code == 200
    c_result = c_response.json()
    assert c_result["item_count"] == 1
    return result, c_result


def post_items(base_url, c_id, items, i_type, i_count, i_count_nested=None):
    response = requests.post(f"{base_url}/collections/{c_id}/items", json=items)
    assert response.status_code == 201
    result = response.json()
    if i_type != "ids":
        POST_LIST_TYPE_MAPPING[i_type].model_validate(result)
        assert "item_count" in result
        assert result["item_count"] == i_count
    c_response = requests.get(f"{base_url}/collections/{c_id}")
    assert c_response.status_code == 200
    c_result = c_response.json()
    assert c_result["item_count"] == i_count_nested if i_count_nested is not None else i_count
    return result, c_result
