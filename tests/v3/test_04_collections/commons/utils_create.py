from typing import List
from uuid import uuid4

from pydantic import UUID4
from sqlalchemy.sql import text

from ...test_01_annotations.commons.utils_create import create_point
from ...test_02_classes.commons.utils_create import create_class
from ...test_03_primitives.commons.utils_create import create_integer
from .utils_check import DESC, NAME

CR_ID = "User"
CR_TYPE = "user"
REF_ID = "WSI"
REF_TYPE = "wsi"


def create_shallow(
    conn,
    item_type,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    result = conn.execute(
        text(
            """
            INSERT INTO v3_c_collections(
                type,
                name,
                description,
                creator_id,
                creator_type,
                reference_id,
                reference_type,
                item_type,
                is_locked,
                collection_ids
            )
            VALUES(
                :type,
                :name,
                :description,
                :creator_id,
                :creator_type,
                :reference_id,
                :reference_type,
                :item_type,
                :is_locked,
                :collection_ids
            ) RETURNING id;
            """
        ),
        type="collection",
        name=NAME,
        description=DESC,
        creator_id=cr_id,
        creator_type=cr_type,
        reference_id=ref_id,
        reference_type=ref_type,
        item_type=item_type,
        is_locked=is_locked,
        collection_ids=collection_ids if collection_ids else [],
    ).first()

    return result["id"]


def add_item(conn, c_id, s_id):
    conn.execute(
        text(
            """
            INSERT INTO v3_c_collection_slides(
                collection_id,
                slide_id
            )
            VALUES(
                :collection_id,
                :slide_id
            );
            """
        ),
        collection_id=c_id,
        slide_id=s_id,
    )


def create_integer_collection(
    conn,
    i_count=10,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    c_id = create_shallow(
        conn,
        item_type="integer",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
        collection_ids=collection_ids,
    )

    for _ in range(i_count):
        create_integer(conn, ref_id=ref_id, collection_ids=[c_id])
    return c_id


def create_point_collection(
    conn,
    i_count=10,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    c_id = create_shallow(
        conn,
        item_type="point",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
        collection_ids=collection_ids,
    )

    for _ in range(i_count):
        create_point(conn, collection_ids=[c_id])
    return c_id


def create_class_collection(
    conn,
    i_count=10,
    creator_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    c_id = create_shallow(
        conn,
        item_type="class",
        cr_id=creator_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
        collection_ids=collection_ids,
    )

    a_id = create_point(conn)
    for _ in range(i_count):
        create_class(conn, a_id, collection_ids=[c_id])
    return c_id


def create_wsi_collection(
    conn,
    i_count=10,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    c_id = create_shallow(
        conn,
        item_type="wsi",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
        collection_ids=collection_ids,
    )

    for _ in range(i_count):
        item_id = str(uuid4())
        add_item(conn, c_id, item_id)
    return c_id


def create_nested_collection(
    conn,
    i_count=10,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    c_id = create_shallow(
        conn,
        item_type="collection",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
        collection_ids=collection_ids,
    )

    for _ in range(i_count):
        create_integer_collection(conn, i_count=i_count, collection_ids=[c_id])
    return c_id


def create_nested_collection_different_references(
    conn,
    i_count=10,
    cr_id="User",
    cr_type="user",
    ref_id_1="WSI 1",
    ref_id_2="WSI 2",
    ref_type="wsi",
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    c_id = create_shallow(
        conn,
        item_type="collection",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id_1,
        ref_type=ref_type,
        is_locked=is_locked,
        collection_ids=collection_ids,
    )

    for _ in range(i_count):
        create_integer_collection(conn, i_count=i_count, ref_id=ref_id_1, collection_ids=[c_id])
        create_integer_collection(conn, i_count=i_count, ref_id=ref_id_2, collection_ids=[c_id])
    return c_id
