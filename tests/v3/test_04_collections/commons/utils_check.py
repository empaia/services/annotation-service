from sqlalchemy.sql import text

NAME = "Collection"
DESC = "Collection"


def check_collection(collection, c_id, c_itype, cr_id, cr_type, ref_id, ref_type, i_count, i_len, item_id=False):
    assert collection["id"] == str(c_id)
    assert collection["name"] == NAME
    assert collection["description"] == DESC
    assert collection["item_type"] == c_itype
    assert collection["type"] == "collection"
    assert collection["creator_id"] == cr_id
    assert collection["creator_type"] == cr_type
    assert collection["reference_id"] == str(ref_id)
    assert collection["reference_type"] == ref_type
    assert collection["item_count"] == i_count
    assert len(collection["items"]) == i_len
    if item_id:
        assert "item_ids" in collection
        assert len(collection["item_ids"]) == i_count
    else:
        assert "item_ids" not in collection


def check_collection_post_put(conn, c_id, collection):
    sql = """
    SELECT *
    FROM v3_c_collections
    WHERE id=:c_id;
    """

    result = conn.execute(
        text(sql),
        c_id=c_id,
    ).first()

    if "name" in collection:
        assert collection["name"] == result["name"]
    if "description" in collection:
        assert collection["description"] == result["description"]
    assert collection["creator_id"] == result["creator_id"]
    assert collection["creator_type"] == result["creator_type"]
    if "reference_id" in collection:
        assert collection["reference_id"] == str(result["reference_id"])
    if "reference_type" in collection:
        assert collection["reference_type"] == result["reference_type"]
    assert collection["item_type"] == result["item_type"]
