from sqlalchemy.sql import text


def get_item_count(conn, c_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM v3_c_collection_slides
            WHERE collection_id=:collection_id;
            """
        ),
        collection_id=c_id,
    ).first()["count"]
