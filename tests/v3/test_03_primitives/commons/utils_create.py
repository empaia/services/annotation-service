from typing import List

from pydantic import UUID4
from sqlalchemy.sql import text

from .utils_check import DESC, NAME, REF_TYPE

CR_ID = "User"
CR_TYPE = "user"
REF_ID = "WSI"
V_I = 42
V_F = 0.42
V_B = True
V_S = "Nice"


INSERT_BASE_SQL = """
INSERT INTO v3_p_primitives(
    type,
    name,
    description,
    creator_id,
    creator_type,
    reference_id,
    reference_type,
    is_locked,
    {type_column},
    collection_ids
)
VALUES(
    :p_type,
    :name,
    :description,
    :creator_id,
    :creator_type,
    :reference_id,
    :reference_type,
    :is_locked,
    :value,
    :collection_ids
) RETURNING id;
"""


def create_primitive(
    conn, p_type, cr_id, cr_type, ref_id, ref_type, value, is_locked, collection_ids: List[UUID4] = None
):
    type_mapping = {
        "integer": "ivalue",
        "float": "fvalue",
        "bool": "bvalue",
        "string": "svalue",
    }

    insert_statement = INSERT_BASE_SQL.format(type_column=type_mapping[p_type])
    result = conn.execute(
        text(insert_statement),
        p_type=p_type,
        name=NAME,
        description=DESC,
        creator_id=cr_id,
        creator_type=cr_type,
        reference_id=ref_id,
        reference_type=ref_type,
        is_locked=is_locked,
        value=value,
        collection_ids=collection_ids if collection_ids else [],
    ).first()
    return result["id"]


def create_integer(
    conn,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    value=V_I,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    return create_primitive(conn, "integer", cr_id, cr_type, ref_id, ref_type, value, is_locked, collection_ids)


def create_float(
    conn,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    value=V_F,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    return create_primitive(conn, "float", cr_id, cr_type, ref_id, ref_type, value, is_locked, collection_ids)


def create_bool(
    conn,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    value=V_B,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    return create_primitive(conn, "bool", cr_id, cr_type, ref_id, ref_type, value, is_locked, collection_ids)


def create_string(
    conn,
    cr_id=CR_ID,
    cr_type=CR_TYPE,
    ref_id=REF_ID,
    ref_type=REF_TYPE,
    value=V_S,
    is_locked=False,
    collection_ids: List[UUID4] = None,
):
    return create_primitive(conn, "string", cr_id, cr_type, ref_id, ref_type, value, is_locked, collection_ids)
