import requests

from annotation_service.models.v3.annotation.primitives import (
    BoolPrimitive,
    FloatPrimitive,
    IntegerPrimitive,
    PrimitiveList,
    StringPrimitive,
)

from ...commons.setup_tests import get_db
from .utils_check import check_primitive_post_put

VALIDATION_MAPPING = {
    "integer": IntegerPrimitive,
    "float": FloatPrimitive,
    "bool": BoolPrimitive,
    "string": StringPrimitive,
}


def post_primitive(up, base_url, primitive, p_type):
    post_response = requests.post(f"{base_url}/primitives", json=primitive)
    assert post_response.status_code == 201
    response_json = post_response.json()
    VALIDATION_MAPPING[p_type].model_validate(response_json)
    with get_db(up.db_container).begin() as conn:
        check_primitive_post_put(conn, response_json["id"], primitive)


def query_primitives(base_url, query, i_count, i_len, params=None):
    response = requests.put(f"{base_url}/primitives/query", json=query, params=params)
    assert response.status_code == 200
    result = response.json()
    PrimitiveList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
    return result


def get_primitives(base_url, i_count, i_len, params=None):
    response = requests.get(f"{base_url}/primitives", params=params)
    assert response.status_code == 200
    result = response.json()
    PrimitiveList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
    return result
