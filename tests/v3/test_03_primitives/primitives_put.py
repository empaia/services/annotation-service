from uuid import uuid4

import requests
from sqlalchemy.sql import text

from annotation_service.models.v3.annotation.primitives import (
    BoolPrimitive,
    FloatPrimitive,
    IntegerPrimitive,
    StringPrimitive,
)

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_job_count, get_job_count_for_item, lock_for_job
from .commons.utils_check import check_primitive_post_put
from .commons.utils_create import REF_TYPE, V_B, V_F, V_I, V_S, create_bool, create_float, create_integer, create_string

CR_ID = "User 1"
CR_TYPE = "job"
REF_ID = "Slide 1"


def primitives_put(up):
    base_url = get_base_url(up.annot_container)

    put_integer(base_url, up)
    put_locked(base_url, up)
    put_float(base_url, up)
    put_bool(base_url, up)
    put_string(base_url, up)
    put_not_found(base_url)
    put_creator_error(base_url, up)
    put_reference_error(base_url, up)
    put_creator_type_error(base_url, up)
    put_type_error(base_url, up)
    lock_primitive(base_url, up)

    clear_database(up)


def put_integer(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_I)

    new_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 43,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_integer)
    assert response.status_code == 200
    result = response.json()
    IntegerPrimitive.model_validate(result)
    with get_db(up.db_container).begin() as conn:
        check_primitive_post_put(conn, result["id"], new_integer)


def put_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_I, True)
    lock_for_job(base_url, "job_id_1", p_id, "primitives")

    new_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 43,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_integer)
    assert response.status_code == 423

    with get_db(up.db_container).begin() as conn:
        result = conn.execute(
            text(
                """
                SELECT *
                FROM v3_p_primitives
                WHERE id=:id;
                """
            ),
            id=p_id,
        ).first()
        assert V_I == result["ivalue"]


def put_float(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_float(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_F)

    new_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.43,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_float)
    assert response.status_code == 200
    result = response.json()
    FloatPrimitive.model_validate(result)
    with get_db(up.db_container).begin() as conn:
        check_primitive_post_put(conn, result["id"], new_float)


def put_bool(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_bool(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_B)

    new_bool = {
        "name": "Nice",
        "description": "Nice",
        "value": False,
        "type": "bool",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_bool)
    assert response.status_code == 200
    result = response.json()
    BoolPrimitive.model_validate(result)
    with get_db(up.db_container).begin() as conn:
        check_primitive_post_put(conn, result["id"], new_bool)


def put_string(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_string(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_S)

    new_string = {
        "name": "Nice",
        "description": "Nice",
        "value": "Not Nice",
        "type": "string",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_string)
    assert response.status_code == 200
    result = response.json()
    StringPrimitive.model_validate(result)
    with get_db(up.db_container).begin() as conn:
        check_primitive_post_put(conn, result["id"], new_string)


def put_not_found(base_url):
    p_id = uuid4()
    new_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 43,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_integer)
    assert response.status_code == 404


def put_creator_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, 42)

    new_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 43,
        "type": "integer",
        "creator_id": "User 2",
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_integer)
    assert response.status_code == 405


def put_reference_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, 42)

    new_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 43,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": "Slide 2",
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_integer)
    assert response.status_code == 405


def put_creator_type_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, 42)

    new_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 43,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "user",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_integer)
    assert response.status_code == 405


def put_type_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, 42)

    new_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "user",
        "reference_id": str(REF_ID),
        "reference_type": REF_TYPE,
    }

    response = requests.put(f"{base_url}/primitives/{p_id}", json=new_float)
    assert response.status_code == 405


def lock_primitive(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_I)

    job_id_1 = "job1"
    job_id_2 = "job2"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_1, p_id, "primitives")
        assert get_job_count_for_item(conn, p_id, "primitives") == 1

        lock_for_job(base_url, job_id_1, p_id, "primitives")
        assert get_job_count_for_item(conn, p_id, "primitives") == 1

        lock_for_job(base_url, job_id_2, p_id, "primitives")
        assert get_job_count_for_item(conn, p_id, "primitives") == 2
        assert get_job_count(conn, p_id, job_id_1, "primitives") == 1
        assert get_job_count(conn, p_id, job_id_2, "primitives") == 1
