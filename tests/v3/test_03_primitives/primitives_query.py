import requests

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import aggregate_job, clear_database, get_count, lock_for_job, query_unique_references
from ..test_01_annotations.commons.utils_create import create_point
from ..test_04_collections.commons.utils_create import create_shallow
from .commons.utils_create import create_bool, create_float, create_integer, create_string
from .commons.utils_requests import query_primitives

CR_ID_1 = "User 1"
CR_ID_2 = "User 2"
CR_TYPE = "job"
REF_ID_1 = "WSI 1"
REF_ID_2 = "WSI 2"


def primitives_query(up):
    base_url = get_base_url(up.annot_container)

    query_type_missing(base_url)

    fill_db(up)

    query_creators(base_url)
    query_references(base_url)
    query_types(base_url)
    query_all(base_url)
    query_skip_limit(base_url)
    query_over_limit(base_url, up)
    query_all_jobs(base_url, up)
    query_unique_references_prim(base_url, up)
    query_invalid_primitive_ids(base_url)

    clear_database(up)


def query_type_missing(base_url):
    query_integer = {"types": ["integer"]}
    query_primitives(base_url, query_integer, 0, 0)


def query_creators(base_url):
    query_1 = {"creators": [CR_ID_1]}
    query_primitives(base_url, query_1, 12, 12)

    query_2 = {"creators": [CR_ID_1, CR_ID_2]}
    query_primitives(base_url, query_2, 20, 20)

    query_3 = {}
    query_primitives(base_url, query_3, 20, 20)


def query_references(base_url):
    query_1 = {"references": [REF_ID_1, None]}
    query_primitives(base_url, query_1, 12, 12)

    query_2 = {"references": [REF_ID_1, REF_ID_2]}
    query_primitives(base_url, query_2, 16, 16)

    query_3 = {"references": [None]}
    query_primitives(base_url, query_3, 4, 4)

    query_4 = {"references": ["WSI 3", None]}
    query_primitives(base_url, query_4, 4, 4)


def query_types(base_url):
    query_1 = {"types": ["integer"]}
    query_primitives(base_url, query_1, 5, 5)

    query_2 = {"types": ["float"]}
    query_primitives(base_url, query_2, 5, 5)

    query_3 = {"types": ["integer", "float"]}
    query_primitives(base_url, query_3, 10, 10)


def query_all(base_url):
    query_1 = {"types": ["integer"], "creators": [CR_ID_1], "references": [REF_ID_1]}
    query_primitives(base_url, query_1, 1, 1)

    query_2 = {
        "types": ["integer", "float", "bool", "string"],
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
    }
    query_primitives(base_url, query_2, 4, 4)

    query_3 = {
        "types": ["integer"],
        "creators": [CR_ID_1, CR_ID_2],
        "references": [REF_ID_1, REF_ID_2, None],
    }
    query_primitives(base_url, query_3, 5, 5)


def query_skip_limit(base_url):
    params = {"skip": 0, "limit": 2}
    query_1 = {
        "creators": [CR_ID_1, CR_ID_2],
        "references": [REF_ID_1, REF_ID_2],
        "types": ["integer", "float"],
    }
    query_primitives(base_url, query_1, 8, 2, params=params)


def query_over_limit(base_url, up):
    fill_db(up, repeat_count=6)

    with get_db(up.db_container).begin() as conn:
        count = get_count(conn, "primitives")

    query_1 = {}
    response = requests.put(f"{base_url}/primitives/query", json=query_1)
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({count}) exceeds server limit of {ITEM_LIMIT}."


def query_all_jobs(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id_1 = create_integer(conn, CR_ID_1, CR_TYPE, REF_ID_1)
        p_id_2 = create_integer(conn, CR_ID_1, CR_TYPE, REF_ID_2)
        p_id_3 = create_integer(conn, CR_ID_2, CR_TYPE, REF_ID_1)
        p_id_4 = create_integer(conn, CR_ID_2, CR_TYPE, REF_ID_2)

    job_id_1 = "job1"
    job_id_2 = "job2"

    lock_for_job(base_url, job_id_1, p_id_1, "primitives")
    lock_for_job(base_url, job_id_1, p_id_2, "primitives")
    lock_for_job(base_url, job_id_1, p_id_3, "primitives")
    lock_for_job(base_url, job_id_1, p_id_4, "primitives")
    lock_for_job(base_url, job_id_2, p_id_1, "primitives")
    lock_for_job(base_url, job_id_2, p_id_2, "primitives")
    lock_for_job(base_url, job_id_2, p_id_3, "primitives")
    lock_for_job(base_url, job_id_2, p_id_4, "primitives")

    aggregate_job(base_url, job_id_1)
    aggregate_job(base_url, job_id_2)

    query_1 = {
        "types": ["integer"],
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": [job_id_1],
    }
    query_primitives(base_url, query_1, 1, 1)

    query_2 = {
        "types": ["integer", "float", "bool", "string"],
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": [job_id_1],
    }
    query_primitives(base_url, query_2, 1, 1)

    query_3 = {
        "types": ["integer"],
        "creators": [CR_ID_1, CR_ID_2],
        "references": [REF_ID_1, REF_ID_2, None],
        "jobs": [job_id_1],
    }
    query_primitives(base_url, query_3, 4, 4)

    query_4 = {
        "references": [None],
        "types": ["float"],
        "jobs": [job_id_1],
    }
    query_primitives(base_url, query_4, 0, 0)

    query_5 = {"jobs": [job_id_1, job_id_2]}
    query_primitives(base_url, query_5, 4, 4)

    query_6 = {"references": [REF_ID_1], "jobs": ["no_job_should_match"]}
    query_primitives(base_url, query_6, 0, 0)

    query_7 = {"primitives": [str(p_id_1), str(p_id_2), str(p_id_3), str(p_id_4)], "jobs": [job_id_1]}
    query_primitives(base_url, query_7, 4, 4)


def query_unique_references_prim(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        c_id = create_shallow(conn, "wsi")
        p_id_1 = create_integer(conn, CR_ID_1, CR_TYPE, "WSI1", "wsi")
        p_id_2 = create_integer(conn, CR_ID_1, CR_TYPE, a_id, "annotation")
        p_id_3 = create_integer(conn, CR_ID_2, CR_TYPE, c_id, "collection")
        p_id_4 = create_integer(conn, CR_ID_2, CR_TYPE, None, None)

    # queries without jobs
    query_one = {}
    r = query_unique_references(base_url, query_one, "primitives")
    assert len(r["annotation"]) == 1
    assert len(r["collection"]) == 1
    assert len(r["wsi"]) == 3
    assert r["contains_items_without_reference"]

    query_two = {"types": ["integer"]}
    r = query_unique_references(base_url, query_two, "primitives")
    assert len(r["annotation"]) == 1
    assert len(r["collection"]) == 1
    assert len(r["wsi"]) == 3
    assert r["contains_items_without_reference"]

    query_three = {"creators": [CR_ID_1], "types": ["float"]}
    r = query_unique_references(base_url, query_three, "primitives")
    assert len(r["annotation"]) == 0
    assert len(r["collection"]) == 0
    assert len(r["wsi"]) == 2
    assert r["contains_items_without_reference"]

    job_id_1 = "job1_ur"
    job_id_2 = "job2_ur"

    lock_for_job(base_url, job_id_1, p_id_1, "primitives")
    lock_for_job(base_url, job_id_1, p_id_2, "primitives")
    lock_for_job(base_url, job_id_1, p_id_3, "primitives")
    lock_for_job(base_url, job_id_1, p_id_4, "primitives")
    lock_for_job(base_url, job_id_2, p_id_1, "primitives")
    lock_for_job(base_url, job_id_2, p_id_2, "primitives")
    lock_for_job(base_url, job_id_2, p_id_3, "primitives")
    lock_for_job(base_url, job_id_2, p_id_4, "primitives")

    aggregate_job(base_url, job_id_1)
    aggregate_job(base_url, job_id_2)

    # queries with jobs
    query_one = {"jobs": [job_id_1]}
    r = query_unique_references(base_url, query_one, "primitives")
    assert len(r["annotation"]) == 1
    assert len(r["collection"]) == 1
    assert len(r["wsi"]) == 1
    assert r["contains_items_without_reference"]

    query_two = {"jobs": [job_id_1], "creators": [CR_ID_1]}
    r = query_unique_references(base_url, query_two, "primitives")
    assert len(r["annotation"]) == 1
    assert len(r["collection"]) == 0
    assert len(r["wsi"]) == 1
    assert not r["contains_items_without_reference"]

    query_three = {"jobs": [job_id_1], "creators": [CR_ID_2]}
    r = query_unique_references(base_url, query_three, "primitives")
    assert len(r["annotation"]) == 0
    assert len(r["collection"]) == 1
    assert len(r["wsi"]) == 0
    assert r["contains_items_without_reference"]


def query_invalid_primitive_ids(base_url):
    query = {"primitives": ["some_invalid_id"]}
    response = requests.put(f"{base_url}/primitives/query", json=query)
    assert response.status_code == 422
    assert response.json()["detail"] == "ERROR: Value for 'primitives' must be of type UUID4."


def fill_db(up, repeat_count=1):
    with get_db(up.db_container).begin() as conn:
        for _ in range(repeat_count):
            create_integer(conn, CR_ID_1, CR_TYPE, REF_ID_1, "wsi", 42)
            create_integer(conn, CR_ID_1, CR_TYPE, REF_ID_2, "wsi", 42)
            create_integer(conn, CR_ID_2, CR_TYPE, REF_ID_1, "wsi", 42)
            create_integer(conn, CR_ID_2, CR_TYPE, REF_ID_2, "wsi", 42)

            create_float(conn, CR_ID_1, CR_TYPE, REF_ID_1, "wsi", 0.42)
            create_float(conn, CR_ID_1, CR_TYPE, REF_ID_2, "wsi", 0.42)
            create_float(conn, CR_ID_2, CR_TYPE, REF_ID_1, "wsi", 0.42)
            create_float(conn, CR_ID_2, CR_TYPE, REF_ID_2, "wsi", 0.42)

            create_bool(conn, CR_ID_1, CR_TYPE, REF_ID_1, "wsi", True)
            create_bool(conn, CR_ID_1, CR_TYPE, REF_ID_2, "wsi", True)
            create_bool(conn, CR_ID_2, CR_TYPE, REF_ID_1, "wsi", True)
            create_bool(conn, CR_ID_2, CR_TYPE, REF_ID_2, "wsi", True)

            create_string(conn, CR_ID_1, CR_TYPE, REF_ID_1, "wsi", "Nice")
            create_string(conn, CR_ID_1, CR_TYPE, REF_ID_2, "wsi", "Nice")
            create_string(conn, CR_ID_2, CR_TYPE, REF_ID_1, "wsi", "Nice")
            create_string(conn, CR_ID_2, CR_TYPE, REF_ID_2, "wsi", "Nice")

            create_integer(conn, CR_ID_1, CR_TYPE, None, None, 42)
            create_float(conn, CR_ID_1, CR_TYPE, None, None, 0.42)
            create_bool(conn, CR_ID_1, CR_TYPE, None, None, True)
            create_string(conn, CR_ID_1, CR_TYPE, None, None, "Nice")
