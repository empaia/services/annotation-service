import requests

from annotation_service.models.v3.annotation.primitives import (
    BoolPrimitive,
    FloatPrimitive,
    IntegerPrimitive,
    StringPrimitive,
)

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database
from .commons.utils_create import (
    DESC,
    NAME,
    REF_TYPE,
    V_F,
    V_I,
    V_S,
    create_bool,
    create_float,
    create_integer,
    create_string,
)
from .commons.utils_requests import get_primitives

CR_ID = "User 1"
CR_TYPE = "job"
REF_ID = "WSI 1"
P_COUNT = 10


def primitives_get_all(up):
    base_url = get_base_url(up.annot_container)

    get_all_empty(base_url)
    get_all_filled(base_url, up)
    get_all_over_limit(base_url, up)

    clear_database(up)


def get_all_empty(base_url):
    get_primitives(base_url, 0, 0)


def get_all_filled(base_url, up):
    p_ids = []
    with get_db(up.db_container).begin() as conn:
        for _ in range(P_COUNT):
            p_ids.append(create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE))
            p_ids.append(create_float(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE))
            p_ids.append(create_bool(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE))
            p_ids.append(create_string(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE))

    result = get_primitives(base_url, P_COUNT * 4, P_COUNT * 4)
    for p in result["items"]:
        assert p["name"] == NAME
        assert p["description"] == DESC
        assert p["creator_id"] == CR_ID
        assert p["creator_type"] == CR_TYPE
        assert p["reference_id"] == REF_ID
        assert p["reference_type"] == REF_TYPE

        p_type = p["type"]
        if p_type == "integer":
            IntegerPrimitive.model_validate(p)
            assert p["value"] == V_I
        elif p_type == "float":
            FloatPrimitive.model_validate(p)
            assert p["value"] == V_F
        elif p_type == "bool":
            BoolPrimitive.model_validate(p)
            assert p["value"]
        elif p_type == "string":
            StringPrimitive.model_validate(p)
            assert p["value"] == V_S
        else:
            assert 0 == 1

        p_id = p["id"]
        p_response = requests.get(f"{base_url}/primitives/{p_id}")
        assert p_response.status_code == 200
        assert p == p_response.json()


def get_all_over_limit(base_url, up):
    count = ITEM_LIMIT - 4 * P_COUNT + 1
    p_ids = []
    with get_db(up.db_container).begin() as conn:
        for _ in range(count):
            p_ids.append(create_integer(conn, CR_ID, CR_TYPE, REF_ID, "wsi"))

    assert len(p_ids) == count

    response = requests.get(f"{base_url}/primitives")
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({ITEM_LIMIT + 1}) exceeds server limit of {ITEM_LIMIT}."
