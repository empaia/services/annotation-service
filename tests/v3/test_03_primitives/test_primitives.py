import docker

from ..commons.setup_tests import Up
from ..commons.utils import clear_database
from .primitives_delete import primitives_delete
from .primitives_get_all import primitives_get_all
from .primitives_get_by_id import primitives_get_by_id
from .primitives_post import primitives_post
from .primitives_put import primitives_put
from .primitives_query import primitives_query


def test_primitives():
    client = docker.from_env()
    with Up(client) as up:
        primitives_get_by_id(up)
        primitives_get_all(up)
        primitives_post(up)
        primitives_put(up)
        primitives_query(up)
        primitives_delete(up)

        clear_database(up)
