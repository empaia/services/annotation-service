from uuid import uuid4

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, lock_for_job
from ..test_04_collections.commons.utils_create import create_wsi_collection
from .commons.utils_requests import get_root_items


def jobs_get_tree_items(up):
    base_url = get_base_url(up.annot_container)

    job_id_1 = "job1"
    job_id_2 = "job2"
    job_id_3 = "job3"
    slide_count = 5

    for _ in range(slide_count):
        slide_id = uuid4()
        lock_for_job(base_url, job_id_1, slide_id, "slides")
        lock_for_job(base_url, job_id_2, slide_id, "slides")

    with get_db(up.db_container).begin() as conn:
        c_id_1 = create_wsi_collection(conn, i_count=slide_count)
        c_id_2 = create_wsi_collection(conn, i_count=slide_count)

    lock_for_job(base_url, job_id_1, c_id_1, "collections")
    lock_for_job(base_url, job_id_2, c_id_2, "collections")

    get_root_items(base_url, job_id_1, 10, 10)
    get_root_items(base_url, job_id_2, 10, 10)
    get_root_items(base_url, job_id_3, 0, 0)

    clear_database(up)
