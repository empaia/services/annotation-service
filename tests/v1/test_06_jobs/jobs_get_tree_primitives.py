from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.jobs import PrimitiveDetails

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, lock_for_job
from ..test_03_primitives.commons.utils_create import create_float, create_integer
from ..test_04_collections.commons.utils_create import create_integer_collection
from .commons.utils_requests import get_root_primitives


def jobs_get_tree_primitives(up):
    base_url = get_base_url(up.annot_container)

    job_id_1 = uuid4()
    job_id_2 = uuid4()
    job_id_3 = uuid4()
    cr_id = uuid4()
    cr_type = "job"
    ref_id = uuid4()
    ref_type = "wsi"

    with get_db(up.db_container).begin() as conn:
        c_id = create_integer_collection(conn, 5, cr_id, cr_type, ref_id, ref_type)

        # integers - ref: wsi
        p_id_1 = create_integer(conn, cr_id, cr_type, ref_id, ref_type, 42)
        p_id_2 = create_integer(conn, cr_id, cr_type, ref_id, ref_type, 42)
        p_id_3 = create_integer(conn, cr_id, cr_type, ref_id, ref_type, 42)

        # integers - ref: None
        p_id_4 = create_integer(conn, cr_id, cr_type, None, None, 42)
        p_id_5 = create_integer(conn, cr_id, cr_type, None, None, 42)
        p_id_6 = create_integer(conn, cr_id, cr_type, None, None, 42)

        # float - ref: collection
        p_id_f = create_float(conn, cr_id, cr_type, c_id, "collection")

    lock_for_job(base_url, job_id_1, c_id, "collections")
    lock_for_job(base_url, job_id_2, c_id, "collections")
    lock_for_job(base_url, job_id_1, p_id_1, "primitives")
    lock_for_job(base_url, job_id_1, p_id_2, "primitives")
    lock_for_job(base_url, job_id_2, p_id_3, "primitives")
    lock_for_job(base_url, job_id_1, p_id_4, "primitives")
    lock_for_job(base_url, job_id_1, p_id_5, "primitives")
    lock_for_job(base_url, job_id_2, p_id_6, "primitives")
    lock_for_job(base_url, job_id_1, p_id_f, "primitives")
    lock_for_job(base_url, job_id_2, p_id_f, "primitives")

    result = get_root_primitives(base_url, job_id_1, 3, 3)

    # get details
    for p in result["items"]:
        p_id = p["id"]
        response = requests.get(f"{base_url}/jobs/{job_id_1}/tree/primitives/{p_id}/details")
        assert response.status_code == 200
        result = response.json()
        PrimitiveDetails.model_validate(result)

        if p_id == str(p_id_4) or p_id == str(p_id_5):
            assert result["reference_type"] is None
            assert result["reference_data"] is None
        else:
            assert result["reference_type"] == "collection"
            reference_data = result["reference_data"]
            assert reference_data["item_count"] == 5
            assert len(reference_data["items"]) == 5

    get_root_primitives(base_url, job_id_2, 2, 2)
    get_root_primitives(base_url, job_id_3, 0, 0)

    clear_database(up)
