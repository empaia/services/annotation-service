import requests

from annotation_service.models.v1.annotation.annotations import AnnotationList
from annotation_service.models.v1.annotation.collections import SlideList
from annotation_service.models.v1.annotation.primitives import PrimitiveList


def get_root_items(base_url, job_id, i_count, i_len):
    response = requests.get(f"{base_url}/jobs/{job_id}/tree/items")
    assert response.status_code == 200
    result = response.json()
    SlideList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len


def get_root_primitives(base_url, job_id, i_count, i_len):
    response = requests.get(f"{base_url}/jobs/{job_id}/tree/primitives")
    assert response.status_code == 200
    result = response.json()
    PrimitiveList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
    return result


def get_tree_node_items(base_url, job_id, node_type, node_id, i_count, i_len, params=None):
    response = requests.get(f"{base_url}/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/items", params=params)
    assert response.status_code == 200
    result = response.json()
    AnnotationList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
    return result


def get_tree_node_primitives(base_url, job_id, node_type, node_id, i_count, i_len):
    response = requests.get(f"{base_url}/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/primitives")
    assert response.status_code == 200
    result = response.json()
    PrimitiveList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len
