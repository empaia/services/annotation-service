from uuid import uuid4

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, lock_for_job
from ..test_03_primitives.commons.utils_create import create_integer
from ..test_04_collections.commons.utils_create import create_integer_collection
from .commons.utils_requests import get_tree_node_primitives


def jobs_get_tree_node_primitives(up):
    base_url = get_base_url(up.annot_container)

    job_id_1 = uuid4()
    job_id_2 = uuid4()
    job_id_3 = uuid4()
    cr_id = uuid4()
    cr_type = "job"
    ref_id = uuid4()
    ref_type = "wsi"
    item_count = 5

    with get_db(up.db_container).begin() as conn:
        p_id_1 = create_integer(conn, cr_id, cr_type, ref_id, ref_type, 42)
        p_id_2 = create_integer(conn, cr_id, cr_type, ref_id, ref_type, 42)
        p_id_3 = create_integer(conn, cr_id, cr_type, ref_id, ref_type, 42)
        p_id_4 = create_integer(conn, cr_id, cr_type, None, None, 42)
        p_id_5 = create_integer(conn, cr_id, cr_type, None, None, 42)
        p_id_6 = create_integer(conn, cr_id, cr_type, None, None, 42)
        c_id_1 = create_integer_collection(conn, item_count, cr_id, cr_type, ref_id, ref_type)
        c_id_2 = create_integer_collection(conn, item_count, cr_id, cr_type, ref_id, ref_type)

    lock_for_job(base_url, job_id_1, p_id_1, "primitives")
    lock_for_job(base_url, job_id_1, p_id_2, "primitives")
    lock_for_job(base_url, job_id_2, p_id_3, "primitives")
    lock_for_job(base_url, job_id_1, p_id_4, "primitives")
    lock_for_job(base_url, job_id_1, p_id_5, "primitives")
    lock_for_job(base_url, job_id_2, p_id_6, "primitives")
    lock_for_job(base_url, job_id_1, c_id_1, "collections")
    lock_for_job(base_url, job_id_2, c_id_2, "collections")

    get_tree_node_primitives(base_url, job_id_1, "wsi", ref_id, 7, 7)
    get_tree_node_primitives(base_url, job_id_2, "wsi", ref_id, 6, 6)
    get_tree_node_primitives(base_url, job_id_3, "wsi", ref_id, 0, 0)

    clear_database(up)
