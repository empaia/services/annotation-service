from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.jobs import TreeNodeSequence

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, lock_for_job
from ..test_01_annotations.commons.utils_create import create_rectangle
from .commons.utils_requests import get_tree_node_items

ITEM_COUNT = 10


def jobs_get_tree_node_items(up):
    base_url = get_base_url(up.annot_container)

    node_type_wsi(base_url, up)
    node_type_annotation(base_url, up)
    skip_limit(base_url)
    node_type_annotation_position(base_url)
    node_type_annotation_position_with_roi(base_url)

    clear_database(up)


def node_type_wsi(base_url, up):
    wsi_id = uuid4()
    job_id = uuid4()

    with get_db(up.db_container).begin() as conn:
        roi_id = create_rectangle(conn)

    annots = []
    for _ in range(ITEM_COUNT):
        annots.append(
            {
                "name": "Nice annotation",
                "reference_id": str(wsi_id),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
                "creator_id": str(job_id),
                "creator_type": "job",
            }
        )

    collection_same_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "reference_id": str(wsi_id),
        "reference_type": "wsi",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    collection_other_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "reference_id": str(roi_id),
        "reference_type": "annotation",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    collection_empty_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_same_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    post_response = requests.post(f"{base_url}/collections", json=collection_other_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    get_tree_node_items(base_url, job_id, "wsi", wsi_id, ITEM_COUNT, ITEM_COUNT)
    get_tree_node_items(base_url, uuid4(), "wsi", wsi_id, 0, 0)

    post_response = requests.post(f"{base_url}/collections", json=collection_empty_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    get_tree_node_items(base_url, job_id, "wsi", wsi_id, ITEM_COUNT * 2, ITEM_COUNT * 2)

    annot_post = {"items": annots}
    post_response = requests.post(f"{base_url}/annotations", json=annot_post)
    assert post_response.status_code == 201

    result = post_response.json()
    for annot in result["items"]:
        lock_for_job(base_url, job_id, annot["id"], "annotations")

    result = get_tree_node_items(base_url, job_id, "wsi", wsi_id, ITEM_COUNT * 3, ITEM_COUNT * 3)
    for annot in result["items"]:
        new_class = {
            "type": "class",
            "value": "value",
            "reference_id": annot["id"],
            "reference_type": "annotation",
            "creator_id": str(job_id),
            "creator_type": "job",
        }
        post_response = requests.post(f"{base_url}/classes", json=new_class)
        assert post_response.status_code == 201
        lock_for_job(base_url, job_id, post_response.json()["id"], "classes")

    result = get_tree_node_items(base_url, job_id, "wsi", wsi_id, ITEM_COUNT * 3, ITEM_COUNT * 3)
    for annot in result["items"]:
        assert len(annot["classes"]) == 1


def node_type_annotation(base_url, up):
    wsi_id = uuid4()
    job_id = uuid4()

    with get_db(up.db_container).begin() as conn:
        roi_id = create_rectangle(conn)

    annots = []
    for _ in range(ITEM_COUNT):
        annots.append(
            {
                "name": "Nice annotation",
                "reference_id": str(wsi_id),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
                "creator_id": str(job_id),
                "creator_type": "job",
            }
        )

    collection_same_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "reference_id": str(wsi_id),
        "reference_type": "wsi",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    collection_other_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "reference_id": str(roi_id),
        "reference_type": "annotation",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    collection_empty_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_same_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    get_tree_node_items(base_url, job_id, "annotation", roi_id, 0, 0)

    post_response = requests.post(f"{base_url}/collections", json=collection_other_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    get_tree_node_items(base_url, job_id, "annotation", roi_id, ITEM_COUNT, ITEM_COUNT)

    post_response = requests.post(f"{base_url}/collections", json=collection_empty_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    get_tree_node_items(base_url, job_id, "annotation", roi_id, ITEM_COUNT, ITEM_COUNT)

    annot_post = {"items": annots}
    post_response = requests.post(f"{base_url}/annotations", json=annot_post)
    assert post_response.status_code == 201

    result = post_response.json()
    for annot in result["items"]:
        lock_for_job(base_url, job_id, annot["id"], "annotations")

    get_tree_node_items(base_url, job_id, "annotation", roi_id, ITEM_COUNT, ITEM_COUNT)


def skip_limit(base_url):
    wsi_id = uuid4()
    job_id = uuid4()

    annots = []
    for _ in range(ITEM_COUNT):
        annots.append(
            {
                "name": "Nice annotation",
                "reference_id": str(wsi_id),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
                "creator_id": str(job_id),
                "creator_type": "job",
            }
        )

    collection_same_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "reference_id": str(wsi_id),
        "reference_type": "wsi",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    for _ in range(ITEM_COUNT + 1):
        post_response = requests.post(f"{base_url}/collections", json=collection_same_ref)
        assert post_response.status_code == 201
        lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    response = requests.get(f"{base_url}/jobs/{job_id}/tree/nodes/wsi/{wsi_id}/items")
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({ITEM_COUNT * 11}) exceeds server limit of {ITEM_LIMIT}."

    params = {"skip": 0, "limit": 10}
    first_ten = get_tree_node_items(base_url, job_id, "wsi", wsi_id, ITEM_COUNT * 11, 10, params=params)

    params = {"skip": 10, "limit": 10}
    second_ten = get_tree_node_items(base_url, job_id, "wsi", wsi_id, ITEM_COUNT * 11, 10, params=params)

    params = {"skip": 0, "limit": 20}
    first_twenty = get_tree_node_items(base_url, job_id, "wsi", wsi_id, ITEM_COUNT * 11, 20, params=params)

    for i in range(10):
        assert first_twenty["items"][i]["id"] == first_ten["items"][i]["id"]
        assert first_twenty["items"][i + 10]["id"] == second_ten["items"][i]["id"]


def node_type_annotation_position(base_url):
    wsi_id_1 = "slide_1"
    wsi_id_2 = "slide_2"
    job_id = uuid4()

    lock_for_job(base_url, job_id, wsi_id_1, "slides")
    lock_for_job(base_url, job_id, wsi_id_2, "slides")

    annots = []
    for _ in range(ITEM_COUNT):
        annots.append(
            {
                "name": "Nice annotation",
                "reference_id": str(wsi_id_2),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
                "creator_id": str(job_id),
                "creator_type": "job",
            }
        )

    collection_same_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "reference_id": str(wsi_id_2),
        "reference_type": "wsi",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    collection_empty_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_same_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    post_response = requests.post(f"{base_url}/collections", json=collection_empty_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    annot_post = {"items": annots}
    post_response = requests.post(f"{base_url}/annotations", json=annot_post)
    assert post_response.status_code == 201

    result = post_response.json()
    for annot in result["items"]:
        lock_for_job(base_url, job_id, annot["id"], "annotations")

    node_result = get_tree_node_items(base_url, job_id, "wsi", wsi_id_2, ITEM_COUNT * 3, ITEM_COUNT * 3)

    for i, item in enumerate(node_result["items"]):
        item_id = item["id"]
        response = requests.get(f"{base_url}/jobs/{job_id}/tree/items/{item_id}/sequence")
        assert response.status_code == 200
        result = response.json()
        TreeNodeSequence.model_validate(result)
        wsi_details = result["node_sequence"][0]
        assert wsi_details["node_id"] == wsi_id_2
        assert wsi_details["node_type"] == "wsi"
        assert wsi_details["position"] == 1
        annot_details = result["node_sequence"][1]
        assert annot_details["node_id"] == item_id
        assert annot_details["node_type"] == "annotation"
        assert annot_details["position"] == i


def node_type_annotation_position_with_roi(base_url):
    wsi_id_1 = "slide_1"
    wsi_id_2 = "slide_2"
    job_id = uuid4()

    lock_for_job(base_url, job_id, wsi_id_1, "slides")
    lock_for_job(base_url, job_id, wsi_id_2, "slides")

    rois = []
    for _ in range(5):
        rois.append(
            {
                "name": "Nice roi",
                "reference_id": str(wsi_id_2),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
                "creator_id": "user_1",
                "creator_type": "user",
            }
        )

    roi_post = {"items": rois}
    post_response = requests.post(f"{base_url}/annotations", json=roi_post)
    assert post_response.status_code == 201

    result = post_response.json()
    for roi in result["items"]:
        lock_for_job(base_url, job_id, roi["id"], "annotations")

    sorted_rois = sorted(result["items"], key=lambda k: k["id"])
    roi_position = 2
    roi_id = sorted_rois[2]["id"]

    annots = []
    for _ in range(ITEM_COUNT):
        annots.append(
            {
                "name": "Nice annotation",
                "reference_id": str(wsi_id_2),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
                "creator_id": str(job_id),
                "creator_type": "job",
            }
        )

    collection_other_ref = {
        "type": "collection",
        "name": "Col",
        "description": "Col",
        "reference_id": roi_id,
        "reference_type": "annotation",
        "item_type": "point",
        "creator_id": str(job_id),
        "creator_type": "job",
        "items": annots,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_other_ref)
    assert post_response.status_code == 201
    lock_for_job(base_url, job_id, post_response.json()["id"], "collections")

    node_result = get_tree_node_items(base_url, job_id, "annotation", roi_id, ITEM_COUNT, ITEM_COUNT)

    for i, item in enumerate(node_result["items"]):
        item_id = item["id"]
        response = requests.get(f"{base_url}/jobs/{job_id}/tree/items/{item_id}/sequence")
        assert response.status_code == 200
        result = response.json()
        TreeNodeSequence.model_validate(result)
        wsi_details = result["node_sequence"][0]
        assert wsi_details["node_id"] == wsi_id_2
        assert wsi_details["node_type"] == "wsi"
        assert wsi_details["position"] == 1
        roi_details = result["node_sequence"][1]
        assert roi_details["node_id"] == roi_id
        assert roi_details["node_type"] == "annotation"
        assert roi_details["position"] == roi_position
        annot_details = result["node_sequence"][2]
        assert annot_details["node_id"] == item_id
        assert annot_details["node_type"] == "annotation"
        assert annot_details["position"] == i
