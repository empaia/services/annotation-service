import docker

from ..commons.setup_tests import Up
from ..commons.utils import clear_database
from .jobs_get_tree_items import jobs_get_tree_items
from .jobs_get_tree_node_items import jobs_get_tree_node_items
from .jobs_get_tree_node_primitives import jobs_get_tree_node_primitives
from .jobs_get_tree_primitives import jobs_get_tree_primitives


def test_jobs():
    client = docker.from_env()
    with Up(client) as up:
        jobs_get_tree_items(up)
        jobs_get_tree_primitives(up)
        jobs_get_tree_node_items(up)
        jobs_get_tree_node_primitives(up)

        clear_database(up)
