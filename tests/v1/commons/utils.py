import requests
from sqlalchemy.sql import text

from annotation_service.models.v1.annotation.jobs import JobLock
from annotation_service.models.v1.commons import UniqueReferences

from .setup_tests import get_db

TYPE_TABLE_MAPPING = {
    "annotations": "a_annotations",
    "classes": "cl_classes",
    "collections": "c_collections",
    "primitives": "p_primitives",
}


TYPE_JOB_TABLE_MAPPING = {
    "annotations": "j_job_annotations",
    "classes": "j_job_classes",
    "collections": "j_job_collections",
    "primitives": "j_job_primitives",
    "slides": "j_job_slides",
}

TYPE_JOB_COLUMN_MAPPING = {
    "annotations": "annotation_id",
    "classes": "class_id",
    "collections": "collection_id",
    "primitives": "primitive_id",
    "slides": "slide_id",
}

BASE_ITEM_COUNT_SQL = """
SELECT COUNT(*) as count
FROM {table_name}
WHERE {column_name}=:item_id;
"""

BASE_ITEM_JOB_COUNT_SQL = """
SELECT COUNT(*) as count
FROM {table_name}
WHERE {column_name}=:item_id AND job_id=:job_id;
"""

BASE_ITEM_COUNT_FOR_JOB = """
SELECT COUNT(*) as count
FROM {table_name}
WHERE job_id=:job_id;
"""


def clear_database(up):
    with get_db(up.db_container).begin() as conn:
        conn.execute(
            text(
                """
                TRUNCATE TABLE j_job_slides;
                TRUNCATE TABLE j_job_primitives CASCADE;
                TRUNCATE TABLE j_job_collections CASCADE;
                TRUNCATE TABLE j_job_classes CASCADE;
                TRUNCATE TABLE j_job_annotations CASCADE;
                TRUNCATE TABLE c_collection_items CASCADE;
                TRUNCATE TABLE c_collections CASCADE;
                TRUNCATE TABLE p_primitives CASCADE;
                TRUNCATE TABLE cl_classes CASCADE;
                TRUNCATE TABLE a_annotations CASCADE;
                """
            )
        )


def lock_for_job(base_url, job_id, element_id, route):
    response = requests.put(f"{base_url}/jobs/{job_id}/lock/{route}/{element_id}")
    assert response.status_code == 200
    result = response.json()
    JobLock.model_validate(result)


def get_count(conn, i_type):
    return conn.execute(
        text(
            f"""
            SELECT COUNT(*) as count
            FROM {TYPE_TABLE_MAPPING[i_type]};
            """
        )
    ).first()["count"]


def get_count_for_id(conn, i_id, i_type):
    return conn.execute(
        text(
            f"""
            SELECT COUNT(*) as count
            FROM {TYPE_TABLE_MAPPING[i_type]}
            WHERE id=:id;
            """
        ),
        id=i_id,
    ).first()["count"]


def get_job_count_for_item(conn, item_id, i_type):
    return conn.execute(
        text(
            BASE_ITEM_COUNT_SQL.format(
                table_name=TYPE_JOB_TABLE_MAPPING[i_type], column_name=TYPE_JOB_COLUMN_MAPPING[i_type]
            )
        ),
        item_id=item_id,
    ).first()["count"]


def get_job_count(conn, item_id, job_id, i_type):
    return conn.execute(
        text(
            BASE_ITEM_JOB_COUNT_SQL.format(
                table_name=TYPE_JOB_TABLE_MAPPING[i_type], column_name=TYPE_JOB_COLUMN_MAPPING[i_type]
            )
        ),
        item_id=item_id,
        job_id=job_id,
    ).first()["count"]


def get_item_count_for_job(conn, job_id, i_type):
    return conn.execute(
        text(BASE_ITEM_COUNT_FOR_JOB.format(table_name=TYPE_JOB_TABLE_MAPPING[i_type])),
        job_id=job_id,
    ).first()["count"]


def query_unique_references(base_url, query, route):
    r = requests.put(f"{base_url}/{route}/query/unique-references", json=query)
    assert r.status_code == 200
    result = r.json()
    UniqueReferences.model_validate(result)
    return result


def query_unique_references_items(base_url, collection_id, query):
    r = requests.put(f"{base_url}/collections/{collection_id}/items/query/unique-references", json=query)
    assert r.status_code == 200
    result = r.json()
    UniqueReferences.model_validate(result)
    return result
