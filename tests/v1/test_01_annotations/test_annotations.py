import docker

from ..commons.setup_tests import Up
from ..commons.utils import clear_database
from .annotations_delete import annotations_delete
from .annotations_get_all import annotations_get_all
from .annotations_get_by_id import annotations_get_by_id
from .annotations_post import annotations_post
from .annotations_put import annotations_put
from .annotations_query import annotations_query


def test_annotations():
    client = docker.from_env()
    with Up(client) as up:
        annotations_get_by_id(up)
        annotations_get_all(up)
        annotations_post(up)
        annotations_put(up)
        annotations_query(up)
        annotations_delete(up)

        clear_database(up)
