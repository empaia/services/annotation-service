from uuid import uuid4

import requests

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, get_count, lock_for_job, query_unique_references
from ..test_02_classes.commons.utils_create import create_class
from .commons.utils_check import check_position
from .commons.utils_count import get_creator_count, get_mixed_count, get_reference_count, get_type_count
from .commons.utils_create import (
    create_arrow,
    create_circle,
    create_line,
    create_point,
    create_point_with_classes_different_creator,
    create_polygon,
    create_rectangle,
    get_large_polygons,
)
from .commons.utils_requests import query_annotations

P_IN = [100, 100]
P_OUT = [1000, 1000]
L_IN = [[100, 100], [200, 200]]
L_OUT = [[100, 100], [1000, 1000]]
RAD = 100
WIDTH = 100
HEIGHT = 100
POLY_ONE_IN = [[100, 100], [500, 1000], [1000, 500]]
POLY_TWO_IN = [[100, 100], [100, 200], [1000, 500]]
POLY_ALL_IN = [[100, 100], [100, 200], [200, 100]]
POLY_NO_IN = [[1000, 1000], [1000, 2000], [2000, 1000]]
CR_ID_1 = "User 1"
CR_ID_2 = "User 2"
CR_ID_3 = "User 3"
CR_ID_4 = "User 4"
CR_TYPE = "job"
REF_ID_1 = "WSI 1"
REF_ID_2 = "WSI 2"
NPP_C_1 = 10.0
NPP_C_2 = 50.0
NPP_V_1 = [10.0, 20.0]
NPP_V_2 = None


def annotations_query(up):
    base_url = get_base_url(up.annot_container)
    query_type_missing(base_url)

    fill_db(up)

    query_creators(base_url, up)
    query_references(base_url, up)
    query_types(base_url, up)
    query_npp(base_url)
    query_all_filters(base_url, up)
    query_viewport(base_url)
    query_all_filters_viewport(base_url)
    query_skip_limit(base_url)
    query_over_limit(base_url, up)
    query_creators_with_classes(base_url, up)
    query_all_filters_jobs(base_url, up)
    query_unique_class_values(base_url, up)
    query_unique_references_annots(base_url, up)
    query_all_filters_jobs_with_class_values(base_url, up)
    query_large_polygons(base_url)
    query_all_filters_viewport_centroids(base_url, up)
    query_invalid_annotation_ids(base_url)

    clear_database(up)


def query_type_missing(base_url):
    query_polygon = {"types": ["polygon"]}
    query_annotations(base_url, query_polygon, 0, 0)


def query_creators(base_url, up):
    with get_db(up.db_container).begin() as conn:
        creator_count = get_creator_count(conn, CR_ID_1)

    query = {"creators": [CR_ID_1]}
    query_annotations(base_url, query, creator_count, creator_count)


def query_references(base_url, up):
    with get_db(up.db_container).begin() as conn:
        reference_count = get_reference_count(conn, REF_ID_1)

    query = {"references": [REF_ID_1]}
    query_annotations(base_url, query, reference_count, reference_count)


def query_types(base_url, up):
    with get_db(up.db_container).begin() as conn:
        point_count = get_type_count(conn, "point")
        line_count = get_type_count(conn, "line")
        arrow_count = get_type_count(conn, "arrow")
        circle_count = get_type_count(conn, "circle")
        rectangle_count = get_type_count(conn, "rectangle")
        polygon_count = get_type_count(conn, "polygon")

    query_point = {"types": ["point"]}
    query_annotations(base_url, query_point, point_count, point_count)

    query_line = {"types": ["line"]}
    query_annotations(base_url, query_line, line_count, line_count)

    query_arrow = {"types": ["arrow"]}
    query_annotations(base_url, query_arrow, arrow_count, arrow_count)

    query_circle = {"types": ["circle"]}
    query_annotations(base_url, query_circle, circle_count, circle_count)

    query_rectangle = {"types": ["rectangle"]}
    query_annotations(base_url, query_rectangle, rectangle_count, rectangle_count)

    query_polygon = {"types": ["polygon"]}
    query_annotations(base_url, query_polygon, polygon_count, polygon_count)


def query_npp(base_url):
    query_npp_viewing_one = {"npp_viewing": [1.0, 30.0]}
    query_annotations(base_url, query_npp_viewing_one, 18, 18)

    query_npp_created_two = {"npp_viewing": [30.0, 60.0]}
    query_annotations(base_url, query_npp_created_two, 11, 11)

    query_npp_created_three = {"npp_viewing": [60.0, 100.0]}
    query_annotations(base_url, query_npp_created_three, 0, 0)

    query_npp_created_four = {"npp_viewing": [1.0, 100.0]}
    query_annotations(base_url, query_npp_created_four, 29, 29)


def query_all_filters(base_url, up):
    with get_db(up.db_container).begin() as conn:
        count = get_mixed_count(conn, CR_ID_1, REF_ID_1, "point")

    query = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "types": ["point"],
        "npp_viewing": [1.0, 100],
    }
    query_annotations(base_url, query, count, count)


def query_viewport(base_url):
    query_inside = {"viewport": {"x": 100, "y": 100, "width": 100, "height": 100}}
    query_annotations(base_url, query_inside, 25, 25)

    query_outside = {"viewport": {"x": 3000, "y": 3000, "width": 200, "height": 200}}
    query_annotations(base_url, query_outside, 0, 0)

    query_all_inside = {"viewport": {"x": 0, "y": 0, "width": 2000, "height": 2000}}
    query_annotations(base_url, query_all_inside, 29, 29)


def query_all_filters_viewport(base_url):
    query_inside = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    query_annotations(base_url, query_inside, 1, 1)

    query_inside_no_npp = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [60.0, 100.0],
    }
    query_annotations(base_url, query_inside_no_npp, 0, 0)

    query_outside = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "types": ["point"],
        "viewport": {"x": 2000, "y": 2000, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    query_annotations(base_url, query_outside, 0, 0)


def query_all_filters_viewport_centroids(base_url, up):
    ref_id = uuid4()
    point = [200, 200]
    line = [[100, 100], [300, 300]]
    head = [100, 100]
    tail = [300, 300]
    width = 200
    height = 200
    points = [[100, 100], [100, 300], [300, 300], [300, 100]]
    npp_v_high = [1000.0, 2000.0]
    npp_v_low = [100.0, 200.0]

    with get_db(up.db_container).begin() as conn:
        create_point(conn, ref_id=ref_id, npp_v=npp_v_high, point=point)
        create_point(conn, ref_id=ref_id, npp_v=npp_v_low, point=point)
        create_line(conn, ref_id=ref_id, npp_v=npp_v_high, line=line)
        create_line(conn, ref_id=ref_id, npp_v=npp_v_low, line=line)
        create_arrow(conn, ref_id=ref_id, npp_v=npp_v_high, head=head, tail=tail)
        create_arrow(conn, ref_id=ref_id, npp_v=npp_v_low, head=head, tail=tail)
        create_circle(conn, ref_id=ref_id, npp_v=npp_v_high, center=point)
        create_circle(conn, ref_id=ref_id, npp_v=npp_v_low, center=point)
        create_rectangle(conn, ref_id=ref_id, npp_v=npp_v_high, upper_left=head, width=width, height=height)
        create_rectangle(conn, ref_id=ref_id, npp_v=npp_v_low, upper_left=head, width=width, height=height)
        create_polygon(conn, ref_id=ref_id, npp_v=npp_v_high, polygon=points)
        create_polygon(conn, ref_id=ref_id, npp_v=npp_v_low, polygon=points)

    params = {"with_low_npp_centroids": True}
    query = {
        "references": [str(ref_id)],
        "viewport": {"x": 0, "y": 0, "width": 1000, "height": 1000},
        "npp_viewing": [150.0, 1500.0],
    }
    query_annotations(base_url, query, 12, 12, params=params, ct_count=0)

    query = {
        "references": [str(ref_id)],
        "viewport": {"x": 0, "y": 0, "width": 1000, "height": 1000},
        "npp_viewing": [500.0, 10000.0],
    }
    query_annotations(base_url, query, 6, 6, params=params, ct_count=6)

    query = {
        "references": [str(ref_id)],
        "viewport": {"x": 0, "y": 0, "width": 1000, "height": 1000},
        "npp_viewing": [5000.0, 10000.0],
    }
    query_annotations(base_url, query, 0, 0, params=params, ct_count=12)

    query = {
        "references": [str(ref_id)],
        "viewport": {"x": 0, "y": 0, "width": 1000, "height": 1000},
        "npp_viewing": [500.0, 10000.0],
    }
    params = {"with_low_npp_centroids": True, "limit": 0}
    query_annotations(base_url, query, 6, 0, params=params, ct_count=6)

    query = {
        "references": [str(ref_id)],
        "viewport": {"x": 0, "y": 0, "width": 1000, "height": 1000},
        "npp_viewing": [500.0, 10000.0],
    }
    params = {"with_low_npp_centroids": True, "skip": 2, "limit": 2}
    query_annotations(base_url, query, 6, 2, params=params, ct_count=6)


def query_large_polygons(base_url):
    large_polygons = get_large_polygons()
    reference_id = large_polygons["ONE"]["reference_id"]
    for poly in large_polygons.values():
        response = requests.post(f"{base_url}/annotations", json=poly)
        assert response.status_code == 201

    query = {"creators": ["ONE", "TWO", "THREE", "FOUR", "FIVE"]}
    result = query_annotations(base_url, query, 5, 5)
    for r in result["items"]:
        if r["reference_id"] == reference_id:
            assert r["coordinates"] == large_polygons[r["creator_id"]]["coordinates"]

    query = {"references": [reference_id]}
    result = query_annotations(base_url, query, 5, 5)
    for r in result["items"]:
        assert r["coordinates"] == large_polygons[r["creator_id"]]["coordinates"]


def query_skip_limit(base_url):
    params = {"skip": 0, "limit": 2}
    query_inside = {
        "creators": [CR_ID_1, CR_ID_2],
        "references": [REF_ID_1, REF_ID_2],
        "types": ["point", "line"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
    }
    query_annotations(base_url, query_inside, 8, 2, params=params)

    query_outside = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "types": ["point"],
        "viewport": {"x": 2000, "y": 2000, "width": 200, "height": 200},
    }
    query_annotations(base_url, query_outside, 0, 0, params=params)


def query_over_limit(base_url, up):
    fill_db(up, repeat_count=3)

    with get_db(up.db_container).begin() as conn:
        annot_count = get_count(conn, "annotations")
    assert annot_count == 4 * 29

    query = {"creators": [CR_ID_1, CR_ID_2]}
    response = requests.put(f"{base_url}/annotations/query", json=query)
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({annot_count}) exceeds server limit of {ITEM_LIMIT}."


def query_creators_with_classes(base_url, up):
    class_count = 2
    c_id_one = uuid4()
    c_id_two = uuid4()
    with get_db(up.db_container).begin() as conn:
        create_point_with_classes_different_creator(
            conn, class_count, c_id_one, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN
        )
        create_point_with_classes_different_creator(
            conn, class_count, c_id_two, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN
        )

    params = {"with_classes": True}
    query = {"creators": [str(c_id_one), str(c_id_two)]}
    query_annotations(base_url, query, 2, 2, params=params, cl_count=class_count)


def query_all_filters_jobs(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id_1 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN)
        a_id_2 = create_point(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, P_IN)
        a_id_3 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, P_IN)
        a_id_4 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)

    job_id_1 = "job1"
    job_id_2 = "job2"

    lock_for_job(base_url, job_id_1, a_id_1, "annotations")
    lock_for_job(base_url, job_id_1, a_id_2, "annotations")
    lock_for_job(base_url, job_id_1, a_id_3, "annotations")
    lock_for_job(base_url, job_id_1, a_id_4, "annotations")
    lock_for_job(base_url, job_id_2, a_id_1, "annotations")
    lock_for_job(base_url, job_id_2, a_id_2, "annotations")
    lock_for_job(base_url, job_id_2, a_id_3, "annotations")
    lock_for_job(base_url, job_id_2, a_id_4, "annotations")

    query_inside = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": [job_id_1, job_id_2],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    query_annotations(base_url, query_inside, 1, 1)

    query_inside_no_npp = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": [job_id_1],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [60.0, 100.0],
    }
    query_annotations(base_url, query_inside_no_npp, 0, 0)

    query_outside = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": [job_id_1],
        "types": ["point"],
        "viewport": {"x": 2000, "y": 2000, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    query_annotations(base_url, query_outside, 0, 0)

    query_job_1 = {"jobs": [job_id_1]}
    query_annotations(base_url, query_job_1, 4, 4)

    query_all_jobs = {"jobs": [job_id_1, job_id_2]}
    query_annotations(base_url, query_all_jobs, 4, 4)

    query_all_jobs_wrong_type = {"types": ["line"], "jobs": [job_id_1, job_id_2]}
    query_annotations(base_url, query_all_jobs_wrong_type, 0, 0)


def query_unique_class_values(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id_1 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN)
        a_id_2 = create_point(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, P_IN)
        a_id_3 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, P_IN)
        a_id_4 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)
        a_id_5 = create_point(conn, CR_ID_3, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)
        a_id_6 = create_point(conn, CR_ID_4, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)

        cl_id_1 = create_class(conn, a_id_1, CR_ID_1, CR_TYPE, "value")
        cl_id_2 = create_class(conn, a_id_1, CR_ID_1, CR_TYPE, "value2")
        cl_id_3 = create_class(conn, a_id_1, CR_ID_2, CR_TYPE, "value")
        cl_id_4 = create_class(conn, a_id_1, CR_ID_1, CR_TYPE, "value")
        cl_id_5 = create_class(conn, a_id_5, CR_ID_3, CR_TYPE, "value")
        cl_id_6 = create_class(conn, a_id_5, CR_ID_4, CR_TYPE, "value")
        cl_id_7 = create_class(conn, a_id_6, CR_ID_3, CR_TYPE, "value")
        cl_id_8 = create_class(conn, a_id_6, CR_ID_4, CR_TYPE, "value")

    job_id_1 = "job1"
    job_id_2 = "job2"

    lock_for_job(base_url, job_id_1, a_id_1, "annotations")
    lock_for_job(base_url, job_id_1, a_id_2, "annotations")
    lock_for_job(base_url, job_id_1, a_id_3, "annotations")
    lock_for_job(base_url, job_id_1, a_id_4, "annotations")
    lock_for_job(base_url, job_id_1, a_id_5, "annotations")
    lock_for_job(base_url, job_id_1, a_id_6, "annotations")
    lock_for_job(base_url, job_id_2, a_id_1, "annotations")
    lock_for_job(base_url, job_id_2, a_id_2, "annotations")
    lock_for_job(base_url, job_id_2, a_id_3, "annotations")
    lock_for_job(base_url, job_id_2, a_id_4, "annotations")
    lock_for_job(base_url, job_id_2, a_id_5, "annotations")
    lock_for_job(base_url, job_id_2, a_id_6, "annotations")

    lock_for_job(base_url, job_id_1, cl_id_1, "classes")
    lock_for_job(base_url, job_id_1, cl_id_2, "classes")
    lock_for_job(base_url, job_id_1, cl_id_3, "classes")
    lock_for_job(base_url, job_id_1, cl_id_4, "classes")
    lock_for_job(base_url, job_id_1, cl_id_5, "classes")
    lock_for_job(base_url, job_id_1, cl_id_6, "classes")
    lock_for_job(base_url, job_id_1, cl_id_7, "classes")
    lock_for_job(base_url, job_id_1, cl_id_8, "classes")
    lock_for_job(base_url, job_id_2, cl_id_1, "classes")
    lock_for_job(base_url, job_id_2, cl_id_2, "classes")
    lock_for_job(base_url, job_id_2, cl_id_3, "classes")
    lock_for_job(base_url, job_id_2, cl_id_4, "classes")
    lock_for_job(base_url, job_id_2, cl_id_5, "classes")
    lock_for_job(base_url, job_id_2, cl_id_6, "classes")
    lock_for_job(base_url, job_id_2, cl_id_7, "classes")
    lock_for_job(base_url, job_id_2, cl_id_8, "classes")

    query_inside = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": [job_id_1, job_id_2],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    query_annotations(base_url, query_inside, ucv_count=3)

    query_inside_no_npp = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": ["job1"],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [60.0, 100.0],
    }
    query_annotations(base_url, query_inside_no_npp, ucv_count=0)

    query_outside = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "jobs": ["job1"],
        "types": ["point"],
        "viewport": {"x": 2000, "y": 2000, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    query_annotations(base_url, query_outside, ucv_count=0)

    query_one_job = {"jobs": [job_id_1]}
    query_annotations(base_url, query_one_job, ucv_count=3)

    query_all_jobs = {"jobs": [job_id_1, job_id_2]}
    query_annotations(base_url, query_all_jobs, ucv_count=3)

    query_all_jobs_wrong_type = {"types": ["line"], "jobs": [job_id_1, job_id_2]}
    query_annotations(base_url, query_all_jobs_wrong_type, ucv_count=0)

    query_creators_match = {"creators": [CR_ID_3]}
    query_annotations(base_url, query_creators_match, ucv_count=1)

    query_jobs_match = {"jobs": [job_id_1]}
    query_annotations(base_url, query_jobs_match, ucv_count=3)

    query_jobs_no_match = {"references": [REF_ID_1], "jobs": ["no_job_should_match"]}
    query_annotations(base_url, query_jobs_no_match, ucv_count=0)


def query_unique_references_annots(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id_1 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN)
        a_id_2 = create_point(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, P_IN)
        a_id_3 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, P_IN)
        a_id_4 = create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)
        a_id_5 = create_point(conn, CR_ID_3, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)
        a_id_6 = create_point(conn, CR_ID_4, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)

        cl_id_1 = create_class(conn, a_id_1, CR_ID_1, CR_TYPE, "value")
        cl_id_2 = create_class(conn, a_id_1, CR_ID_1, CR_TYPE, "value2")
        cl_id_3 = create_class(conn, a_id_1, CR_ID_2, CR_TYPE, "value")
        cl_id_4 = create_class(conn, a_id_1, CR_ID_1, CR_TYPE, "value")
        cl_id_5 = create_class(conn, a_id_5, CR_ID_3, CR_TYPE, "value")
        cl_id_6 = create_class(conn, a_id_5, CR_ID_4, CR_TYPE, "value")
        cl_id_7 = create_class(conn, a_id_6, CR_ID_3, CR_TYPE, "value")
        cl_id_8 = create_class(conn, a_id_6, CR_ID_4, CR_TYPE, "value")

    refs = [REF_ID_1, REF_ID_2]

    # queries without jobs
    query_one = {}
    r = query_unique_references(base_url, query_one, "annotations")
    assert len(r["wsi"]) == 2
    for ref in r["wsi"]:
        assert ref in refs

    query_two = {"jobs": [None]}
    r = query_unique_references(base_url, query_two, "annotations")
    assert len(r["wsi"]) == 2
    for ref in r["wsi"]:
        assert ref in refs

    query_three = {"class_values": ["value2"], "jobs": [None]}
    r = query_unique_references(base_url, query_three, "annotations")
    assert len(r["wsi"]) == 1
    assert r["wsi"][0] == REF_ID_1

    query_four = {
        "class_values": ["value", "value2"],
        "jobs": [None],
        "creators": [CR_ID_1],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    r = query_unique_references(base_url, query_four, "annotations")
    assert len(r["wsi"]) == 1
    assert r["wsi"][0] == REF_ID_1

    job_id_1 = "job1_ur"
    job_id_2 = "job2_ur"

    lock_for_job(base_url, job_id_1, a_id_1, "annotations")
    lock_for_job(base_url, job_id_1, a_id_2, "annotations")
    lock_for_job(base_url, job_id_1, a_id_3, "annotations")
    lock_for_job(base_url, job_id_1, a_id_4, "annotations")
    lock_for_job(base_url, job_id_1, a_id_5, "annotations")
    lock_for_job(base_url, job_id_1, a_id_6, "annotations")
    lock_for_job(base_url, job_id_2, a_id_1, "annotations")
    lock_for_job(base_url, job_id_2, a_id_2, "annotations")
    lock_for_job(base_url, job_id_2, a_id_3, "annotations")
    lock_for_job(base_url, job_id_2, a_id_4, "annotations")
    lock_for_job(base_url, job_id_2, a_id_5, "annotations")
    lock_for_job(base_url, job_id_2, a_id_6, "annotations")

    lock_for_job(base_url, job_id_1, cl_id_1, "classes")
    lock_for_job(base_url, job_id_1, cl_id_2, "classes")
    lock_for_job(base_url, job_id_1, cl_id_3, "classes")
    lock_for_job(base_url, job_id_1, cl_id_4, "classes")
    lock_for_job(base_url, job_id_1, cl_id_5, "classes")
    lock_for_job(base_url, job_id_1, cl_id_6, "classes")
    lock_for_job(base_url, job_id_1, cl_id_7, "classes")
    lock_for_job(base_url, job_id_1, cl_id_8, "classes")
    lock_for_job(base_url, job_id_2, cl_id_1, "classes")
    lock_for_job(base_url, job_id_2, cl_id_2, "classes")
    lock_for_job(base_url, job_id_2, cl_id_3, "classes")
    lock_for_job(base_url, job_id_2, cl_id_4, "classes")
    lock_for_job(base_url, job_id_2, cl_id_5, "classes")
    lock_for_job(base_url, job_id_2, cl_id_6, "classes")
    lock_for_job(base_url, job_id_2, cl_id_7, "classes")
    lock_for_job(base_url, job_id_2, cl_id_8, "classes")

    # queries with jobs
    query_one = {"jobs": [job_id_1, job_id_2]}
    r = query_unique_references(base_url, query_one, "annotations")
    assert len(r["wsi"]) == 2
    for ref in r["wsi"]:
        assert ref in refs

    query_two = {"class_values": ["value2"], "jobs": [job_id_1]}
    r = query_unique_references(base_url, query_two, "annotations")
    assert len(r["wsi"]) == 1
    assert r["wsi"][0] == REF_ID_1

    query_three = {
        "class_values": ["value", "value2"],
        "jobs": [job_id_1],
        "creators": [CR_ID_1],
        "types": ["point"],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
    }
    r = query_unique_references(base_url, query_three, "annotations")
    assert len(r["wsi"]) == 1
    assert r["wsi"][0] == REF_ID_1


def query_all_filters_jobs_with_class_values(base_url, up):
    cr_id_1 = "C_ONE"
    cr_id_2 = "C_TWO"
    wsi_1 = "W_ONE"
    wsi_2 = "W_TWO"
    v_1 = "V_ONE"
    v_2 = "V_TWO"
    v_3 = "V_THREE"

    with get_db(up.db_container).begin() as conn:
        a_id_1 = create_point(conn, cr_id_1, CR_TYPE, wsi_1, NPP_C_1, NPP_V_1, P_IN)
        a_id_2 = create_point(conn, cr_id_2, CR_TYPE, wsi_1, NPP_C_2, NPP_V_1, P_IN)
        a_id_3 = create_point(conn, cr_id_1, CR_TYPE, wsi_2, NPP_C_1, NPP_V_2, P_IN)
        a_id_4 = create_point(conn, cr_id_2, CR_TYPE, wsi_2, NPP_C_2, NPP_V_2, P_OUT)
        a_id_5 = create_point(conn, cr_id_1, CR_TYPE, wsi_1, NPP_C_2, NPP_V_2, P_OUT)
        a_id_6 = create_point(conn, cr_id_1, CR_TYPE, wsi_1, NPP_C_2, NPP_V_2, P_OUT)

        # a_1 -> two classes, same creator
        cl_id_1 = create_class(conn, a_id_1, cr_id_1, CR_TYPE, v_1)
        cl_id_2 = create_class(conn, a_id_1, cr_id_1, CR_TYPE, v_2)

        # a_2 -> one class, other creator
        cl_id_3 = create_class(conn, a_id_2, cr_id_1, CR_TYPE, v_1)

        # a_3 -> one class, same creator
        cl_id_4 = create_class(conn, a_id_3, cr_id_1, CR_TYPE, v_1)

        # a_4 -> one class, same creator
        cl_id_5 = create_class(conn, a_id_4, cr_id_2, CR_TYPE, v_1)

        # a_5 -> three classes, same creator, value twice
        # a_5 -> one class with different value for filter testing
        cl_id_6 = create_class(conn, a_id_5, cr_id_1, CR_TYPE, v_1)
        cl_id_7 = create_class(conn, a_id_5, cr_id_1, CR_TYPE, v_1)
        cl_id_8 = create_class(conn, a_id_5, cr_id_1, CR_TYPE, v_2)
        cl_id_9 = create_class(conn, a_id_5, cr_id_1, CR_TYPE, v_3)

    job_id_1 = "job1"
    job_id_2 = "job2"
    job_id_3 = "job3"

    # lock annotations
    lock_for_job(base_url, job_id_1, a_id_1, "annotations")
    lock_for_job(base_url, job_id_1, a_id_2, "annotations")
    lock_for_job(base_url, job_id_1, a_id_3, "annotations")
    lock_for_job(base_url, job_id_1, a_id_4, "annotations")
    lock_for_job(base_url, job_id_1, a_id_5, "annotations")
    lock_for_job(base_url, job_id_1, a_id_6, "annotations")
    lock_for_job(base_url, job_id_2, a_id_1, "annotations")
    lock_for_job(base_url, job_id_2, a_id_2, "annotations")
    lock_for_job(base_url, job_id_2, a_id_3, "annotations")
    lock_for_job(base_url, job_id_2, a_id_4, "annotations")
    lock_for_job(base_url, job_id_2, a_id_5, "annotations")
    lock_for_job(base_url, job_id_2, a_id_6, "annotations")

    # lock classes
    lock_for_job(base_url, job_id_1, cl_id_1, "classes")
    lock_for_job(base_url, job_id_1, cl_id_2, "classes")
    lock_for_job(base_url, job_id_1, cl_id_3, "classes")
    lock_for_job(base_url, job_id_1, cl_id_4, "classes")
    lock_for_job(base_url, job_id_1, cl_id_5, "classes")
    lock_for_job(base_url, job_id_1, cl_id_6, "classes")
    lock_for_job(base_url, job_id_1, cl_id_7, "classes")
    lock_for_job(base_url, job_id_1, cl_id_8, "classes")
    lock_for_job(base_url, job_id_2, cl_id_1, "classes")
    lock_for_job(base_url, job_id_2, cl_id_2, "classes")
    lock_for_job(base_url, job_id_2, cl_id_3, "classes")
    lock_for_job(base_url, job_id_2, cl_id_4, "classes")
    lock_for_job(base_url, job_id_2, cl_id_5, "classes")
    lock_for_job(base_url, job_id_2, cl_id_6, "classes")
    lock_for_job(base_url, job_id_2, cl_id_7, "classes")
    lock_for_job(base_url, job_id_2, cl_id_8, "classes")
    lock_for_job(base_url, job_id_3, cl_id_9, "classes")

    params = {"with_classes": True}
    query_1 = {
        "creators": [cr_id_1],
        "references": [wsi_1],
        "jobs": [job_id_1],
        "viewport": {"x": 0, "y": 0, "width": 10000, "height": 10000},
        "npp_viewing": [1.0, 10000.0],
        "class_values": [v_1],
    }
    result = query_annotations(base_url, query_1, 2, 2, params=params)
    check_position(base_url, result["items"], query_1)
    query_annotations(base_url, query_1, ucv_count=3)

    query_2 = {
        "creators": [cr_id_1],
        "references": [wsi_1],
        "jobs": [job_id_1],
        "viewport": {"x": 0, "y": 0, "width": 10000, "height": 10000},
        "npp_viewing": [60.0, 100.0],
        "class_values": [v_1],
    }
    query_annotations(base_url, query_2, 0, 0, params=params)
    query_annotations(base_url, query_2, ucv_count=0)

    query_3 = {
        "creators": [cr_id_1],
        "references": [wsi_1],
        "jobs": [job_id_1],
        "viewport": {"x": 2000, "y": 2000, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100.0],
        "class_values": [v_1],
    }
    query_annotations(base_url, query_3, 0, 0, params=params)
    query_annotations(base_url, query_3, ucv_count=0)

    query_4 = {
        "references": [wsi_1],
        "jobs": [job_id_1],
        "class_values": [v_1],
    }
    result = query_annotations(base_url, query_4, 3, 3, params=params)
    check_position(base_url, result["items"], query_4)
    query_annotations(base_url, query_4, ucv_count=3)

    query_5 = {
        "references": [wsi_1],
        "jobs": [job_id_1],
        "class_values": [v_1, None],
    }
    result = query_annotations(base_url, query_5, 4, 4, params=params)
    check_position(base_url, result["items"], query_5)
    query_annotations(base_url, query_5, ucv_count=3)

    query_6 = {
        "references": ["WSI"],
        "jobs": [job_id_1],
        "class_values": [v_1, None],
    }
    query_annotations(base_url, query_6, 0, 0, params=params)
    query_annotations(base_url, query_6, ucv_count=0)

    query_7 = {
        "references": [wsi_1],
        "jobs": ["JOB"],
        "class_values": [v_1, None],
    }
    query_annotations(base_url, query_7, 0, 0, params=params)
    query_annotations(base_url, query_7, ucv_count=0)

    query_8 = {
        "references": [wsi_1],
        "jobs": [job_id_1],
        "class_values": [None],
    }
    result = query_annotations(base_url, query_8, 1, 1, params=params)
    check_position(base_url, result["items"], query_8)
    query_annotations(base_url, query_8, ucv_count=3)

    query_9 = {
        "references": [wsi_1, wsi_2],
        "jobs": [job_id_1, job_id_2],
        "class_values": [None, v_1, v_2],
    }
    result = query_annotations(base_url, query_9, 6, 6, params=params)
    check_position(base_url, result["items"], query_9)
    query_annotations(base_url, query_9, ucv_count=3)

    query_10 = {
        "references": [wsi_1, wsi_2],
        "jobs": [job_id_1, job_id_2, job_id_3],
        "class_values": [None, v_1, v_2, v_3],
    }
    result = query_annotations(base_url, query_10, 6, 6, params=params)
    check_position(base_url, result["items"], query_10)
    query_annotations(base_url, query_10, ucv_count=4)

    query_11 = {
        "creators": [cr_id_1],
        "references": [wsi_1],
        "jobs": [None],
        "viewport": {"x": 0, "y": 0, "width": 10000, "height": 10000},
        "npp_viewing": [1.0, 10000.0],
        "class_values": [v_1],
    }
    query_annotations(base_url, query_11, 0, 0, params=params)
    query_annotations(base_url, query_11, ucv_count=0)

    with get_db(up.db_container).begin() as conn:
        a_id_new = create_point(conn, cr_id_1, CR_TYPE, wsi_1, NPP_C_1, NPP_V_1, P_IN)
        create_class(conn, a_id_new, cr_id_1, CR_TYPE, v_1)

    query_annotations(base_url, query_11, 1, 1)
    query_annotations(base_url, query_1, 2, 2, params=params)
    query_annotations(base_url, query_1, ucv_count=3)


def query_invalid_annotation_ids(base_url):
    query = {"annotations": ["some_invalid_id"]}
    response = requests.put(f"{base_url}/annotations/query", json=query)
    assert response.status_code == 422
    assert response.json()["detail"] == "ERROR: Value for 'annotations' must be of type UUID4."


def fill_db(up, repeat_count=1):
    with get_db(up.db_container).begin() as conn:
        for _ in range(repeat_count):
            create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN)
            create_point(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, P_IN)
            create_point(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, P_IN)
            create_point(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT)

            create_line(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, L_IN)
            create_line(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, L_IN)
            create_line(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, L_IN)
            create_line(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, L_OUT)
            create_line(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, L_IN)

            create_arrow(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN, P_IN)
            create_arrow(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, P_IN, P_IN)
            create_arrow(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, P_IN, P_IN)
            create_arrow(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT, P_IN)
            create_arrow(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_IN, P_IN)

            create_circle(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN, RAD)
            create_circle(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, P_IN, RAD)
            create_circle(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, P_IN, RAD)
            create_circle(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT, RAD)

            create_rectangle(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, P_IN, WIDTH, HEIGHT)
            create_rectangle(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, P_IN, WIDTH, HEIGHT)
            create_rectangle(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, P_IN, WIDTH, HEIGHT)
            create_rectangle(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_OUT, WIDTH, HEIGHT)
            create_rectangle(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, P_IN, WIDTH, HEIGHT)

            create_polygon(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, POLY_ONE_IN)
            create_polygon(conn, CR_ID_2, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_1, POLY_ONE_IN)
            create_polygon(conn, CR_ID_1, CR_TYPE, REF_ID_2, NPP_C_1, NPP_V_2, POLY_ONE_IN)
            create_polygon(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, POLY_TWO_IN)
            create_polygon(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, POLY_ALL_IN)
            create_polygon(conn, CR_ID_1, CR_TYPE, REF_ID_1, NPP_C_2, NPP_V_2, POLY_NO_IN)
