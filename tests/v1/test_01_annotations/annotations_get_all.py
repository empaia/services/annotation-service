import requests

from annotation_service.models.v1.annotation.annotations import (
    ArrowAnnotation,
    CircleAnnotation,
    LineAnnotation,
    PointAnnotation,
    PolygonAnnotation,
    RectangleAnnotation,
)

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, get_count
from .commons.utils_check import check_base_annotation
from .commons.utils_create import (
    create_arrow,
    create_circle,
    create_line,
    create_point,
    create_polygon,
    create_rectangle,
    get_large_polygons,
)
from .commons.utils_requests import get_annotations

CR_ID = "User 1"
CR_TYPE = "job"
REF_ID = "WSI 1"
NPP_C = 10.0
NPP_V = None
POINT = [100, 200]
LINE = [[100, 100], [200, 200]]
HEAD = [100, 200]
TAIL = [300, 400]
RADIUS = 100
WIDTH = 200
HEIGHT = 250
POLYGON = [[100, 100], [200, 100], [200, 200], [100, 200]]
ANNOT_COUNT = 10


def annotations_get_all(up):
    base_url = get_base_url(up.annot_container)
    get_empty(base_url)
    get_filled(base_url, up)
    get_over_limit(base_url, up)

    clear_database(up)


def get_empty(base_url):
    get_annotations(base_url, 0, 0)


def get_filled(base_url, up):
    a_ids = []
    with get_db(up.db_container).begin() as conn:
        for _ in range(ANNOT_COUNT):
            a_ids.append(create_point(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT))
            a_ids.append(create_line(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, LINE))
            a_ids.append(create_arrow(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, HEAD, TAIL))
            a_ids.append(create_circle(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT, RADIUS))
            a_ids.append(create_rectangle(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT, WIDTH, HEIGHT))
            a_ids.append(create_polygon(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POLYGON))

        large_polygons = get_large_polygons()

        for poly in large_polygons.values():
            a_ids.append(create_polygon(conn, poly["creator_id"], CR_TYPE, REF_ID, NPP_C, NPP_V, poly["coordinates"]))

    assert len(a_ids) == ANNOT_COUNT * 6 + 5

    result = get_annotations(base_url, ANNOT_COUNT * 6 + 5, ANNOT_COUNT * 6 + 5)

    for annot in result["items"]:
        a_id = annot["id"]
        a_type = annot["type"]
        if annot["creator_id"] not in large_polygons:
            check_base_annotation(annot, a_id, a_type, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V)
        if a_type == "point":
            PointAnnotation.model_validate(annot)
            assert annot["coordinates"] == POINT
        elif a_type == "line":
            LineAnnotation.model_validate(annot)
            assert annot["coordinates"] == LINE
        elif a_type == "arrow":
            ArrowAnnotation.model_validate(annot)
            assert annot["head"] == HEAD
            assert annot["tail"] == TAIL
        elif a_type == "circle":
            CircleAnnotation.model_validate(annot)
            assert annot["center"] == POINT
            assert annot["radius"] == RADIUS
        elif a_type == "rectangle":
            RectangleAnnotation.model_validate(annot)
            assert annot["upper_left"] == POINT
            assert annot["width"] == WIDTH
            assert annot["height"] == HEIGHT
        elif a_type == "polygon":
            PolygonAnnotation.model_validate(annot)
            if annot["creator_id"] in large_polygons:
                assert annot["coordinates"] == large_polygons[annot["creator_id"]]["coordinates"]
            else:
                assert annot["coordinates"] == POLYGON
        else:
            assert 0 == 1

        annot_response = requests.get(f"{base_url}/annotations/{a_id}")
        assert annot_response.status_code == 200
        assert sorted(annot) == sorted(annot_response.json())


def get_over_limit(base_url, up):
    a_ids = []
    with get_db(up.db_container).begin() as conn:
        annot_count = ITEM_LIMIT - get_count(conn, "annotations") + 1
        for _ in range(annot_count):
            a_ids.append(create_point(conn, CR_ID, CR_TYPE, REF_ID, NPP_C, NPP_V, POINT))

    response = requests.get(f"{base_url}/annotations")
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({ITEM_LIMIT + 1}) exceeds server limit of {ITEM_LIMIT}."
