from sqlalchemy.sql.expression import text


def get_creator_count(conn, cr_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM a_annotations
            WHERE creator_id=:creator_id;
            """
        ),
        creator_id=cr_id,
    ).first()["count"]


def get_reference_count(conn, ref_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM a_annotations
            WHERE reference_id=:reference_id;
            """
        ),
        reference_id=ref_id,
    ).first()["count"]


def get_type_count(conn, a_type):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM a_annotations
            WHERE type=:annotation_type;
            """
        ),
        annotation_type=a_type,
    ).first()["count"]


def get_mixed_count(conn, cr_id, ref_id, a_type):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM a_annotations
            WHERE creator_id=:creator_id AND reference_id=:reference_id AND type=:annotation_type;
            """
        ),
        creator_id=cr_id,
        reference_id=ref_id,
        annotation_type=a_type,
    ).first()["count"]


def get_job_count_in_classes(conn, a_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM j_job_classes
            WHERE annot_id=:annotation_id;
            """
        ),
        annotation_id=a_id,
    ).first()["count"]


def get_class_count(conn, a_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM cl_classes
            WHERE reference_id=:annot_id;
            """
        ),
        annot_id=a_id,
    ).first()["count"]
