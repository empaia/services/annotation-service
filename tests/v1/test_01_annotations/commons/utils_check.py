import requests
from shapely.geometry import Polygon
from sqlalchemy.sql.expression import text

from annotation_service.models.v1.annotation.annotations import AnnotationQueryPosition

NAME = "Annotation"
DESC = "Description"
REF_TYPE = "wsi"


def get_centroid(a_type, line=None, head=None, tail=None, upper_left=None, width=None, height=None, points=None):
    if a_type == "line":
        return [round((line[0][0] + line[1][0]) / 2), round((line[0][1] + line[1][1]) / 2)]
    elif a_type == "arrow":
        return [round((head[0] + tail[0]) / 2), round((head[1] + tail[1]) / 2)]
    elif a_type == "rectangle":
        return [round((upper_left[0] * 2 + width) / 2), round((upper_left[1] * 2 + height) / 2)]
    elif a_type == "polygon":
        coords = Polygon(points).centroid.coords
        return [round(coords[0][0]), round(coords[0][1])]


def check_base_annotation(annot, a_id, a_type, cr_id, cr_type, ref_id, npp_c, npp_v=None):
    assert annot["id"] == str(a_id)
    assert annot["name"] == NAME
    assert annot["description"] == DESC
    assert annot["creator_id"] == cr_id
    assert annot["creator_type"] == cr_type
    assert annot["reference_id"] == ref_id
    assert annot["reference_type"] == REF_TYPE
    assert annot["npp_created"] == npp_c
    if npp_v:
        assert annot["npp_viewing"] == npp_v
    assert annot["type"] == a_type
    assert annot["created_at"] is not None
    assert annot["updated_at"] is not None


def check_annotation_post_put(conn, a_id, annot):
    result = conn.execute(
        text(
            """
            SELECT * FROM a_annotations WHERE id=:annot_id;
            """
        ),
        annot_id=a_id,
    ).first()

    assert str(a_id) == str(result["id"])
    assert annot["name"] == result["name"]
    if "description" in annot:
        assert annot["description"] == result["description"]
    assert annot["creator_id"] == str(result["creator_id"])
    assert annot["creator_type"] == result["creator_type"]
    assert annot["reference_id"] == str(result["reference_id"])
    assert annot["reference_type"] == result["reference_type"]
    assert annot["npp_created"] == result["npp_created"]
    if "npp_viewing" in annot:
        assert annot["npp_viewing"][0] == result["npp_viewing_min"]
        assert annot["npp_viewing"][1] == result["npp_viewing_max"]
    assert annot["type"] == result["type"]
    assert result["created_at"] is not None
    assert result["updated_at"] is not None
    if annot["type"] == "point":
        assert annot["coordinates"] == result["coordinates"]
        if "centroid" in annot:
            assert annot["centroid"] == result["centroid"]
        else:
            assert annot["coordinates"] == result["centroid"]
        bounding_box = f"""
        ({annot["coordinates"][0]},
        {annot["coordinates"][1]}),
        ({annot["coordinates"][0]},
        {annot["coordinates"][1]})
        """
        assert bounding_box.replace("\n", "").replace(" ", "").strip() == result["bounding_box"]
    elif annot["type"] == "line":
        assert annot["coordinates"] == result["coordinates"]
        if "centroid" in annot:
            assert annot["centroid"] == result["centroid"]
        else:
            assert get_centroid(annot["type"], line=annot["coordinates"]) == result["centroid"]
        bounding_box = f"""
        ({annot["coordinates"][1][0]},
        {annot["coordinates"][1][1]}),
        ({annot["coordinates"][0][0]},
        {annot["coordinates"][0][1]})
        """
        assert bounding_box.replace("\n", "").replace(" ", "").strip() == result["bounding_box"]
    elif annot["type"] == "arrow":
        assert annot["head"] == result["head"]
        assert annot["tail"] == result["tail"]
        if "centroid" in annot:
            assert annot["centroid"] == result["centroid"]
        else:
            assert get_centroid(annot["type"], head=annot["head"], tail=annot["tail"]) == result["centroid"]
        bounding_box = f"""
        ({annot["tail"][0]},
        {annot["tail"][1]}),
        ({annot["head"][0]},
        {annot["head"][1]})
        """
        assert bounding_box.replace("\n", "").replace(" ", "").strip() == result["bounding_box"]
    elif annot["type"] == "circle":
        assert annot["center"] == result["center"]
        assert annot["radius"] == result["radius"]
        if "centroid" in annot:
            assert annot["centroid"] == result["centroid"]
        else:
            assert annot["center"] == result["centroid"]
        bounding_box = f"""
        ({annot["center"][0] + annot["radius"]},
        {annot["center"][1] + annot["radius"]}),
        ({annot["center"][0] - annot["radius"]},
        {annot["center"][1] - annot["radius"]})
        """
        assert bounding_box.replace("\n", "").replace(" ", "").strip() == result["bounding_box"]
    elif annot["type"] == "rectangle":
        assert annot["upper_left"] == result["upper_left"]
        assert annot["width"] == result["width"]
        assert annot["height"] == result["height"]
        if "centroid" in annot:
            assert annot["centroid"] == result["centroid"]
        else:
            assert (
                get_centroid(
                    annot["type"], upper_left=annot["upper_left"], width=annot["width"], height=annot["height"]
                )
                == result["centroid"]
            )
        bounding_box = f"""
        ({annot["upper_left"][0] + annot["width"]},
        {annot["upper_left"][1] + annot["height"]}),
        ({annot["upper_left"][0]},
        {annot["upper_left"][1]})
        """
        assert bounding_box.replace("\n", "").replace(" ", "").strip() == result["bounding_box"]
    elif annot["type"] == "polygon":
        assert annot["coordinates"] == result["coordinates"]
        if "centroid" in annot:
            assert annot["centroid"] == result["centroid"]
        else:
            assert get_centroid(annot["type"], points=annot["coordinates"]) == result["centroid"]
        x_coords = [x for x in [p[0] for p in annot["coordinates"]]]
        y_coords = [y for y in [p[1] for p in annot["coordinates"]]]
        bounding_box = f"""
        ({max(x_coords)},
        {max(y_coords)}),
        ({min(x_coords)},
        {min(y_coords)})
        """
        assert bounding_box.replace("\n", "").replace(" ", "").strip() == result["bounding_box"]


def check_position(base_url, annots, query):
    for i in range(len(annots)):
        a_id = annots[i]["id"]
        position_response = requests.put(f"{base_url}/annotations/{a_id}/query", json=query)
        assert position_response.status_code == 200
        result = position_response.json()
        AnnotationQueryPosition.model_validate(result)
        assert result["id"] == a_id
        assert result["position"] == i


def check_class(class_, cr_id, cr_type, a_id):
    assert class_["creator_id"] == cr_id
    assert class_["creator_type"] == cr_type
    assert class_["reference_id"] == str(a_id)
    assert class_["reference_type"] == "annotation"
    assert class_["value"] == "value"
    assert class_["type"] == "class"
