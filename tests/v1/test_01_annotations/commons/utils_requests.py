import requests

from annotation_service.models.v1.annotation.annotations import (
    AnnotationCountResponse,
    AnnotationList,
    AnnotationViewerList,
    ArrowAnnotation,
    CircleAnnotation,
    LineAnnotation,
    PointAnnotation,
    PolygonAnnotation,
    RectangleAnnotation,
    UniqueClassValues,
)

from ...commons.setup_tests import get_db
from .utils_check import check_annotation_post_put

VALIDATION_MAPPING = {
    "point": PointAnnotation,
    "line": LineAnnotation,
    "arrow": ArrowAnnotation,
    "circle": CircleAnnotation,
    "rectangle": RectangleAnnotation,
    "polygon": PolygonAnnotation,
}


def post_annotation(up, base_url, annot, a_type):
    post_response = requests.post(f"{base_url}/annotations", json=annot)
    assert post_response.status_code == 201
    response_json = post_response.json()
    VALIDATION_MAPPING[a_type].model_validate(response_json)
    with get_db(up.db_container).begin() as conn:
        check_annotation_post_put(conn, response_json["id"], annot)


def query_annotations(
    base_url, query, i_count=None, i_len=None, params=None, ct_count=None, cl_count=None, ucv_count=None
):
    if ucv_count is not None:
        value_query = query.copy()
        value_query.pop("class_values", None)
        response = requests.put(f"{base_url}/annotations/query/unique-class-values", json=value_query)
    else:
        response = requests.put(f"{base_url}/annotations/query", json=query, params=params)
    assert response.status_code == 200
    result = response.json()
    if ucv_count is not None:
        UniqueClassValues.model_validate(result)
        assert len(result["unique_class_values"]) == ucv_count
    else:
        AnnotationList.model_validate(result)
        assert result["item_count"] == i_count
        assert len(result["items"]) == i_len

        r_count = requests.put(f"{base_url}/annotations/query/count", json=query)
        assert r_count.status_code == 200
        result_count = r_count.json()
        AnnotationCountResponse.model_validate(result_count)
        assert result_count["item_count"] == i_count

    if params and "with_low_npp_centroids" in params and params["with_low_npp_centroids"] and ct_count is not None:
        assert len(result["low_npp_centroids"]) == ct_count
        for centroid in result["low_npp_centroids"]:
            assert centroid == [200, 200]

    if params and "with_classes" in params and params["with_classes"] and cl_count:
        for annot in result["items"]:
            assert len(annot["classes"]) == cl_count

    # Viewer endpoint test
    if params and "with_low_npp_centroids" in params and params["with_low_npp_centroids"] and ct_count is not None:
        response = requests.put(f"{base_url}/annotations/query/viewer", json=query)
        assert response.status_code == 200
        viewer_result = response.json()
        AnnotationViewerList.model_validate(viewer_result)
        assert len(viewer_result["annotations"]) == i_count
        assert len(viewer_result["low_npp_centroids"]) == ct_count
        for centroid in viewer_result["low_npp_centroids"]:
            assert centroid == [200, 200]
        if i_count > 0:
            new_query = query.copy()
            new_query["annotations"] = viewer_result["annotations"]
            response = requests.put(f"{base_url}/annotations/query", json=new_query)
            assert response.status_code == 200
            query_result = response.json()
            AnnotationList.model_validate(query_result)
            assert query_result["item_count"] == i_count
            assert len(query_result["items"]) == i_count

    return result


def get_annotations(base_url, i_count, i_len, params=None):
    response = requests.get(f"{base_url}/annotations", params=params)
    assert response.status_code == 200
    result = response.json()
    AnnotationList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == i_len

    return result


def get_annotations_count(base_url, i_count, params=None):
    response = requests.get(f"{base_url}/annotations/query/count", params=params)
    assert response.status_code == 200
    result = response.json()
    AnnotationList.model_validate(result)
    assert result["item_count"] == i_count
    assert len(result["items"]) == 0

    return result
