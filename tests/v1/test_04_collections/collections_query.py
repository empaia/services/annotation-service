import requests

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, lock_for_job, query_unique_references
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_create import create_shallow
from .commons.utils_requests import query_collections

CR_ID_1 = "User 1"
CR_ID_2 = "User 2"
CR_ID_3 = "User 3"
REF_ID_1 = "WSI 1"
REF_ID_2 = "WSI 2"
REF_ID_3 = "WSI 3"


def collections_query(up):
    base_url = get_base_url(up.annot_container)

    with get_db(up.db_container).begin() as conn:
        create_shallow(conn, item_type="integer", cr_id=CR_ID_1, ref_id=REF_ID_1)
        create_shallow(conn, item_type="integer", cr_id=CR_ID_2, ref_id=REF_ID_1)
        create_shallow(conn, item_type="integer", cr_id=CR_ID_1, ref_id=REF_ID_2)
        create_shallow(conn, item_type="integer", cr_id=CR_ID_2, ref_id=REF_ID_2)
        create_shallow(conn, item_type="integer", cr_id=CR_ID_1, ref_id=REF_ID_3)
        create_shallow(conn, item_type="integer", cr_id=CR_ID_2, ref_id=REF_ID_3)
        create_shallow(conn, item_type="float", cr_id=CR_ID_1, ref_id=REF_ID_3)
        create_shallow(conn, item_type="float", cr_id=CR_ID_2, ref_id=REF_ID_3)

    query_creators(base_url)
    query_references(base_url)
    query_types(base_url)
    query_all(base_url)
    query_all_jobs(base_url, up)
    query_over_limit(base_url, up)
    query_unique_references_coll(base_url, up)

    clear_database(up)


def query_creators(base_url):
    query_1 = {"creators": [CR_ID_1]}
    query_collections(base_url, query_1, 4, 4)

    query_2 = {"creators": [CR_ID_1, CR_ID_2]}
    query_collections(base_url, query_2, 8, 8)

    query_3 = {"creators": [CR_ID_3]}
    query_collections(base_url, query_3, 0, 0)


def query_references(base_url):
    query_1 = {"references": [REF_ID_1]}
    query_collections(base_url, query_1, 2, 2)

    query_2 = {"references": [REF_ID_1, REF_ID_2]}
    query_collections(base_url, query_2, 4, 4)

    query_3 = {"references": [REF_ID_1, REF_ID_2, REF_ID_3]}
    query_collections(base_url, query_3, 8, 8)


def query_types(base_url):
    query_1 = {"item_types": ["integer"]}
    query_collections(base_url, query_1, 6, 6)

    query_2 = {"item_types": ["integer", "float"]}
    query_collections(base_url, query_2, 8, 8)

    query_3 = {"item_types": ["point"]}
    query_collections(base_url, query_3, 0, 0)


def query_all(base_url):
    query_1 = {"creators": [CR_ID_1], "references": [REF_ID_1], "item_types": ["integer"]}
    query_collections(base_url, query_1, 1, 1)

    query_2 = {
        "creators": [CR_ID_1, CR_ID_2],
        "references": [REF_ID_1, REF_ID_2],
        "item_types": ["integer"],
    }
    query_collections(base_url, query_2, 4, 4)

    query_3 = {
        "creators": [CR_ID_1, CR_ID_2],
        "references": [REF_ID_1, REF_ID_2, REF_ID_3],
        "item_types": ["integer", "float"],
    }
    query_collections(base_url, query_3, 8, 8)


def query_all_jobs(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id_1 = create_shallow(conn, item_type="integer", cr_id=CR_ID_1, ref_id=REF_ID_1)
        c_id_2 = create_shallow(conn, item_type="integer", cr_id=CR_ID_2, ref_id=REF_ID_1)
        c_id_3 = create_shallow(conn, item_type="integer", cr_id=CR_ID_1, ref_id=REF_ID_2)
        c_id_4 = create_shallow(conn, item_type="integer", cr_id=CR_ID_2, ref_id=REF_ID_2)

    job_id_1 = "job1"
    job_id_2 = "job2"

    lock_for_job(base_url, job_id_1, c_id_1, "collections")
    lock_for_job(base_url, job_id_1, c_id_2, "collections")
    lock_for_job(base_url, job_id_1, c_id_3, "collections")
    lock_for_job(base_url, job_id_1, c_id_4, "collections")
    lock_for_job(base_url, job_id_2, c_id_1, "collections")
    lock_for_job(base_url, job_id_2, c_id_2, "collections")
    lock_for_job(base_url, job_id_2, c_id_3, "collections")
    lock_for_job(base_url, job_id_2, c_id_4, "collections")

    query_1 = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "item_types": ["integer"],
        "jobs": [job_id_1],
    }
    query_collections(base_url, query_1, 1, 1)

    query_2 = {
        "creators": [CR_ID_1],
        "references": [REF_ID_1],
        "item_types": ["integer"],
        "jobs": [job_id_1, job_id_2],
    }
    query_collections(base_url, query_2, 1, 1)

    query_3 = {"jobs": [job_id_1, job_id_2]}
    query_collections(base_url, query_3, 4, 4)

    query_4 = {
        "jobs": [job_id_1, job_id_2],
        "item_types": ["float"],
    }
    query_collections(base_url, query_4, 0, 0)

    query_5 = {"references": [REF_ID_1], "jobs": ["no_job_should_match"]}
    query_collections(base_url, query_5, 0, 0)


def query_over_limit(base_url, up):
    c_count = ITEM_LIMIT - 8 + 1 - 4
    with get_db(up.db_container).begin() as conn:
        for _ in range(c_count):
            create_shallow(conn, item_type="integer", cr_id=CR_ID_1)

    query_one = {"creators": [CR_ID_1, CR_ID_2]}
    response = requests.put(f"{base_url}/collections/query", json=query_one)
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({ITEM_LIMIT + 1}) exceeds server limit of {ITEM_LIMIT}."


def query_unique_references_coll(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        c_id_1 = create_shallow(conn, item_type="integer", cr_id=CR_ID_1, ref_id=a_id, ref_type="annotation")
        c_id_2 = create_shallow(conn, item_type="integer", cr_id=CR_ID_2, ref_id="WSI1", ref_type="wsi")
        c_id_3 = create_shallow(conn, item_type="integer", cr_id=CR_ID_1, ref_id=None, ref_type=None)

    # queries without jobs
    query_one = {}
    r = query_unique_references(base_url, query_one, "collections")
    assert len(r["annotation"]) == 1
    assert len(r["wsi"]) == 5
    assert r["contains_items_without_reference"]

    query_two = {"item_types": ["integer"]}
    r = query_unique_references(base_url, query_two, "collections")
    assert len(r["annotation"]) == 1
    assert len(r["wsi"]) == 5
    assert r["contains_items_without_reference"]

    query_three = {"item_types": ["integer"], "creators": [CR_ID_1]}
    r = query_unique_references(base_url, query_three, "collections")
    assert len(r["annotation"]) == 1
    assert len(r["wsi"]) == 4
    assert r["contains_items_without_reference"]

    job_id_1 = "job1_ur"
    job_id_2 = "job2_ur"

    lock_for_job(base_url, job_id_1, c_id_1, "collections")
    lock_for_job(base_url, job_id_1, c_id_2, "collections")
    lock_for_job(base_url, job_id_1, c_id_3, "collections")
    lock_for_job(base_url, job_id_2, c_id_1, "collections")
    lock_for_job(base_url, job_id_2, c_id_2, "collections")
    lock_for_job(base_url, job_id_2, c_id_3, "collections")

    # queries without jobs
    query_one = {"jobs": [job_id_1]}
    r = query_unique_references(base_url, query_one, "collections")
    assert len(r["annotation"]) == 1
    assert len(r["wsi"]) == 1
    assert r["contains_items_without_reference"]

    query_two = {"jobs": [job_id_1], "item_types": ["integer"]}
    r = query_unique_references(base_url, query_two, "collections")
    assert len(r["annotation"]) == 1
    assert len(r["wsi"]) == 1
    assert r["contains_items_without_reference"]

    query_three = {"jobs": [job_id_1], "item_types": ["integer"], "creators": [CR_ID_1]}
    r = query_unique_references(base_url, query_three, "collections")
    assert len(r["annotation"]) == 1
    assert len(r["wsi"]) == 0
    assert r["contains_items_without_reference"]

    query_four = {"jobs": [job_id_1], "item_types": ["integer"], "creators": [CR_ID_2]}
    r = query_unique_references(base_url, query_four, "collections")
    assert len(r["annotation"]) == 0
    assert len(r["wsi"]) == 1
    assert not r["contains_items_without_reference"]
