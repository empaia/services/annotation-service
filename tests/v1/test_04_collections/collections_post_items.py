from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.collections import Collection

from ..commons.setup_tests import POST_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database
from ..test_01_annotations.commons.utils_create import create_point
from ..test_02_classes.commons.utils_create import create_class
from ..test_03_primitives.commons.utils_create import create_integer
from .commons.utils_create import create_nested_collection, create_point_collection, create_shallow
from .commons.utils_requests import post_item, post_items

CR_ID = "User"
REF_ID = "WSI"


def collections_post_items(up):
    base_url = get_base_url(up.annot_container)

    # single item
    add_integer(base_url, up)
    add_point(base_url, up)
    add_class(base_url, up)
    add_wsi(base_url, up)
    add_shallow_collection(base_url, up)
    add_filled_collection(base_url, up)
    add_wrong_item_type(base_url, up)
    add_not_exist(base_url, up)
    clear_database(up)

    # single item by id
    add_integer_by_id(base_url, up)
    add_point_by_id(base_url, up)
    add_class_by_id(base_url, up)
    add_wsi_by_id(base_url, up)
    add_shallow_collection_by_id(base_url, up)
    add_filled_collection_by_id(base_url, up)
    add_unknown_id(base_url, up)
    add_existing_id_twice(base_url, up)
    clear_database(up)

    # multiple items
    add_integers(base_url, up)
    add_points(base_url, up)
    add_classes(base_url, up)
    add_wsis(base_url, up)
    add_collections(base_url, up)
    add_nested_collections(base_url, up)
    add_over_limit(base_url, up)
    add_wrong_type_in_list(base_url, up)
    clear_database(up)

    # multiple items by ids
    add_integers_by_id(base_url, up)
    add_points_by_id(base_url, up)
    add_classes_by_id(base_url, up)
    add_wsis_by_id(base_url, up)
    add_collections_by_id(base_url, up)
    add_double(base_url, up)
    clear_database(up)


def add_integer(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_item(base_url, c_id, p_integer, "integer")


def add_point(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "point")

    point_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "point",
        "coordinates": [100, 200],
    }
    post_item(base_url, c_id, point_annot, "point")


def add_class(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "class")
        annot_id = create_point(conn)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(annot_id),
        "reference_type": "annotation",
    }
    post_item(base_url, c_id, new_class, "class")


def add_wsi(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "wsi")

    wsi = {"id": "Nice ID", "type": "wsi"}

    post_item(base_url, c_id, wsi, "wsi")


def add_shallow_collection(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "collection")

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "point",
    }
    result, _ = post_item(base_url, c_id, collection, "collection")
    assert result["item_count"] == 0
    assert len(result["items"]) == 0


def add_filled_collection(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "collection")

    point_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "point",
        "coordinates": [100, 200],
    }

    a_list = []
    for _ in range(10):
        a_list.append(point_annot)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "point",
        "items": a_list,
    }
    result, c_result = post_item(base_url, c_id, collection, "collection")
    assert result["item_count"] == 10
    assert len(result["items"]) == 10
    assert c_result["item_count"] == 1
    assert len(c_result["items"]) == 1
    c_inner = c_result["items"][0]
    assert c_inner["item_count"] == 10
    assert len(c_inner["items"]) == 10


def add_wrong_item_type(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    p_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=p_float)
    assert response.status_code == 405
    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    assert result["item_count"] == 0


def add_not_exist(base_url, up):
    c_id = uuid4()

    p_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=p_float)
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "annotation",
    }
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=p_integer)
    assert response.status_code == 422

    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(uuid4()),
        "reference_type": "annotation",
    }
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=p_integer)
    assert response.status_code == 404


def add_integer_by_id(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")
        p_id = create_integer(conn)

    p_integer = {"id": str(p_id)}

    post_item(base_url, c_id, p_integer, "id")


def add_point_by_id(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "point")
        a_id = create_point(conn)

    point_annot = {"id": str(a_id)}

    post_item(base_url, c_id, point_annot, "id")


def add_class_by_id(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "class")
        annot_id = create_point(conn)
        cl_id = create_class(conn, annot_id)

    new_class = {"id": str(cl_id)}

    post_item(base_url, c_id, new_class, "id")


def add_wsi_by_id(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "wsi")

    new_class = {"id": "NEW_WSI"}

    post_item(base_url, c_id, new_class, "id")


def add_shallow_collection_by_id(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "collection")
        i_c_id = create_shallow(conn, "point")

    new_collection = {"id": str(i_c_id)}

    post_item(base_url, c_id, new_collection, "id")


def add_filled_collection_by_id(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "collection")
        i_c_id = create_point_collection(conn)

    new_collection = {"id": str(i_c_id)}

    _, c_result = post_item(base_url, c_id, new_collection, "id")

    c_inner = c_result["items"][0]
    assert c_inner["item_count"] == 10
    assert len(c_inner["items"]) == 10


def add_unknown_id(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "class")

    post_id = {"id": str(uuid4())}

    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=post_id)
    assert response.status_code == 404
    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == 0


def add_existing_id_twice(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")
        p_id = create_integer(conn)

    p_integer = {"id": str(p_id)}

    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=p_integer)
    assert response.status_code == 201
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=p_integer)
    assert response.status_code == 405
    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == 1


def add_integers(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    items = []
    for _ in range(POST_LIMIT):
        items.append(p_integer)

    post = {"items": items}
    post_items(base_url, c_id, post, "integers", POST_LIMIT)


def add_points(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "point")

    point_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "point",
        "coordinates": [100, 200],
    }

    items = []
    for _ in range(POST_LIMIT):
        items.append(point_annot)

    post = {"items": items}
    post_items(base_url, c_id, post, "points", POST_LIMIT)


def add_classes(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "class")
        annot_id = create_point(conn)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(annot_id),
        "reference_type": "annotation",
    }

    items = []
    for _ in range(POST_LIMIT):
        items.append(new_class)

    post = {"items": items}
    post_items(base_url, c_id, post, "classes", POST_LIMIT)


def add_wsis(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "wsi")

    items = []
    for i in range(POST_LIMIT):
        items.append({"id": f"Slide {i}", "type": "wsi"})

    post = {"items": items}
    post_items(base_url, c_id, post, "wsis", POST_LIMIT)

    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=post)
    assert response.status_code == 405
    post_duplicates = {"items": [{"id": "WSI1", "type": "wsi"}, {"id": "WSI1", "type": "wsi"}]}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=post_duplicates)
    assert response.status_code == 405


def add_collections(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "collection")

    new_shallow_collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "point",
    }

    items = []
    for _ in range(10):
        items.append(new_shallow_collection)

    post = {"items": items}
    post_items(base_url, c_id, post, "collections", 10)


def add_nested_collections(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_nested_collection(conn)

    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    p_items = []
    for _ in range(10):
        p_items.append(p_integer)

    new_collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
        "items": p_items,
    }

    items = []
    for _ in range(10):
        items.append(new_collection)

    post = {"items": items}
    result, c_result = post_items(base_url, c_id, post, "collections", 10, 20)
    for coll in result["items"]:
        assert coll["item_count"] == 10
        assert len(coll["items"]) == 10
    assert c_result["item_count"] == 20


def add_over_limit(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "wsi")

    items = []
    for i in range(POST_LIMIT + 1):
        items.append({"id": f"Slide {i}"})

    post = {"items": items}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=post)
    assert response.status_code == 413


def add_wrong_type_in_list(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, item_type="integer")

    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    p_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    post = {"items": [p_integer, p_float]}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=post)
    assert response.status_code == 422
    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == 0


def add_integers_by_id(base_url, up):
    p_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")
        for _ in range(POST_LIMIT):
            p_ids.append({"id": str(create_integer(conn))})

    post = {"items": p_ids}
    post_items(base_url, c_id, post, "ids", POST_LIMIT)


def add_points_by_id(base_url, up):
    a_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "point")
        for _ in range(POST_LIMIT):
            a_ids.append({"id": str(create_point(conn))})

    post = {"items": a_ids}
    post_items(base_url, c_id, post, "ids", POST_LIMIT)


def add_classes_by_id(base_url, up):
    cl_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "class")
        annot_id = create_point(conn)
        for _ in range(POST_LIMIT):
            cl_ids.append({"id": str(create_class(conn, annot_id))})

    post = {"items": cl_ids}
    post_items(base_url, c_id, post, "ids", POST_LIMIT)


def add_wsis_by_id(base_url, up):
    w_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "wsi")

    for _ in range(POST_LIMIT):
        w_ids.append({"id": str(uuid4())})

    post = {"items": w_ids}
    post_items(base_url, c_id, post, "ids", POST_LIMIT)


def add_collections_by_id(base_url, up):
    c_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "collection")
        for _ in range(POST_LIMIT):
            c_ids.append({"id": str(create_shallow(conn, "integer"))})

    post = {"items": c_ids}
    _, c_result = post_items(base_url, c_id, post, "ids", POST_LIMIT)
    for item in c_result["items"]:
        Collection.model_validate(item)
        assert item["item_type"] == "integer"


def add_double(base_url, up):
    p_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")
        for _ in range(POST_LIMIT // 2):
            p_id = str(create_integer(conn))
            p_ids.append({"id": p_id})
            p_ids.append({"id": p_id})

    post = {"items": p_ids}
    post_items(base_url, c_id, post, "ids", POST_LIMIT // 2)
