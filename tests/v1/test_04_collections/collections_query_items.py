import requests

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, query_unique_references_items
from ..test_01_annotations.commons.utils_create import create_point
from ..test_02_classes.commons.utils_create import create_class
from ..test_03_primitives.commons.utils_create import create_integer
from .commons.utils_create import (
    create_integer_collection,
    create_nested_collection_different_references,
    create_shallow,
)
from .commons.utils_requests import query_items

CR_ID = "User"
CR_TYPE = "user"
REF_ID = "WSI"
REF_ID_1 = "WSI 1"
REF_ID_2 = "WSI 2"
REF_ID_3 = "WSI 3"
NPP_C_1 = 10.0
NPP_C_2 = 50.0
NPP_V_1 = [10.0, 20.0]
NPP_V_2 = None


def collections_query_items(up):
    base_url = get_base_url(up.annot_container)

    # query basic
    query_empty(base_url, up)
    query_over_limit(base_url, up)
    clear_database(up)

    # annotations
    query_annotations(base_url, up)
    clear_database(up)

    # classes
    query_classes(base_url, up)
    clear_database(up)

    # primitives
    query_primitives(base_url, up)
    clear_database(up)

    # collections
    query_collections(base_url, up)
    clear_database(up)


def query_empty(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    query = {"references": [CR_ID]}
    query_items(base_url, c_id, query, 0, 0)


def query_over_limit(base_url, up):
    i_count = ITEM_LIMIT + 1
    with get_db(up.db_container).begin() as conn:
        c_id = create_integer_collection(conn, i_count=i_count)

    query = {"references": [REF_ID]}
    response = requests.put(f"{base_url}/collections/{c_id}/items/query", json=query)
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({i_count}) exceeds server limit of {ITEM_LIMIT}."


def query_annotations(base_url, up):
    a_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "point", ref_id=REF_ID_1)
        a_ids.append({"id": str(create_point(conn, CR_ID, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_1, [100, 100]))})
        a_ids.append({"id": str(create_point(conn, CR_ID, CR_TYPE, REF_ID_2, NPP_C_2, NPP_V_1, [100, 100]))})
        a_ids.append({"id": str(create_point(conn, CR_ID, CR_TYPE, REF_ID_1, NPP_C_1, NPP_V_2, [500, 500]))})
        a_ids.append({"id": str(create_point(conn, CR_ID, CR_TYPE, REF_ID_2, NPP_C_2, NPP_V_2, [500, 500]))})

    point_annot = {"items": a_ids}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=point_annot)
    assert response.status_code == 201

    query_1 = {"references": [REF_ID_1]}
    query_items(base_url, c_id, query_1, 2, 2)

    query_2 = {"references": [REF_ID_1, REF_ID_2]}
    query_items(base_url, c_id, query_2, 4, 4)

    query_3 = {"viewport": {"x": 0, "y": 0, "width": 200, "height": 200}}
    query_items(base_url, c_id, query_3, 2, 2)

    query_4 = {"viewport": {"x": 1000, "y": 1000, "width": 200, "height": 200}}
    query_items(base_url, c_id, query_4, 0, 0)

    query_5 = {"viewport": {"x": 0, "y": 0, "width": 2000, "height": 2000}}
    query_items(base_url, c_id, query_5, 4, 4)

    query_6 = {"npp_viewing": [1.0, 30.0]}
    query_items(base_url, c_id, query_6, 3, 3)

    query_7 = {"npp_viewing": [30.0, 60.0]}
    query_items(base_url, c_id, query_7, 1, 1)

    query_8 = {"npp_viewing": [1.0, 100]}
    query_items(base_url, c_id, query_8, 4, 4)

    query_9 = {"npp_viewing": [60.0, 100]}
    query_items(base_url, c_id, query_9, 0, 0)

    query_10 = {"references": [REF_ID_1], "viewport": {"x": 0, "y": 0, "width": 200, "height": 200}}
    query_items(base_url, c_id, query_10, 1, 1)

    query_11 = {
        "references": [REF_ID_1, REF_ID_2],
        "viewport": {"x": 0, "y": 0, "width": 200, "height": 200},
        "npp_viewing": [1.0, 100],
    }
    query_items(base_url, c_id, query_11, 2, 2)

    query_12 = {
        "references": [REF_ID_3],
        "viewport": {"x": 0, "y": 0, "width": 2000, "height": 2000},
        "npp_viewing": [1.0, 100],
    }
    query_items(base_url, c_id, query_12, 0, 0)

    query_13 = {
        "references": [REF_ID_1, REF_ID_2],
        "viewport": {"x": 2000, "y": 2000, "width": 2000, "height": 2000},
        "npp_viewing": [1.0, 100],
    }
    query_items(base_url, c_id, query_13, 0, 0)

    query_14 = {
        "references": [REF_ID_1, REF_ID_2],
        "viewport": {"x": 0, "y": 0, "width": 2000, "height": 2000},
        "npp_viewing": [1.0, 100],
    }
    query_items(base_url, c_id, query_14, 4, 4)

    query_15 = {
        "references": [REF_ID_1, REF_ID_2],
        "viewport": {"x": 0, "y": 0, "width": 2000, "height": 2000},
        "npp_viewing": [60.0, 100],
    }
    query_items(base_url, c_id, query_15, 0, 0)

    # queries for unique references
    query_ref = {
        "viewport": {"x": 0, "y": 0, "width": 2000, "height": 2000},
        "npp_viewing": [1.0, 100],
    }
    r = query_unique_references_items(base_url, c_id, query_ref)
    assert len(r["wsi"]) == 2


def query_classes(base_url, up):
    cl_ids = []
    with get_db(up.db_container).begin() as conn:
        a_id_1 = create_point(conn)
        a_id_2 = create_point(conn)
        c_id = create_shallow(conn, "class", ref_id=REF_ID_1)
        cl_ids.append({"id": str(create_class(conn, a_id_1, CR_ID, CR_TYPE))})
        cl_ids.append({"id": str(create_class(conn, a_id_2, CR_ID, CR_TYPE))})

    cl = {"items": cl_ids}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=cl)
    assert response.status_code == 201

    query_1 = {"references": [str(a_id_1)]}
    query_items(base_url, c_id, query_1, 1, 1)

    query_2 = {"references": [str(a_id_1), str(a_id_2)]}
    query_items(base_url, c_id, query_2, 2, 2)

    query_3 = {}
    query_items(base_url, c_id, query_3, 2, 2)

    # queries for unique references
    query_ref = {}
    r = query_unique_references_items(base_url, c_id, query_ref)
    assert len(r["annotation"]) == 2


def query_primitives(base_url, up):
    p_ids = []
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer", ref_id=REF_ID_1)
        p_ids.append({"id": str(create_integer(conn, CR_ID, CR_TYPE, REF_ID_1, "wsi", 42))})
        p_ids.append({"id": str(create_integer(conn, CR_ID, CR_TYPE, REF_ID_2, "wsi", 42))})

    p = {"items": p_ids}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=p)
    assert response.status_code == 201

    query_1 = {"references": [REF_ID_1]}
    query_items(base_url, c_id, query_1, 1, 1)

    query_2 = {"references": [REF_ID_1, REF_ID_2]}
    query_items(base_url, c_id, query_2, 2, 2)

    query_3 = {}
    query_items(base_url, c_id, query_3, 2, 2)

    # queries for unique references
    query_ref = {}
    r = query_unique_references_items(base_url, c_id, query_ref)
    assert len(r["wsi"]) == 2
    assert not r["contains_items_without_reference"]


def query_collections(base_url, up):
    item_count = 5
    with get_db(up.db_container).begin() as conn:
        c_id = create_nested_collection_different_references(
            conn, i_count=item_count, ref_id_1=REF_ID_1, ref_id_2=REF_ID_2
        )

    query_1 = {"references": [REF_ID_1]}
    query_items(base_url, c_id, query_1, item_count, item_count)

    query_2 = {"references": [REF_ID_1, REF_ID_2]}
    query_items(base_url, c_id, query_2, item_count * 2, item_count * 2)

    query_3 = {}
    query_items(base_url, c_id, query_3, item_count * 2, item_count * 2)

    # queries for unique references
    query_ref = {}
    r = query_unique_references_items(base_url, c_id, query_ref)
    assert len(r["wsi"]) == 2
    assert not r["contains_items_without_reference"]
