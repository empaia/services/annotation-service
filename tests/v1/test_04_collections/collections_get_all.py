import requests

from annotation_service.models.v1.annotation.collections import CollectionList

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database
from .commons.utils_create import (
    create_class_collection,
    create_integer_collection,
    create_nested_collection,
    create_point_collection,
    create_shallow,
    create_wsi_collection,
)
from .commons.utils_requests import get_collections

ITEM_COUNT = 10


def collections_get_all(up):
    base_url = get_base_url(up.annot_container)

    get_all_empty(base_url)
    get_all_filled(base_url, up)
    get_all_skip_limit(base_url)
    get_all_over_limit(base_url, up)

    clear_database(up)


def get_all_empty(base_url):
    response = requests.get(f"{base_url}/collections")
    assert response.status_code == 200
    result = response.json()
    CollectionList.model_validate(result)
    assert result["item_count"] == 0
    assert len(result["items"]) == 0


def get_all_filled(base_url, up):
    with get_db(up.db_container).begin() as conn:
        create_shallow(conn, "integer")
        create_integer_collection(conn, ITEM_COUNT)
        create_point_collection(conn, ITEM_COUNT)
        create_class_collection(conn, ITEM_COUNT)
        create_wsi_collection(conn, ITEM_COUNT)
        create_nested_collection(conn, ITEM_COUNT)

    c_count = 6 + ITEM_COUNT
    get_collections(base_url, c_count, c_count, ITEM_COUNT, ITEM_COUNT)


def get_all_skip_limit(base_url):
    c_count = 6 + ITEM_COUNT
    params = {"skip": 0, "limit": 2}
    get_collections(base_url, c_count, 2, ITEM_COUNT, ITEM_COUNT, params=params)


def get_all_over_limit(base_url, up):
    c_count = ITEM_LIMIT - (6 + ITEM_COUNT) + 1

    with get_db(up.db_container).begin() as conn:
        for _ in range(c_count):
            create_shallow(conn, item_type="integer")
    response = requests.get(f"{base_url}/collections")
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({ITEM_LIMIT + 1}) exceeds server limit of {ITEM_LIMIT}."
