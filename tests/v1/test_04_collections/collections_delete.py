import requests

from annotation_service.models.v1.commons import IdObject

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_count, get_count_for_id
from .commons.utils_count import get_item_count
from .commons.utils_create import create_integer_collection, create_shallow


def collections_delete(up):
    base_url = get_base_url(up.annot_container)

    delete_shallow(base_url, up)
    delete_locked(base_url, up)
    delete_integer(base_url, up)

    clear_database(up)


def delete_shallow(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer")

    response = requests.delete(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(c_id)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, c_id, "collections") == 0


def delete_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer", is_locked=True)

    response = requests.delete(f"{base_url}/collections/{c_id}")
    assert response.status_code == 423
    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200


def delete_integer(base_url, up):
    item_count = 10
    with get_db(up.db_container).begin() as conn:
        c_id = create_integer_collection(conn, item_count)

    response = requests.delete(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(c_id)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, c_id, "collections") == 0
        assert get_item_count(conn, c_id) == 0
        assert get_count(conn, "primitives") == item_count
