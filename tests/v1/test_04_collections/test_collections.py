import docker

from ..commons.setup_tests import Up
from ..commons.utils import clear_database
from .collections_delete import collections_delete
from .collections_delete_item import collections_delete_item
from .collections_get_all import collections_get_all
from .collections_get_by_id import collections_get_by_id
from .collections_post import collections_post
from .collections_post_items import collections_post_items
from .collections_put import collections_put
from .collections_query import collections_query
from .collections_query_items import collections_query_items


def test_collections():
    client = docker.from_env()
    with Up(client) as up:
        collections_get_by_id(up)
        collections_get_all(up)
        collections_post(up)
        collections_put(up)
        collections_query(up)
        collections_delete(up)
        collections_post_items(up)
        collections_query_items(up)
        collections_delete_item(up)

        clear_database(up)
