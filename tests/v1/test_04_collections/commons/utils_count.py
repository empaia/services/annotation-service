from sqlalchemy.sql import text


def get_item_count(conn, c_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM c_collection_items
            WHERE collection_id=:collection_id;
            """
        ),
        collection_id=c_id,
    ).first()["count"]
