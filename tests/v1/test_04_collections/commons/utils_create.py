from uuid import uuid4

from sqlalchemy.sql import text

from ...test_01_annotations.commons.utils_create import create_point
from ...test_02_classes.commons.utils_create import create_class
from ...test_03_primitives.commons.utils_create import create_integer
from .utils_check import DESC, NAME

CR_ID = "User"
CR_TYPE = "user"
REF_ID = "WSI"
REF_TYPE = "wsi"


def create_shallow(conn, item_type, cr_id=CR_ID, cr_type=CR_TYPE, ref_id=REF_ID, ref_type=REF_TYPE, is_locked=False):
    result = conn.execute(
        text(
            """
            INSERT INTO c_collections(
                type,
                name,
                description,
                creator_id,
                creator_type,
                reference_id,
                reference_type,
                item_type,
                is_locked
            )
            VALUES(
                :type,
                :name,
                :description,
                :creator_id,
                :creator_type,
                :reference_id,
                :reference_type,
                :item_type,
                :is_locked
            ) RETURNING id;
            """
        ),
        type="collection",
        name=NAME,
        description=DESC,
        creator_id=cr_id,
        creator_type=cr_type,
        reference_id=ref_id,
        reference_type=ref_type,
        item_type=item_type,
        is_locked=is_locked,
    ).first()

    return result["id"]


def add_item(conn, c_id, i_id):
    conn.execute(
        text(
            """
            INSERT INTO c_collection_items(
                collection_id,
                item_id
            )
            VALUES(
                :collection_id,
                :item_id
            );
            """
        ),
        collection_id=c_id,
        item_id=i_id,
    )


def create_integer_collection(
    conn, i_count=10, cr_id=CR_ID, cr_type=CR_TYPE, ref_id=REF_ID, ref_type=REF_TYPE, is_locked=False
):
    c_id = create_shallow(
        conn,
        item_type="integer",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
    )

    for _ in range(i_count):
        item_id = create_integer(conn, ref_id=ref_id)
        add_item(conn, c_id, item_id)
    return c_id


def create_point_collection(
    conn, i_count=10, cr_id=CR_ID, cr_type=CR_TYPE, ref_id=REF_ID, ref_type=REF_TYPE, is_locked=False
):
    c_id = create_shallow(
        conn,
        item_type="point",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
    )

    for _ in range(i_count):
        item_id = create_point(conn)
        add_item(conn, c_id, item_id)
    return c_id


def create_class_collection(
    conn, i_count=10, creator_id=CR_ID, cr_type=CR_TYPE, ref_id=REF_ID, ref_type=REF_TYPE, is_locked=False
):
    c_id = create_shallow(
        conn,
        item_type="class",
        cr_id=creator_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
    )

    a_id = create_point(conn)
    for _ in range(i_count):
        item_id = create_class(conn, a_id)
        add_item(conn, c_id, item_id)
    return c_id


def create_wsi_collection(
    conn, i_count=10, cr_id=CR_ID, cr_type=CR_TYPE, ref_id=REF_ID, ref_type=REF_TYPE, is_locked=False
):
    c_id = create_shallow(
        conn,
        item_type="wsi",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
    )

    for _ in range(i_count):
        item_id = str(uuid4())
        add_item(conn, c_id, item_id)
    return c_id


def create_nested_collection(
    conn, i_count=10, cr_id=CR_ID, cr_type=CR_TYPE, ref_id=REF_ID, ref_type=REF_TYPE, is_locked=False
):
    c_id = create_shallow(
        conn,
        item_type="collection",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id,
        ref_type=ref_type,
        is_locked=is_locked,
    )

    for _ in range(i_count):
        item_id = create_integer_collection(conn, i_count=i_count)
        add_item(conn, c_id, item_id)
    return c_id


def create_nested_collection_different_references(
    conn, i_count=10, cr_id="User", cr_type="user", ref_id_1="WSI 1", ref_id_2="WSI 2", ref_type="wsi", is_locked=False
):
    c_id = create_shallow(
        conn,
        item_type="collection",
        cr_id=cr_id,
        cr_type=cr_type,
        ref_id=ref_id_1,
        ref_type=ref_type,
        is_locked=is_locked,
    )

    for _ in range(i_count):
        item_id_1 = create_integer_collection(conn, i_count=i_count, ref_id=ref_id_1)
        add_item(conn, c_id, item_id_1)
        item_id_2 = create_integer_collection(conn, i_count=i_count, ref_id=ref_id_2)
        add_item(conn, c_id, item_id_2)
    return c_id
