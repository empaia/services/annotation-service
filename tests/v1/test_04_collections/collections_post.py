from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.annotations import (
    ArrowAnnotation,
    CircleAnnotation,
    LineAnnotation,
    PointAnnotation,
    PolygonAnnotation,
    RectangleAnnotation,
)
from annotation_service.models.v1.annotation.classes import Class
from annotation_service.models.v1.annotation.collections import Collection, SlideItem
from annotation_service.models.v1.annotation.primitives import (
    BoolPrimitive,
    FloatPrimitive,
    IntegerPrimitive,
    StringPrimitive,
)
from annotation_service.models.v1.commons import IdObject

from ..commons.setup_tests import POST_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, get_count
from ..test_01_annotations.commons.utils_count import get_type_count as annot_type_count
from ..test_01_annotations.commons.utils_create import create_point
from ..test_03_primitives.commons.utils_count import get_type_count as primitive_type_count
from .commons.utils_check import check_collection_post_put

CR_ID = uuid4()
REF_ID = uuid4()
ITEM_COUNT = 10
C_COUNT = 10
L_COUNT = 10


def collections_post(up):
    base_url = get_base_url(up.annot_container)

    # shallow
    post_shallow(base_url, up)
    post_empty_items(base_url)
    post_missing_optionals(base_url)
    post_reference_error(base_url)
    post_external(base_url)
    clear_database(up)

    # annotations
    post_points(base_url, up)
    post_lines(base_url, up)
    post_arrows(base_url, up)
    post_circles(base_url, up)
    post_rectangles(base_url, up)
    post_polygons(base_url, up)
    clear_database(up)

    # classes
    post_classes(base_url, up)
    clear_database(up)

    # primitives
    post_integers(base_url, up)
    post_floats(base_url, up)
    post_bools(base_url, up)
    post_strings(base_url, up)
    clear_database(up)

    # wsi
    post_wsis(base_url, up)
    clear_database(up)

    # item ids
    post_item_id(base_url, up)
    clear_database(up)

    # nested
    post_shallow_nested(base_url, up)
    post_filled(base_url, up)
    post_over_limit(base_url)
    post_wrong_item_type(base_url)
    post_mixed_types_in_list(base_url)
    clear_database(up)


def post_shallow(base_url, up):
    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, result["id"], collection)
    assert result["item_count"] == 0
    assert len(result["items"]) == 0


def post_empty_items(base_url):
    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
        "items": [],
    }
    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)


def post_missing_optionals(base_url):
    collection = {
        "type": "collection",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "item_type": "integer",
    }
    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)


def post_reference_error(base_url):
    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "item_type": "integer",
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 422

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_type": "wsi",
        "item_type": "integer",
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 422

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "annotation",
        "item_type": "integer",
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 404
    assert post_response.json()["detail"].endswith("does not exist.")


def post_external(base_url):
    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
    }
    params = {"external_ids": True}
    post_response = requests.post(f"{base_url}/collections", params=params, json=collection)
    assert post_response.status_code == 201
    response_json = post_response.json()
    Collection.model_validate(response_json)
    assert response_json["id"] != collection["id"]


def post_points(base_url, up):
    point_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "point",
        "coordinates": [100, 200],
    }

    a_list = []
    for _ in range(0, ITEM_COUNT):
        a_list.append(point_annot)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "point",
        "items": a_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        PointAnnotation.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert annot_type_count(conn, "point") == ITEM_COUNT


def post_lines(base_url, up):
    line_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "line",
        "coordinates": [[100, 200], [300, 400]],
    }

    a_list = []
    for _ in range(0, ITEM_COUNT):
        a_list.append(line_annot)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "line",
        "items": a_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        LineAnnotation.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert annot_type_count(conn, "line") == ITEM_COUNT


def post_arrows(base_url, up):
    arrow_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "arrow",
        "head": [100, 200],
        "tail": [300, 400],
    }

    a_list = []
    for _ in range(0, ITEM_COUNT):
        a_list.append(arrow_annot)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "arrow",
        "items": a_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        ArrowAnnotation.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert annot_type_count(conn, "arrow") == ITEM_COUNT


def post_circles(base_url, up):
    circle_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "circle",
        "center": [100, 200],
        "radius": 100,
    }

    a_list = []
    for _ in range(0, ITEM_COUNT):
        a_list.append(circle_annot)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "circle",
        "items": a_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        CircleAnnotation.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert annot_type_count(conn, "circle") == ITEM_COUNT


def post_rectangles(base_url, up):
    rect_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "rectangle",
        "upper_left": [100, 200],
        "width": 200,
        "height": 300,
    }

    a_list = []
    for _ in range(0, ITEM_COUNT):
        a_list.append(rect_annot)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "rectangle",
        "items": a_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        RectangleAnnotation.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert annot_type_count(conn, "rectangle") == ITEM_COUNT


def post_polygons(base_url, up):
    poly_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "polygon",
        "coordinates": [[100, 200], [300, 400], [500, 600]],
    }

    a_list = []
    for _ in range(0, ITEM_COUNT):
        a_list.append(poly_annot)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "polygon",
        "items": a_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        PolygonAnnotation.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert annot_type_count(conn, "polygon") == ITEM_COUNT


def post_classes(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)

    cl = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(a_id),
        "reference_type": "annotation",
    }

    cl_list = []
    for _ in range(0, ITEM_COUNT):
        cl_list.append(cl)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "class",
        "items": cl_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        Class.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert get_count(conn, "classes") == ITEM_COUNT


def post_integers(base_url, up):
    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    p_list = []
    for _ in range(0, ITEM_COUNT):
        p_list.append(p_integer)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
        "items": p_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        IntegerPrimitive.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert primitive_type_count(conn, "integer") == ITEM_COUNT

    # inconsistent references
    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
        "items": [
            {
                "name": "Nice",
                "description": "Nice",
                "value": 42,
                "type": "integer",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(uuid4()),
                "reference_type": "annotation",
            }
        ],
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 404
    assert post_response.json()["detail"].endswith("nonexistent annotation.")


def post_floats(base_url, up):
    p_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    p_list = []
    for _ in range(0, ITEM_COUNT):
        p_list.append(p_float)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "float",
        "items": p_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        FloatPrimitive.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert primitive_type_count(conn, "float") == ITEM_COUNT


def post_bools(base_url, up):
    p_bool = {
        "name": "Nice",
        "description": "Nice",
        "value": True,
        "type": "bool",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    p_list = []
    for _ in range(0, ITEM_COUNT):
        p_list.append(p_bool)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "bool",
        "items": p_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        BoolPrimitive.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert primitive_type_count(conn, "bool") == ITEM_COUNT


def post_strings(base_url, up):
    p_string = {
        "name": "Nice",
        "description": "Nice",
        "value": "Nice",
        "type": "string",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    p_list = []
    for _ in range(0, ITEM_COUNT):
        p_list.append(p_string)

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "string",
        "items": p_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        StringPrimitive.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)
        assert primitive_type_count(conn, "string") == ITEM_COUNT


def post_wsis(base_url, up):
    w_list = []
    for i in range(0, ITEM_COUNT):
        w_list.append({"id": f"Slide {i}", "type": "wsi"})

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "wsi",
        "items": w_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        SlideItem.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)

    collection_duplicates = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "wsi",
        "items": [{"id": "WSI1", "type": "wsi"}, {"id": "WSI1", "type": "wsi"}],
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_duplicates)
    assert post_response.status_code == 405


def post_item_id(base_url, up):
    item_list = []
    with get_db(up.db_container).begin() as conn:
        for _ in range(0, ITEM_COUNT):
            item_list.append({"id": str(create_point(conn))})

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "point",
        "items": item_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == ITEM_COUNT
    assert len(result["items"]) == ITEM_COUNT
    for annot in result["items"]:
        IdObject.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection, i_count=ITEM_COUNT)

    collection_duplicates = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "wsi",
        "items": [{"id": "WSI1"}, {"id": "WSI1"}],
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_duplicates)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection_duplicates, i_count=1)


def post_shallow_nested(base_url, up):
    c_list = []
    for i in range(C_COUNT):
        c_list.append(
            {
                "type": "collection",
                "name": f"Nice_{i}",
                "description": "Nice",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
                "item_type": "point",
            }
        )

    collection_outer = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "collection",
        "items": c_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_outer)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    assert result["item_count"] == C_COUNT
    assert len(result["items"]) == C_COUNT
    for c in result["items"]:
        Collection.model_validate(c)
        assert c["item_count"] == 0
        assert len(c["items"]) == 0
    # check for correct order of response
    for i in range(C_COUNT):
        assert c_list[i]["name"] == result["items"][i]["name"]

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, collection_outer, i_count=C_COUNT)
        assert get_count(conn, "collections") == C_COUNT + 1
        for c in result["items"]:
            check_collection_post_put(conn, c["id"], c)

    c_list = []
    for i in range(C_COUNT):
        c_list.append(
            {
                "type": "collection",
                "name": f"Nice_{i}",
                "description": "Nice",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
                "item_type": "point",
                "items": [],
            }
        )

    collection_outer = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "collection",
        "items": c_list,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_outer)
    assert post_response.status_code == 201
    result = post_response.json()
    Collection.model_validate(result)
    for c in result["items"]:
        Collection.model_validate(c)
        assert c["item_count"] == 0
        assert len(c["items"]) == 0
    # check for correct order of response
    for i in range(C_COUNT):
        assert c_list[i]["name"] == result["items"][i]["name"]


def post_filled(base_url, up):
    a_list = []
    for i in range(L_COUNT):
        a_list.append(
            {
                "name": f"Nice annotation_{i}",
                "description": "Very nice annotation",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
            }
        )

    c_list = []
    for i in range(C_COUNT):
        c_list.append(
            {
                "type": "collection",
                "name": "Nice_{i}",
                "description": "Nice",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
                "item_type": "point",
                "items": a_list,
            }
        )

    collection_outer = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "collection",
        "items": c_list,
    }

    response = requests.post(f"{base_url}/collections", json=collection_outer)
    assert response.status_code == 201
    result = response.json()
    Collection.model_validate(result)
    for c in result["items"]:
        Collection.model_validate(c)
        for annot in c["items"]:
            PointAnnotation.model_validate(annot)

    c_id = result["id"]
    with get_db(up.db_container).begin() as conn:
        check_collection_post_put(conn, c_id, result, i_count=C_COUNT)
        assert get_count(conn, "collections") == (C_COUNT + 1) * 3
        assert get_count(conn, "annotations") == C_COUNT * L_COUNT

    # check for correct order of response
    for i in range(C_COUNT):
        coll = result["items"][i]
        assert c_list[i]["name"] == coll["name"]
        for j in range(L_COUNT):
            assert a_list[j]["name"] == coll["items"][j]["name"]


def post_over_limit(base_url):
    point_annot = {
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "point",
        "coordinates": [100, 200],
    }

    a_list = [point_annot for _ in range(POST_LIMIT)]

    collection_inner = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "point",
        "items": a_list,
    }

    c_list = [collection_inner, collection_inner]

    collection_nested = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "collection",
        "items": c_list,
    }

    collection_outer = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "collection",
        "items": [collection_nested, collection_nested],
    }

    post_response = requests.post(f"{base_url}/collections", json=collection_outer)
    assert post_response.status_code == 413


def post_wrong_item_type(base_url):
    p_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    items = [p_float, p_float]

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
        "items": items,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 405


def post_mixed_types_in_list(base_url):
    p_integer = {
        "name": "Some value",
        "description": "Very nice value",
        "creator_id": "Job 1",
        "creator_type": "job",
        "reference_id": "annot_id",
        "reference_type": "annotation",
        "type": "integer",
        "value": 42,
    }

    p_float = {
        "name": "Some value",
        "description": "Very nice value",
        "creator_id": "Job 1",
        "creator_type": "job",
        "reference_id": "annot_id",
        "reference_type": "annotation",
        "type": "float",
        "value": 0.42,
    }

    items = [p_integer, p_float]

    collection = {
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
        "items": items,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 422
