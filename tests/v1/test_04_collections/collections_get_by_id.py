from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.annotations import PointAnnotation
from annotation_service.models.v1.annotation.classes import Class
from annotation_service.models.v1.annotation.collections import Collection, SlideItem
from annotation_service.models.v1.annotation.primitives import IntegerPrimitive

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database
from ..test_04_collections.commons.utils_check import check_collection
from .commons.utils_create import (
    create_class_collection,
    create_integer_collection,
    create_nested_collection,
    create_point_collection,
    create_shallow,
    create_wsi_collection,
)

CR_ID = "Job 1"
CR_TYPE = "job"
REF_ID = uuid4()
REF_TYPE = "wsi"
ITEM_COUNT = 10


def collections_get_by_id(up):
    base_url = get_base_url(up.annot_container)

    get_shallow(base_url, up)
    get_integer(base_url, up)
    get_point(base_url, up)
    get_class(base_url, up)
    get_wsi(base_url, up)
    get_collection(base_url, up)
    get_non_existent(base_url)

    clear_database(up)


def get_shallow(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "integer", CR_ID, CR_TYPE, REF_ID, REF_TYPE)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "integer", CR_ID, CR_TYPE, REF_ID, REF_TYPE, 0, 0)


def get_integer(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_integer_collection(conn, ITEM_COUNT, CR_ID, CR_TYPE, REF_ID, REF_TYPE)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "integer", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for item in result["items"]:
        IntegerPrimitive.model_validate(item)
        assert item["type"] == "integer"

    params = {"shallow": True}
    response = requests.get(f"{base_url}/collections/{c_id}", params=params)
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "integer", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, 0)

    params = {"with_leaf_ids": True}
    response = requests.get(f"{base_url}/collections/{c_id}", params=params)
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "integer", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT, item_id=True)

    params = {"shallow": True, "with_leaf_ids": True}
    response = requests.get(f"{base_url}/collections/{c_id}", params=params)
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "integer", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, 0, item_id=True)


def get_point(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_point_collection(conn, ITEM_COUNT, CR_ID, CR_TYPE, REF_ID, REF_TYPE)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "point", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for item in result["items"]:
        PointAnnotation.model_validate(item)
        assert item["type"] == "point"


def get_class(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_class_collection(conn, ITEM_COUNT, CR_ID, CR_TYPE, REF_ID, REF_TYPE)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "class", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for item in result["items"]:
        Class.model_validate(item)
        assert item["value"] == "value"


def get_wsi(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_wsi_collection(conn, ITEM_COUNT, CR_ID, CR_TYPE, REF_ID, REF_TYPE)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "wsi", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for item in result["items"]:
        SlideItem.model_validate(item)


def get_collection(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_nested_collection(conn, ITEM_COUNT, CR_ID, CR_TYPE, REF_ID, REF_TYPE)

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "collection", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for coll in result["items"]:
        Collection.model_validate(coll)
        assert coll["item_count"] == ITEM_COUNT
        assert len(coll["items"]) == ITEM_COUNT
        for integer in coll["items"]:
            IntegerPrimitive.model_validate(integer)
            assert integer["type"] == "integer"

    params = {"shallow": True}
    response = requests.get(f"{base_url}/collections/{c_id}", params=params)
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "collection", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for coll in result["items"]:
        Collection.model_validate(coll)
        assert coll["item_count"] == ITEM_COUNT
        assert coll["items"] == []
        assert "item_ids" not in coll

    params = {"with_leaf_ids": True}
    response = requests.get(f"{base_url}/collections/{c_id}", params=params)
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "collection", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for coll in result["items"]:
        Collection.model_validate(coll)
        assert coll["item_count"] == ITEM_COUNT
        assert len(coll["items"]) == ITEM_COUNT
        assert "item_ids" in coll
        assert len(coll["item_ids"]) == ITEM_COUNT

    params = {"shallow": True, "with_leaf_ids": True}
    response = requests.get(f"{base_url}/collections/{c_id}", params=params)
    assert response.status_code == 200
    result = response.json()
    Collection.model_validate(result)
    check_collection(result, c_id, "collection", CR_ID, CR_TYPE, REF_ID, REF_TYPE, ITEM_COUNT, ITEM_COUNT)
    for coll in result["items"]:
        Collection.model_validate(coll)
        assert coll["item_count"] == ITEM_COUNT
        assert coll["items"] == []
        assert "item_ids" in coll
        assert len(coll["item_ids"]) == ITEM_COUNT


def get_non_existent(base_url):
    response = requests.get(f"{base_url}/collections/{uuid4()}")
    assert response.status_code == 404
