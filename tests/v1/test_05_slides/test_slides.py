from uuid import uuid4

import docker

from ..commons.setup_tests import Up, get_base_url, get_db
from ..commons.utils import clear_database, get_job_count, get_job_count_for_item, lock_for_job


def test_slides():
    client = docker.from_env()
    with Up(client) as up:
        base_url = get_base_url(up.annot_container)

        job_id_1 = "job1"
        job_id_2 = "job2"

        with get_db(up.db_container).begin() as conn:
            slide_id = str(uuid4())

            lock_for_job(base_url, job_id_1, slide_id, "slides")
            assert get_job_count_for_item(conn, slide_id, "slides") == 1

            lock_for_job(base_url, job_id_1, slide_id, "slides")
            assert get_job_count_for_item(conn, slide_id, "slides") == 1

            lock_for_job(base_url, job_id_2, slide_id, "slides")
            assert get_job_count_for_item(conn, slide_id, "slides") == 2
            assert get_job_count(conn, slide_id, job_id_1, "slides") == 1
            assert get_job_count(conn, slide_id, job_id_2, "slides") == 1

        clear_database(up)
