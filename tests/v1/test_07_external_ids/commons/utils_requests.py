from uuid import uuid4

import requests

from ...commons.setup_tests import get_db
from ...commons.utils import get_count, get_count_for_id


def post_external(base_url, item, i_type, up=None, ci_type=None):
    post_response = requests.post(f"{base_url}/{i_type}", json=item)
    assert post_response.status_code == 201
    response_json = post_response.json()
    assert response_json["id"] != item["id"]

    if i_type == "collections" and "items" in item:
        with get_db(up.db_container).begin() as conn:
            for i in item["items"]:
                assert get_count_for_id(conn, i["id"], ci_type) == 0

    params = {"external_ids": True}
    post_response = requests.post(f"{base_url}/{i_type}", params=params, json=item)
    assert post_response.status_code == 201
    response_json = post_response.json()
    assert response_json["id"] == item["id"]

    if i_type == "collections" and "items" in item:
        with get_db(up.db_container).begin() as conn:
            for i in item["items"]:
                assert get_count_for_id(conn, i["id"], ci_type) == 1

    post_response = requests.post(f"{base_url}/{i_type}", params=params, json=item)
    assert post_response.status_code == 422
    assert post_response.text.endswith('with given id already exists"}')

    if i_type == "collections" and "items" in item:
        item["id"] = str(uuid4())
        post_response = requests.post(f"{base_url}/{i_type}", params=params, json=item)
        assert post_response.status_code == 422
        assert post_response.text.endswith('with given id already exists"}')

        old_items = item["items"]
        new_items = []
        for i in item["items"]:
            new_item = dict(i)
            new_item["id"] = str(uuid4())
            new_items.append(new_item)

        item["id"] = str(uuid4())
        item["items"] = new_items
        item["items"][-1].pop("id")
        post_response = requests.post(f"{base_url}/{i_type}", params=params, json=item)
        assert post_response.status_code == 422
        assert post_response.text.startswith('{"detail":"ERROR: No valid value')

        with get_db(up.db_container).begin() as conn:
            for i in item["items"]:
                if "id" in i:
                    assert get_count_for_id(conn, i["id"], "annotations") == 0
            assert get_count_for_id(conn, item["id"], "collections") == 0

        item["items"] = old_items

    item.pop("id")
    post_response = requests.post(f"{base_url}/{i_type}", params=params, json=item)
    assert post_response.status_code == 422
    assert post_response.text.startswith('{"detail":"ERROR: No valid value')


def post_many_external(up, base_url, items, i_type, i_count):
    with get_db(up.db_container).begin() as conn:
        count_before = get_count(conn, i_type)

    post_response = requests.post(f"{base_url}/{i_type}", json=items)
    assert post_response.status_code == 201
    response = post_response.json()
    assert len(response["items"]) == i_count

    with get_db(up.db_container).begin() as conn:
        count_after = get_count(conn, i_type)
        assert count_after - count_before == i_count

    params = {"external_ids": True}
    post_response = requests.post(f"{base_url}/{i_type}", params=params, json=items)
    assert post_response.status_code == 201
    response = post_response.json()

    with get_db(up.db_container).begin() as conn:
        count_after = get_count(conn, i_type)
        assert count_after - count_before == i_count * 2
        for i in items["items"]:
            assert get_count_for_id(conn, i["id"], i_type) == 1

    post_response = requests.post(f"{base_url}/{i_type}", params=params, json=items)
    assert post_response.status_code == 422
    assert post_response.text.endswith('with given id already exists"}')

    for i in items["items"]:
        i["id"] = str(uuid4())

    items["items"][-1].pop("id")
    post_response = requests.post(f"{base_url}/{i_type}", params=params, json=items)
    assert post_response.status_code == 422
    assert post_response.text.startswith('{"detail":"ERROR: No valid value')


def add_item(base_url, c_id, item):
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=item)
    assert response.status_code == 201
    result = response.json()
    assert result["id"] != item["id"]

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == 1

    params = {"external_ids": True}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", params=params, json=item)
    assert response.status_code == 201
    result = response.json()
    assert result["id"] == item["id"]

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == 2

    response = requests.post(f"{base_url}/collections/{c_id}/items/", params=params, json=item)
    assert response.status_code == 422

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == 2


def add_items(base_url, c_id, items, i_count):
    response = requests.post(f"{base_url}/collections/{c_id}/items/", json=items)
    assert response.status_code == 201

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == i_count

    params = {"external_ids": True}
    response = requests.post(f"{base_url}/collections/{c_id}/items/", params=params, json=items)
    assert response.status_code == 201

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == i_count * 2

    response = requests.post(f"{base_url}/collections/{c_id}/items/", params=params, json=items)
    assert response.status_code == 422

    response = requests.get(f"{base_url}/collections/{c_id}")
    assert response.status_code == 200
    response = response.json()
    assert response["item_count"] == i_count * 2
