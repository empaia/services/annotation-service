from uuid import uuid4

import requests

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_count_for_id
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_requests import post_external

CR_ID = uuid4()
REF_ID = uuid4()
POST_COUNT = 10


def collections_post_external(up):
    base_url = get_base_url(up.annot_container)

    post_shallow(base_url, up)
    post_points(base_url, up)
    post_integers(base_url, up)
    post_classes(base_url, up)
    post_collections_shallow(base_url, up)
    post_collections_filled(base_url, up)

    clear_database(up)


def post_shallow(base_url, up):
    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
    }
    post_external(base_url, collection, "collections", up, "primitives")


def post_points(base_url, up):
    items = []
    for _ in range(POST_COUNT):
        i = {
            "id": str(uuid4()),
            "name": "Nice annotation",
            "description": "Very nice annotation",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(REF_ID),
            "reference_type": "wsi",
            "npp_created": 45.67,
            "type": "point",
            "coordinates": [100, 200],
        }
        items.append(i)

    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "point",
        "items": items,
    }
    post_external(base_url, collection, "collections", up, "annotations")


def post_integers(base_url, up):
    items = []
    for _ in range(POST_COUNT):
        i = {
            "id": str(uuid4()),
            "name": "Nice",
            "description": "Nice",
            "value": 42,
            "type": "integer",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(REF_ID),
            "reference_type": "wsi",
        }
        items.append(i)

    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "integer",
        "items": items,
    }
    post_external(base_url, collection, "collections", up, "primitives")


def post_classes(base_url, up):
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)

    items = []
    for _ in range(POST_COUNT):
        i = {
            "id": str(uuid4()),
            "type": "class",
            "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(annot_id),
            "reference_type": "annotation",
        }
        items.append(i)

    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "class",
        "items": items,
    }
    post_external(base_url, collection, "collections", up, "classes")


def post_collections_shallow(base_url, up):
    items = []
    for _ in range(POST_COUNT):
        i = {
            "id": str(uuid4()),
            "type": "collection",
            "name": "Nice",
            "description": "Nice",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(REF_ID),
            "reference_type": "wsi",
            "item_type": "point",
        }
        items.append(i)

    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "collection",
        "items": items,
    }
    post_external(base_url, collection, "collections", up, "collections")


def post_collections_filled(base_url, up):
    all_items = []
    inner_cols = []
    for _ in range(POST_COUNT):
        items = []
        for _ in range(POST_COUNT):
            i = {
                "id": str(uuid4()),
                "name": "Nice annotation",
                "description": "Very nice annotation",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
            }
            items.append(i)
            all_items.append(i)

        c = {
            "id": str(uuid4()),
            "type": "collection",
            "name": "Nice",
            "description": "Nice",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(REF_ID),
            "reference_type": "wsi",
            "item_type": "point",
        }
        c["items"] = items
        inner_cols.append(c)

    collection = {
        "id": str(uuid4()),
        "type": "collection",
        "name": "Nice",
        "description": "Nice",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "item_type": "collection",
        "items": inner_cols,
    }

    post_response = requests.post(f"{base_url}/collections", json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    assert result["id"] != collection["id"]

    with get_db(up.db_container).begin() as conn:
        for i in inner_cols:
            assert get_count_for_id(conn, i["id"], "collections") == 0
            for a in all_items:
                assert get_count_for_id(conn, a["id"], "annotations") == 0

    params = {"external_ids": True}
    post_response = requests.post(f"{base_url}/collections", params=params, json=collection)
    assert post_response.status_code == 201
    result = post_response.json()
    assert result["id"] == collection["id"]

    with get_db(up.db_container).begin() as conn:
        for i in inner_cols:
            assert get_count_for_id(conn, i["id"], "collections") == 1
            for a in all_items:
                assert get_count_for_id(conn, a["id"], "annotations") == 1

    post_response = requests.post(f"{base_url}/collections", params=params, json=collection)
    assert post_response.status_code == 422
    assert post_response.text.endswith('with given id already exists"}')

    collection["id"] = str(uuid4())
    post_response = requests.post(f"{base_url}/collections", params=params, json=collection)
    assert post_response.status_code == 422
    assert post_response.text.endswith('with given id already exists"}')

    all_items_2 = []
    inner_cols_2 = []
    for _ in range(POST_COUNT):
        items = []
        for _ in range(POST_COUNT):
            i = {
                "id": str(uuid4()),
                "name": "Nice annotation",
                "description": "Very nice annotation",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
                "npp_created": 45.67,
                "type": "point",
                "coordinates": [100, 200],
            }
            items.append(i)
            all_items_2.append(i)

        c = {
            "id": str(uuid4()),
            "type": "collection",
            "name": "Nice",
            "description": "Nice",
            "creator_id": str(CR_ID),
            "creator_type": "job",
            "reference_id": str(REF_ID),
            "reference_type": "wsi",
            "item_type": "point",
        }
        c["items"] = items
        inner_cols_2.append(c)

    collection["id"] = str(uuid4())
    collection["items"] = inner_cols_2
    collection["items"][-1]["items"][-1].pop("id")
    post_response = requests.post(f"{base_url}/collections", params=params, json=collection)
    assert post_response.status_code == 422
    assert post_response.text.startswith('{"detail":"ERROR: No valid value')

    with get_db(up.db_container).begin() as conn:
        for i in inner_cols_2:
            assert get_count_for_id(conn, i["id"], "collections") == 0
            for a in all_items_2:
                if "id" in a:
                    assert get_count_for_id(conn, a["id"], "annotations") == 0
