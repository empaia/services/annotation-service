from uuid import uuid4

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database
from ..test_01_annotations.commons.utils_create import create_point
from ..test_04_collections.commons.utils_create import create_shallow
from .commons.utils_requests import add_item

CR_ID = "User"
REF_ID = "WSI"


def collections_add_item_external(up):
    base_url = get_base_url(up.annot_container)

    add_integer(base_url, up)
    add_point(base_url, up)
    add_class(base_url, up)

    clear_database(up)


def add_integer(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, item_type="integer")

    p_integer = {
        "id": str(uuid4()),
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    add_item(base_url, c_id, p_integer)


def add_point(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "point")

    point_annot = {
        "id": str(uuid4()),
        "name": "Nice annotation",
        "description": "Very nice annotation",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
        "npp_created": 45.67,
        "type": "point",
        "coordinates": [100, 200],
    }
    add_item(base_url, c_id, point_annot)


def add_class(base_url, up):
    with get_db(up.db_container).begin() as conn:
        c_id = create_shallow(conn, "class")
        annot_id = create_point(conn)

    new_class = {
        "id": str(uuid4()),
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(annot_id),
        "reference_type": "annotation",
    }
    add_item(base_url, c_id, new_class)
