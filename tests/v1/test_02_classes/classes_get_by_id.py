from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.classes import Class

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_create import create_class

CR_ID = "Job 1"
CR_TYPE = "job"


def classes_get_by_id(up):
    base_url = get_base_url(up.annot_container)

    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)
        class_id = create_class(conn, annot_id, CR_ID, CR_TYPE)

    class_response = requests.get(f"{base_url}/classes/{class_id}")
    assert class_response.status_code == 200
    class_response = class_response.json()
    Class.model_validate(class_response)
    assert class_response["id"] == str(class_id)
    assert class_response["type"] == "class"
    assert class_response["value"] == "value"
    assert class_response["creator_id"] == CR_ID
    assert class_response["creator_type"] == CR_TYPE
    assert class_response["reference_id"] == str(annot_id)
    assert class_response["reference_type"] == "annotation"

    response = requests.get(f"{base_url}/classes/{uuid4()}")
    assert response.status_code == 404

    clear_database(up)
