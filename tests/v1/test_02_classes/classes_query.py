import requests

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, lock_for_job, query_unique_references
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_create import create_class
from .commons.utils_requests import query_classes

CR_ID_1 = "User 1"
CR_ID_2 = "User 2"
CR_ID_3 = "User 3"
CR_TYPE = "job"
V_1 = "org.empaia.my_vendor.my_app.v1.classes.non_tumor"
V_2 = "org.empaia.my_vendor.my_app.v1.classes.tumor"
V_3 = "org.empaia.my_vendor.my_app.v1.classes.maybe_tumor"


def classes_query(up):
    base_url = get_base_url(up.annot_container)

    a_ids = []
    with get_db(up.db_container).begin() as conn:
        a_id_1 = create_point(conn)
        a_id_2 = create_point(conn)
        a_id_3 = create_point(conn)
        a_ids = [a_id_1, a_id_2, a_id_3]
        create_class(conn, a_id_1, CR_ID_1, CR_TYPE, V_1)
        create_class(conn, a_id_1, CR_ID_2, CR_TYPE, V_1)
        create_class(conn, a_id_2, CR_ID_1, CR_TYPE, V_2)
        create_class(conn, a_id_2, CR_ID_2, CR_TYPE, V_2)
        create_class(conn, a_id_3, CR_ID_1, CR_TYPE, V_3)
        create_class(conn, a_id_3, CR_ID_2, CR_TYPE, V_3)

    query_creators(base_url)
    query_references(base_url, up, a_ids)
    query_all(base_url, up, a_ids)
    query_all_with_values(base_url, up, a_ids)
    query_over_limit(base_url, up)
    query_all_jobs(base_url, up, a_ids)
    query_unique_references_classes(base_url, up, a_ids)
    query_invalid_class_ids(base_url)

    clear_database(up)


def query_creators(base_url):
    query_1 = {"creators": [CR_ID_1]}
    query_classes(base_url, query_1, 3, 3)

    query_2 = {"creators": [CR_ID_1, CR_ID_2]}
    query_classes(base_url, query_2, 6, 6)

    query_3 = {"creators": [CR_ID_1, CR_ID_2, CR_ID_3]}
    query_classes(base_url, query_3, 6, 6)


def query_references(base_url, up, annot_ids):
    query_1 = {"references": [str(annot_ids[0])]}
    query_classes(base_url, query_1, 2, 2)

    query_2 = {"references": [str(annot_ids[0]), str(annot_ids[1])]}
    query_classes(base_url, query_2, 4, 4)

    query_3 = {"references": [str(annot_ids[0]), str(annot_ids[1]), str(annot_ids[2])]}
    query_classes(base_url, query_3, 6, 6)


def query_all(base_url, up, annot_ids):
    query_1 = {"creators": [CR_ID_1], "references": [str(annot_ids[0])]}
    query_classes(base_url, query_1, 1, 1)

    query_2 = {"creators": [CR_ID_1, CR_ID_2], "references": [str(annot_ids[0]), str(annot_ids[1])]}
    query_classes(base_url, query_2, 4, 4)

    query_3 = {"creators": [CR_ID_3]}
    query_classes(base_url, query_3, 0, 0)


def query_all_with_values(base_url, up, annot_ids):
    params = {"with_unique_class_values": True}
    query_1 = {"creators": [CR_ID_1], "references": [str(annot_ids[0])]}
    query_classes(base_url, query_1, 1, 1, params=params, ucv_count=1)

    query_2 = {"creators": [CR_ID_1, CR_ID_2], "references": [str(annot_ids[0]), str(annot_ids[1])]}
    query_classes(base_url, query_2, 4, 4, params=params, ucv_count=2)

    query_3 = {"creators": [CR_ID_3]}
    query_classes(base_url, query_3, 0, 0, params=params, ucv_count=0)


def query_over_limit(base_url, up):
    class_count = ITEM_LIMIT + 1

    cl_ids = []
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)
        for _ in range(class_count - 6):
            cl_ids.append(create_class(conn, annot_id, CR_ID_1, CR_TYPE))

    query_1 = {"creators": [CR_ID_1, CR_ID_2]}
    response = requests.put(f"{base_url}/classes/query", json=query_1)
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({class_count}) exceeds server limit of {ITEM_LIMIT}."


def query_all_jobs(base_url, up, annot_ids):
    with get_db(up.db_container).begin() as conn:
        cl_id_1 = create_class(conn, annot_ids[0], CR_ID_1, CR_TYPE)
        cl_id_2 = create_class(conn, annot_ids[1], CR_ID_1, CR_TYPE)
        cl_id_3 = create_class(conn, annot_ids[0], CR_ID_2, CR_TYPE)
        cl_id_4 = create_class(conn, annot_ids[1], CR_ID_2, CR_TYPE)

    job_id_1 = "job1"
    job_id_2 = "job2"

    lock_for_job(base_url, job_id_1, cl_id_1, "classes")
    lock_for_job(base_url, job_id_1, cl_id_2, "classes")
    lock_for_job(base_url, job_id_1, cl_id_3, "classes")
    lock_for_job(base_url, job_id_1, cl_id_4, "classes")
    lock_for_job(base_url, job_id_2, cl_id_1, "classes")
    lock_for_job(base_url, job_id_2, cl_id_2, "classes")
    lock_for_job(base_url, job_id_2, cl_id_3, "classes")
    lock_for_job(base_url, job_id_2, cl_id_4, "classes")

    query_1 = {"creators": [CR_ID_1], "references": [str(annot_ids[0])], "jobs": [job_id_1]}
    query_classes(base_url, query_1, 1, 1)

    query_2 = {"creators": [CR_ID_1], "references": [str(annot_ids[0])], "jobs": [job_id_1, job_id_2]}
    query_classes(base_url, query_2, 1, 1)

    query_3 = {"jobs": [job_id_1, job_id_2]}
    query_classes(base_url, query_3, 4, 4)

    query_4 = {"references": [str(annot_ids[0])], "jobs": ["no_job_should_match"]}
    query_classes(base_url, query_4, 0, 0)

    query_5 = {"classes": [str(cl_id_1), str(cl_id_2), str(cl_id_3), str(cl_id_4)], "jobs": [job_id_1]}
    query_classes(base_url, query_5, 4, 4)


def query_unique_references_classes(base_url, up, annot_ids):
    # queries without jobs
    query_one = {}
    r = query_unique_references(base_url, query_one, "classes")
    assert len(r["annotation"]) == 4

    query_two = {"creators": [CR_ID_1]}
    r = query_unique_references(base_url, query_two, "classes")
    assert len(r["annotation"]) == 4

    with get_db(up.db_container).begin() as conn:
        cl_id_1 = create_class(conn, annot_ids[0], CR_ID_1, CR_TYPE)
        cl_id_2 = create_class(conn, annot_ids[1], CR_ID_1, CR_TYPE)
        cl_id_3 = create_class(conn, annot_ids[0], CR_ID_2, CR_TYPE)
        cl_id_4 = create_class(conn, annot_ids[1], CR_ID_2, CR_TYPE)

    refs = [str(annot_ids[0]), str(annot_ids[1])]

    job_id_1 = "job1_ur"
    job_id_2 = "job2_ur"

    lock_for_job(base_url, job_id_1, cl_id_1, "classes")
    lock_for_job(base_url, job_id_1, cl_id_2, "classes")
    lock_for_job(base_url, job_id_1, cl_id_3, "classes")
    lock_for_job(base_url, job_id_1, cl_id_4, "classes")
    lock_for_job(base_url, job_id_2, cl_id_1, "classes")
    lock_for_job(base_url, job_id_2, cl_id_2, "classes")
    lock_for_job(base_url, job_id_2, cl_id_3, "classes")
    lock_for_job(base_url, job_id_2, cl_id_4, "classes")

    # queries with jobs
    query_one = {"jobs": [job_id_1]}
    r = query_unique_references(base_url, query_one, "classes")
    assert len(r["annotation"]) == 2
    for ref in r["annotation"]:
        assert ref in refs

    query_two = {"jobs": [job_id_1], "creators": [CR_ID_1]}
    r = query_unique_references(base_url, query_two, "classes")
    assert len(r["annotation"]) == 2
    for ref in r["annotation"]:
        assert ref in refs


def query_invalid_class_ids(base_url):
    query = {"classes": ["some_invalid_id"]}
    response = requests.put(f"{base_url}/classes/query", json=query)
    assert response.status_code == 422
    assert response.json()["detail"] == "ERROR: Value for 'classes' must be of type UUID4."
