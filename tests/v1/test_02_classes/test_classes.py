import docker

from ..commons.setup_tests import Up
from ..commons.utils import clear_database
from .classes_delete import classes_delete
from .classes_get_all import classes_get_all
from .classes_get_by_id import classes_get_by_id
from .classes_post import classes_post
from .classes_put import classes_put
from .classes_query import classes_query


def test_classes():
    client = docker.from_env()
    with Up(client) as up:
        classes_get_by_id(up)
        classes_get_all(up)
        classes_post(up)
        classes_put(up)
        classes_query(up)
        classes_delete(up)

        clear_database(up)
