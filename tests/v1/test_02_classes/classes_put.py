from uuid import uuid4

import requests
from sqlalchemy.sql import text

from annotation_service.models.v1.annotation.classes import Class

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_job_count, get_job_count_for_item, lock_for_job
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_create import create_class

CR_ID = "User 1"
CR_TYPE = "job"


def classes_put(up):
    base_url = get_base_url(up.annot_container)

    put(base_url, up)
    put_locked(base_url, up)
    put_not_found(base_url)
    put_creator_error(base_url, up)
    put_reference_error(base_url, up)
    put_creator_type_error(base_url, up)
    lock_class(base_url, up)

    clear_database(up)


def put(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(a_id),
        "reference_type": "annotation",
    }

    response = requests.put(f"{base_url}/classes/{cl_id}", json=new_class)
    assert response.status_code == 200
    response_json = response.json()
    Class.model_validate(response_json)

    with get_db(up.db_container).begin() as conn:
        result = conn.execute(
            text(
                """
                SELECT *
                FROM cl_classes;
                """
            )
        ).first()

        assert new_class["value"] == result["value"]
        assert new_class["creator_id"] == result["creator_id"]
        assert new_class["creator_type"] == result["creator_type"]
        assert new_class["reference_id"] == str(result["reference_id"])
        assert new_class["reference_type"] == result["reference_type"]


def put_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE, is_locked=True)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(a_id),
        "reference_type": "annotation",
    }

    response = requests.put(f"{base_url}/classes/{cl_id}", json=new_class)
    assert response.status_code == 423

    with get_db(up.db_container).begin() as conn:
        result = conn.execute(
            text(
                """
                SELECT *
                FROM cl_classes
                WHERE id=:cl_id;
                """
            ),
            cl_id=cl_id,
        ).first()

        assert "value" == result["value"]


def put_not_found(base_url):
    cl_id = uuid4()

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.tumor",
        "creator_id": CR_ID,
        "creator_type": "job",
        "reference_id": str(uuid4()),
        "reference_type": "annotation",
    }

    response = requests.put(f"{base_url}/classes/{cl_id}", json=new_class)
    assert response.status_code == 404


def put_creator_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.tumor",
        "creator_id": "User 2",
        "creator_type": "job",
        "reference_id": str(a_id),
        "reference_type": "annotation",
    }

    response = requests.put(f"{base_url}/classes/{cl_id}", json=new_class)
    assert response.status_code == 405


def put_reference_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.tumor",
        "creator_id": CR_ID,
        "creator_type": "job",
        "reference_id": str(uuid4()),
        "reference_type": "annotation",
    }

    response = requests.put(f"{base_url}/classes/{cl_id}", json=new_class)
    assert response.status_code == 405


def put_creator_type_error(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.tumor",
        "creator_id": CR_ID,
        "creator_type": "user",
        "reference_id": str(a_id),
        "reference_type": "annotation",
    }

    response = requests.put(f"{base_url}/classes/{cl_id}", json=new_class)
    assert response.status_code == 405


def lock_class(base_url, up):
    with get_db(up.db_container).begin() as conn:
        a_id = create_point(conn)
        cl_id = create_class(conn, a_id, CR_ID, CR_TYPE)

    job_id_1 = "job1"
    job_id_2 = "job2"

    with get_db(up.db_container).begin() as conn:
        lock_for_job(base_url, job_id_1, cl_id, "classes")
        assert get_job_count_for_item(conn, str(cl_id), "classes") == 1

        lock_for_job(base_url, job_id_1, cl_id, "classes")
        assert get_job_count_for_item(conn, str(cl_id), "classes") == 1

        lock_for_job(base_url, job_id_2, cl_id, "classes")
        assert get_job_count_for_item(conn, str(cl_id), "classes") == 2
        assert get_job_count(conn, str(cl_id), job_id_1, "classes") == 1
        assert get_job_count(conn, str(cl_id), job_id_2, "classes") == 1
