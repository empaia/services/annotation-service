from sqlalchemy.sql import text


def create_class(conn, a_id, cr_id="User", cr_type="user", value="value", is_locked=False):
    result = conn.execute(
        text(
            """
            INSERT INTO cl_classes(
                type,
                creator_id,
                creator_type,
                reference_id,
                reference_type,
                value,
                is_locked
            )
            VALUES(
                :type,
                :creator_id,
                :creator_type,
                :reference_id,
                :reference_type,
                :value,
                :is_locked
            ) RETURNING id;
            """
        ),
        type="class",
        creator_id=cr_id,
        creator_type=cr_type,
        reference_id=a_id,
        reference_type="annotation",
        value=value,
        is_locked=is_locked,
    ).first()

    return result["id"]
