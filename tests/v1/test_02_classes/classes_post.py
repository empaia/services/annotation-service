from uuid import uuid4

import requests
from sqlalchemy.sql import text

from annotation_service.models.v1.annotation.classes import Class, ClassListResponse

from ..commons.setup_tests import POST_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, get_count
from ..test_01_annotations.commons.utils_create import create_point

CR_ID = uuid4()


def classes_post(up):
    base_url = get_base_url(up.annot_container)

    post(base_url, up)
    post_many(base_url, up)
    post_over_limit(base_url, up)
    post_error(base_url)

    clear_database(up)


def post(base_url, up):
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(annot_id),
        "reference_type": "annotation",
    }

    post_response = requests.post(f"{base_url}/classes", json=new_class)
    assert post_response.status_code == 201
    response_json = post_response.json()
    Class.model_validate(response_json)

    with get_db(up.db_container).begin() as conn:
        result = conn.execute(
            text(
                """
                SELECT *
                FROM cl_classes;
                """
            )
        ).first()

        assert new_class["type"] == result["type"]
        assert new_class["value"] == result["value"]
        assert new_class["creator_id"] == result["creator_id"]
        assert new_class["creator_type"] == result["creator_type"]
        assert new_class["reference_id"] == str(result["reference_id"])
        assert new_class["reference_type"] == result["reference_type"]

    new_class["id"] = str(uuid4())
    params = {"external_ids": True}
    post_response = requests.post(f"{base_url}/classes", params=params, json=new_class)
    assert post_response.status_code == 201
    response_json = post_response.json()
    Class.model_validate(response_json)
    assert response_json["id"] != new_class["id"]


def post_many(base_url, up):
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)

    post_count = 100
    classes = []
    for i in range(post_count):
        classes.append(
            {
                "type": "class",
                "value": f"org.empaia.my_vendor.my_app.v1.classes.non_tumor_{i}",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(annot_id),
                "reference_type": "annotation",
            }
        )

    class_post = {"items": classes}
    post_response = requests.post(f"{base_url}/classes", json=class_post)
    assert post_response.status_code == 201
    response = post_response.json()
    ClassListResponse.model_validate(response)

    assert len(response["items"]) == post_count

    with get_db(up.db_container).begin() as conn:
        assert get_count(conn, "classes") == post_count + 2

    # check for correct order of response
    for i in range(post_count):
        assert classes[i]["value"] == response["items"][i]["value"]


def post_over_limit(base_url, up):
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)

    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(annot_id),
        "reference_type": "annotation",
    }

    post_count = POST_LIMIT + 1
    classes = []
    for _ in range(0, post_count):
        classes.append(new_class)

    class_post = {"items": classes}
    post_response = requests.post(f"{base_url}/classes", json=class_post)
    assert post_response.status_code == 413


def post_error(base_url):
    new_class = {
        "type": "class",
        "value": "org.empaia.my_vendor.my_app.v1.classes.non_tumor",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(uuid4()),
        "reference_type": "annotation",
    }

    class_response = requests.post(f"{base_url}/classes", json=new_class)
    assert class_response.status_code == 404
