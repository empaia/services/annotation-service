import requests

from annotation_service.models.v1.annotation.classes import Class, ClassList

from ..commons.setup_tests import ITEM_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database
from ..test_01_annotations.commons.utils_create import create_point
from .commons.utils_create import create_class
from .commons.utils_requests import get_classes

CR_ID = "User 1"
CR_TYPE = "job"
CLASS_COUNT = 10


def classes_get_all(up):
    base_url = get_base_url(up.annot_container)

    get_empty(base_url)
    get_filled(base_url, up)
    get_over_limit(base_url, up)

    clear_database(up)


def get_empty(base_url):
    get_classes(base_url, 0, 0)


def get_filled(base_url, up):
    class_ids = []
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)
        for _ in range(CLASS_COUNT):
            class_ids.append(create_class(conn, annot_id, CR_ID, CR_TYPE))
            class_ids.append(create_class(conn, annot_id, CR_ID, CR_TYPE, "value2"))
    assert len(class_ids) == CLASS_COUNT * 2
    result = get_classes(base_url, CLASS_COUNT * 2, CLASS_COUNT * 2)

    for cl in result["items"]:
        Class.model_validate(cl)
        assert cl["type"] == "class"
        assert cl["value"] in ["value", "value2"]
        assert cl["creator_id"] == CR_ID
        assert cl["creator_type"] == CR_TYPE
        assert cl["reference_id"] == str(annot_id)
        assert cl["reference_type"] == "annotation"

    params = {"with_unique_class_values": True}
    response = requests.get(f"{base_url}/classes", params=params)
    assert response.status_code == 200
    result = response.json()
    ClassList.model_validate(result)

    assert "unique_class_values" in result
    assert len(result["unique_class_values"]) == 2
    assert sorted(result["unique_class_values"]) == ["value", "value2"]


def get_over_limit(base_url, up):
    c_count = ITEM_LIMIT + 1

    class_ids = []
    with get_db(up.db_container).begin() as conn:
        annot_id = create_point(conn)
        for _ in range(c_count - CLASS_COUNT * 2):
            class_ids.append(create_class(conn, annot_id, CR_ID, CR_TYPE))

    response = requests.get(f"{base_url}/classes")
    assert response.status_code == 413
    text = response.text.split(":")[1][1:-2]
    assert text == f"Count of requested items ({c_count}) exceeds server limit of {ITEM_LIMIT}."
