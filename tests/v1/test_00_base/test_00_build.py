import docker
import requests

from annotation_service.models.v1.annotation.server_settings import AnnotationServiceSettings

from ..commons.setup_tests import ITEM_LIMIT, POST_LIMIT, Up, docker_build, get_base_url


def test_build():
    client = docker.from_env()
    docker_build(client)


def test_run():
    client = docker.from_env()
    with Up(client) as up:
        base_url = get_base_url(up.annot_container)
        r = requests.get(f"{base_url}/settings")
        assert r.status_code == 200
        settings = r.json()
        AnnotationServiceSettings.model_validate(settings)
        assert settings["item_limit"] == ITEM_LIMIT
        assert settings["post_limit"] == POST_LIMIT


def test_run_external():
    client = docker.from_env()
    with Up(client, allow_external_ids=True):
        pass
