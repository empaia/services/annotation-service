import requests

from annotation_service.models.v1.commons import IdObject

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database, get_count_for_id
from .commons.utils_create import create_bool, create_float, create_integer, create_string


def primitives_delete(up):
    base_url = get_base_url(up.annot_container)

    delete_integer(base_url, up)
    delete_float(base_url, up)
    delete_bool(base_url, up)
    delete_string(base_url, up)
    delete_locked(base_url, up)

    clear_database(up)


def delete_integer(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn)

    response = requests.delete(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(p_id)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, p_id, "primitives") == 0


def delete_float(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_float(conn)

    response = requests.delete(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(p_id)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, p_id, "primitives") == 0


def delete_bool(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_bool(conn)

    response = requests.delete(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(p_id)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, p_id, "primitives") == 0


def delete_string(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_string(conn)

    response = requests.delete(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    IdObject.model_validate(result)
    assert result["id"] == str(p_id)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 404

    with get_db(up.db_container).begin() as conn:
        assert get_count_for_id(conn, p_id, "primitives") == 0


def delete_locked(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, is_locked=True)

    response = requests.delete(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 423

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
