from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.primitives import (
    BoolPrimitive,
    FloatPrimitive,
    IntegerPrimitive,
    StringPrimitive,
)

from ..commons.setup_tests import get_base_url, get_db
from ..commons.utils import clear_database
from .commons.utils_check import check_base_primitive
from .commons.utils_create import V_B, V_F, V_I, V_S, create_bool, create_float, create_integer, create_string

CR_ID = "Job 1"
CR_TYPE = "job"
REF_ID = uuid4()
REF_TYPE = "wsi"


def primitives_get_by_id(up):
    base_url = get_base_url(up.annot_container)

    get_integer(base_url, up)
    get_float(base_url, up)
    get_bool(base_url, up)
    get_string(base_url, up)
    get_non_existent(base_url, up)

    clear_database(up)


def get_integer(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_integer(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_I)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    IntegerPrimitive.model_validate(result)
    check_base_primitive(result, p_id, "integer", CR_ID, CR_TYPE, REF_ID, V_I)


def get_float(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_float(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_F)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    FloatPrimitive.model_validate(result)
    check_base_primitive(result, p_id, "float", CR_ID, CR_TYPE, REF_ID, V_F)


def get_bool(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_bool(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_B)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    BoolPrimitive.model_validate(result)
    check_base_primitive(result, p_id, "bool", CR_ID, CR_TYPE, REF_ID, V_B)


def get_string(base_url, up):
    with get_db(up.db_container).begin() as conn:
        p_id = create_string(conn, CR_ID, CR_TYPE, REF_ID, REF_TYPE, V_S)

    response = requests.get(f"{base_url}/primitives/{p_id}")
    assert response.status_code == 200
    result = response.json()
    StringPrimitive.model_validate(result)
    check_base_primitive(result, p_id, "string", CR_ID, CR_TYPE, REF_ID, V_S)


def get_non_existent(base_url, up):
    response = requests.get(f"{base_url}/primitives/{uuid4()}")
    assert response.status_code == 404
