from sqlalchemy.sql import text


def get_type_count(conn, p_type):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM p_primitives
            WHERE type=:p_type;
            """
        ),
        p_type=p_type,
    ).first()["count"]


def get_integer_count(conn, p_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM p_primitives
            WHERE id=:id AND type='integer';
            """
        ),
        id=p_id,
    ).first()["count"]


def get_float_count(conn, p_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM p_primitives
            WHERE id=:id AND type='float';
            """
        ),
        id=p_id,
    ).first()["count"]


def get_bool_count(conn, p_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM p_primitives
            WHERE id=:id AND type='bool';
            """
        ),
        id=p_id,
    ).first()["count"]


def get_string_count(conn, p_id):
    return conn.execute(
        text(
            """
            SELECT COUNT(*) as count
            FROM p_primitives
            WHERE id=:id AND type='string';
            """
        ),
        id=p_id,
    ).first()["count"]
