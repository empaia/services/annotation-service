from sqlalchemy.sql import text

NAME = "Primitive"
DESC = "Description"
REF_TYPE = "wsi"


def check_base_primitive(primitive, p_id, p_type, cr_id, cr_type, ref_id, value):
    assert primitive["id"] == str(p_id)
    assert primitive["name"] == NAME
    assert primitive["description"] == DESC
    assert primitive["value"] == value
    assert primitive["type"] == p_type
    assert primitive["creator_id"] == cr_id
    assert primitive["creator_type"] == cr_type
    assert primitive["reference_id"] == str(ref_id)
    assert primitive["reference_type"] == REF_TYPE


def check_primitive_post_put(conn, p_id, primitive):
    result = conn.execute(
        text(
            """
            SELECT * FROM p_primitives WHERE id=:p_id;
            """
        ),
        p_id=p_id,
    ).first()

    assert str(p_id) == str(result["id"])
    assert primitive["name"] == result["name"]
    if "description" in primitive:
        assert primitive["description"] == result["description"]
    assert primitive["creator_id"] == str(result["creator_id"])
    assert primitive["creator_type"] == result["creator_type"]
    if "reference_id" in primitive:
        assert primitive["reference_id"] == str(result["reference_id"])
    if "reference_type" in primitive:
        assert primitive["reference_type"] == result["reference_type"]
    assert primitive["type"] == result["type"]
    if primitive["type"] == "integer":
        assert primitive["value"] == result["ivalue"]
    elif primitive["type"] == "float":
        assert primitive["value"] == result["fvalue"]
    elif primitive["type"] == "bool":
        assert bool(primitive["value"]) == result["bvalue"]
    elif primitive["type"] == "string":
        assert primitive["value"] == result["svalue"]
