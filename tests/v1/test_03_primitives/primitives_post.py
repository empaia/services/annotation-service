from uuid import uuid4

import requests

from annotation_service.models.v1.annotation.primitives import IntegerPrimitive, PrimitiveList

from ..commons.setup_tests import POST_LIMIT, get_base_url, get_db
from ..commons.utils import clear_database, get_count
from .commons.utils_requests import post_primitive

CR_ID = uuid4()
REF_ID = uuid4()
POST_COUNT = 100


def primitives_post(up):
    base_url = get_base_url(up.annot_container)

    post_integer(base_url, up)
    post_float(base_url, up)
    post_bool(base_url, up)
    post_string(base_url, up)
    post_external_id(base_url)
    post_many(base_url, up)
    post_many_over_limit(base_url)
    post_error_type(base_url, up)
    post_error_reference(base_url, up)
    post_mixed_types_in_list(base_url, up)
    post_invalid_type_in_list(base_url, up)

    clear_database(up)


def post_integer(base_url, up):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_primitive(up, base_url, primitive, "integer")


def post_float(base_url, up):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_primitive(up, base_url, primitive, "float")


def post_bool(base_url, up):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": "true",
        "type": "bool",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_primitive(up, base_url, primitive, "bool")


def post_string(base_url, up):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": "Value",
        "type": "string",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_primitive(up, base_url, primitive, "string")


def post_external_id(base_url):
    primitive = {
        "id": str(uuid4()),
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    params = {"external_ids": True}
    post_response = requests.post(f"{base_url}/primitives", params=params, json=primitive)
    assert post_response.status_code == 201

    response_json = post_response.json()
    IntegerPrimitive.model_validate(response_json)
    assert response_json["id"] != primitive["id"]


def post_many(base_url, up):
    primitives = []
    for i in range(POST_COUNT):
        primitives.append(
            {
                "name": f"Nice_{i}",
                "description": "Nice",
                "value": 42,
                "type": "integer",
                "creator_id": str(CR_ID),
                "creator_type": "job",
                "reference_id": str(REF_ID),
                "reference_type": "wsi",
            }
        )
    primitive_post = {"items": primitives}

    post_response = requests.post(f"{base_url}/primitives", json=primitive_post)
    assert post_response.status_code == 201

    response = post_response.json()
    PrimitiveList.model_validate(response)
    assert response["item_count"] == POST_COUNT
    assert len(response["items"]) == POST_COUNT

    with get_db(up.db_container).begin() as conn:
        assert get_count(conn, "primitives") == POST_COUNT + 5

    # check for correct order of response
    for i in range(POST_COUNT):
        assert primitives[i]["name"] == response["items"][i]["name"]


def post_many_over_limit(base_url):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }
    post_count = POST_LIMIT + 1
    primitives = []
    for _ in range(post_count):
        primitives.append(primitive)
    primitive_post = {"items": primitives}
    post_response = requests.post(f"{base_url}/primitives", json=primitive_post)
    assert post_response.status_code == 413


def post_error_type(base_url, up):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "bool",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    post_response = requests.post(f"{base_url}/primitives", json=primitive)
    assert post_response.status_code == 422


def post_error_reference(base_url, up):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
    }

    post_response = requests.post(f"{base_url}/primitives", json=primitive)
    assert post_response.status_code == 422

    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_type": "wsi",
    }

    post_response = requests.post(f"{base_url}/primitives", json=primitive)
    assert post_response.status_code == 422

    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "annotation",
    }

    post_response = requests.post(f"{base_url}/primitives", json=primitive)
    assert post_response.status_code == 404
    assert post_response.json()["detail"].endswith("nonexistent annotation.")

    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "collection",
    }

    post_response = requests.post(f"{base_url}/primitives", json=primitive)
    assert post_response.status_code == 404
    assert post_response.json()["detail"].endswith("nonexistent collection.")


def post_mixed_types_in_list(base_url, up):
    p_integer = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    p_float = {
        "name": "Nice",
        "description": "Nice",
        "value": 0.42,
        "type": "float",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    items = [p_integer, p_float]
    primitive_post = {"items": items}
    post_response = requests.post(f"{base_url}/primitives", json=primitive_post)
    assert post_response.status_code == 422


def post_invalid_type_in_list(base_url, up):
    primitive = {
        "name": "Nice",
        "description": "Nice",
        "value": 42,
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    primitive_inv = {
        "name": "Nice",
        "description": "Nice",
        "value": "true",
        "type": "integer",
        "creator_id": str(CR_ID),
        "creator_type": "job",
        "reference_id": str(REF_ID),
        "reference_type": "wsi",
    }

    items = [primitive, primitive_inv]
    primitive_post = {"items": items}
    post_response = requests.post(f"{base_url}/primitives", json=primitive_post)
    assert post_response.status_code == 422
