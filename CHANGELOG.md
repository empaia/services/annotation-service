# Annotation Service (AS) - Changelog

---

## Changes in Version 0.38.9

- fixes in pixelmap_file.py

## Changes in Version 0.38.8

- fix critical bug in pixelmapd, where pixelmaps are overriden when reopening h5 file in "w" mode, instead of "a" mode

## Changes in Version 0.23.1

- handle edge case, where pixelmap content-encoding header indicates gzip, but sent tile data is not gzipped
- gzip_compresslevel is now a pixelmapd setting

## Changes in Version 0.23.0

- improved pixelmapd interface to provide more options for custom implementations

## Changes in Version 0.22.0

- added pixelmaps info route to support EMP-0108

## Changes in Version 0.21.0

- added Accept-Encoding and Content-Encoding headers in pixelmaps API to support EMP-0107

## Changes in Version 0.20.0

- Improved pixelmaps implementation, by storing all pixelmap tiles compressed in a single hdf5 file per pixelmap
- Now requires to run pixelmapd in a separate container

## Changes in Version 0.19.4

- updated models

## Changes in Version 0.19.3

- remove put pixelmaps data overwrite limitation

## Changes in Version 0.19.2

- Updated dependencies and base images
- added pixelmap tests

## Changes in Version 0.19.1

- Updated dependencies and base images

## Changes in Version 0.19.0

- implemented Pixelmaps in v3 API based on [EMP-0102](https://gitlab.com/empaia/emps/-/blob/main/emp-0102.md)

## Changes in Version 0.18.4

- Updated dependencies and base images

## Changes in Version 0.18.3

- Updated dependencies and base images

## Changes in Version 0.18.2

- Updated dependencies and base images

## Changes in Version 0.18.1

- Updated dependencies and base images

## Changes in Version 0.18.0

- changes in v3 API
  - optimization of all queries
  - added private endpoint to aggregate job results
    - create materialized views for all data types to improve queries for job results
  - added collection ids to data tables
  - added class values to annotation table

## Changes in Version 0.17.1

- updated for pydantic v2

## Changes in Version 0.17.0

- changes in v3 API
  - changed recursive locking of collections
  - removed recursive locking for collection items (except if items are collections)
  - adapted queries for new locking logic
  - removed filter for unlocked annotations (jobs: [None])
  - removed filter for annotations without class (class_values: [None])
  - removed unnecessary transactions in collection client
    - POST endpoints use random names for temporary tables for parallel DB inserts
  - updated endpoint comments
  - updated v3 models to improve POST performance when extending collections
  - DB migration to add item type to locked collections

## Changes in Version 0.16.5

- fixed error in annotations locking

## Changes in Version 0.16.4

- Performance optimizations in v3 API for
  - locking
  - (bulk) inserts

## Changes in Version 0.16.3

- Updated dependencies and base images

## Changes in Version 0.16.2

- Updated dependencies and base images

## Changes in Version 0.16.1

- fixed error in migration step 10

## Changes in Version 0.16.0

- renamed v2 API to v3 API
- Updated dependencies and base images
- added db migration to create tables for v3 API

## Changes in Version 0.15.0

- added v2 API

## Changes in Version 0.14.57

- Updated dependencies and base images

## Changes in Version 0.14.56

- updated models

## Changes in Version 0.14.55

- added new filter options to class and primitives queries

## Changes in Version 0.14.54

- Added file based profiling support

## Changes in Version 0.14.53

- Updated dependencies and base images

## Changes in Version 0.14.52

- Updated dependencies and base images

## Changes in Version 0.14.51

- changed endpoint to retrieve collection by id
  - replaced `leaf_response` query parameter with `shallow`
  - added query parameter `with_leaf_ids` to include item ids of leafs

## Changes in Version 0.14.50

- Updated dependencies and base images

## Changes in Version 0.14.49

- Updated dependencies and base images

## Changes in Version 0.14.48

- Updated dependencies and base images

## Changes in Version 0.14.47

- replaced `shallow` query parameter with `leaf_response`

## Changes in Version 0.14.46

- Updated dependencies and base images

## Changes in Version 0.14.45

- Updated dependencies and base images

## Changes in Version 0.14.44

- Updated dependencies and base images

## Changes in Version 0.14.43

- Updated dependencies and base images

## Changes in Version 0.14.42

- Updated dependencies and base images

## Changes in Version 0.14.41

- Updated dependencies and base images

## Changes in Version 0.14.40

- Updated dependencies and base images

## Changes in Version 0.14.39

- Updated dependencies and base images

## Changes in Version 0.14.38

- Updated dependencies and base images

## Changes in Version 0.14.37

- Optimized SQL statements for all query endpoints
- Refactored SQL helpers

## Changes in Version 0.14.36

- Updated dependencies and base images

## Changes in Version 0.14.35

- Updated dependencies and base images

## Changes in Version 0.14.34

- Updated dependencies and base images

## Changes in Version 0.14.33

- Updated dependencies and base images

## Changes in Version 0.14.32

- Updated dependencies and base images

## Changes in Version 0.14.31

- Updated dependencies and base images

## Changes in Version 0.14.30

- Updated dependencies
- Changed postgres image to custom postgres

## Changes in Version 0.14.29

- Updated dependencies

## Changes in Version 0.14.28

- Updated dependencies

## Changes in Version 0.14.27

- Updated CI

## Changes in Version 0.14.26

- added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

## Changes in Version 0.14.25

- allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly
use an authorization header)

## Changes in Version 0.14.24

- Fixed error in collection input validation

## Changes in Version 0.14.23

- Fixed error in class query SQL

## Changes in Version 0.14.22

- Changed response model for `PUT /v1/annotations/query/count` endpoint

- Changed response annotation query count

## Changes in Version 0.14.21

- Fixed error in annotations query if jobs is None

## Changes in Version 0.14.20

- Changed SELECT and ORDER BY in SQL-Statement for annotation queries

## Changes in Version 0.14.19

- Updated models

## Changes in Version 0.14.18

- Added new endpoint `PUT /v1/annotations/query/count`
- Added new endpoint `PUT /v1/annotations/query/unique-references`
- Added new endpoint `PUT /v1/classes/query/unique-references`
- Added new endpoint `PUT /v1/collections/query/unique-references`
- Added new endpoint `PUT /v1/collections/{collection_id}/items/query/unique-references`
- Added new endpoint `PUT /v1/primitives/query/unique-references`
- Added custom PostgreSQL configuration
- Added new `DataCreatorType` : `SCOPE = "scope"`

## Changes in Version 0.14.17

- removed logging timestamps

## Changes in Version 0.14.16

- Deprecated Result Tree routes

## Changes in Version 0.14.15

- Fixed error in result tree node items sql statement

## Changes in Version 0.14.14

- Added consistency check for reference id and reference type when posting collection(s)
- Added consistency check for reference id and reference type when posting primitive(s)

## Changes in Version 0.14.13

- Fixed error in annotation query response

## Changes in Version 0.14.12

- Fixed error in annotation queries if class values were not set

## Changes in Version 0.14.11

- Added new endpoint `PUT /v1/annotations/query/viewer`
- Added new endpoint `PUT /v1/annotations/query/unique-class-values`
- Removed `with_unique_class_values` from `PUT /v1/annotations/query/viewer` endpoint
- Changed SQL-Statements for annotation queries
- Added function for custom SQL-Exception

## Changes in Version 0.14.10

- Added new endpoint `GET /v1/jobs/{job_id}/tree/items/{item_id}/sequence`
- Added timestamps to logger
- Changed position of annotation in query response to start at 0

## Changes in Version 0.14.9

- Fixed error in annotation query SQL statement when filtering centroids
- Adapted test

## Changes in Version 0.14.8

## Changes in Version 0.14.7

- added prefix for migration_steps table

## Changes in Version 0.14.6

- Updated models
- Updated README
- Updated `Dockerfile` and `docker-compose.yml`
- Updated dependencies
- Migrated from sqlalchemy / psycopg2 to asyncpg
- Changed profiling library to pyinstrument
- Refactored locking of collections

## Changes in Version 0.14.5

- Updated models
- Updated tests
- Changed response for `/alive` endpoint:
  - includes service version
- Adapted for new item models
- Collections can be posted with item ids only

## Changes in Version 0.14.4

- Updated README
- Adapted `gitlab-ci.yml`
- Updated models

## Changes in Version 0.14.3

- Fixed bug in annotation query logic when requesting unique class values for annotations not locked for any job

## Changes in Version 0.14.2

- Updated tests

## Changes in Version 0.14.1

- Added comments for all routes
- Changed version of postgres container in tests from latest to 12

## Changes in Version 0.14.0

- Renamed service to Annotation Service
- Updated models
- Updated tests

## Changes in Version 0.13.0

- Added `None` to valid values for `jobs` in `AnnotationQuery`
  - `jobs` can either be
    - `[None]`: list with single value
    - `[Id]`: list with one or more job Ids
- Added `PrimitiveDetails`
- Added new result tree route for WorkbenchService
  - `GET /v1/jobs/{job_id}/tree/primitives/{primitive_id}/details`
    - return deatils for specified primitive
- When `jobs` in `AnnotationQuery` is `null` Annotations not locked for any job are returned
- For primitives in the result tree details can be requested

## Changes in Version 0.12.3

- Updated models (type Literal) require fastapi update to version 0.65.1

## Changes in Version 0.12.2

- Updated models

## Changes in Version 0.12.1

- Minor model changes
- Changed SQL-Statement to retrieve unique class values in annotation query

## Changes in Version 0.12.0

- Models are now imported from new model repository [https://gitlab.com/empaia/services/models](https://gitlab.com/empaia/services/models)
- Added post models
- Refactored some models
- Updated for model changes
- Added column `type` to tables `cl_classes` an `c_collections`

## Changes in Version 0.11.0

- Added new route to get position of annotation in result of query
  - `PUT /v1/annotations/{annotation_id}/query`
- All routes where collections can be posted (post collection, post items when item_type of outer collection is collection) now return collection(s) with properties `item_count` and `items`
- All occurrences of `ShallowCollection` removed
- Changed `INSERT` sql statement for collection

## Changes in Version 0.10.0

- Added `jobs` tag for routes
- Refactored all lock routes:
  - old
    - `PUT /v1/annotations/{annotation_id}/jobs/{job_id}/lock`
    - `PUT /v1/classes/{class_id}/jobs/{job_id}/lock`
    - `PUT /v1/collections/{collection_id}/jobs/{job_id}/lock`
    - `PUT /v1/primitives/{primitive_id}/jobs/{job_id}/lock`
  - new
    - `PUT /v1/jobs/{job_id}/lock/annotations/{annotation_id}`
    - `PUT /v1/jobs/{job_id}/lock/classes/{class_id}`
    - `PUT /v1/jobs/{job_id}/lock/collections/{collection_id}`
    - `PUT /v1/jobs/{job_id}/lock/primitives/{primitive_id}`
  - Changes tag of all lock route to `jobs`
  - Added lock route for slides
    - `PUT /v1/jobs/{job_id}/lock/slides/{slide_id}`
  - Added result tree routes for WorkbenchService
    - `GET /v1/jobs/{job_id}/tree/items`
    - `GET /v1/jobs/{job_id}/tree/primitives`
    - `GET /v1/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/items`
    - `GET /v1/jobs/{job_id}/tree/nodes/{node_type}/{node_id}/primitives`
- Added new table `j_job_slides` to persist locking states for slides
- Added result tree logic
  - Request all slides locked in a specified job
  - Request all primitives without reference created in a specified job
  - Request all annotation referencing a given result tree node
    - If an annotation is referencing the result tree node via a collection the reference of the annotation is ignored
  - Request all primitives referencing a given result tree node
    - References of collections are ignored
- Slides (input of jobs) can be locked for a specified job

## Changes in Version 0.9.0

- Internal rework of all `POST` routes:
  - SQL `INSERT` statements now return complete JSON encoded inserted elements
  - Response order of elements is now the order of posted elements
- Some code style fixes
- Complete rework of database schema for primitives
  - Table `p_primitives` was extended with additional columns
    - `ivalue` &rarr; integer value (type `integer`)
    - `fvalue` &rarr; float value (type `float`)
    - `bvalue` &rarr; bool value (type `bool`)
    - `svalue` &rarr; string value (type `text`)
  - Droped tables
    - `p_integer_primitives`
    - `p_float_primitives`
    - `p_bool_primitives`
    - `p_string_primitives`

## Changes in Version 0.8.0

- Table `a_annotations` was extended with additional column
  - `bounding_box` &rarr; bounding box of annotation for viewport queries (type `box`)
- Added indexer for `bounding_box`
- Deleted columns from table `a_annotations`
  - `centroid_x`
  - `centroid_y`
- For all posted annotations a bounding box is calculated and inserted into the database
- All viewport queries will use the bounding box instead of centroid

## Changes in Version 0.7.1

- Removed some unused code
- Removed custom swagger-ui version
- Refactoring
- Removed check for posted polygon annotations that first point is not equal to last point
- Fixed rounding error in centroid calculation
- Added tests for centroid calculation

## Changes in Version 0.7.0

- Removed update route for annotations `PUT /v1/annotations/{annotation_id}`
- Added *Shapely* ([Link](https://github.com/Toblerity/Shapely)) as dependency (used for centroid calculation of polygons)
- Complete rework of database schema for annotations
  - Table `a_annotations` was extended with additional columns
    - `coordinates` &rarr; coordinates for point, line and polygon annotations (type `integer[]`)
    - `head` &rarr; coordinates of arrow head (type `integer[]`)
    - `tail` &rarr; coordinates of arrow tail (type `integer[]`)
    - `center` &rarr; coordinates of circle center (type `integer[]`)
    - `radius` &rarr; value of circle radius (type `integer`)
    - `upper_left` &rarr; coordinates of rectangle upper left corner (type `integer[]`)
    - `width` &rarr; value of rectangle width (type `integer`)
    - `height` &rarr; value of rectangle height (type `integer`)
    - `centroid` &rarr; coordinates of centroid (type `integer[]`)
    - `centroid_x` &rarr; x coordinate of centroid (type `integer`)
    - `centroid_y` &rarr; y coordinate of centroid (type `integer`)
  - Added indexer for `centroid_x` and `centroid_y`
  - Droped tables
    - `a_point_annotations`
    - `a_line_annotations`
    - `a_arrow_annotations`
    - `a_circle_annotations`
    - `a_rectangle_annotations`
    - `a_polygon_annotations`
- Added `centroid` to annotation fields
- If centroid is given in posted annotations it is inserted into database
  - it will be calculated if not
- Changed internal logic to return centroids if query parameter `with_low_npp_centroids=True` and `skip` or `limit` are not `None`
- Polygon annotations will be rejected (error) if first point equals last point
- Rework of table `j_jobs_classes`
  - Added new columns
    - `annot_id` (type `uuid`, foreign key to `id` in `a_annotations`)
    - `value` (type `text`)
  - Altered columns
    - `class_id` &rarr; changed type to `text`, droped froreign key constraint
  - Changed primary key to `job_id`, `class_id`, `annot_id`
- Fixed an error with `skip` and `limit` in route `PUT /v1/collections/{collection_id}/items/query`

## Changes in Version 0.6.2

- Fixed an error that occurred when a collection with `"items": []` was posted
  - The error occurred with single collections as well as with nested collections

## Changes in Version 0.6.1

- Added tests for `/v1/annotations/query` route when `with_low_npp_centroids=True`
- Fixed an error that caused no centroids to be returned if the annotation count of the original query was 0

## Changes in Version 0.6.0

- Added `collection` as allowed `item_type` when extending an existing collection via `POST /v1/collections/{collection_id}/items` endpoint

## Changes in Version 0.5.1

- Fixed a bug that caused incorrect order of points of polygon annotations under certain circumstances

## Changes in Version 0.5.0

- Added new query parameter `external_ids` (type `bool`, default=`False`) to all POST routes:
  - `POST /v1/annotations`
  - `POST /v1/classes`
  - `POST /v1/collections`
  - `POST /v1/collections/{collection_id}/items`
  - `POST /v1/primitives`
- if query parameter `external_ids` **and** ENV variable `ANNOT_ALLOW_EXTERNAL_IDS` are `True`:
  - All posted items **must** have a valid id
  - Ids are inserted into the database (no auto generation of ids)
  - If ids are missing or already present in database &rarr; error message
- Added new ENV variable `ANNOT_ALLOW_EXTERNAL_IDS` (type `bool`, default=`False`)

## Changes in Version 0.4.0

- Added new route to check status of service:
  - `GET /alive`
- Added health check for service inside `/alive` route:
  - as long as service is reachable, response code is 200
  - if database is not reachable by service &rarr; error message is returned

## Changes in Version 0.3.0

- Added query parameter `with_low_npp_centroids` to annotations query route:
  - `PUT /v1/annotations/query`
- Queries on annotations can return a list of low npp centroids if query parameter `with_low_npp_centroids` is set to `True` in `PUT /v1/annotations/query`

## Changes in Version 0.2.3

- added ci version check
- removed ci trigger for medical data service

## Changes in Version 0.2.2

- removed file `version.py`
- version number is retrieved with `importlib.metadata.version` from installed service package

## Changes in Version 0.2.1

- Changed filter logic for queries on annotations:
  - if `jobs` is set and `with_unique_class_values` is `True` &rarr; only class values from classes locked in one of the given jobs are returned
  - if `jobs` is set and `with_classes` is `True` &rarr; only classes locked in one of the given jobs are returned

## Changes in Version 0.2.0

- Added route to retrieve server settings
  - `GET /v1/settings`
- Changed response model for post items route in collections
- Changed response model for all delete routes to return id of deleted item
- Changed response model for all update routes to return complete items
- Changed all lock routes: routes are now as followed:
  - `PUT /v1/annotations/{annotation_id}/jobs/{job_id}/lock`
  - `PUT /v1/classes/{class_id}/jobs/{job_id}/lock`
  - `PUT /v1/collections/{collection_id}/jobs/{job_id}/lock`
  - `PUT /v1/primitives/{primitive_id}/jobs/{job_id}/lock`
- Changed response model of lock routes to `LockResponse`
- Added query parameter `with_unique_class_values` to annotations GET all and query route:
  - `GET /v1/annotations`
  - `PUT /v1/annotations/query`
- Added query parameter `with_unique_class_values` to classes GET all and query route:
  - `GET /v1/classes`
  - `PUT /v1/classes/query`
- Reformatted code
- Updated tests
- Added profiling middleware and profiling code
- Added new ENV variable to enable profiling
- Added code to handle server settings and to return error messages when limits are exeeded
- Added new ENV variables to set server limits for all `POST`, `GET` and Query routes
- Added GitLab-CI
- Integrated new PostgreSQL extension to generate UUID4 values
- Changed all sequences in database from `int` to `bigint`
- Deleted no longer needed table `a_polygon_annotations`
- Changed all database insert operations to be more performant
- Changed all select statements to return correctly formatted JSON-strings
- Changed all responses to `JSONResponse` to avoid time costly `pydantic` validation
- Changes on api_integration
- Changed all delete operations to return id of deleted item
- changed all update operations to return complete item
- Changed interna of all lock functions
- Added `jobs` as new optional filter to query route for `annotations`, `classes`, `collections` and `primitives`
- Added indexer
- Added check if database is reachable on server start
- Set postgres image version in `docker-compose.yml` to version 12
- Implemented viewport query for missing types
- Renamed database column `resolution` in `a_annotations` table to `npp_created` and added `NOT NULL` constraint
- Added optional fields `npp_viewing_min` and `npp_viewing_max` in `a_annotations` table
- Changed filter logic when `with_classes=True` and `creators` are included in the query
  - only classes with the same creator as the annotation are returned
- Changed filter logic when `with_classes=True` and `jobs` are included in the query
  - only classes locked in one of the given jobs are returned
- Added `npp_viewing` to possible query filters
  - if present `npp_viewing` of annotations is used
  - if `npp_viewing` is not present, `npp_created` is used as fallback
- Added new table `j_job_annotations`
- Queries on annotations can return a list of unique class values if query parameter `with_unique_class_values` is set to `True` in `GET /v1/annotations` and `PUT /v1/annotations/query`
- Added `class_values` to possible query filters
  - `class_values` can contain `null` &rarr; if `null` is present, annotations without classes are also returned
- Added new table `j_job_classes`
- Queries on classes can return a list of unique class names if query parameter `with_unique_class_values` is set to `True` in `GET /v1/classes` and `PUT /v1/classes/query`
- Added new index for table `cl_classes` on column `value`
- Removed `ǸOT NULL` constraint from database for `reference_id` and `reference_type`
- Added recursive locking to collections
- Added `npp_viewing` to possible query filters for collection items (only used if item type is an annotation type)
- Added new table `j_job_collections`
- Renamed type `Wsi` to `WsiItem`
- Removed `ǸOT NULL` constraint from database for `reference_id` and `reference_type`
- Added new table `j_job_primitives`
- When `null` is specified in `references` in `PrimitivesQuery`, primitives without a reference are included in the result
- Docker-compose build network configurable via .env
